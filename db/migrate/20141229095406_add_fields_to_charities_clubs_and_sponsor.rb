class AddFieldsToCharitiesClubsAndSponsor < ActiveRecord::Migration
  def change
  	add_column :charities, :contact_name, :string
  	add_column :charities, :contact_phone_number, :integer
  	add_column :charities, :abn_number, :string
  	add_column :clubs, :club_url, :string
  	add_column :clubs, :president, :string
  	add_column :clubs, :email, :string
  	add_column :event_sponsors, :sponsor_url, :string
  	add_column :users, :country, :string
  	add_column :events, :meridian, :string
  end
end
