class CreateLoginImages < ActiveRecord::Migration
  def change
    create_table :login_images do |t|
      t.attachment :login_image
      t.string :first_name
      t.string :last_name
      t.string :nick_name
      t.string :city
      t.string :state
      t.references :charity, index: true
      t.timestamps
    end
  end
end
