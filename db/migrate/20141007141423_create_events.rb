class CreateEvents < ActiveRecord::Migration
  def change
    create_table :events do |t|
      t.string :name
      t.datetime :from_date
      t.datetime :to_date
      t.string :status, :default => "Upcoming"
      t.string :address_line_1
      t.string :address_line_2
      t.string :city
      t.string :state
      t.string :country
      t.has_attached_file :image

      t.timestamps
    end
  end
end
