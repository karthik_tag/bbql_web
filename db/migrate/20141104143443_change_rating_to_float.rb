class ChangeRatingToFloat < ActiveRecord::Migration
  def up
  	change_column :judge_votes, :rating, :float
  end

  def down
  	change_column :judge_votes, :rating, :integer
  end
end
