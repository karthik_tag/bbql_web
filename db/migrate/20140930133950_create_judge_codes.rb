class CreateJudgeCodes < ActiveRecord::Migration
  def change
    create_table :judge_codes do |t|
      t.string :judge_code
      t.boolean :status, :default => 0

      t.timestamps
    end
  end
end
