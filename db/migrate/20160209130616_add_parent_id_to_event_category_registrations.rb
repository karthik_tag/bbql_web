class AddParentIdToEventCategoryRegistrations < ActiveRecord::Migration
  def change
  	add_column :event_category_registrations, :parent_id, :integer
  	add_column :event_category_registrations, :is_removed, :boolean, :default=>false
  end
end
