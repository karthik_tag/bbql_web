class CreateEventFanVotings < ActiveRecord::Migration
  def change
    create_table :event_fan_votings do |t|
      t.references :event, index: true
      t.timestamps
    end
  end
end
