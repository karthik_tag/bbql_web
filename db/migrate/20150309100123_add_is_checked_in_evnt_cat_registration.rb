class AddIsCheckedInEvntCatRegistration < ActiveRecord::Migration
  def change
  	add_column :event_category_registrations, :is_checked_in, :boolean, :default=>0
  end
end
