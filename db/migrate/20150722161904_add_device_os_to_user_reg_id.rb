class AddDeviceOsToUserRegId < ActiveRecord::Migration
  def change
    add_column :user_reg_ids, :device_os, :string
  end
end
