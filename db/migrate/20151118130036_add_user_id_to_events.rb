class AddUserIdToEvents < ActiveRecord::Migration
  def change
    add_column :events, :user_id, :integer
    add_column :events, :is_olympic_rules, :boolean, :default => false
  end
end
