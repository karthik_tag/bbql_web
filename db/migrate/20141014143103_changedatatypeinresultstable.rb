class Changedatatypeinresultstable < ActiveRecord::Migration
  def up
    change_column :event_competitor_results, :avg_judge_results, :float
  	change_column :event_competitor_results, :avg_visitor_results, :float
  	remove_column :visitor_votes, :event_id
  	remove_column :visitor_votes, :event_competitor_id
  	remove_column :judge_votes, :event_id
  end

  def down
  	change_column :event_competitor_results, :avg_judge_results, :integer
  	change_column :event_competitor_results, :avg_visitor_results, :integer
  	add_column :visitor_votes, :event_id, :integer
  	add_column :visitor_votes, :event_competitor_id, :integer
  	add_column :judge_votes, :event_id, :integer
  end
end
