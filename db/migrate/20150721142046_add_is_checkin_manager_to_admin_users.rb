class AddIsCheckinManagerToAdminUsers < ActiveRecord::Migration
  def change
    add_column :admin_users, :is_checkin_manager, :boolean, :default=>false
  end
end
