class CreateEventCategories < ActiveRecord::Migration
  def change
    create_table :event_categories do |t|
      t.references :event, index: true
      t.references :category, index: true
      t.boolean :is_published, :default => nil

      t.timestamps
    end
  end
end
