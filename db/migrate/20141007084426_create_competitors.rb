class CreateCompetitors < ActiveRecord::Migration
  def change
    create_table :competitors do |t|
	  t.references :category, index: true
	  t.references :charity, index: true
	  t.references :club, index: true
	  t.references :user, index: true
      t.string :nick_name
      t.string :city
      t.string :state

      t.timestamps
    end
  end
end
