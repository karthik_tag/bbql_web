class AddStateToClubs < ActiveRecord::Migration
  def change
    add_column :clubs, :state, :string
  end
end
