class AddAdditionalFieldsToEvents < ActiveRecord::Migration
  def change
  	# add_column :events, :requester_name, :string
  	add_column :events, :requester_mobile, :string
  	add_column :events, :venue_name, :string
  	add_column :events, :no_of_categories, :integer, :default=>0
  	add_column :events, :club_id, :integer
  	add_index :events, :club_id
  end
end
