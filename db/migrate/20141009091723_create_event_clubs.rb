class CreateEventClubs < ActiveRecord::Migration
  def change
    create_table :event_clubs do |t|
      t.references :event, index: true
      t.references :club, index: true
      t.timestamps
    end
  end
end
