class CreateEventCategoryRegistrations < ActiveRecord::Migration
  def change
    create_table :event_category_registrations do |t|
      t.references :event_registration, index: true
      t.references :event, index: true
      t.references :category, index: true
      t.references :user, index: true
      t.attachment :event_photo

      t.timestamps
    end
  end
end
