class CreateEventRequesterShippingDetails < ActiveRecord::Migration
  def change
    create_table :event_requester_shipping_details do |t|
      t.string :shipping_contact_name
      t.string :shipping_address_line_1
      t.string :shipping_address_line_2
      t.string :shipping_city
      t.string :shipping_country
      t.string :shipping_state
      t.integer :shipping_zip

      t.references :event, index: true
      t.references :user, index: true
      t.timestamps
    end
  end
end
