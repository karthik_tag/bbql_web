class CreateAppImages < ActiveRecord::Migration
  def change
    create_table :app_images do |t|
      t.text :content
      t.attachment :app_image

      t.timestamps
    end
  end
end
