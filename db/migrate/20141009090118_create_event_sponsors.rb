class CreateEventSponsors < ActiveRecord::Migration
  def change
    create_table :event_sponsors do |t|
      t.references :event, index: true
      t.string :sponsor_name
      t.string :sponsor_description

      t.timestamps
    end
  end
end
