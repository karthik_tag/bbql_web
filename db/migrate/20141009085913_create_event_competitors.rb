class CreateEventCompetitors < ActiveRecord::Migration
  def change
    create_table :event_competitors do |t|
      t.boolean :status
      t.string :position
      t.references :competitor
      t.references :event_category
      t.timestamps
    end
  end
end
