class CreateNews < ActiveRecord::Migration
  def change
    create_table :news do |t|
      t.string :header_text
      t.text :blog_content
      t.string :location
      t.datetime :news_datetime

      t.timestamps
    end
  end
end
