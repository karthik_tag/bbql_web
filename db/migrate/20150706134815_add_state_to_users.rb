class AddStateToUsers < ActiveRecord::Migration
  def change
    add_column :users, :state, :string
    add_column :users, :card_id, :integer
  end
end