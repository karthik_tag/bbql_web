class CreateEventCompetitorResults < ActiveRecord::Migration
  def change
    create_table :event_competitor_results do |t|
      t.integer :avg_visitor_results
      t.references :event_category
      t.integer :avg_judge_results
      t.references :event_competitor
      t.integer :position
      t.timestamps
    end
  end
end
