class ChangeDefaultValueOfStatusInUserRegIds < ActiveRecord::Migration
  def up
  	change_column :user_reg_ids, :status, :boolean, :default => false
  end

  def down
  	change_column :user_reg_ids, :status, :boolean, :default => true
  end
end
