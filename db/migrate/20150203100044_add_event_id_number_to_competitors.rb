class AddEventIdNumberToCompetitors < ActiveRecord::Migration
  def change
  	add_column :competitors, :number, :integer
  	add_column :competitors, :event_id, :integer
  	add_column :competitors, :compete_category_id, :integer
  	add_column :event_category_registrations, :competitor_id, :integer
  end
end
