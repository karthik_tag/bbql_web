class ChangeEventIdArrayInAdminUsers < ActiveRecord::Migration
  def up
  	add_column :admin_users, :event_ids, :string, :default => []
  	remove_column :admin_users, :event_id
  end

  def down
  	add_column :admin_users, :event_id, :integer
  	remove_column :admin_users, :event_ids
  end
end
