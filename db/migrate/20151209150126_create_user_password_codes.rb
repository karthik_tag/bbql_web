class CreateUserPasswordCodes < ActiveRecord::Migration
  def change
    create_table :user_password_codes do |t|
      t.integer :security_code
      t.boolean :status, :default => true
      t.references :user, index: true

      t.timestamps
    end
  end
end
