class AddNonProfitToEvents < ActiveRecord::Migration
  def change
  	add_column :events, :non_profit, :float, :default=>0
  end
end
