class CreateCategoryQuestions < ActiveRecord::Migration
  def change
    create_table :category_questions do |t|
      t.references :category, index: true
      t.string :question

      t.timestamps
    end
  end
end
