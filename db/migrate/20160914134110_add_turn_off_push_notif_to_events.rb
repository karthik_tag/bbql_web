class AddTurnOffPushNotifToEvents < ActiveRecord::Migration
  def change
  	add_column :events, :enable_notification_on_launch, :boolean, :default=>true
  end
end
