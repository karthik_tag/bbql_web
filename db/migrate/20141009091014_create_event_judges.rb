class CreateEventJudges < ActiveRecord::Migration
  def change
    create_table :event_judges do |t|
      t.references :event, index: true
      t.references :user, index: true
      t.timestamps
    end
  end
end
