class AddTiePositionToEventCategories < ActiveRecord::Migration
  def change
    add_column :event_categories, :tie_position, :integer, :default => 0
  end
end
