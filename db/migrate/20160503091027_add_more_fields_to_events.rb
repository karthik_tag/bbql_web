class AddMoreFieldsToEvents < ActiveRecord::Migration
  def change
  	add_column :events, :no_of_judges, :integer
  	add_column :events, :no_of_expected_competitors, :integer
  	add_column :events, :nonprofit_cause, :string  	
  end
end