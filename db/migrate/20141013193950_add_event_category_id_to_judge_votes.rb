class AddEventCategoryIdToJudgeVotes < ActiveRecord::Migration
  def change
    add_column :judge_votes, :event_category_id, :integer
  end
end
