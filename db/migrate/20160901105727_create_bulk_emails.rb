class CreateBulkEmails < ActiveRecord::Migration
  def change
    create_table :bulk_emails do |t|
      t.string :title
      t.text :content
      t.string :user_ids, :default => []
      
      t.references :user, index: true
      t.timestamps
    end
  end
end
