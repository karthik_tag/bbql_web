class DefaultValueForNoOfCategoriesInEventRequests < ActiveRecord::Migration
  def up
  	change_column :events, :no_of_categories, :integer, :default => nil
  end

  def down
  	change_column :events, :no_of_categories, :integer, :default => 0
  end
end
