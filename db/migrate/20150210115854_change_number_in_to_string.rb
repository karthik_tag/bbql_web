class ChangeNumberInToString < ActiveRecord::Migration
  def up
  	change_column :event_category_registrations, :number, :string
  	change_column :competitors, :number, :string
  	add_column :competitors, :edited_by, :string
  end

  def down
  	change_column :event_category_registrations, :number, :integer
  	change_column :competitors, :number, :integer
  	remove_column :competitors, :edited_by
  end
end
