class CreateUserRegIds < ActiveRecord::Migration
  def change
    create_table :user_reg_ids do |t|
      t.text :user_gcm_reg_id
      t.references :user, index: true

      t.timestamps
    end
  end
end
