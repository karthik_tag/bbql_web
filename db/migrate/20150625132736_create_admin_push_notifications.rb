class CreateAdminPushNotifications < ActiveRecord::Migration
  def change
    create_table :admin_push_notifications do |t|
      t.text :content
      t.boolean :status

      t.timestamps
    end
  end
end
