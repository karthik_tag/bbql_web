class CreateEventMcs < ActiveRecord::Migration
  def change
    create_table :event_mcs do |t|
      t.references :event, index: true
      t.references :user, index: true
      t.timestamps
    end
  end
end
