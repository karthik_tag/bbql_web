class AddPointsToEventCompetitors < ActiveRecord::Migration
  def change
  	add_column :event_competitors, :points, :integer, :default => 25
  end
end
