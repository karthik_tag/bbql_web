class ChangeLegalcopyToBoolean < ActiveRecord::Migration
  def up
  	change_column :users, :legal_copy, :boolean
  end

  def down
  	change_column :users, :legal_copy, :text
  end
end
