class AddIsEventBannerToEvents < ActiveRecord::Migration
  def change
    add_column :events, :is_event_banner, :boolean, :default=>false
  end
end
