class AddEventCategoryIdToVisitorVotes < ActiveRecord::Migration
  def change
    add_column :visitor_votes, :event_category_id, :integer
  end
end
