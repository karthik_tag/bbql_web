class CreateEventBanners < ActiveRecord::Migration
  def change
    create_table :event_banners do |t|
      t.text :content
      t.attachment :event_banner
      t.references :event, index: true

      t.timestamps
    end
  end
end
