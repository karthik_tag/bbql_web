class AddSelfRegistrationInEvents < ActiveRecord::Migration
  def up
    add_column :events, :is_self_registartion_on, :boolean, :default => false
  end

  def down
  	remove_column :events, :is_self_registartion_on
  end
end
