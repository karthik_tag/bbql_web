class AddGroupImagetoClubs < ActiveRecord::Migration
  def up
  	add_attachment :clubs, :group_image
  end

  def down
  	remove_attachment :clubs, :group_image
  end
end
