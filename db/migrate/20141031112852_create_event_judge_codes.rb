class CreateEventJudgeCodes < ActiveRecord::Migration
  def change
    create_table :event_judge_codes do |t|
      t.references :event, index: true
      t.references :judge_code, index: true
      t.references :user, index: true
      t.timestamps
    end
  end
end
