class ChangeCompetitorToUserLoginImages < ActiveRecord::Migration
  def up
  	rename_column :login_images, :competitor_id, :user_id
  end

  def down
  	rename_column :login_images, :user_id, :competitor_id
  end
end
