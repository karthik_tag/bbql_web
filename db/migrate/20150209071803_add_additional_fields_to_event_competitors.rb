class AddAdditionalFieldsToEventCompetitors < ActiveRecord::Migration
  def change
  	add_column :event_competitors, :event_category_registration_id, :integer
  	add_column :event_category_registrations, :number, :integer
  end
end
