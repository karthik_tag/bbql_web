class AddJudgeCodeIdInUsers < ActiveRecord::Migration
  def up
  	add_column :users, :judge_code_id, :integer
  	rename_column :judge_codes, :judge_code, :name
  end

  def down
  	remove_column :users, :judge_code_id
  	rename_column :judge_codes, :name, :judge_code
  end
end
