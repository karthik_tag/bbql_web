class CreateNonProfits < ActiveRecord::Migration
  def change
    create_table :non_profits do |t|
      t.string :url
      t.boolean :status

      t.timestamps
    end
  end
end
