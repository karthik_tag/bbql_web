class CreateMergeAccounts < ActiveRecord::Migration
  def change
    create_table :merge_accounts do |t|
      t.integer :actual_account_id
      t.integer :duplicate_account_id
      t.timestamps
    end
  end
end
