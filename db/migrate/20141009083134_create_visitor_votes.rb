class CreateVisitorVotes < ActiveRecord::Migration
  def change
    create_table :visitor_votes do |t|
      t.integer :position1
      t.integer :position2
      t.integer :position3
      t.references :event
      t.references :user
      t.references :event_competitor
      t.timestamps
    end
  end
end
