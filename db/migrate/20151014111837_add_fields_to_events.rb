class AddFieldsToEvents < ActiveRecord::Migration
  def change
    add_column :events, :event_id, :integer
    add_column :events, :user_ids, :string, :default => []
  end
end
