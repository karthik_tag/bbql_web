class CreateEventRegistrations < ActiveRecord::Migration
  def change
    create_table :event_registrations do |t|      
      t.references :event, index: true

      t.timestamps
    end
  end
end
