class ChangeNumberDatatypeInEvtCatReg < ActiveRecord::Migration
  def up
  	change_column :event_category_registrations, :number, :integer
  	change_column :competitors, :number, :integer
  	add_column :login_images, :competitor_id, :integer, :references => "competitors"
  end

  def down
  	change_column :event_category_registrations, :number, :string
  	change_column :competitors, :number, :string
  	remove_column :login_images, :competitor_id
  end
end
