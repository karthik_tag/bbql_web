class CreateJudgeVotes < ActiveRecord::Migration
  def change
    create_table :judge_votes do |t|
      t.integer :rating
      t.references :category_question
      t.references :event
      t.references :event_competitor
      t.references :user
      t.timestamps
    end
  end
end
