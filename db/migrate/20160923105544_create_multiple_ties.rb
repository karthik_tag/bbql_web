class CreateMultipleTies < ActiveRecord::Migration
  def change
    create_table :multiple_ties do |t|
  	  t.float :rank
      t.float :score
      t.integer :position

      t.references :event_competitor, index: true
      t.references :event_category, index: true
      t.references :user, index: true
      t.timestamps
    end
  end
end
