class ChangeColumCardIdToString < ActiveRecord::Migration
  def up
  	change_column :users, :card_id, :string
  end

  def down
  	change_column :users, :card_id, :integer
  end
end
