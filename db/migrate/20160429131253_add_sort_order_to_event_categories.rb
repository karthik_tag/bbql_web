class AddSortOrderToEventCategories < ActiveRecord::Migration
  def change
  	add_column :event_categories, :sort_order, :integer
  end
end
