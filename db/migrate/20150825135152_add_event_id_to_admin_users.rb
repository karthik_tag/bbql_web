class AddEventIdToAdminUsers < ActiveRecord::Migration
  def change
    add_column :admin_users, :event_id, :integer
    add_column :user_reg_ids, :status, :boolean, :default=> true
  end
end
