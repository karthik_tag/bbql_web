# encoding: utf-8
# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)


# Static data to feed contests table with contests name.
user_types_array = ["Judge","Competitor","Visitor"]
user_types_array.each do |user_type|
	user_type_obj = UserType.find_or_create_by_name(user_type)	
end

# Update all is_published value to 0 for the existing db records. Now its default value is NULL. Migration file is updated to ":default => 0". It will work fine, if the fresh DB is created.
EventCategory.where(:is_published=>nil).update_all(:is_published => 0)

# Static datas for categories
main_category_array ={
	"Moustache"=>"Upper Lip Territory here.",
	"Partial Beard"=>"An assortment of categories that involve partial beards and includes Donegal, Chops, Musketeer and more.",
	"Full Beard"=>"Exactly what is says. Let that shit grow top and bottom. Put up the razor. And if you trim that 'stache, it'll cost you points.",
	"Whiskerinas"=>"Ladies Categories. Always Competitive. Always Fun.",
	"Childrens"=>"Under 18 contestants",
	"Specialty"=>"These are unique categories that are our of the norm from traditional WBMA categories, but still lot's of fun."
}
main_category_array.each do |main_category,description|
	main_category_obj = MainCategory.find_or_create_by_name(:name => main_category, :description => description)
	case main_category
	when "Moustache"
		category_array={
			"Natural Moustache"=>["Moustache as it grows naturally. The more natural the better. No closed curls. Only those hairs growing from within 1.5 cm past the end of the upper lip may be grown out. The moustache may be shaped but without aids. No aids allowed. NO AIDS, DAMNIT!", "Moustache---Natural"],
			"English"=>["Slender, the hairs are combed from the middle of upper lip to the sides. The tips can be raised slightly. Only hairs growing on the upper lip may be grown out. Aids allowed.", "Moustache---English"],
			"Dali"=>["Slender, with the tips curved upward. Only hairs on the upper lip may be grown out. The styled Dali Moustache my not extend over the eye brows. Aids allowed.","Moustache---Dali-2"],
			"Imperial Moustache"=>["Small and bushy wit the tips curving upward. Only hairs of the upper lip may be grown out. Aids allowed.", "Moustache---Imperial"],
			"Hungarian"=>["Large and bushy: Hairs combed from the middle of the upper lip to the sides. Only those hairs growing from within 1.5 cm past the end of the upper lip may be grown out. Aids allowed.", "Moustache---Hungarian"],
			"Freestyle"=>["Free design and styling of the moustache. All moustaches that do not qualify for other categories may compete in this category. Only those hairs growing from within 1.5 cm past the end of the upper lip may be grown out. Aids allowed.", "Moustache---Freestyle"],
			"Styled Mustache"=>["Free design and styling of the mustache using aids such as wax, hairspray, and similar hair cosmetics. Only those hairs growing from within 1.5 cm past the end of the upper lip may be grown out. Hair extensions, false facial hair, or any form of hairpins are prohibited. Chin hair is not permitted in this category.", "Moustache---Dali"]
		}
		category_array.each do |category, des|
			category_obj = main_category_obj.categories.find_or_create_by_name!(:name => category, :description => des[0])
			category_obj.create_attachment(:attachment => File.new("#{Rails.root}/public/images/categories/#{des[1]}.png")) if category_obj.attachment.nil?
		end

	when "Partial Beard"
		category_array ={
			"Natural Goatee"=>["Goatee as it grows naturally, the more natural, the better. Moustache not required. Goatee and moustache can be accentuated, but without styling aids. No curing moustaches. There must be a clean shaven section at least 4 cm wide.", "Partial-Beard---Natural-Goatee"],
			"Fu Manchu"=>["Hair growing from within 2 cm past the end of the upper lip and downward to the beginning of the chin, with the tips long and pointing downward. Styling aids allowed.", "Partial-Beard---Fu-Manchu"],
			"Musketeer"=>["Narrow chin beard that comes to a point. Moustache narrow, long, lightly curved, and combed to the sides. Moustache may consist only of hairs that grow within 1.5 cm past the end of the upper lip. Styling aids allowed.", "Partial-Beard---Musketeer"],
			"Imperial"=>["Hair on the cheeks and upper lip. There must be clean shaven spaces of at least 4 cm wide (the width of a razor blade) on the chin and between the facial hair and the onset of the head hair. The whiskers are styled upwards. No closed curls.", "Partial-Beard---Imperial"],
			"Sideburns Natural"=>["Hair connecting from the sides of the face to the hairline. Chin must be shaved at a razors width, with moustache permitted but not required. No styling aids are permitted.", "Partial-Beard---Sideburns"],
			"Alaskan Whaler /Donegal"=>["Traditional Alaskan seafarer’s beard. Bushy hair grown on cheeks, chin and lower lip. No hair grown on upper lip.", "Partial-Beard---Alaskan-Whaler-Donegal"],
			"Partial Beard Freestyle"=>["Free design and styling of the beard. All partial beards that do not qualify for other categories may compete in this category. There must be a clean shaven space at least 4 cm wide (the width of a razor) either on the check, the chin, or between the head.", "Partial-Beard---Freestyle"],
			"Chops"=>["A true sideburn, no natural or shaven gap below the ear. Chin is shaven, however, hair on the upper lip is optional. Styling aids are permitted.", "Partial-Beard---Chops"]
		}
		category_array.each do |category, des|
			category_obj = main_category_obj.categories.find_or_create_by_name!(:name => category, :description => des[0])
			category_obj.create_attachment(:attachment => File.new("#{Rails.root}/public/images/categories/#{des[1]}.png")) if category_obj.attachment.nil?
		end
	when "Full Beard"
		category_array={
			"Business/College beard"=>["Full Beard under 1/2 inch in length, as it grows and left natural, the more natural the better. The moustache is blended into the beard. No aids allowed.", "Full-Beard---Business_College-beard--Short-beard,-under-1_2"],
			'Full beard Natural under 6"'=>["Full Beard as it grows and left natural, the more natural the better. The moustache is blended into the beard. No aids allowed.", "Full-Beard---Full-Beard-Natural-under-6"],
			'Full Beard Natural under 8"'=>["Full Beard as it grows and left natural, the more natural the better. The moustache is blended into the beard. No aids allowed.", "Full-Beard---Full-Beard-Natural-under-8"],
			'Full beard natural under 12"'=>["Full Beard as it grows and left natural, the more natural the better. The moustache is blended into the beard. No aids allowed.", "Full-Beard---Full-beard-natural-under-12"],
			'Full Beard Natural over 12"'=>["Full Beard as it grows and left natural, the more natural the better. The moustache is blended into the beard. No aids allowed.", "Full-Beard---Full-Beard-Natural-over-12"],
			'Best Groomed Beard under 6"'=>["This category is to be judged on precise lines between the area of the face that is shaved and unshaved, consistent thickness and overall groomed look of each competitor. Styling aids permitted but not required.", "Full-Beard---Best-Groomed-Beard-under-6"],
			'Best Groomed Beard over 6"'=>["This category is to be judged on precise lines between the area of the face that is shaved and unshaved, consistent thickness and overall groomed look of each competitor. Styling aids permitted but not required.", "Full-Beard---Best-Groomed-Beard-over-6"],
			'Verdi'=>["Beard is short and round as the bottom, no longer than 10 cm below the bottom lower lip. As it grows naturally, the more natural, the better. Moustache is consisting only of hairs growing from within 1.5 cm past the end of the upper lip, should be promine", "Full-Beard---Verdi"],
			'Garibaldi'=>["Beard is round and wide at the bottom, no longer than 20 cm from the bottom of the lower lip. The beard is as it grows naturally, the more natural the better. The moustache may not be separated from the beard or made prominent. Aids allowed.", "Full-Beard---Garibaldi"],
			'Natural Full Beard'=>["Full Beard as it grows and left natural, the more natural the better. The moustache is blended into the beard. No aids allowed.", "Full-Beard---Natural-Full-Beard"],
			'Natural Full Beard with Styled Moustache'=>["Moustache prominent and consisting of hairs growing from within 1.5 cm past the end of the upper lip. Styling aids allowed. Beard: as it grows naturally, the more natural, the better. No styling aids allowed in the beard.", "Full-Beard---Natural-Full-Beard-with-Styled-Moustache"],
			'Full Beard Freestyle'=>["Free design and styling of the beard. All full beards that do not qualify for other categories compete in the category. Aids allowed.", "Partial-Beard---Full-Beard-Freestyle"],
			"Worst Beard In The Universe"=>["No criteria necessary, if you do not have any standards for your facial hair, then neither do we! This is not an all-encompassing category. Only contestants who enter this category will be considered for Worst Beard in the Universe. Styling aids are permitted.", "Moustache---Freestyle"]
		}
		category_array.each do |category, des|
			category_obj = main_category_obj.categories.find_or_create_by_name!(:name => category, :description => des[0])
			category_obj.create_attachment(:attachment => File.new("#{Rails.root}/public/images/categories/#{des[1]}.png")) if category_obj.attachment.nil?
		end
	when "Whiskerinas"
		category_array={
			"Ladies Creative Beard" => ["Full beard, anything goes. This category is to be judged on creativity, impressiveness, complexity and overall fit per competitor.", "Whiskerinas---Ladies-Creative-Beard"],
			"Ladies Creative Moustache" => ["A moustache is the hair that is grown above the upper lip, and not hair that is extending from the cheeks. The boundary between the upper lip and cheek will be considered on a person-to-person basis by the judges. This category is to be judged on creativity, impressiveness, complexity and overall fit of hair attached from this area of the face per competitor.", "Whiskerinas---Ladies-Creative-Moustache"],
			"Ladies Realistic Beard" => ["Focus is on a natural looking beard and overall appearance of the full beard, real or artificial, on the competitor. Historically attached/grown facial hair wins this category.", "Whiskerinas---Ladies-Realistic-Beard"],
			"Ladies Realistic Moustache" => ["Focus is on a natural looking moustache and the overall appearance of the moustache, real or artificial, on the competitor. Historically actual attached/grown facial hair wins this category.", "Whiskerinas---Ladies-Realistic-Moustache"]
		}
		category_array.each do |category, des|
			category_obj = main_category_obj.categories.find_or_create_by_name!(:name => category, :description => des[0])
			category_obj.create_attachment(:attachment => File.new("#{Rails.root}/public/images/categories/#{des[1]}.png")) if category_obj.attachment.nil?
		end
	when "Childrens"
		category_array={
			"Childrens Under 10 yrs old"=>["Anything goes! Children judged on Creativity, Design, Cuteness and Complexity.","Moustache---Freestyle"],
			"Children 10-17 yrs old"=>["Anything goes! Children judged on Creativity, Design, Cuteness and Complexity.","Whiskerinas---Ladies-Realistic-Beard"]
		}
		category_array.each do |category, des|
			category_obj = main_category_obj.categories.find_or_create_by_name!(:name => category, :description => des[0])
			category_obj.create_attachment(:attachment => File.new("#{Rails.root}/public/images/categories/#{des[1]}.png")) if category_obj.attachment.nil?
		end
	when "Specialty"
		specialty_hash={
			  "Movember Mustache" => ["Contestants must provide proof that they we're clean shaven on November 1st, and will only be judged on the mustache portion of their facial hair.", "Moustache---Natural"],
			  "Styled Goatee" => ["Is a Specialty category with emphasis on chin growth. Must not be connected at the sideburns, and can include a mustache. Styling products are permitted", "Partial-Beard---Chops"],
			  "Texas Redbeard" => ["Only contestants with a certain 'Gingerness' may enter. The color is the emphasis, but growth should also be considered.", "Partial-Beard---Alaskan-Whaler-Donegal"],
			  "Uber Stache" => ["This category is for facial hair that is primarily a moustache but is not wholly contained to the upper lip. Musketeers, Horseshoes, and other moustache styles not limited to 1.5 cm from the upper lip should register for this category. Styling aides encouraged but not mandatory. Prejudge will place contestant into a partial beard category if he determines the primary focus of the facial hair is not the moustache.", "Moustache---Freestyle"],
			  "Silver Natural Beard" => ["","Moustache---Freestyle"],
			  "Freestyle" => ["","Moustache---Freestyle"],
			  "General Partial Bread" => ["","Moustache---Freestyle"],
			  "Best In Show-King & Queen" => ["","Moustache---Freestyle"],
			  "Show Stopper" => ["","Moustache---Freestyle"],
			  "Natural Whiskerina" => ["", "Whiskerinas---Ladies-Realistic-Moustache"],
			  "Fantasy Whiskerina" => ["", "Whiskerinas---Ladies-Creative-Moustache"]
			}
		specialty_hash.each do |category, des|
			category_obj = main_category_obj.categories.find_or_create_by_name!(:name => category, :description => des[0])
			category_obj.create_attachment(:attachment => File.new("#{Rails.root}/public/images/categories/#{des[1]}.png")) if category_obj.attachment.nil?
		end
	end

end


# Static datas for club
File.open("#{Rails.root}/public/clubs.csv") do |clubs|
  clubs.read.each_line do |club|
    name, location, state = club.chomp.split(",")
    # to create each record in the database
    club = Club.find_or_create_by_name(:name => name, :location => location, :state => state)
    #club.create_attachment(:attachment => File.new("#{Rails.root}/public/images/noClubIcon.png")) if club.attachment.nil?
  end
end

# Static datas for charity
File.open("#{Rails.root}/public/Charities.csv") do |charities|
  charities.read.each_line do |charity|
    name = charity.chomp.split(",")[0]
    # to create each record in the database
    charity = Charity.find_or_create_by_name(:name => name)
    #charity.create_attachment(:attachment => File.new("#{Rails.root}/public/images/NoCharity.png")) if charity.attachment.nil?
  end
end