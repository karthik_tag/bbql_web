Devrails::Application.routes.draw do

  # get "eventbrite/index"

  devise_for :users

  match 'generate_judge_code', :to => 'judge_codes#generate_judge_code', :as => 'generate_judge_code', :via => [:get]
  match 'generate_competitor_position', :to => 'event_categories#generate_competitor_position', :as => 'generate_competitor_position', :via => [:get]
  match 'validate_judge_code', :to => 'judge_codes#validate_judge_code', :as => 'validate_judge_code', :via => [:get]
  match 'validate_app_user_by_email', :to => 'user#validate_app_user_by_email', :as => 'validate_app_user_by_email', :via => [:get]
  match 'populate_category_by_main_category', :to => 'categories#populate_category_by_main_category', :as => 'populate_category_by_main_category', :via => [:get]
  match 'populate_event_categories', :to => 'categories#populate_event_categories', :as => 'populate_event_categories', :via => [:get]
  match 'populate_user_not_participated_in_this_event', :to => 'events#populate_user_not_participated_in_this_event', :as => 'populate_user_not_participated_in_this_event', :via => [:get]
  match 'check_if_events', :to => 'events#check_if_events', :as => 'check_if_events', :via => [:get]
  match 'check_if_events_for_tabs', :to => 'events#check_if_events_for_tabs', :as => 'check_if_events_for_tabs', :via => [:get]
  match 'pull_event_registrants', :to => 'events#pull_event_registrants', :as => 'pull_event_registrants', :via => [:get]
  match 'participate_event_category', :to => 'events#participate_event_category', :as => 'participate_event_category', :via => [:post]
  match 'api/get_all_events_details', :to => 'events#get_all_events_details', :as => 'get_all_events_details', :via => [:get]
  match 'gcm_register', :to => 'user_reg_ids#gcm_register', :as => 'gcm_register', :via => [:post]
  match 'toggle_push_notification', :to => 'user_reg_ids#toggle_push_notification', :as => 'toggle_push_notification', :via => [:post]
  match 'get_states', :to => 'user#get_states', :as => 'get_states', :via => [:get]

  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)

  namespace :admin do
      match 'take_photo', :to => 'event_category_registrations#take_photo', :as => 'take_photo', :via => [:get]
      match 'image_pop', :to => 'event_category_registrations#image_pop', :as => 'image_pop', :via => [:get]
      match 'update_photo', :to => 'event_category_registrations#update_photo', :as => 'update_photo', :via => [:put]
      match 'new_bulk_ck_mnrs', :to => 'admin_users#new_bulk_ck_mnrs', :as => 'new_bulk_ck_mnrs', :via => [:get]
      match 'save_bulk_ck_mnrs', :to => 'admin_users#save_bulk_ck_mnrs', :as => 'save_bulk_ck_mnrs', :via => [:post]
      match 'bulk_checkin_managers_onsubmit_validation', :to => 'admin_users#bulk_checkin_managers_onsubmit_validation', :as => 'bulk_checkin_managers_onsubmit_validation', :via => [:post]
      match 'select_event_photo', :to => 'events#select_event_photo', :as => 'select_event_photo', :via => [:get]
      match 'update_event_photo', :to => 'events#update_event_photo', :as => 'update_event_photo', :via => [:put]
      match 'select_event_clubs', :to => 'event_clubs#select_event_clubs', :as => 'select_event_clubs', :via => [:get]
      match 'update_event_clubs', :to => 'event_clubs#update_event_clubs', :as => 'update_event_clubs', :via => [:post]
      resource :event_category_registrations do
        member do
          put :update, :as => "update"
        end
      end
      resource :event_categories do
        member do
          put :update, :as => "update"
        end
      end
  end
  
  match 'user/sign_in' => 'user#sign_in'
  match 'user/sign_up' => 'user#sign_up'
  match 'user/create_non_profit_url' => 'user#create_non_profit_url'
  match 'user/delete_non_profit_url' => 'user#delete_non_profit_url'
  # match 'user/create_admin_push_notification' => 'user#create_admin_push_notification'
  # match 'user/delete_admin_push_notification' => 'user#delete_admin_push_notification'
  match '/populate_user_details' => 'user#populate_user_details', :via => [:get]
  match 'self_check_in' => 'events#self_check_in', :via => [:post]
  match 'forgot_password', :to => 'user#forgot_password', :as => 'forgot_password', :via => [:post]
  match 'validate_security_code', :to => 'user#validate_security_code', :as => 'validate_security_code', :via => [:post]
  match 'update_password_details', :to => 'user#update_password_details', :as => 'update_password_details', :via => [:post]
  match 'validate_number', :to => 'user#validate_number', :as => 'validate_number', :via => [:get]
  match 'generate_users_list', :to => 'user#generate_users_list', :as => 'generate_users_list', :via => [:get]
  match 'app_landing_page', :to => 'user#app_landing_page', :as => 'app_landing_page', :via => [:get]
  match '/get_competitor_details' => 'cards#get_competitor_details', :via => [:get]
  match '/my_card_details' => 'cards#my_card_details', :via => [:get]
  # match '/search_cards' => 'cards#search_cards', :via => [:get]
  match '/update_competitor_details' => 'cards#update_competitor_details', :via => [:post]
  match '/categories' => 'home#get_categories'
  match '/charities' => 'home#get_charities'
  match '/get_all_dropdown_values' => 'home#get_all_dropdown_values'
  match 'home' => 'home#index'
  match '/update_judge_code' => 'home#update_judge_code'
  match '/update_profile' => 'profiles#update_profile'
  match '/signup_clubs' => 'home#signup_clubs'
  match '/news' => 'home#news'
  # match '/eventbrite' => 'eventbrite#index'
  # match '/eventbrite/create_registration' => 'eventbrite#create_registration'
  match '/validate_email_on_create_new_user' => 'mobile_checkins#validate_email_on_create_new_user'
  match '/submit_tie_breaker' => 'event_categories#submit_tie_breaker', :via => [:post]
  # match 'motta_sathu', :to => 'events#motta_sathu', :as => 'motta_sathu', :via => [:get]
  # match '/delete_sathu' => 'event_categories#delete_sathu', :via => [:get]
  
  resources :cards
  resources :clubs
  resources :profiles
  resources :events do
    get 'results', :on => :member
    get 'photos', :on => :member
    get 'launch_event', :on => :member
    get 'event_stats', :on => :member
    get 'close_event', :on => :member
    get 'get_non_closed_event_category', :on => :member
    get 'load_more_judge_results_data', :on => :member
    get 'show_season_wise_donations', :on => :collection
  end
  resources :event_categories do
    post 'voting', :on => :member
    get 'announce_event_category_results', :on => :member
    get 'close_event_category', :on => :member
    get 'get_event_category_competitors', :on => :member
    get 'validate_category_name', :on => :member
    get 'force_close_judging_by_mc', :on => :member
    get 'fetch_tied_competitors', :on => :collection
  end

  resources :visitor_votes do
    collection do
      get 'load_visitor_votes'
    end
  end

  resources :judge_votes do
    member do
      get 'get_judges_votes'
    end
  end

  resources :event_fan_voting_results do
    collection do
      get 'track_overall_fan_voting_results'
      get 'track_category_fan_voting_results'
    end
  end

  resources :mobile_checkins do
    get 'event_checkin', :on => :member
    get 'take_photo', :to => 'mobile_checkins#take_photo', :as => 'take_photo'
    get 'search_users', :to => 'mobile_checkins#search_users', :as => 'search_users'
    get 'checkin_users', :to => 'mobile_checkins#checkin_users', :as => 'checkin_users'
    post 'update_checkin_users', :to => 'mobile_checkins#update_checkin_users', :as => 'update_checkin_users'
    post 'remove_checkin_users', :to => 'mobile_checkins#remove_checkin_users', :as => 'remove_checkin_users'
    put 'update_photo', :to => 'mobile_checkins#update_photo', :as => 'update_photo'
    put 'register_comp_to_events', :to => 'mobile_checkins#register_comp_to_events', :as => 'register_comp_to_events'
    get 'edit_checkedin_comp', :to => 'mobile_checkins#edit_checkedin_comp', :as => 'edit_checkedin_comp'
    put 'update_checkedin_comp', :to => 'mobile_checkins#update_checkedin_comp', :as => 'update_checkedin_comp'
    post 'create_new_competitor', :to => 'mobile_checkins#create_new_competitor', :as => 'create_new_competitor'
    delete 'delete_checkedin_comp', :to => 'mobile_checkins#delete_checkedin_comp', :as => 'delete_checkedin_comp'
    get 'delete_checkedin_competitor', :to => 'mobile_checkins#delete_checkedin_competitor', :as => 'delete_checkedin_competitor'
  end

  resources :event_requests do
    # get 'event_checkin', :on => :member    
    # put 'update_checkedin_comp', :to => 'mobile_checkins#update_checkedin_comp', :as => 'update_checkedin_comp'
    # post 'create', :to => 'event_requests#create', :as => 'create'
  end
  
   root :to => 'admin/dashboard#index'

end
