Paperclip::Attachment.default_options[:s3_host_name] = 's3-us-west-2.amazonaws.com'
Paperclip::Attachment.default_options[:url] = '/attachments/:id/:style/:basename.:extension'
Paperclip::Attachment.default_options[:path] = '/attachments/:id/:style/:basename.:extension'