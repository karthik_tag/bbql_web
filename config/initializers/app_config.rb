require 'ostruct'
require 'yaml'

all_config = YAML.load_file("#{Rails.root}/config/constants.yml") || {}
env_config = all_config[Rails.env] || {}
AppConfig = OpenStruct.new(env_config)

# eventbrite_config = YAML.load_file("#{Rails.root}/config/eventbrite.yml") || {}
# evt_brt_env_config = eventbrite_config[Rails.env] || {}
# EventBriteConfig = OpenStruct.new(evt_brt_env_config)

groupMe_config = YAML.load_file("#{Rails.root}/config/groupMe.yml") || {}
group_me_env_config = groupMe_config[Rails.env] || {}
GroupMeConfig = OpenStruct.new(group_me_env_config)

pushNotification_config = YAML.load_file("#{Rails.root}/config/push_notification.yml") || {}
push_notif_env_config = pushNotification_config[Rails.env] || {}
PushNotifConfig = OpenStruct.new(push_notif_env_config)