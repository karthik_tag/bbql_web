class Rpush::Apns::Notification
  def self.import(bulk_data)
    sql = ""
    if bulk_data.present?
    	until bulk_data.empty?
    	  row = bulk_data.pop
    	  sql += "('#{row.device_token}', '#{row.alert}', '#{DateTime.now}', '#{DateTime.now}', '#{row.delivered}', '#{row.delivered_at}', '#{row.type}', '#{row.registration_ids}', '#{row.url_args}', '#{row.app_id}', '#{row.collapse_key}')"
    	end
    	ActiveRecord::Base.connection.execute("INSERT INTO rpush_notifications (device_token, alert, created_at, updated_at, delivered, delivered_at, type, registration_ids, url_args, app_id, collapse_key) VALUES #{sql.gsub(')(','),(')}")
  	end
  end
end