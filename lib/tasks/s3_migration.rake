namespace :attachments do
  desc "migrate to s3"  
  task :migrate_to_s3 => :environment do
    s3_options = YAML.load_file(File.join(Rails.root, 'config/aws.yml')).symbolize_keys
    bucket = s3_options[:development]["bucket_name"]
    
    s3 = AWS::S3.new(s3_options)
    styles = []
    # Collect all the styles, all are uploaded because `rake paperclip:refresh CLASS=Attachment` is broken for mongoid
    Attachment.first.attachment.styles.each do |style|
      styles.push(style[0])
    end
    # also the :original "style"
    styles.push(:original)
    
    # Process each attachment
    Attachment.where('updated_at > ?', "2015-04-28 14:29:40").each_with_index do |attachment, n|
      styles.each do |style|
        path = attachment.attachment.path(style)
        
        image_url = attachment.attachment_file_name.present? ? "#{Rails.root.to_s}/public/assets/attachments/#{attachment.id}/#{style}/"+attachment.attachment_file_name : ""
          if !Dir.glob(image_url).empty?
            file = open("#{Rails.root.to_s}/public/assets/attachments/#{attachment.id}/#{style}/"+attachment.attachment_file_name)
          else
            file = nil
          end        
        
        begin        
          key = "attachments/#{attachment.id}/#{style}/"+attachment.attachment_file_name if file.present?
          s3.buckets["fhlwebprod"].objects[key].write(:file => file) if file.present?
          #rescue AWS::S3::ResponseError => e
          #raise
          puts "Saved #{path} to S3 (#{n}/#{Attachment.count})"
        end
        
      end
    end
  end
end 