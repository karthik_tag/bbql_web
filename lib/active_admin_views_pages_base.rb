class ActiveAdmin::Views::Pages::Base < Arbre::HTML::Document

  private

  # Renders the content for the footer
  def build_footer
    div :id => "footer" do
    	para "Copyright #{Date.today.year.to_s} &copy; All Rights Reserved.".html_safe
    end
  end

end