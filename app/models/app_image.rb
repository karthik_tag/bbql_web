class AppImage < ActiveRecord::Base
  has_attached_file :app_image, :styles => { :medium => "350x350", :small => "180x180" }

  attr_accessor :_destroy
  attr_accessible :content, :app_image, :app_image_file_name, :app_image_content_type, :app_image_file_size, :app_image_updated_at, :_destroy

  validates :app_image, :presence => true
  validates_attachment :app_image, size: { less_than: 10.megabytes, message: "Must be less than 10MB" }, content_type: { content_type: ["image/png", "image/jpg", "image/jpeg", ""]}
end
