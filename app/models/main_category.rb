class MainCategory < ActiveRecord::Base 

  has_many :categories 
  has_one :attachment, :as => :attachable, :dependent => :destroy

  accepts_nested_attributes_for :attachment, :allow_destroy => true
  attr_accessible :description, :name, :attachment_attributes

  validates :name, :presence => true, :uniqueness => true

  scope :get_main_category, lambda { |id| where("id = ?", id)}
end
