class MultipleTy < ActiveRecord::Base
  attr_accessible :rank, :score, :event_competitor_id, :event_category_id, :user_id, :position

  belongs_to :event_competitor
  belongs_to :event_category
  belongs_to :user
end
