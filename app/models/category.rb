class Category < ActiveRecord::Base
  has_many :category_questions, :dependent => :destroy
  has_many :competitors
  has_one :attachment, :as => :attachable, :dependent => :destroy
  has_many :events, :through => :event_categories
  has_many :event_categories, :dependent => :destroy
  belongs_to :main_category

  accepts_nested_attributes_for :attachment, :allow_destroy => true
  accepts_nested_attributes_for :category_questions, :allow_destroy => true
  attr_accessible :description, :name, :main_category_id, :attachment_attributes, :category_questions_attributes
  
  validates :name, :presence => true
  validates :main_category_id, :presence => true

  scope :get_category, lambda{|id| where("id=?",id).includes(:category_questions, :competitors, :event_categories)}

  def get_category_questions
    self.category_questions
  end
  
end