class EventSponsor < ActiveRecord::Base
  has_one :attachment, :as => :attachable, :dependent => :destroy
  belongs_to :event

  accepts_nested_attributes_for :attachment, :allow_destroy => true
  attr_accessible :event_id, :sponsor_description, :sponsor_name, :sponsor_url, :attachment_attributes

  validates :event_id, :sponsor_name, :presence => :true
  def display_name # Dont easily change this. This is tied with URL of breakcrumb with event filters in js file
    # "#{sponsor_name}"
    "#{self.try(:event).try(:id)}"
  end
end
