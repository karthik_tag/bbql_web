class Competitor < ActiveRecord::Base
  has_many :attachments, :as => :attachable, :dependent => :destroy
  attr_accessor :step, :main_category_id, :edit_state

  accepts_nested_attributes_for :attachments, :allow_destroy => true
  attr_accessible :category_id, :club_id, :charity_id, :city, :state, :nick_name, :user_id, :attachments_attributes, :step, :main_category_id, :number, :event_id, :compete_category_id, :edited_by, :edit_state
  belongs_to :event
  belongs_to :category #, :foreign_key=>"compete_category_id"
  belongs_to :charity
  belongs_to :club
  belongs_to :user
  belongs_to :login_image
  has_many :event_competitors, :dependent => :destroy
  has_many :event_category_registrations, :dependent => :destroy

  # before_create :check_all_params
  # after_create :map_event_categories
  after_update :update_login_image_details
  after_save :event_registration

  #Presence check
  validates :category_id, :charity_id, presence: true , :if => Proc.new { |competitor|  competitor.step == "2"}
  
  # validates :number, :presence =>false, :allow_nil =>true
  # validates :number, :numericality => true, :allow_nil =>true
  # validates :number, :uniqueness => {:scope => :event_id}, :allow_nil =>true

  # def check_all_params
  #   if self.nick_name.present?
  #     Competitor.create(:category_id=>category_id, :club_id=> club_id, :charity_id=> charity_id, :city=>city , :state=>state , :nick_name =>nick_name )
  #   end
  # end
  # def map_event_categories
  #   upcoming_event_ids = Event.upcoming_and_live_events.collect(&:id)
  #   event_categories = EventCategory.where("category_id=? and event_id in (?)",category_id,upcoming_event_ids)
  #   event_categories.each do |event_category|
  #     EventCompetitor.create(:event_category_id=>event_category.id,:competitor_id=>id)
  #   end
  # end

  def event_registration
    # self.create_event_category_registration!(:event_registration_id=>self.try(:event).try(:event_registration).try(:id),:event_id=>self.event_id,:category_id=>self.compete_category_id,:user_id=>self.user.try(:id)) if self.event_id.present?
    if self.try(:edited_by) == "admin" && self.event_id.present?
      unless EventCategoryRegistration.find_by_event_id_and_category_id_and_competitor_id(self.event_id,self.compete_category_id,self.id).present?
        evnt_cat_reg = EventCategoryRegistration.create!(:event_id => self.event_id, :competitor_id => self.id)
        evnt_cat_reg.update_attributes(:event_registration_id=>self.try(:event).try(:event_registration).try(:id), :category_id=>self.compete_category_id, :user_id=>self.user.try(:id), :number=>self.number) if evnt_cat_reg
      end
      #Add the currently registered member(through admin) to the GroupMe group.
      # groupme_response = GroupMe.add_members_to_group(GroupMeConfig.access_token,'groups',evnt_cat_reg) if evnt_cat_reg.present?
      
      event_category = EventCategory.find_by_event_id_and_category_id(self.event_id,self.compete_category_id)
      unless EventCompetitor.find_by_competitor_id_and_event_category_id(evnt_cat_reg.try(:competitor_id),event_category.try(:id)).present?
        evnt_comp = EventCompetitor.create!(:event_category_registration_id=>evnt_cat_reg.try(:id))
        evnt_comp.update_attributes(:competitor_id=>evnt_cat_reg.try(:competitor_id), :event_category_id=>event_category.try(:id)) if evnt_cat_reg.present?
      end      
      EventCompetitor.where("event_category_id is null").delete_all
      EventCategoryRegistration.where("event_id is null").delete_all      
    end
  end

  private

  def update_login_image_details
    if self.try(:user).try(:login_image).present?
      self.try(:user).try(:login_image).update_attributes(:nick_name=>self.try(:nick_name).try(:capitalize), :charity_id=>self.try(:charity).try(:id))
    end
  end

end
