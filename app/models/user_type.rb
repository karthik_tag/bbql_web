class UserType < ActiveRecord::Base
  attr_accessible :name
  scope :get_id, lambda {|name| where("name = ?", name)  }
  scope :get_user_type, lambda {|id| where("id = ?", id)  }
end
