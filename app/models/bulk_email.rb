class BulkEmail < ActiveRecord::Base
  attr_accessor :select_users_list
  attr_accessible :title, :content, :user_ids, :select_users_list
  serialize :user_ids, Array

  validates :title, :content, :user_ids, :select_users_list, :presence => true

  after_save :send_bulk_emails

  private

  def send_bulk_emails
  	users = User.where(:id=>self.user_ids)
  	Thread.new{UserMailer.send_bulk_email_to_selected_users(users,self).deliver}
  end
end
