class Club < ActiveRecord::Base  
  has_many :competitors
  has_one :attachment, :as => :attachable, :dependent => :destroy
  has_many :events, :through => :event_clubs
  has_many :event_clubs, :dependent => :destroy

  after_save :create_attachment_if_not
  
  accepts_nested_attributes_for :attachment, :allow_destroy => true
  attr_accessor :_destroy #, :group_image, :group_image_file_name, :group_image_content_type, :group_image_file_size, :group_image_updated_at, :_destroy
  attr_accessible :location, :name, :attachment_attributes, :state, :club_url, :president, :email, :group_image, :group_image_file_name, :group_image_content_type, :group_image_file_size, :group_image_updated_at, :_destroy
  has_attached_file :group_image, :styles => { :medium => "350x350", :small => "180x180" } #,
                    # :url  => "/assets/attachments/:id/:style/:basename.:extension",
                    # :path => ":rails_root/public/assets/attachments/:id/:style/:basename.:extension"

  validates :name, :presence => true, :uniqueness => true
  validates_attachment :group_image, content_type: { content_type: ["image/png", "image/jpg", "image/jpeg", ""] }

  def self.fetch_all_clubs
  	club_array = []
    clubs_set = includes(:competitors,:attachment).select("id, name, location, state, competitors.id").order('name ASC').order('name ASC')
    # clubs_set = includes(:competitors).select("id, name, location, state, competitors.id").order('name ASC').includes(:attachment).limit(10).offset(20).order('name ASC')
    clubs_set.each do |club|
    	club_data = club
    	club_data["club_name_index"] = club.try(:name).slice(0).capitalize if club.try(:name).present?
       club_data["club_info"] = (club.name || "") + " " + (club.location || "") + " " + (club.state || "")
       club_data["competitors_count"] = club.try(:competitors).try(:size)
       club_image_url = club.attachment ? club.attachment.attachment.url(:small) : "#{AppConfig.image_url}" + "/images/noClubIcon.png"
    	club_data["club_image_url"] = club_image_url
    	club_array.push(club_data)
    end
  end

  def self.get_club_details(data_hash,offset_count,limit_count,side_bar,search_text)
    # Get club
    before_list = includes(:competitors,:attachment).select("id, name, location, state, competitors.id").order('name ASC').order('name ASC')
    clubs = before_list.limit(limit_count).offset(offset_count)
    if (side_bar == 'true')
      searched_clubs_list = clubs.where("name like '#{search_text}%'")
    elsif (side_bar == 'false' && search_text.present?)
      searched_clubs_list = clubs.where("name like '%#{search_text}%'")
    else
      searched_clubs_list = clubs
    end
    fhl_clubs = self.construct_club_details(data_hash,before_list,searched_clubs_list)
    # Sort by name of the club
    return fhl_clubs #.sort_by{|hsh| hsh[:user_details][:first_name]}
  end

  def self.construct_club_details(data_hash,before_list,clubs_set)
    fhl_club = []
    data_hash["side_bar"] = before_list.map{|clb| clb.name[0].capitalize}.uniq.sort
    hell = before_list.map{|clb| clb.name[0].capitalize}.uniq.sort
    clubs_set.each do |club|      
      if club.present?
        data_hash = club
        data_hash["club_name_index"] = club.try(:name).slice(0).capitalize if club.try(:name).present?
        data_hash["club_info"] = (club.name || "") + " " + (club.location || "") + " " + (club.state || "")
        data_hash["competitors_count"] = club.try(:competitors).try(:size)
        club_image_url = club.attachment ? club.attachment.attachment.url(:small) : "#{AppConfig.image_url}" + "/images/noClubIcon.png"
        data_hash["club_image_url"] = club_image_url        
      end
      fhl_club.push(data_hash)      
    end
    return fhl_club
  end

  private

  def create_attachment_if_not
    if ( !self.attachment.present? || !self.attachment.attachment_file_name.present? )
        if self.attachment.present?
          attach = self.attachment
          create_attachmnt(attach)
        else
          attach = self.build_attachment
          create_attachmnt(attach)
        end
    end
  end

  def create_attachmnt(attach)
    path = "#{Rails.root}/public/attachments/medium/missing.png"
    file = File.open(path)
    attach.attachment = file
    file.close
    attach.save!
  end
end