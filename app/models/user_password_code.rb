class UserPasswordCode < ActiveRecord::Base
  attr_accessible :security_code, :status
  belongs_to :user
end
