require 'rubygems'
require 'httparty'
class GroupMe < ActiveRecord::Base
  include HTTParty
  base_uri 'https://api.groupme.com'

  def self.create_group(access_token,method,event_name,share,image_url)
    return post("/v3/#{method}?token=#{access_token}", :query => {
      :name => event_name,
      :share => share,
      :image_url => image_url
      })
  end

  def self.add_members_to_group(access_token,method,evnt_cat_reg)
	first_name = evnt_cat_reg.user.try(:first_name).try(:capitalize)
	user_email = evnt_cat_reg.user.try(:email)
	group_id = evnt_cat_reg.event.try(:group_id)

    return post("/v3/#{method}/#{group_id}/members/add?token=#{access_token}", :query =>{
    "members" => [{
      :nickname => first_name,
      :email => user_email
      }]})
  end

end
