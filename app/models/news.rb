class News < ActiveRecord::Base
  has_one :attachment, :as => :attachable, :dependent => :destroy
  
  accepts_nested_attributes_for :attachment, :allow_destroy => true
  attr_accessible :blog_content, :header_text, :location, :news_datetime, :attachment_attributes
end
