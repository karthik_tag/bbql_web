class EventClub < ActiveRecord::Base
  attr_accessible :event_id, :club_id
  belongs_to :event
  belongs_to :club

  validates :club_id, :presence => :true
  validates :club_id, :uniqueness => {:scope => :event_id}

  def display_name # Dont easily change this. This is tied with URL of breakcrumb with event filters in js file
    # "#{self.try(:club).try(:name)}"
    "#{self.try(:event).try(:id)}"
  end
end
