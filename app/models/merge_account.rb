class MergeAccount < ActiveRecord::Base
  attr_accessible :actual_account_id, :duplicate_account_id
  belongs_to :actual_account, :class_name => 'User', :foreign_key => 'actual_account_id'
  belongs_to :duplicate_account, :class_name => 'User', :foreign_key => 'duplicate_account_id'
end