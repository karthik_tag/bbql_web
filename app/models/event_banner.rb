class EventBanner < ActiveRecord::Base
  has_attached_file :event_banner, :styles => { :medium => "350x350", :small => "180x180" }

  attr_accessor :_destroy
  attr_accessible :content, :event_banner, :event_banner_file_name, :event_banner_content_type, :event_banner_file_size, :event_banner_updated_at, :_destroy, :event_id

  validates :event_banner, :presence => true
  validates_attachment :event_banner, size: { less_than: 10.megabytes, message: "Must be less than 10MB" }, content_type: { content_type: ["image/png", "image/jpg", "image/jpeg", ""]}
  belongs_to :event
end
