class EventFanVotingResult < ActiveRecord::Base
  # attr_accessible :title, :body

  def self.track_category_fan_voting_results(event_id,category_id)
    event_category = EventCategory.where(:event_id=>event_id,:category_id=>category_id).includes(:visitor_votes).first
    avg_jud_res = EventCompetitorResult.where(:event_category_id=>event_category.try(:id), :avg_visitor_results => 0).order(:position).limit(3) # To fetch top 3 competiors based on position (rank)
    hash = self.do_calculations(avg_jud_res,event_category) if avg_jud_res.present? && event_category.present?
    return hash
  end

  def self.track_individual_scores(event_id,category_id,user_id)
    event_category = EventCategory.includes(:visitor_votes).where(:event_id=>event_id,:category_id=>category_id).try(:first)
    avg_jud_res = event_category.event_competitor_results.where(:avg_visitor_results => 0).order(:position).limit(3)
    first = second = third = nil
    avg_jud_res.each_with_index do |result,index|
      visi_votes_1 = event_category.visitor_votes.find_all{|vote| vote.position1==result.event_competitor_id and vote.user_id == user_id}
      visi_votes_2 = event_category.visitor_votes.find_all{|vote| vote.position2==result.event_competitor_id and vote.user_id == user_id}
      visi_votes_3 = event_category.visitor_votes.find_all{|vote| vote.position3==result.event_competitor_id and vote.user_id == user_id}
      if (index == 0)
        first, second, third = self.get_three_positions(visi_votes_1,visi_votes_2,visi_votes_3,10,7,3,first,second,third)
      elsif (index == 1)
        first, second, third = self.get_three_positions(visi_votes_1,visi_votes_2,visi_votes_3,6,9,5,first,second,third)
      else
        first, second, third = self.get_three_positions(visi_votes_1,visi_votes_2,visi_votes_3,2,4,8,first,second,third)
      end
    end
    return first, second, third
  end

  def self.get_three_positions(visi_votes_1,visi_votes_2,visi_votes_3,one,two,three,first,second,third)
    first = visi_votes_1.present? ? one : first
    second = visi_votes_2.present? ? two : second
    third = visi_votes_3.present? ? three : third
    return first, second, third
  end

  def self.do_calculations(avg_jud_res,event_category)
    arr = []
    hash = {}
    avg_jud_res.each_with_index do |result,index|
      visi_votes_1 = event_category.visitor_votes.find_all{|vote| vote.position1==result.event_competitor_id}.collect(&:user_id)
      visi_votes_2 = event_category.visitor_votes.find_all{|vote| vote.position2==result.event_competitor_id}.collect(&:user_id)
      visi_votes_3 = event_category.visitor_votes.find_all{|vote| vote.position3==result.event_competitor_id}.collect(&:user_id)
      visi_votes_4 = event_category.visitor_votes.find_all{|vote| (vote.position1 != result.event_competitor_id && vote.position2 != result.event_competitor_id && vote.position3 != result.event_competitor_id)}.collect(&:user_id) # To get the fans who have not event match a single judge pick and they will be awarded 0 points
      if (index == 0)
        self.push_hash_arr(arr,visi_votes_1,10)
        self.push_hash_arr(arr,visi_votes_2,7)
        self.push_hash_arr(arr,visi_votes_3,3)
      elsif (index == 1)
        self.push_hash_arr(arr,visi_votes_1,6)
        self.push_hash_arr(arr,visi_votes_2,9)
        self.push_hash_arr(arr,visi_votes_3,5)
      else
        self.push_hash_arr(arr,visi_votes_1,2)
        self.push_hash_arr(arr,visi_votes_2,4)
        self.push_hash_arr(arr,visi_votes_3,8)
      end
      self.push_hash_arr(arr,visi_votes_4,0)
    end    
    hash = arr.compact.inject{|key, val| key.merge( val ){|k, old_v, new_v| old_v + new_v}} if arr.present?
    hash = Hash[hash.sort_by{|k, v| v}.reverse] if hash.present?
    return hash
  end

  def self.push_hash_arr(arr,visi_votes,value)
    visi_votes.each do |visi|
      arr.push({visi=>value})
    end
  end

  def self.track_overall_fan_voting_results(event_id)
    event = Event.find(event_id)
    event_categories = EventCategory.where(:event_id=>event_id) unless event_id.nil?
    main_arr = []
    main_hash = {}
    event_categories.includes(:event_competitor_results,:visitor_votes=>:user).each do |event_category|
      avg_jud_res = event_category.event_competitor_results.where(:avg_visitor_results => 0).order(:position).limit(3) #EventCompetitorResult.where(:event_category_id=>event_category.try(:id), :avg_visitor_results => 0).order(:position).limit(3) # To fetch top 3 competiors based on position (rank)
      hash = self.do_calculations(avg_jud_res,event_category) if avg_jud_res.present?
      main_arr.push(hash)
    end if event_categories.present?
    main_hash = main_arr.compact.inject{|memo, el| memo.merge( el ){|k, old_v, new_v| old_v + new_v}} if main_arr.present?
    main_hash = Hash[main_hash.sort_by{|k, v| v}.reverse] if main_hash.present?
    return main_hash
  end

	def self.to_csv(main_hash)
		CSV.generate do |csv|
        csv << ["Rank", "First Name", "Last Name", "Email", "Fan Score"] ## Header values of CSV
        main_hash.each_with_index do |hash,index|
          user = User.find_by_id(hash[0])                
          csv << [index + 1, user.try(:first_name).try(:capitalize), user.try(:last_name).try(:capitalize), user.try(:email), hash[1]] ##Row values of CSV
        end
      end
  end
end