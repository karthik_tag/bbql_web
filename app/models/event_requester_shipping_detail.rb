class EventRequesterShippingDetail < ActiveRecord::Base
  # attr_accessible :title, :body
  belongs_to :event
  # belongs_to :user

  attr_accessor :edit_shipping_state

  # validates :shipping_contact_name, :shipping_address_line_1, :shipping_city, :shipping_country, :shipping_state, :shipping_zip, :presence => true
  attr_accessible :shipping_contact_name, :shipping_address_line_1, :shipping_address_line_2, :shipping_city, :shipping_country, :shipping_state, :shipping_zip, :edit_shipping_state
end
