class CategoryQuestion < ActiveRecord::Base
  
  attr_accessor :main_category_id
  attr_accessible :question, :category_id, :main_category_id
  belongs_to :category
  validates :question, :presence => true # Dont add validates presence for question_id, because it will not get added in category add/edit page
  validates :question, :uniqueness => {:scope => :category_id}

  def display_name
    "#{category.try(:name)}"
  end
end


