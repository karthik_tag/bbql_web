class EventJudgeCode < ActiveRecord::Base
  belongs_to :event
  belongs_to :judge_code
  belongs_to :user
  attr_accessor :generate_judge_code
  accepts_nested_attributes_for :judge_code, :allow_destroy => true
  attr_accessible :event_id, :judge_code_id, :user_id, :status, :generate_judge_code, :judge_code_attributes, :name

  validates :event_id, :presence => true
  validates :judge_code_id, :uniqueness => {:scope => :event_id}
  validates :user_id, :uniqueness => {:scope => :event_id}, :allow_blank =>true
  # after_save :judge_code_status_change
  after_save :update_user_judge_code_id

  # def display_name
  #   event_name = event.try(:name)
  #   dots = event_name.try(:size) >= 65 ? "..." : ""    
  #   "#{event_name.slice(0, 65)}#{dots}"
  # end

  def display_name # Dont easily change this. This is tied with URL of breakcrumb with event filters in js file
    # "#{self.try(:club).try(:name)}"
    "#{self.try(:event).try(:id)}"
  end

  private

  # def judge_code_status_change
  #   if self.user_id.present?
  #     self.judge_code.update_attributes(:status=>true)
  #   else
  #     self.judge_code.update_attributes(:status=>false)
  #   end
  # end

  def update_user_judge_code_id
    if self.user_id.present?
      self.user.update_attributes(:judge_code_id=>self.try(:judge_code).id)
    end
  end  

end
