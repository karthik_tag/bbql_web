class EventCompetitorResult < ActiveRecord::Base
  belongs_to :event_competitor
  belongs_to :event_category
  attr_accessor :first_name, :last_name, :user_id, :badge_id, :category_name
  attr_accessible :avg_judge_results, :avg_visitor_results, :event_competitor_id, :position, :event_category_id, :first_name, :last_name, :user_id, :badge_id, :category_name

  def self.test
  	event_category = EventCategory.find 1
      # event_category.update_attributes(:is_published => true)

      # Judge Votes Calculation goes here ====> starts#
        all_judge_votes = event_category.judge_votes
        # avg_rating = all_judge_votes.group(:event_competitor_id).average(:rating)
        all_event_competitors_judge = all_judge_votes.map(&:event_competitor_id).uniq
        all_event_competitors_judge.each do |event_competitor_id|
          avg_rating = all_judge_votes.where(:event_competitor_id => event_competitor_id).average(:rating)
          judge_result = event_category.event_competitor_results.build(:avg_judge_results=>avg_rating, :avg_visitor_results => 0, :event_competitor_id => event_competitor_id )
          judge_result.save
        end
        judge_results_arr = EventCompetitorResult.where(:event_competitor_id => all_event_competitors_judge, :avg_visitor_results=> 0).order("avg_judge_results DESC")
        judge_results_arr.each_with_index do |judge_results, index|
          judge_results.update_attributes(:position => index+1)
        end
      # Judge Votes Calculation goes here ====> ends#


      # Visitor Votes Calculation goes here ====> starts#
        all_vistor_votes = event_category.visitor_votes
        pos1_count = all_vistor_votes.group(:position1).count
        pos2_count = all_vistor_votes.group(:position2).count
        pos3_count = all_vistor_votes.group(:position3).count

        # Multiply the values by its points respectively #
        pos1_count = Hash[pos1_count.map { |k, v| [k, v * 3] }]
        pos2_count = Hash[pos2_count.map { |k, v| [k, v * 2] }]
        pos3_count = Hash[pos3_count.map { |k, v| [k, v * 1] }]

        counts_arr = [pos1_count, pos2_count, pos3_count] # Make an array of hashes
        counts_hash = counts_arr.inject{|key, val| key.merge( val ){|k, old_v, new_v| old_v + new_v}} #group hashes by keys and sum the values
        counts_hash.delete(0)
        counts_hash.each do |key,value|
          visitor_result = event_category.event_competitor_results.build(:avg_judge_results=> 0, :avg_visitor_results => value, :event_competitor_id => key )
          visitor_result.save
        end

        visitor_results_arr = EventCompetitorResult.where(:event_competitor_id => counts_hash.keys, :avg_judge_results=> 0).order("avg_visitor_results DESC")
        visitor_results_arr.each_with_index do |visitor_results, index|
          visitor_results.update_attributes(:position => index+1)
        end
      # Visitor Votes Calculation goes here ====> ends#
  end

  def self.dynamic_class_colorcode(position)
      if position == 1
        class_name = "class1"
      elsif position == 2
        class_name = "class2"
      elsif position == 3
        class_name = "class3"
      else
        class_name = "class4"
      end
      return class_name
  end

  def display_name
    user = self.try(:event_competitor).try(:competitor).try(:user)
    "#{user.try(:first_name)} #{user.try(:last_name)}"
  end

  def self.close_event_category(event_category)
    event_category.update_attributes(:is_published => true)
    # redirect_to admin_event_competitor_results_path(params) #old redirection
    # Redirect to the next un-published category of this event to publish with view results calculations
    cat_event = event_category.event

    # Find the 1st place winner
    top_winner = EventCompetitorResult.where(:event_category_id => event_category.try(:id), :avg_visitor_results => 0).order(:position).first.try(:event_competitor)
    top_winner_competitor = top_winner.try(:competitor)
    top_winner_user = top_winner_competitor.try(:user)

    visitors_event_competitor_results = event_category.event_competitor_results.where(:avg_judge_results=>0)
    unless visitors_event_competitor_results.present?
      EventCategory.close_fan_voting(event_category)
    end
    return cat_event
  end

end
