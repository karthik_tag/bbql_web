class LoginImage < ActiveRecord::Base
  attr_accessor :_destroy
  attr_accessible :login_image, :login_image_file_name, :login_image_content_type, :login_image_file_size, :login_image_updated_at, :_destroy, :first_name, :last_name, :nick_name, :city, :state, :charity_id, :user_id, :_destroy
  belongs_to :user
  belongs_to :charity
  has_attached_file :login_image, :styles => { :original => "5000x5000>", :medium => "350x350", :small => "180x180" } #,
                    # :url  => "/assets/attachments/:id/:style/:basename.:extension",
                    # :path => ":rails_root/public/assets/attachments/:id/:style/:basename.:extension"
  validates_attachment :login_image, content_type: { content_type: ["image/png", "image/jpg", "image/jpeg", ""] }
  validates :user_id, :login_image, presence: true

    def display_name
    "#{first_name.try(:capitalize)}"
  	end
end
