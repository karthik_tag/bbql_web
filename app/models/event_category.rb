class EventCategory < ActiveRecord::Base
  belongs_to :event
  belongs_to :category
  has_many :event_competitors, :dependent => :destroy
  has_many :visitor_votes, :dependent => :destroy
  has_many :judge_votes, :dependent => :destroy
  has_many :event_competitor_results, :dependent => :destroy
  has_many :multiple_ties, :dependent => :destroy
  attr_accessor :main_category_id, :regd_comps, :chckd_in_comps, :total_comps
  attr_accessible :is_published, :event_id, :main_category_id, :category_id, :regd_comps, :chckd_in_comps, :total_comps, :sort_order, :tie_position
  # acts_as_list column: :sort_order
  #scope :get_event_category, lambda{|id| where("id=?",id).includes(:event_competitors,:event_competitor_results, :judge_votes, :visitor_votes)}
  scope :get_event_category, lambda{|id| where("id=?",id).includes(:event_competitor_results, :judge_votes, :visitor_votes, :event_competitors=>[:competitor=>[:attachments,:club=>:attachment,:user=>:attachment]])}

  before_create :update_sort_order

  # validates_uniqueness_of :category_id, :scope =>[:event_id]
  validates :category_id, :presence => :true
  # validates :category_id, :uniqueness =>true, :scope => [:event_id] # ,:message => "Category already exist"
  validates :category_id, :uniqueness => {:scope => :event_id}
  # validates :main_category_id, :presence => :true , :if => Proc.new { |event_cat|  !event_cat.category_id}

  # after_create :map_event_competitors

  def update_sort_order
    max_sort_order = EventCategory.where(:event_id=>self.event_id).maximum(:sort_order)
    self.sort_order = max_sort_order.to_i + 1
  end

  def self.close_judge_voting(event_category)
    # event_category = EventCategory.find params[:id]
    all_judge_votes = event_category.judge_votes
    # avg_rating = all_judge_votes.group(:event_competitor_id).average(:rating)
    all_event_competitors_judge = all_judge_votes.map(&:event_competitor_id).uniq
    all_event_competitors_judge.each do |event_competitor_id|
    all_jud_votes = all_judge_votes.where(:event_competitor_id => event_competitor_id)

    #Olympic rules:: This block is for calculating the judges results with the Olympic rules (if the event is enabled as olypic rules)
    if all_jud_votes.count >= 5 && event_category.try(:event).try(:is_olympic_rules)
      max_a_j_v = all_jud_votes.order("rating DESC").first.try(:id)
      min_a_j_v = all_jud_votes.order("rating ASC").first.try(:id)
      all_jud_votes = all_jud_votes.where("id not in (?)",[max_a_j_v,min_a_j_v])
    end

    avg_rating = all_jud_votes.average(:rating)

    judge_result = event_category.event_competitor_results.build(:avg_judge_results=>avg_rating, :avg_visitor_results => 0, :event_competitor_id => event_competitor_id )
    judge_result.save
    end
    judge_results_arr = EventCompetitorResult.where(:event_competitor_id => all_event_competitors_judge, :avg_visitor_results=> 0).where("avg_judge_results != 0").order("avg_judge_results DESC")

    if judge_results_arr.size>0
      highest_judge_points = judge_results_arr[0].avg_judge_results
    end
    judge_position=1
    judge_results_arr.each_with_index do |judge_result, index|
    if highest_judge_points > judge_result.avg_judge_results
      judge_position = index+1
      highest_judge_points = judge_result.avg_judge_results
    end
    judge_result.update_attributes(:position => judge_position)
    end
  end

  def self.close_fan_voting(event_category)
    ### Visitor Votes Calculation goes here ====> starts#
      all_vistor_votes = event_category.visitor_votes
      pos1_count = all_vistor_votes.group(:position1).count
      pos2_count = all_vistor_votes.group(:position2).count
      pos3_count = all_vistor_votes.group(:position3).count

      # Multiply the values by its points respectively #
      pos1_count = Hash[pos1_count.map { |k, v| [k, v * 3] }]
      pos2_count = Hash[pos2_count.map { |k, v| [k, v * 2] }]
      pos3_count = Hash[pos3_count.map { |k, v| [k, v * 1] }]

      counts_arr = [pos1_count, pos2_count, pos3_count] # Make an array of hashes
      counts_hash = counts_arr.inject{|key, val| key.merge( val ){|k, old_v, new_v| old_v + new_v}} #group hashes by keys and sum the values
      counts_hash.delete(0)
      counts_hash.each do |key,value|
        visitor_result = event_category.event_competitor_results.build(:avg_judge_results=> 0, :avg_visitor_results => value, :event_competitor_id => key )
        visitor_result.save
      end
      visitor_results_arr = EventCompetitorResult.where(:event_competitor_id => counts_hash.keys, :avg_judge_results=> 0).where("avg_visitor_results != 0").order("avg_visitor_results DESC")
      if visitor_results_arr.size>0
        highest_visitor_points = visitor_results_arr[0].avg_visitor_results
      end
      visitor_position=1
      visitor_results_arr.each_with_index do |visitor_result, index|
        if highest_visitor_points > visitor_result.avg_visitor_results
          visitor_position = index+1
          highest_visitor_points = visitor_result.avg_visitor_results
        end
        visitor_result.update_attributes(:position => visitor_position)
      end
    ### Visitor Votes Calculation goes here ====> ends#
  end

  # Get all the competitors of an event
  def get_event_competitors
    self.event_competitors.includes(:competitor=>[:event_category_registrations]).order("event_category_registrations.number ASC")
  end

  # Get the competitors who have checked-in for an event
  def get_checked_in_event_competitors(old_event_competitor_ids)
    event_id = self.try(:event_id)
    category_id = self.try(:category_id)
    if old_event_competitor_ids.present?
      self.event_competitors.includes(:competitor=>[:event_category_registrations]).where("event_competitors.id not in (?) and event_category_registrations.is_checked_in = ? and event_category_registrations.event_id = ? and event_category_registrations.category_id =?",old_event_competitor_ids, true, event_id, category_id).order("event_category_registrations.number")
    else
      self.event_competitors.includes(:competitor=>[:event_category_registrations]).where("event_category_registrations.is_checked_in = ? and event_category_registrations.event_id = ? and event_category_registrations.category_id =?",true, event_id, category_id).order("event_category_registrations.number")
    end
  end

  def display_name # Dont easily change this. This is tied with URL of breakcrumb with event filters in js file
    # "#{self.try(:category).try(:name)}"
    "#{self.try(:event).try(:id)}"
  end

  def self.total_judges_voted(event_category)
    total_judges = event_category.try(:event).try(:event_judge_codes).count
    voted_judges = event_category.try(:judge_votes).pluck(:user_id).uniq.count
    return total_judges,voted_judges
  end

  # def map_event_competitors    
  #   category_competitors = Competitor.where("category_id=?",category_id)
  #   category_competitors.each do |competitor|
  #     EventCompetitor.create(:event_category_id=>id,:competitor_id=>competitor.id)
  #   end
  # end
end
