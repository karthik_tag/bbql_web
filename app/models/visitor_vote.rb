class VisitorVote < ActiveRecord::Base
  attr_accessible :position1, :position2, :position3, :event_id, :event_competitor_id, :event_category_id, :user_id
  belongs_to :event_category
  belongs_to :user
end
