class AdminPushNotification < ActiveRecord::Base
  attr_accessible :content, :status

  validates :content, :presence => true

  # One time code to be run when the app server starts, for staging
  def self.one_time_execution_for_push_notification_on_staging	
	app = Rpush::Apns::App.new
	app.name = "ios_app"
	#openssl pkcs12 -clcerts -in cert.p12 -out <environment>.pem
	app.certificate = File.read(Rails.root + "config/development.pem")
	app.environment = "sandbox" # APNs environment.
	app.password = "123456"
	app.connections = 1
	app.save!
  end

  # One time code to be run when the app server starts, for Production
  def self.one_time_execution_for_push_notification_on_production	
	app = Rpush::Apns::App.new
	app.name = "ios_app"
	#openssl pkcs12 -clcerts -in cert.p12 -out <environment>.pem
	app.certificate = File.read(Rails.root + "config/production.pem")
	app.environment = "production" # APNs environment.
	app.password = ""
	app.connections = 1
	app.save!
  end


end
