class EventCompetitor < ActiveRecord::Base
  belongs_to :event_category
	belongs_to :competitor
	has_many :event_competitor_result, :dependent => :destroy
  belongs_to :event_category_registration
	attr_accessor :event, :category, :first_name, :last_name
  attr_accessible :position, :status, :event_category_id, :competitor_id, :event, :category, :points, :first_name, :last_name, :event_category_registration_id
  # validates :competitor_id, :uniqueness => {:scope => :event_category_id}
  validates :points, :presence => true
  validates :points,  numericality: {greater_than_or_equal_to: 1}

  before_destroy :delete_event_cat_registration

  def self.get_card_details(data_hash,offset_count,limit_count,side_bar,search_text)
    # fhl_contestent = []
    # Get contestent
    #contestents = EventCompetitor.includes(:competitor).group_by(&:competitor_id)
    # contestents = Competitor.where("nick_name != ''").includes(:charity, :category, :user=>:attachment) # Before adding offset and limit
    all_competitors_ids = User.where(:user_type_id=>2)
    before_list = Competitor.where("nick_name != '' and user_id in (?)",all_competitors_ids.pluck(:id)).includes(:charity, :category, :user=>:attachment).order("nick_name ASC") #.joins(:charity, :category, :user=>:attachment)
    contestents = before_list.limit(limit_count).offset(offset_count)
    if (side_bar == 'true')
      searched_cards_list = contestents.where("nick_name like '#{search_text}%'")
    elsif (side_bar == 'false' && search_text.present?)      
      splitted_names = search_text.split(" ")
      if (splitted_names.count <= 1)
        searched_cards_list = contestents.where("nick_name like '%#{search_text}%' or users.first_name like '%#{search_text}%' or users.last_name like '%#{search_text}%'")
      else
        searched_cards_list = contestents.where("nick_name like '%#{search_text}%' or (users.first_name like '%#{splitted_names[1]}%' and users.last_name like '%#{splitted_names[0]}%') or (users.first_name like '%#{splitted_names[0]}%' and users.last_name like '%#{splitted_names[1]}%')")
      end
    else
      searched_cards_list = contestents
    end
    fhl_contestent = self.construct_competitor_details(data_hash,before_list,searched_cards_list)
    # Sort by first name of contestent
    return fhl_contestent.sort_by{|hsh| hsh[:user_details][:first_name]}
  end

  def self.construct_competitor_details(data_hash,before_list,contestents)
    fhl_contestent = []
    data_hash["side_bar"] = before_list.map{|comp| comp.nick_name[0].capitalize}.uniq.sort
    contestents.where("user_id IS NOT NULL").each do |competitor|
      #competitor = event_contestents.first.competitor#Competitor.where(:id => contestent).first
      if competitor.present?
        data_hash = competitor
        data_hash["category_name"] = competitor.category.try(:name)
        data_hash["charity_name"] = competitor.charity.try(:name)
        #data_hash["club_name"] = competitor.club.try(:name)
        #data_hash["club_url"] = competitor.club.try(:attachment) ? "#{AppConfig.image_url}" +competitor.club.try(:attachment).try(:attachment).try(:url) : ''
        data_hash["competitor_image_url"] = competitor.user.try(:attachment) ? competitor.user.try(:attachment).try(:attachment).url(:small) : "#{AppConfig.image_url}" + "/attachments/small/beaker-card.jpg"
        data_hash["nick_name_index"] = competitor.try(:nick_name).slice(0).capitalize if competitor.nick_name.present?
        data_hash["complete_name"] = competitor.try(:user).present? ? competitor.try(:user).first_name.try(:capitalize) + " " + competitor.try(:user).last_name.try(:capitalize) + " " + competitor.try(:nick_name).try(:capitalize) : ""
        data_hash["user_details"] = competitor.try(:user)
      end
      fhl_contestent.push(data_hash)
    end
    return fhl_contestent
  end

  def self.profile_details(data_hash,user,competitor)
    data_hash = user
    data_hash["full_name"] = "#{user.try(:first_name)} #{user.try(:last_name)}"
    data_hash["nick_name"] = competitor.try(:nick_name)
    data_hash["category"] = competitor.try(:category).try(:name)
    data_hash["club"] = competitor.try(:club).try(:name)
    data_hash["non_profit"] = competitor.try(:charity).try(:name)
    data_hash["city"] = competitor.try(:city)
    data_hash["state"] = competitor.try(:state)
    data_hash["image_url"] = user.attachment.attachment.url
    return data_hash
  end

  def self.history_details(data_hash,user,competitor)
    data_hash = competitor
    history_details_array = []
    # event_competitors = competitor.try(:event_competitors).joins(:event_category=>[:event]).where("events.status = ? and events.from_date > ? and events.from_date < ?","Completed",Time.now.beginning_of_year, Time.now.end_of_year)
    event_competitors = competitor.try(:event_competitors).joins(:event_category=>[:event]).where("events.status = ?","Completed")
    # Added in BBQL starts here
      total_evnts_attended = event_competitors.joins(:event_category).group("event_categories.event_id").length
      data_hash["events_attended"] = total_evnts_attended
    # Added in BBQL ends here
    # data_hash["events_attended"] = event_competitors.count #hided for BBQL. It was the code in FHL.
    # results = EventCompetitorResult.joins(:event_category=>[:event]).where("events.status = ? and events.from_date > ? and events.from_date < ? and event_categories.is_published = ? and event_competitor_results.event_competitor_id in (?) and event_competitor_results.avg_visitor_results = ?","Completed",Time.now.beginning_of_year, Time.now.end_of_year,true,event_competitors.present? ? event_competitors.pluck(:id) : [],0)
    results = EventCompetitorResult.joins(:event_category=>[:event]).where("events.status = ? and event_categories.is_published = ? and event_competitor_results.event_competitor_id in (?) and event_competitor_results.avg_visitor_results = ?","Completed",true,event_competitors.present? ? event_competitors.pluck(:id) : [],0).order("events.from_date DESC")
    result_positions = results.present? ? results.pluck(:position) : []
    data_hash["wins"] = result_positions.count(1)
    data_hash["places"] = result_positions.count(2)
    data_hash["shows"] = result_positions.count(3)
    data_hash["event_year"] = "2015 - 2016" #Date.today.year

    results.includes(:event_category=>[:event,:category]).each do |result|
      data = {}
      #Newly Added to find tie and tie counts
      event_category = result.try(:event_category)
      total_results = EventCompetitorResult.where(:event_category_id=>event_category.try(:id),:avg_visitor_results=>0)
      tie_count = total_results.present? ? total_results.pluck(:position).count(result.try(:position)) : 0
      is_tie = (tie_count <= 1) ? false : true
      #Newly added ends here
      data["place"] = result.try(:position).eql?(1) ? "1st #{self.form_rank_string(total_results.count,is_tie,tie_count)}" : result.try(:position).eql?(2) ? "2nd #{self.form_rank_string(total_results.count,is_tie,tie_count)}" : result.try(:position).eql?(3) ? "3rd #{self.form_rank_string(total_results.count,is_tie,tie_count)}" : result.try(:position).to_i.eql?(0) ? "#{total_results.count+1}th #{self.form_rank_string(total_results.count+1,is_tie,tie_count)}" : "#{self.ordinal_suffix_of(result.try(:position))} #{self.form_rank_string(total_results.count,is_tie,tie_count)}"
      event = result.try(:event_category).try(:event)
      category = result.try(:event_category).try(:category)
      data["event"] =  event.try(:name)
      data["event_date"] =  event.meridian.eql?("PM") ? (event.try(:from_date)+12.hours).strftime("%Y-%m-%d %H:%M:%S") : event.try(:from_date).strftime("%Y-%m-%d %H:%M:%S")
      data["category"] =  category.try(:name)
      evnt_cat_reg = EventCategoryRegistration.find_by_event_id_and_category_id_and_user_id(event.try(:id), category.try(:id), user.try(:id))
      data["image_url"] =  (evnt_cat_reg.present? && evnt_cat_reg.event_photo_file_name.present?) ? "#{AppConfig.s3_url}/attachments/#{evnt_cat_reg.id}/original/"+evnt_cat_reg.event_photo_file_name : "#{AppConfig.image_url}" + "/attachments/medium/missing.png"
      data["charting_data"] = self.charting_data(result.try(:event_competitor_id),result.try(:event_category_id))
      data["avg_judge_results"] = result.try(:avg_judge_results)

      #Bar chart data
      data["comp_ranks"] = total_results.pluck(:position).sort
      data["user_rank"] = result.try(:position)

      history_details_array.push(data)
    end

    # This is the block to find the competitors, who have participated in an event but not getting any votes from the judges
    left_evnt_comp_ids =  (event_competitors.present? ? event_competitors.pluck(:id) : []) - (results.present? ? results.pluck(:event_competitor_id) : [])
    left_evnt_comps = EventCompetitor.where(:id=>left_evnt_comp_ids)
    left_evnt_comps.includes(:event_competitor_result, :event_category=>[:event, :category]).each do |event_comp|
      left_data = {}
      # left_data["place"] = "4TH PLACE"
      position = event_comp.try(:event_competitor_result).where(:avg_visitor_results=>0).first.try(:position).to_i
      event_category = event_comp.try(:event_category)
      total_results = EventCompetitorResult.where(:event_category_id=>event_category.try(:id),:avg_visitor_results=>0)
      tie_count = total_results.present? ? total_results.pluck(:position).count(position) : 0
      is_tie = (tie_count <= 1) ? false : true
      left_data["place"] = position.eql?(0) ? "#{total_results.count+1}th #{self.form_rank_string(total_results.count+1,is_tie,tie_count)}" : self.ordinal_suffix_of(position) + " #{self.form_rank_string(total_results.count,is_tie,tie_count)}"      
      event_cat_reg = EventCategoryRegistration.find_by_event_id_and_category_id_and_user_id(event_category.try(:event_id), event_category.try(:category_id), user.try(:id))
      left_data["event"] =  event_category.try(:event).try(:name)
      left_data["event_date"] =  event_category.try(:event).try(:meridian).eql?("PM") ? (event_category.try(:event).try(:from_date)+12.hours).strftime("%Y-%m-%d %H:%M:%S") : event_category.try(:event).try(:from_date).strftime("%Y-%m-%d %H:%M:%S")
      left_data["category"] =  event_category.try(:category).try(:name)
      left_data["image_url"] =  (event_cat_reg.present? && event_cat_reg.event_photo_file_name.present?) ? "#{AppConfig.s3_url}/attachments/#{event_cat_reg.id}/original/"+event_cat_reg.event_photo_file_name : "#{AppConfig.image_url}" + "/attachments/medium/missing.png"
      # judge_votes = self.charting_data(event_comp.try(:id),event_category.try(:id))
      left_data["charting_data"] = [] #judge_votes
      # avg = judge_votes.map(&:rating).sum / judge_votes.map(&:rating).size.to_f
      left_data["avg_judge_results"] = "" #(judge_votes.map(&:rating).sum / judge_votes.map(&:rating).size.to_f)
      history_details_array.push(left_data)
    end
    
    data_hash["images"] = history_details_array
    return data_hash
  end

  def self.charting_data(event_competitor_id,event_category_id)
    charting_data = []
    judge_votes = JudgeVote.where(:event_competitor_id=>event_competitor_id,:event_category_id=>event_category_id)
    judge_votes = judge_votes.group(:user_id)  # Added in BBQ.
    judge_votes.includes(:user).each_with_index do |judge_vote,index|
      left_data = {}
      left_data["label"] = "Judge #{index+1}"
      #left_data["value"] = judge_vote.try(:rating) # Old FHL code. Changed in BBQ.
      left_data["value"] = judge_votes.average(:rating)[judge_vote.user_id] # Added in BBQ.
      charting_data.push(left_data)
    end
    return charting_data    
  end

  def self.club_history_details(data_hash,event_competitors)
    history_details_array = []
    results = EventCompetitorResult.joins(:event_category=>[:event]).where("events.status = ? and event_categories.is_published = ? and event_competitor_results.event_competitor_id in (?) and event_competitor_results.avg_visitor_results = ?","Completed",true,event_competitors.present? ? event_competitors.pluck(:id) : [],0).order("events.from_date DESC")
    
    results.includes(:event_competitor=>[:competitor=>:user],:event_category=>[:event,:category]).each do |result|
      data = {}
      #Newly Added to find tie and tie counts
        event_category = result.try(:event_category)
        total_results = EventCompetitorResult.where(:event_category_id=>event_category.try(:id),:avg_visitor_results=>0)
        tie_count = total_results.present? ? total_results.pluck(:position).count(result.try(:position)) : 0
        is_tie = (tie_count <= 1) ? false : true
      #Newly added ends here
      user = result.try(:event_competitor).try(:competitor).try(:user)
      data["user_name"] = "#{user.try(:first_name)} #{user.try(:last_name)}"
      data["place"] = result.try(:position).eql?(1) ? "1st #{self.form_rank_string(total_results.count,is_tie,tie_count)}" : result.try(:position).eql?(2) ? "2nd #{self.form_rank_string(total_results.count,is_tie,tie_count)}" : result.try(:position).eql?(3) ? "3rd #{self.form_rank_string(total_results.count,is_tie,tie_count)}" : result.try(:position).to_i.eql?(0) ? "#{total_results.count+1}th #{self.form_rank_string(total_results.count+1,is_tie,tie_count)}" : "#{self.ordinal_suffix_of(result.try(:position))} #{self.form_rank_string(total_results.count,is_tie,tie_count)}"
      event = result.try(:event_category).try(:event)
      category = result.try(:event_category).try(:category)
      data["event"] =  event.try(:name)
      data["event_date"] =  event.meridian.eql?("PM") ? (event.try(:from_date)+12.hours).strftime("%Y-%m-%d %H:%M:%S") : event.try(:from_date).strftime("%Y-%m-%d %H:%M:%S")
      data["category"] =  category.try(:name)
      evnt_cat_reg = EventCategoryRegistration.find_by_event_id_and_category_id_and_user_id(event.try(:id), category.try(:id), user.try(:id))
      data["image_url"] =  (evnt_cat_reg.present? && evnt_cat_reg.event_photo_file_name.present?) ? "#{AppConfig.s3_url}/attachments/#{evnt_cat_reg.id}/original/"+evnt_cat_reg.event_photo_file_name : "#{AppConfig.image_url}" + "/attachments/medium/missing.png"
      history_details_array.push(data)
    end
    # This is the block to find the competitors, who have participated in an event but not getting any votes from the judges
    left_evnt_comp_ids =  (event_competitors.present? ? event_competitors.pluck(:id) : []) - (results.present? ? results.pluck(:event_competitor_id) : [])
    left_evnt_comps = EventCompetitor.where(:id=>left_evnt_comp_ids)
    left_evnt_comps.includes(:event_competitor_result, :event_category=>[:event, :category]).each do |event_comp|
      left_data = {}
      # left_data["place"] = "4TH PLACE"
      position = event_comp.try(:event_competitor_result).where(:avg_visitor_results=>0).first.try(:position).to_i
      event_category = event_comp.try(:event_category)
      total_results = EventCompetitorResult.where(:event_category_id=>event_category.try(:id),:avg_visitor_results=>0)
      tie_count = total_results.present? ? total_results.pluck(:position).count(position) : 0
      is_tie = (tie_count <= 1) ? false : true
      user = event_comp.try(:competitor).try(:user)
      left_data["user_name"] = "#{user.try(:first_name)} #{user.try(:last_name)}"
      left_data["place"] = position.eql?(0) ? "#{total_results.count+1}th #{self.form_rank_string(total_results.count+1,is_tie,tie_count)}" : self.ordinal_suffix_of(position) + " #{self.form_rank_string(total_results.count,is_tie,tie_count)}"      
      event_cat_reg = EventCategoryRegistration.find_by_event_id_and_category_id_and_user_id(event_category.try(:event_id), event_category.try(:category_id), user.try(:id))
      left_data["event"] =  event_category.try(:event).try(:name)
      left_data["event_date"] =  event_category.try(:event).try(:meridian).eql?("PM") ? (event_category.try(:event).try(:from_date)+12.hours).strftime("%Y-%m-%d %H:%M:%S") : event_category.try(:event).try(:from_date).strftime("%Y-%m-%d %H:%M:%S")
      left_data["category"] =  event_category.try(:category).try(:name)
      left_data["image_url"] =  (event_cat_reg.present? && event_cat_reg.event_photo_file_name.present?) ? "#{AppConfig.s3_url}/attachments/#{event_cat_reg.id}/original/"+event_cat_reg.event_photo_file_name : "#{AppConfig.image_url}" + "/attachments/medium/missing.png"
      history_details_array.push(left_data)
    end
    
    data_hash["images"] = history_details_array
    return data_hash
  end

  def self.form_rank_string(total_positions,is_tie,tie_count)
    tie = is_tie ? "(T#{tie_count})" : nil
    tie.present? ? "Place #{tie} of #{total_positions}" : "Place of #{total_positions}"
  end

  def self.ordinal_suffix_of(i)
    j = i % 10
    k = i % 100
    return "#{i}st" if (j == 1 && k != 11)
    return "#{i}nd" if (j == 2 && k != 12)
    return "#{i}rd" if (j == 3 && k != 13)
    return "#{i}th"
  end

  def self.gallery_details(data_hash,user,competitor)
    arr = []
    attachments = competitor.try(:attachments).present? ? competitor.try(:attachments).select("id,attachment_file_name,attachment_updated_at").order("attachment_updated_at DESC") : []
    attachments.each do |attachment|
      data_hash = attachment
      data_hash["image_url"] = attachment.try(:attachment).url(:original)
      arr.push(data_hash)
    end
    return arr
  end

# Need to enable the below block to break the ties
  def self.fetch_tied_competitors(event_category_id)
    event_category = EventCategory.find_by_id(event_category_id)
    ties_arr = []
    is_tie = false
    ordered_tie_recs = event_category.try(:event_competitor_results).includes(:event_competitor=>[:competitor=>[:user=>:attachment]]).where("avg_visitor_results = 0 and position < 4").order("avg_judge_results DESC")
    if ordered_tie_recs.present?
      first_position_ties = ordered_tie_recs.find_all{|item| item.position==1}
      second_position_ties = ordered_tie_recs.find_all{|item| item.position==2}
      third_position_ties = ordered_tie_recs.find_all{|item| item.position==3}
      if first_position_ties.length>1
        is_tie=true
        ties_arr = first_position_ties
      elsif second_position_ties.length>1
        is_tie=true
        ties_arr = second_position_ties
      elsif third_position_ties.length>1
        is_tie=true
        ties_arr = third_position_ties
      end
    end
    return is_tie,ties_arr
  end

  def self.fetch_round_1_tied_competitors(event_category_id,tie_position)
    event_category = EventCategory.find_by_id(event_category_id)
    ties_arr = []
    is_tie = false
    # round_1_completed = false
    ordered_tie_recs = event_category.try(:multiple_ties).where(:position=>tie_position).includes(:event_competitor=>[:competitor=>[:user=>:attachment]]).group_by(&:rank).sort_by{|k,v| k}.first(3)

    if ordered_tie_recs.present?
      first_position_ties = ordered_tie_recs[0].present? ? ordered_tie_recs[0][1] : []
      second_position_ties = ordered_tie_recs[1].present? ? ordered_tie_recs[1][1] : []
      third_position_ties = ordered_tie_recs[2].present? ? ordered_tie_recs[2][1] : []

      #update the evt_comp_results table finally if there are no ties
      self.update_result_table_after_round_1(first_position_ties,tie_position,0.03) if first_position_ties.present?
      self.update_result_table_after_round_1(second_position_ties,tie_position+1,0.02) if second_position_ties.present?
      self.update_result_table_after_round_1(third_position_ties,tie_position+2,0.01) if third_position_ties.present?

      is_tie, ties_arr = self.fetch_tied_competitors(event_category_id)

    #   if first_position_ties.count>1
    #     is_tie=true
    #     tie_position = 1
    #     round_1_completed = true
    #     ties_arr = first_position_ties
    #   elsif second_position_ties.count>1
    #     is_tie=true
    #     tie_position = 2
    #     round_1_completed = true
    #     ties_arr = second_position_ties
    #   elsif third_position_ties.count>1
    #     is_tie=true
    #     tie_position = 3
    #     round_1_completed = true
    #     ties_arr = third_position_ties
    #   end
    end
    # final_arr = []
    # ties_arr.each do |tie_arr|
    #   final_arr << EventCompetitorResult.find_by_event_competitor_id_and_event_category_id(tie_arr.try(:event_competitor_id),tie_arr.try(:event_category_id))
    # end
    return is_tie,ties_arr
  end

  def self.update_result_table_after_round_1(position_ties,position,add_score)
      if (position_ties.count == 1)
        object = position_ties.first
        evt_comp_res = EventCompetitorResult.find_by_event_competitor_id_and_event_category_id(object.try(:event_competitor_id),object.try(:event_category_id))
        evt_comp_res.update_attributes(:position=>position,:avg_judge_results=>evt_comp_res.try(:avg_judge_results)+add_score)
      end

      # if (position_ties.count < 1)
      #   position_ties.each do |pos_tie|
      #     # multiple_tie_obj = MultipleTy.find_by_event_competitor_id_and_event_category_id_and_position(object.try(:event_competitor_id),object.try(:event_category_id),position)
      #     pos_tie.update_attributes(:rank=>position,:avg_judge_results=>evt_comp_res.try(:avg_judge_results)+add_score)
      #   end
      # end
  end

  def self.check_all_registered_members_checkd_in(event)
    reg_comp = EventCategoryRegistration.where(:event_id=>event.try(:id), :is_checked_in=>false).includes(:user,:category).order("users.last_name ASC, users.first_name ASC")
    registered_competitors = reg_comp.where("last_name != ''") + reg_comp.where("last_name = ''")
  end

	def display_name # Dont easily change this. This is tied with URL of breakcrumb with event filters in js file
		# "#{competitor.try(:nick_name)}"
    "<a>#{self.try(:event_category).try(:event).try(:name)}</a>".html_safe
	end

  [:contains, :equals, :starts_with, :ends_with].each do |condition_type|
        search_methods "competitor_filter_#{condition_type}"
        search_methods "event_filter_#{condition_type}"
        search_methods "category_filter_#{condition_type}"
        search_methods "first_name_filter_#{condition_type}"
        search_methods "last_name_filter_#{condition_type}"

        scope "competitor_filter_#{condition_type}", lambda { |q|
          joins(:competitor).merge Competitor.search("nick_name_#{condition_type}" => q).relation
        }
        scope "event_filter_#{condition_type}", lambda { |q|
          joins(:event_category=>:event).merge Event.search("name_#{condition_type}" => q).relation
        }

        scope "category_filter_#{condition_type}", lambda { |q|
          joins(:event_category=>:category).merge Category.search("name_#{condition_type}" => q).relation
        }

        scope "first_name_filter_#{condition_type}", lambda { |q|
          joins(:competitor=>:user).merge User.search("first_name_#{condition_type}" => q).relation
        }

        scope "last_name_filter_#{condition_type}", lambda { |q|
          joins(:competitor=>:user).merge User.search("last_name_#{condition_type}" => q).relation
        }
  end

  private

  def delete_event_cat_registration
    self.event_category_registration.delete if self.event_category_registration.present?
  end
end