class AdminUser < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, 
         :recoverable, :rememberable, :trackable, :validatable
  attr_accessor :email_text, :quantity
  # Setup accessible (or protected) attributes for your model
  attr_accessible :email, :password, :password_confirmation, :remember_me, :is_checkin_manager, :encrypted_password, :event_ids, :email_text, :quantity
  # belongs_to :event
  serialize :event_ids, Array
  # attr_accessible :title, :body

# Enable the below code if client asks separate tab menu for checkin managers and create new login page for the checkinmanagers.

 #  before_save :create_or_update_or_delete_checkin_manager

 #  def create_or_update_or_delete_checkin_manager
 #  	admin_user = AdminUser.find_by_id(self.id)
	# checkin_manager = CheckinManager.find_by_email(admin_user.email) if admin_user.present?
	# if checkin_manager.present? && self.is_checkin_manager
	# 	if self.password.present?
 #  			checkin_manager.update_attributes(:email=>self.email,:password=>self.password,:password_confirmation=>self.password_confirmation)
 #  		else
	# 		checkin_manager.update_attributes(:email=>self.email,:encrypted_password=>admin_user.encrypted_password)
 #  		end
 #  	elsif !checkin_manager.present? && self.is_checkin_manager
 #  		if self.password.present?
 #  			CheckinManager.create(:email=>self.email,:password=>self.password,:password_confirmation=>self.password_confirmation)
 #  		else
	# 		CheckinManager.create(:email=>self.email,:password=>"password",:password_confirmation=>"password",:encrypted_password=>admin_user.encrypted_password)			
 #  		end
 #  	elsif checkin_manager.present? && !self.is_checkin_manager
 #  		checkin_manager.destroy
 #  	end
 #  end

end
