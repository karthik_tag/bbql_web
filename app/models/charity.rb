class Charity < ActiveRecord::Base
  has_many :competitors
  has_one :attachment, :as => :attachable, :dependent => :destroy
  accepts_nested_attributes_for :attachment, :allow_destroy => true
  attr_accessible :link, :name, :contact_name, :contact_phone_number, :abn_number, :attachment_attributes
  validates :name, :presence => true
  validates :name, :uniqueness => true
end
