class JudgeCode < ActiveRecord::Base
  attr_accessor :generate_judge_code
  attr_accessible :name, :status, :generate_judge_code
  has_many :event_judge_codes, :dependent => :destroy
  validates :name, :presence => true
  # validates :name, :uniqueness => true
  scope :get_status, lambda {|code| where("name = ?", code)  }	  
end
