class EventFanVoting < ActiveRecord::Base
  attr_accessor :event_date
  attr_accessible :event_id, :event_date
  belongs_to :event
end
