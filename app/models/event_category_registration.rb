class EventCategoryRegistration < ActiveRecord::Base
  # attr_accessible :title, :body
  belongs_to :event_registration
  belongs_to :event
  belongs_to :category
  belongs_to :user
  belongs_to :competitor
  belongs_to :parent, :class_name => 'EventCategoryRegistration'
  has_one :children, :class_name => 'EventCategoryRegistration', :foreign_key => 'parent_id'
  has_one :event_competitor, :dependent => :destroy

  has_attached_file :event_photo, :styles => { :original => "5000x5000>", :medium => "350x350", :small => "180x180" } #,
                    # :url  => "/assets/attachments/:id/:style/:basename.:extension",
                    # :path => ":rails_root/public/assets/attachments/:id/:style/:basename.:extension"

  attr_accessor :first_name, :last_name, :email_filter, :_destroy, :old_number
  attr_accessible :first_name, :last_name, :event_registration_id, :event_id, :category_id, :user_id, :event_photo, :event_photo_file_name, :event_photo_content_type, :event_photo_file_size, :event_photo_updated_at, :email_filter, :number, :is_checked_in, :_destroy, :old_number, :parent_id, :is_removed, :competitor_id

  validates_attachment :event_photo, content_type: { content_type: ["image/png", "image/jpg", "image/jpeg", ""] }
  do_not_validate_attachment_file_type :event_photo, if: Proc.new { |obj| obj.parent_id.present? }
  # validates :event_id, :category_id, :number, :presence => :true

  [:contains, :equals, :starts_with, :ends_with].each do |condition_type|
    ["email","first_name","last_name"].each do |filter_string|
      search_methods "#{filter_string}_filter_#{condition_type}"

      scope "#{filter_string}_filter_#{condition_type}", lambda { |q|
        joins(:user).merge User.search("#{filter_string}_#{condition_type}" => q).relation
      }
    end
  end # This is the custom filter for user first name, last name, email associated with this class with string field


  def display_name
    event_name = event.try(:name)
    dots = event_name.try(:size) >= 25 ? "..." : ""
    "#{event_name.slice(0, 25)}#{dots}"
  end

  def self.find_checkedin_val_or_not(event)
    nt_ev_ca_reg = EventCategoryRegistration.where(:event_id=>event, :is_checked_in=>0).length
    ch_ev_ca_reg = EventCategoryRegistration.where(:event_id=>event, :is_checked_in=>1).length
    val = (ch_ev_ca_reg > 0 && nt_ev_ca_reg == 0) ? 1 : 0
    return val
  end
end
