class Event < ActiveRecord::Base
  include ActiveModel::Dirty
  has_many :attachments, :as => :attachable, :dependent => :destroy
  has_many :categories, :through => :event_categories
  has_many :event_categories, :dependent => :destroy
  has_many :users, :through => :event_judges
  has_many :competitors
  has_many :event_judges, :dependent => :destroy
  has_many :clubs, :through => :event_clubs
  has_many :event_clubs, :dependent => :destroy
  has_many :event_judge_codes, :dependent => :destroy
  has_many :event_mcs, :dependent => :destroy
  has_many :event_sponsors, :dependent => :destroy
  has_one :event_registration, :dependent => :destroy
  has_one :event_fan_voting, :dependent => :destroy
  has_one :event_requester_shipping_detail, :dependent => :destroy
  has_one :event_banner, :dependent => :destroy
  belongs_to :club
  belongs_to :user

  geocoded_by :address, :latitude  => :latitude, :longitude => :longitude # ActiveRecord
  after_validation :geocode
  after_create :create_event_registration, :create_event_fan_voting, :groupme_group_creation, :create_event_registrations_from_other_event
  before_save :update_time_zone_if_empty
  after_save :update_event_clubs, :create_event_image_if_not
  
  has_attached_file :image, :styles => { :medium => "350x350", :small => "180x180" } #,
                    # :url  => "/assets/attachments/:id/:style/:basename.:extension",
                    # :path => ":rails_root/public/assets/attachments/:id/:style/:basename.:extension"
  
  accepts_nested_attributes_for :attachments, :allow_destroy => true
  accepts_nested_attributes_for :event_categories, :allow_destroy => true
  accepts_nested_attributes_for :event_clubs, :allow_destroy => true
  accepts_nested_attributes_for :event_requester_shipping_detail, :allow_destroy => true
  attr_accessor :_destroy, :edit_state, :event, :total_comps
  attr_accessible :name, :from_date, :status, :address_line_1, :address_line_2, :city, :state, 
                  :country, :attachments_attributes, :image, :image_file_name, :image_content_type,
                  :image_file_size, :image_updated_at, :_destroy, :event_categories_attributes,
                  :event_clubs_attributes, :meridian, :paticipate_url, :non_profit, :group_id,
                  :edit_state, :event_id, :user_ids, :event, :user_id, :is_olympic_rules,
                  :is_self_registartion_on,:requester_mobile,:venue_name,:no_of_categories, :club_id,
                  :time_zone, :no_of_judges, :no_of_expected_competitors, :nonprofit_cause,
                  :event_requester_shipping_detail, :event_requester_shipping_detail_attributes,
                  :total_comps, :is_event_banner, :enable_notification_on_launch
  serialize :user_ids, Array
  just_define_datetime_picker :from_date, :add_to_attr_accessible => true

  validates :name, :from_date, :address_line_1, :city, :state, :country, :presence => true
  validates_attachment :image, size: { less_than: 10.megabytes, message: "Must be less than 10MB" }, content_type: { content_type: ["image/png", "image/jpg", "image/jpeg", ""]}
  validates :from_date, :uniqueness => true
  validates_time :from_date, :between => ['0:00am', '12:00pm']
  validates :non_profit,:no_of_categories,:no_of_judges,:no_of_expected_competitors,:requester_mobile, :numericality => true
  
  scope :get_event, lambda{|id| where("id=?",id).includes(:attachments, :event_categories, :categories, :clubs=>:attachment)}
  
  # Get most recent upcoming events
  def self.get_upcoming_events
    # self.event_get(["Upcoming"],"ASC")
    self.event_get_all_years(["Upcoming"],"ASC")
  end

  # Get last events
  def self.get_last_event
    self.event_get(["Completed"],"DESC")
  end

  # Get live events
  def self.get_live_event
    self.event_get(["OnGoing"],"DESC")
  end

  # Get live event
  def self.get_live_events_id
    self.event_get(["OnGoing"],"DESC").pluck(:id)
  end

  # Get Upcoming event
  def self.get_upcoming_events_id
    self.event_get(["Upcoming"],"ASC").pluck(:id)
  end

  def self.event_get(status,order)
    # where("from_date > ? AND from_date < ? and status in (?)", Time.now.beginning_of_year, Time.now.end_of_year,status).order("from_date #{order}")
    where("status in (?)", status).order("from_date #{order}")
  end

  def self.event_get_all_years(status,order)
    where("status in (?)", status).order("from_date #{order}")
  end

  def self.event_get_all_years(status,order)
    where("status in (?)", status).order("from_date #{order}")
  end

  # Get the event photos
  def get_event_photos
    self.attachments
  end

  # Get the event categories
  def get_event_categories
    self.event_categories.includes(:category=>:attachment).order('sort_order IS NULL, sort_order ASC')
  end

  # Get the event sponsors
  def get_event_sponsors
    self.event_sponsors.select("id,sponsor_name,sponsor_description,sponsor_url")
  end
  
  # Get the upcoming events
  def self.get_upcoming_events_list
    self.event_get(["Upcoming"],"ASC")
  end
 
  # Get the past events
  def self.get_past_events_list
    self.event_get_all_years(["Completed"],"DESC")
  end

  def self.upcoming_and_live_events
    self.event_get(['Upcoming', 'OnGoing'],"ASC")
  end

  def self.total_donations_raised
    self.event_get(["Upcoming","Completed","OnGoing"],"ASC").pluck(:non_profit).compact.sum
  end

  def address
    [ address_line_1, city, state, country].compact.join(', ')
  end

  def create_event_registration
    self.create_event_registration!
  end

  def create_event_fan_voting
    self.create_event_fan_voting!
  end

  def update_time_zone_if_empty
    unless self.time_zone.present?
      self.time_zone = "Central Time (US & Canada)"
    end
  end

  def update_event_clubs    
    EventClub.find_or_create_by_event_id_and_club_id(self.id,self.club_id)
  end

  def groupme_group_creation
    image_url = self.image.present? ? self.image.url(:small) : ""
    groupme_response = GroupMe.create_group(GroupMeConfig.access_token,'groups',self.name,true,image_url)
    if (groupme_response.parsed_response["meta"]["code"] == 201)
      self.update_attributes(:group_id=>groupme_response.parsed_response["response"]["group_id"])
    end
  end

  def create_event_registrations_from_other_event
    (self.user_ids.compact - [""]).each do |user_id|
      user = User.find(user_id)
      competitor = user.try(:competitor)
      if competitor.present?
        categories,event_number,no_event = ApplicationController.new.populate_badge_id_for_an_event(self.try(:id))
        evnt_cat_reg = EventCategoryRegistration.find_or_create_by_event_id_and_competitor_id(self.try(:id), competitor.try(:id))
        evnt_cat_reg.update_attributes(:event_registration_id=>self.try(:event_registration).try(:id), :category_id=>categories.try(:first).try(:id), :user_id=>user.try(:id), :number=>event_number+1) if evnt_cat_reg

        #Add the currently registered member(through admin) to the GroupMe group.
        # groupme_response = GroupMe.add_members_to_group(GroupMeConfig.access_token,'groups',evnt_cat_reg) if evnt_cat_reg.present?
        
        event_category = EventCategory.find_by_event_id_and_category_id(self.try(:id),categories.try(:first).try(:id))
        evnt_comp = EventCompetitor.find_or_create_by_event_category_registration_id(evnt_cat_reg.try(:id))
        evnt_comp.update_attributes(:competitor_id=>evnt_cat_reg.try(:competitor_id), :event_category_id=>event_category.try(:id)) if evnt_cat_reg.present?
        EventCompetitor.where("event_category_id is null").delete_all
        EventCategoryRegistration.where("event_id is null").delete_all
      end
    end
  end

  def self.create_event_registrations_from_other_event_on_update(event,day_event_id,user_ids)
    pulled_in_users = EventCategoryRegistration.where("event_id=? and user_id in (?)",day_event_id,user_ids) 
    (user_ids.compact - [""]).each do |user_id|
      user = User.find(user_id)
      competitor = user.try(:competitor)
      if competitor.present?
        categories,event_number,no_event = ApplicationController.new.populate_badge_id_for_an_event(event.try(:id))
        parent_obj = pulled_in_users.where(:user_id=>user_id).first#.find_all{|item| item.user_id==user_id}.try(:first)
        parent_id = parent_obj.try(:id)
        # number = parent_obj.try(:number)
        event_photo = parent_obj.try(:event_photo)
        # category_id = parent_obj.try(:category_id)
        
        evnt_cat_reg = EventCategoryRegistration.find_or_create_by_event_id_and_competitor_id(event.try(:id), competitor.try(:id))
        # event_existing_badge_id = EventCategoryRegistration.where(:event_id=>event.try(:id)).pluck(:number).try(:uniq)
        # number = event_existing_badge_id.include?(number) ? event_number+1 : number
        number = event_number+1
        evnt_cat_reg.update_attributes(:category_id=>categories.try(:first).try(:id),:event_registration_id=>event.try(:event_registration).try(:id), :user_id=>user.try(:id),:parent_id=>parent_id,:number=>number) if evnt_cat_reg
        
        #Add the currently registered member(through admin) to the GroupMe group.
        # groupme_response = GroupMe.add_members_to_group(GroupMeConfig.access_token,'groups',evnt_cat_reg) if evnt_cat_reg.present?
        
        event_category = EventCategory.find_by_event_id_and_category_id(event.try(:id),categories.try(:first).try(:id))
        evnt_comp = EventCompetitor.find_or_create_by_event_category_registration_id(evnt_cat_reg.try(:id))
        evnt_comp.update_attributes(:competitor_id=>evnt_cat_reg.try(:competitor_id), :event_category_id=>event_category.try(:id)) if evnt_cat_reg.present?
        EventCompetitor.where("event_category_id is null").delete_all
        EventCategoryRegistration.where("event_id is null").delete_all
      end
    end
  end

  def self.best_in_show(event)
    event_categories = event.try(:event_categories).where(:is_published=>true).order(:sort_order)
    event_comp_results = EventCompetitorResult.where(:event_category_id=>event_categories.pluck(:id), :avg_visitor_results=>0 ,:position=>1).pluck(:id)
    return event_comp_results
  end

  def display_name
    event_name = self.try(:name)
    dots = event_name.try(:size) >= 70 ? "..." : ""
    "#{event_name.slice(0, 70)}#{dots}"
  end

  def self.onetime_seed_evnt_reg ## onetime_seed_to_populate_event_registration_for_all_events. Need to run in production server and staging
    Event.where("status = ? or status = ?", "Upcoming", "OnGoing").each do |event|
      event.create_event_registration!
    end
  end

  def self.onetime_seed_evnt_fan_voting ## Need to change (create when calculate results) onetime_seed_to_populate_event_fan_voting_for_all_events. Need to run in production server and staging
    Event.all.each do |event| #where("status = ? or status = ?", "Upcoming", "OnGoing").each do |event|
      event.create_event_fan_voting!
    end
  end

  def self.check_for_next_event
    next_event_started = true
    offset = 0
    #while next_event_started
      next_event = Event.where("from_date >= ? and status = ?",Date.today,"Upcoming").order("from_date asc").limit(1).offset(offset)
      offset+=1
      next_event = next_event.try(:first)
      next_event_date = next_event.try(:from_date)
      diff = Time.zone.now.in_time_zone(next_event.try(:time_zone)).utc_offset
      if next_event_date.hour.to_i < 12 and next_event.meridian=="PM"
        next_event_date += 12.hours
      end
      elapsed_time = ((next_event_date - Time.at(Time.now.utc.to_i + diff))).to_i      
      #next_event_started = elapsed_time < 0
    #end
      return elapsed_time, next_event
  end

  def self.ordered_events(event_status)
    future_events_asc_sorted = Event.where('from_date >= ?', Date.today).where(:status=>event_status).order("from_date ASC")
    history_events_desc_sorted = Event.where('from_date < ?', Date.today).where(:status=>event_status).order("from_date DESC")
    total_events_sorted = future_events_asc_sorted + history_events_desc_sorted
    return total_events_sorted
  end

  def self.close_event(event)
    event.update_attributes(:status => "Completed")

        # Find all users of the application with notification settings enabled
        users_ids = UserRegId.where(:status=>true).pluck(:user_id).uniq

        # Only fans of the event, who have voted atleast one category in the same event
        event_categories_ids = event.try(:event_categories).pluck(:id)
        ffhl_users_ids = VisitorVote.where(:event_category_id=>event_categories_ids).pluck(:user_id).try(:compact).try(:uniq)

        ##Push Notification to all users except the fans to announce the event close notification and the judges results.
        users_ids = users_ids - ffhl_users_ids
        # Android Push Notification to be sent first . Before IOS notifications. because IOS creates table records.
        title = "Facial Hair League"
        message = "#{event.try(:name).try(:strip).try(:upcase).gsub("'"){"\\'"}} has just now finished and the Official Results are live! Click here to see pictures of the winners."
        # users_ids = all_users
        # Android push notification code
        ApplicationController.new.android_push_notification(title,message,users_ids,event.try(:id))
        # IOS push notification code
        ApplicationController.new.ios_push_notification(message,users_ids,event.try(:id))
        # ApplicationController.new.ios_apns_push

      #Push notification for FFH results. Will send only to the Fans who have voted for the event categories. Will send when an event is closed.        
        # Find the overall fan voting results of an event
        overall_results_hash = EventFanVotingResult.track_overall_fan_voting_results(event.try(:id))
        ApplicationController.new.ffhl_android_push_notification(title,ffhl_users_ids,event.try(:id),overall_results_hash)
        ApplicationController.new.ffhl_ios_push_notification(ffhl_users_ids,event.try(:id),overall_results_hash)
        # ApplicationController.new.ios_apns_push #hidden here and in application_controller. Is is enough to initialize once. So, moved this to initializers.rb at the EOL.

        # Delete all the event associated checkin-managers. Delete the checkinmanagers associated only to this event
        AdminUser.all.select { |au| au.delete if (au.try(:event_ids).include? "#{event.id}") }

      #Report generation for the closed event.
      self.report_generation(event)

      # All event registration details will delete after the event is closed. But event cat regs details wont delete.
      event.event_registration.delete if event.try(:event_registration).present? # Need to run onetime rake to generate
  end

  def create_event_image_if_not
    if ( !self.image.present? || !self.image_file_name.present? )
      path = "#{Rails.root}/public/attachments/medium/missing.png"
      file = File.open(path)
      self.image = file
      file.close
      self.save!    
    end
    self.attachments.where(:attachment_file_name=>nil).destroy_all
  end

  def self.report_generation(event)
    Axlsx::Package.new do |p|
      ##### Judge results report #1st Sheet
      # event = Event.find(96) #### Have to change it dynamically
      # Total competitors participated (checked-in comps)
      evt_comps = EventCategoryRegistration.where(:event_id=>event.try(:id))
      event_comps = evt_comps.present? ? "#{evt_comps.length}" : "0"
      p.workbook.add_worksheet(:name => "Event Stats") do |sheet|
        blank_title,title,left_black_border,red_text,blue_text,center_text,image_obj,center_black_border = self.sheet_attributes(sheet)
        sheet.add_row
        sheet.add_row ["","#{event.try(:name)}"], :style=>red_text
        sheet.add_row ["","#{event_comps} Total Competitors"], :style=>blue_text
        sheet.add_row
        sheet.add_row ["","First Name","Last Name","eMail","Badge Id","Nick Name","Category","Rank","Avg Score","Club Affiliation","State",""], :style=>[blank_title,title,title,title,title,title,title,title,title,title,title,blank_title]
        sheet.column_widths nil,10,10,23,8,15,20,4,10,20,8,25
        event_categories = event.try(:event_categories).includes(:category,:event_competitor_results=>[:event_competitor=>[:event_category_registration,:competitor=>[:user,:club]]]).where(:is_published=>true)
        judge_values_array = []
        event_categories.each do |event_category|
          event_comp_results = event_category.try(:event_competitor_results).includes(:event_category)
          judge_results = event_comp_results.where(:avg_visitor_results=>0).order("event_categories.sort_order asc, event_competitor_results.position asc")
          judge_results.each do |judge_result|
            event_competitor = judge_result.try(:event_competitor)
            evnt_cat_reg = event_competitor.try(:event_category_registration)
            competitor = event_competitor.try(:competitor)
            user = competitor.try(:user)
            judge_values_array << ["","#{user.try(:first_name).try(:capitalize).try(:strip)}","#{user.try(:last_name).try(:capitalize).try(:strip)}","#{user.try(:email)}","#{evnt_cat_reg.try(:number)}","#{competitor.try(:nick_name)}","#{event_category.try(:category).try(:name)}","#{judge_result.try(:position)}","#{judge_result.try(:avg_judge_results)}","#{competitor.try(:club).try(:name)}","#{(competitor.try(:state).present? && competitor.try(:state) != "Please select a state" && competitor.try(:state) != "State") ? competitor.try(:state) : '-'}",""]
          end #judge_results
        end #event_categories end
        judge_result_values_array = judge_values_array - []
        if judge_result_values_array.present?
          judge_result_values_array.each { |judge_result_value| sheet.add_row judge_result_value,:style=>[blank_title,left_black_border,left_black_border,left_black_border,center_black_border,left_black_border,left_black_border,center_black_border,center_black_border,left_black_border,center_black_border,blank_title] }
        else
          sheet.add_row ["","","","","","","No data to display","","","","",""],:style=>[blank_title,left_black_border,left_black_border,left_black_border,center_black_border,left_black_border,left_black_border,center_black_border,center_black_border,left_black_border,center_black_border,blank_title]
        end
      end # worksheet end

      ##### Fan results report #2nd Sheet      
      # event = Event.find(64) #### Have to change it dynamically
      #Total fans voted
      main_hash = EventFanVotingResult.track_overall_fan_voting_results(event.id)
      p.workbook.add_worksheet(:name => "Fantasy Facial Hair") do |sheet|
        blank_title,title,left_black_border,red_text,blue_text,center_text,image_obj,center_black_border = self.sheet_attributes(sheet)
        sheet.add_row
        sheet.add_row ["","#{event.try(:name)}"], :style=>red_text
        fans_count = main_hash.try(:length).present? ? main_hash.try(:length) : 0
        sheet.add_row ["","#{fans_count} Fans Voting"], :style=>blue_text
        sheet.add_row
        sheet.add_row ["","Rank","Avg Score","First Name","Last Name","eMail","State"], :style=>[blank_title,title,title,title,title,title,title,blank_title]
        sheet.column_widths 40,10, 10, 10, 10, 25,8,50
        # sheet.column_widths *([30]*sheet.column_info.count) # Column width equal splits
        fan_values_array = []
        if main_hash.present?
          current_max=main_hash.first[1]
          current_rank=1
          users = User.where(:id=>main_hash.try(:keys))
          main_hash.each_with_index  do |hsh,index|
            user = users.find_all{|user| user.id==hsh[0]}.first
            if hsh[1] < current_max
              current_rank = index+1
              current_max=hsh[1]      
            end
            fan_values_array << ["","#{current_rank}","#{hsh[1]}","#{user.try(:first_name).try(:capitalize).try(:strip)}","#{user.try(:last_name).try(:capitalize).try(:strip)}","#{user.try(:email)}","#{(user.try(:state).present? && user.try(:state) != "Please select a state" && user.try(:state) != "State") ? user.try(:state) : '-'}",""]
          end
        end
        fan_result_values_array = fan_values_array - []
        if fan_result_values_array.present?
          fan_result_values_array.each { |fan_result_value| sheet.add_row fan_result_value,:style=>[blank_title,center_black_border,center_black_border,left_black_border,left_black_border,left_black_border,center_black_border,blank_title] }
        else
          sheet.add_row ["","","","","No data to display","",""],:style=>[blank_title,center_black_border,center_black_border,left_black_border,left_black_border,left_black_border,center_black_border,blank_title]
        end
      end # worksheet end
      p.serialize("Event Wrapup#{event.try(:id)}.xlsx")
      @event = event
    end # Axlsx Package end
    # Mail sending to the event owner & the super admin Brett
    event_owner = @event.try(:user)
    mail_users = [event_owner,User.find_by_email("#{AppConfig.admin_email}")]
    mail_users.compact.uniq.each do |mail_user|
      Thread.new{UserMailer.send_event_report_to_event_owner(mail_user,@event).deliver}
    end    
  end # method end

  def self.sheet_attributes(sheet)
    title = sheet.styles.add_style(:bg_color => "001E4E79", :fg_color=>"FFFFFFFF", :sz=>10,  :border=> {:style => :thin, :color => "00000000"})
    blank_title = sheet.styles.add_style(:bg_color => "FFFFFFFF", :fg_color=>"FFFFFFFF")
    left_black_border = sheet.styles.add_style(:border=> {:style => :thin, :color => "00000000"}, :alignment=>{:wrap_text => true,:horizontal=> :left})
    center_black_border = sheet.styles.add_style(:border=> {:style => :thin, :color => "00000000"}, :alignment=>{:wrap_text => true,:horizontal=> :center})
    red_text = sheet.styles.add_style(:fg_color=> "00C71E1E", :sz=>20, :b=>:true)
    blue_text = sheet.styles.add_style(:fg_color=> "001E4E79", :sz=>15, :b=>:true)
    center_text = sheet.styles.add_style(:fg_color=> "00C71E1E", :b=>:true)
    path = "#{Rails.root}/public/images/FHL_report_logo.jpg"
    img = File.expand_path(path)
    image_obj = sheet.add_image(:image_src => img, :noSelect => true, :noMove => true, :hyperlink=>"") do |image|
      image.width = 400
      image.height = 60
      image.hyperlink.tooltip = "Labeled Link"
      image.start_at 6, 1
    end
    return blank_title,title,left_black_border,red_text,blue_text,center_text,image_obj,center_black_border
  end

end