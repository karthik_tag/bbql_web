class Attachment < ActiveRecord::Base
  # attr_accessible :title, :body
  belongs_to :attachable, :polymorphic => true
  has_attached_file :attachment, :styles => { :original => "5000x5000>", :medium => "350x350", :small => "180x180" } #,
                    # :url  => "/assets/attachments/:id/:style/:basename.:extension",
                    # :path => ":rails_root/public/assets/attachments/:id/:style/:basename.:extension"
  validates_attachment :attachment, content_type: { content_type: ["image/png", "image/jpg", "image/jpeg"] }


  attr_accessible :attachment, :attachable_id, :attachable_type, :attachment_file_name, :attachment_content_type, :attachment_file_size, :attachment_updated_at

end
