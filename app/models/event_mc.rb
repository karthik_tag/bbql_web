class EventMc < ActiveRecord::Base
  belongs_to :event
  belongs_to :user
  attr_accessible :event_id, :user_id

  validates :event_id, :presence => true
  validates :user_id, :uniqueness => {:scope => :event_id}, :allow_blank =>true

  def display_name # Dont easily change this. This is tied with URL of breakcrumb with event filters in js file
    # "#{self.try(:club).try(:name)}"
    "#{self.try(:event).try(:id)}"
  end
end
