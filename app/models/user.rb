class User < ActiveRecord::Base
  include ActiveModel::Dirty
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable,
         :recoverable, :rememberable, :trackable, :validatable
  
  has_one :competitor, :dependent => :destroy
  has_one :attachment, :as => :attachable, :dependent => :destroy
  has_one :login_image, :dependent => :destroy
  has_many :events, :through => :event_judges
  has_many :event_judges, :dependent => :destroy
  has_many :event_judge_codes, :dependent => :destroy
  has_many :event_mcs, :dependent => :destroy
  has_many :visitor_votes, :dependent => :destroy
  has_many :judge_votes, :dependent => :destroy
  has_many :user_reg_ids, :dependent => :destroy
  has_many :user_password_codes, :dependent => :destroy
  belongs_to :user_type
  belongs_to :judge_code

  after_update :update_login_image_details
  after_create :create_attachment_if_not

  after_save :delete_empty_comp_images

  attr_accessor :judge_code, :edit_password
  # Setup accessible (or protected) attributes for your model
  accepts_nested_attributes_for :competitor, :allow_destroy => true
  accepts_nested_attributes_for :attachment, :allow_destroy => true
  attr_accessible :email, :password, :password_confirmation, :remember_me, :first_name, :last_name, :zip_code, :legal_copy, :status, :user_type_id, :judge_code_id, :competitor_attributes, :category_id, :charity_id, :club_id, :attachment_attributes, :judge_code, :edit_password, :country, :state, :card_id

  validates :first_name, :presence => true
  #validates :country, :presence => true
  # validates :card_id,  numericality: {greater_than_or_equal_to: 1}, :allow_blank => true
  # validates :card_id,  :allow_blank => true
  # validates :card_id,  uniqueness: true, :allow_blank => true #, :message => "Member Id has already been taken"
  validates :card_id, :allow_blank => true, uniqueness: {:case_sensitive => false, message: "Member Id has already been taken"}
  validates :password, :presence => true, on: :create
  validates :user_type_id, presence: true
  validates :judge_code_id, presence: true , :if => Proc.new { |user|  user.user_type_id == 1}
  
  scope :get_user_by_email, lambda {|email| where("email = ?", email)  }
  scope :get_user_by_id, lambda {|id| where("id = ?", id)  }

  def display_name
    "#{first_name.try(:capitalize)} #{last_name.try(:capitalize)}"
  end

  # ----- Script starts here -----
    # One time rake to be run in production server to update the nick name as "Nick Name" to its corresponding first names
    def self.update_nick_name_as_first_name
      user_ids = Competitor.where("nick_name = 'Nick Name'").pluck(:user_id) # when run this script in production, check if all the records doesnt have event_id in competitor object. If it is there and event competitor/event registration record is not there, then it will create one record. check competitor after_update
      # user_ids = [1614]
      users = User.where(:id=>user_ids)
      # users = User.where("id in (?) and user_type_id != ?",user_ids,3)
      users.each do |user|
        user.competitor.update_attributes(:nick_name=>user.try(:first_name).try(:capitalize))
      end
    end
  # ----- Script ends here -----

  def self.toggle_main_menu(user)    
    if ["admin@bbql.com","brett@facialhairleague.com","brett@fhl.com"].include?(user.try(:email))
      return true
    elsif user.try(:is_checkin_manager) && !["admin@bbql.com","brett@facialhairleague.com","brett@fhl.com"].include?(user.try(:email))
      return false    
    elsif !user.try(:is_checkin_manager) && !["admin@bbql.com","brett@facialhairleague.com","brett@fhl.com"].include?(user.try(:email))
      return true
    end
  end

  private

  def update_login_image_details
    if self.try(:login_image).present?
      self.try(:login_image).update_attributes(:first_name=>self.first_name.try(:capitalize), :last_name => self.last_name.try(:capitalize))
    end
  end

  def create_attachment_if_not
    if ( !self.attachment.present? || !self.attachment.attachment_file_name.present? )
        if self.attachment.present?
          attach = self.attachment
          create_attachmnt(attach)
        else
          attach = self.build_attachment
          create_attachmnt(attach)
        end
    end
  end

  def create_attachmnt(attach)
    path = "#{Rails.root}/public/attachments/medium/missing.png"
    file = File.open(path)
    attach.attachment = file
    file.close
    attach.save!
  end

  def delete_empty_comp_images
    competitor = self.try(:competitor)
    if competitor.present? && competitor.try(:attachments).present?
      self.try(:competitor).attachments.where(:attachment_file_name=>nil).destroy_all
    end
  end

end