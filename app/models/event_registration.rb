class EventRegistration < ActiveRecord::Base
  attr_accessor :event_date
  attr_accessible :event_date
  belongs_to :event
end
