class JudgeVote < ActiveRecord::Base
  attr_accessible :rating, :event_id, :event_competitor_id, :category_question_id, :user_id, :event_category_id
  belongs_to :event_competitor
  belongs_to :category_question
  belongs_to :event_category
  belongs_to :user
end
