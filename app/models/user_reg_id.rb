class UserRegId < ActiveRecord::Base
  attr_accessible :user_gcm_reg_id, :user_id, :status
  belongs_to :user
end
