class UserMailer < ActionMailer::Base
  default from: "brett@facialhairleague.com"
  def send_registration_success(user)
  	@user = user
  	mail(to: @user.email, subject: 'Congratulations')
  end

  def reset_password(user,secure_code)
    @first_name = user.try(:first_name).try(:capitalize)
    @last_name = user.try(:last_name).try(:capitalize)
    @email = user.try(:email)
    @secure_code = secure_code # 6 digit random code.
    @expires_in_minutes = 60 # expires in how much minute.
    @attachment_url = "#{AppConfig.image_url}/images/emailbanner.jpg"
    mail(to: @email, subject: "BBQL : #{@secure_code} is your verification code for secure access")
  end

  def send_mail_after_password_reset(user)
    @first_name = user.try(:first_name).try(:capitalize)
    @last_name = user.try(:last_name).try(:capitalize)
    @email = user.try(:email)
    @attachment_url = "#{AppConfig.image_url}/images/emailbanner.jpg"
    mail(to: @email, subject: "BBQL : Password Reset Notification")
  end  

  def create_user(user,pwd)
    @first_name = user.try(:first_name).try(:capitalize)
    @last_name = user.try(:last_name).try(:capitalize)
    @email = user.try(:email)
    @pwd = pwd
    @attachment_url = "#{AppConfig.image_url}/images/emailbanner.jpg"
    mail(to: @email, subject: 'BBQL : Account Creation Notification')
  end

  def duplicate_account_deletion(actual_user,duplicate_email)
    @first_name = actual_user.try(:first_name).try(:capitalize)
    @last_name = actual_user.try(:last_name).try(:capitalize)
    @email = actual_user.try(:email)
    @duplicate_email = duplicate_email
    @attachment_url = "#{AppConfig.image_url}/images/emailbanner.jpg"
    mail(to: @email, subject: 'BBQL : Account Deletion Notification')
  end

  def send_bulk_email_to_selected_users(users,object)
    @object = object
    @attachment_url = "#{AppConfig.image_url}/images/emailbanner.jpg"
    emails = users.collect(&:email).join(",")
    mail(to: emails, subject: "BBQL : #{@object.try(:title).try(:capitalize)}")
  end
  def send_event_report_to_event_owner(user,object)
    @object = object
    @attachment_url = "#{AppConfig.image_url}/images/emailbanner.jpg"
    @user = user
    @email = @user.try(:email)
    attachments["Event Wrapup.xlsx"] = File.read("#{Rails.root}/Event Wrapup#{object.try(:id)}.xlsx")
    mail(to: @email, subject: "BBQL Event Report : #{@object.try(:name)}")
  end

end