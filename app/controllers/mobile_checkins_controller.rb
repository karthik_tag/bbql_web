class MobileCheckinsController < ApplicationController
  layout "mobile_checkin_layout"
  def index
    if ["admin@bbql.com","brett@facialhairleague.com","brett@fhl.com"].include?(current_admin_user.try(:email))
      @events = Event.where(:id=>EventRegistration.pluck(:event_id)).order(:from_date)
    else
      @events = Event.where(:id=>current_admin_user.try(:event_ids)).order(:from_date)
    end
  end

  def event_checkin
  	@event = Event.find_by_id(params[:id])
    reg_comp = EventCategoryRegistration.where(:event_id=>params[:id], :is_checked_in=>false).includes(:user).order("users.last_name ASC, users.first_name ASC")
    @registered_competitors = reg_comp.where("last_name != ''") + reg_comp.where("last_name = ''")
  	chekd_in_comp = EventCategoryRegistration.where(:event_id=>params[:id], :is_checked_in=>true).includes(:user).order("users.last_name ASC, users.first_name ASC")
    @checkedin_competitors = chekd_in_comp.where("last_name != ''") + chekd_in_comp.where("last_name = ''")
  	@evnt_cat_reg = EventCategoryRegistration.find_by_id(params[:evnt_cat_reg_id]) if params[:evnt_cat_reg_id].present?
  	@event_categories = @event.try(:categories).order('event_categories.sort_order IS NULL, event_categories.sort_order ASC')
  end

  def take_photo
	@evnt_cat_reg = EventCategoryRegistration.find_by_id(params[:evnt_cat_reg_id])
	@user_name = "#{@evnt_cat_reg.user.try(:last_name).try(:capitalize)}, #{@evnt_cat_reg.user.try(:first_name).try(:capitalize)}"
  end

  def update_photo
	evnt_cat_reg = EventCategoryRegistration.find_by_id(params[:event_category_registration][:id])
	evnt_cat_reg.update_attributes(:event_photo=>params[:event_category_registration][:event_photo], :is_checked_in=>true, :is_removed=>false) if params[:event_category_registration].present? && params[:event_category_registration][:event_photo].present?
    redirect_to event_checkin_mobile_checkin_path(:id=>evnt_cat_reg.try(:event_id), :evnt_cat_reg_id=>evnt_cat_reg, "success_msg"=>true)
  end

  # def search_users
  # 	@event = Event.find_by_id(params[:mobile_checkin_id]) if params[:mobile_checkin_id].present?
  # 	registered_competitors_ids = EventCategoryRegistration.where(:event_id=>@event.try(:id)).pluck(:competitor_id)
  #   judges_user_ids = EventJudgeCode.where(:event_id=>@event.try(:id)).pluck(:user_id)
  #   judges_comp_ids = Competitor.where(:user_id => judges_user_ids).pluck(:id)

  # 	query_string = (params[:first_name].present? && !params[:last_name].present?) ? "users.first_name like '%#{params[:first_name]}%'" : (params[:last_name].present? && !params[:first_name].present?) ? "users.last_name like '%#{params[:last_name]}%'" : "users.first_name like '%#{params[:first_name]}%' OR users.last_name like '%#{params[:last_name]}%'"
  # 	search_results_competitors_ids = Competitor.includes(:user).where("nick_name IS NOT NULL and nick_name != '' and users.user_type_id=2 and competitors.event_id IS NOT NULL").where("#{query_string}").map(&:id)
  # 	@all_competitors = Competitor.where(:id=>(search_results_competitors_ids - registered_competitors_ids - judges_comp_ids)).includes(:user).order("users.first_name ASC, users.last_name ASC")
  # 	@event_categories = @event.try(:categories).order("sort_order ASC")
  # end

  def search_users
    @event = Event.find_by_id(params[:mobile_checkin_id]) if params[:mobile_checkin_id].present?
    registered_user_ids = EventCategoryRegistration.where(:event_id=>@event.try(:id)).pluck(:user_id)
    judges_user_ids = EventJudgeCode.where(:event_id=>@event.try(:id)).pluck(:user_id)
    # judges_comp_ids = Competitor.where(:user_id => judges_user_ids).pluck(:id)

    # query_string = (params[:first_name].present? && !params[:last_name].present?) ? "first_name like '%#{escape_char_in_string(params[:first_name])}%'" : (params[:last_name].present? && !params[:first_name].present?) ? "last_name like '%#{escape_char_in_string(params[:last_name])}%'" : "first_name like '%#{escape_char_in_string(params[:first_name])}%' OR last_name like '%#{escape_char_in_string(params[:last_name])}%'"
    query_string = (params[:first_name].present? && !params[:last_name].present?) ? "first_name like '%#{escape_char_in_string(params[:first_name].strip)}%'" : (params[:last_name].present? && !params[:first_name].present?) ? "last_name like '%#{escape_char_in_string(params[:last_name].strip)}%'" : (params[:first_name].present? && params[:last_name].present?) ? "first_name like '%#{escape_char_in_string(params[:first_name].strip)}%' AND last_name like '%#{escape_char_in_string(params[:last_name].strip)}%'" : "first_name like '%#{escape_char_in_string(params[:first_name].strip)}%' OR last_name like '%#{escape_char_in_string(params[:last_name].strip)}%'"
    search_results_users_ids = User.where(:user_type_id=>2).where("#{query_string}").map(&:id)
    @all_users = User.where(:id=>(search_results_users_ids - registered_user_ids - judges_user_ids)).order("first_name ASC, last_name ASC")
    @event_categories = @event.try(:categories).order('event_categories.sort_order IS NULL, event_categories.sort_order ASC')
  end

  def checkin_users
    @event = Event.find_by_id(params[:mobile_checkin_id]) if params[:mobile_checkin_id].present?    
    @registered_users = EventCategoryRegistration.where(:event_id=>@event.try(:id),:is_checked_in=>to_boolean(params[:is_checked_in]), :is_removed=>to_boolean(params[:is_removed])).includes(:parent=>:category,:user=>:competitor).order("event_category_registrations.number ASC")
    judges_user_ids = EventJudgeCode.where(:event_id=>@event.try(:id)).pluck(:user_id)
    registered_user_ids = @registered_users.pluck(:user_id)
    @all_users = User.where(:id=>(registered_user_ids - judges_user_ids)).order("first_name ASC, last_name ASC")
    @event_categories = @event.try(:categories).order('event_categories.sort_order IS NULL, event_categories.sort_order ASC')
  end

  def update_checkin_users
    begin
      ecr = EventCategoryRegistration.find_by_id(params[:id])
      ecr.event_photo = ecr.try(:parent).try(:event_photo)
      ecr.is_checked_in = true
      ecr.is_removed = false
      ecr.category_id = params[:category_id]
      ecr.save
      event_category = EventCategory.find_by_event_id_and_category_id(ecr.try(:event_id),params[:category_id])
      ecr.try(:event_competitor).update_attributes(:event_category_id =>event_category.try(:id)) if event_category.present?
      render :json =>{:status=>true, :message=>"Checked in successfully"}
    rescue
      render :json =>{:status=>false, :message=>"Internal server error, please try again"}
    end
  end

  def remove_checkin_users
    begin
      ecr = EventCategoryRegistration.find_by_id(params[:id])
      ecr.is_removed = true
      ecr.is_checked_in = false
      ecr.event_photo = nil
      ecr.save
      render :json =>{:status=>true, :message=>"Removed Competitor successfully"}
    rescue
      render :json =>{:status=>false, :message=>"Internal server error, please try again"}
    end
  end

  def escape_char_in_string(string)
    return MobileCheckin.connection.quote_string(string)
  end

  def to_boolean(str)
      str == 'true'
  end

  def edit_checkedin_comp
  	@event = Event.find_by_id(params[:mobile_checkin_id])
  	@user = User.find_by_id(params[:selected_user_id])
  	@event_categories = @event.try(:categories).order('event_categories.sort_order IS NULL, event_categories.sort_order ASC')
  	@evnt_cat_reg = EventCategoryRegistration.find_by_event_id_and_user_id(@event.try(:id),@user.try(:id))
  	@category  = @evnt_cat_reg.try(:category)
  end

  def update_checkedin_comp
  	@event = Event.find_by_id(params[:mobile_checkin_id])
  	@user = User.find_by_id(params[:user_id])
  	category = Category.find_by_name(params[:category])
  	@evnt_cat_reg = EventCategoryRegistration.find_by_id(params[:evnt_cat_reg_id])
    event_competitor = @evnt_cat_reg.try(:event_competitor)
    event_category = EventCategory.find_by_event_id_and_category_id(@event.try(:id),category.try(:id))
  	@user.update_attributes(:first_name=>params[:first_name],:last_name=>params[:last_name])
  	@evnt_cat_reg.update_attributes(:category_id=>category.try(:id), :number=>params[:badge_id])
  	@evnt_cat_reg.update_attributes(:event_photo=>params[:event_category_registration][:event_photo], :is_checked_in=>true) if params[:event_category_registration].present? && params[:event_category_registration][:event_photo].present?
    event_competitor.update_attributes(:event_category_id=>event_category.try(:id)) if event_competitor.present?
  	redirect_to event_checkin_mobile_checkin_path(:id=>@event.id, :evnt_cat_reg_id=>@evnt_cat_reg, "success_msg"=>true)
  end

  def delete_checkedin_comp
    @event = Event.find_by_id(params[:mobile_checkin_id])
    if params[:checkedin_comp].present?      
      @evnt_cat_regs = EventCategoryRegistration.where(:event_id => @event.try(:id), :user_id => params[:checkedin_comp])
      @evnt_cat_regs.destroy_all      
    end
    redirect_to event_checkin_mobile_checkin_path(:id=>@event.id, :evnt_cat_reg_id=>nil)
  end

  def delete_checkedin_competitor
    @event = Event.find_by_id(params[:mobile_checkin_id])
    evnt_cat_reg = EventCategoryRegistration.find_by_id(params[:evnt_cat_reg_id])
    evnt_cat_reg.destroy if evnt_cat_reg.present?
    redirect_to event_checkin_mobile_checkin_path(:id=>@event.id, :evnt_cat_reg_id=>nil)
  end

  def register_comp_to_events
  	competitor = Competitor.find_by_id(params[:competitor_id])  	
  	evnt_cat_reg = register_competitor(params[:event_id],competitor,params[:category_id])

    redirect_to mobile_checkin_take_photo_path(:evnt_cat_reg_id=>evnt_cat_reg.try(:id))
  end

  def create_new_competitor
  	user = User.find_by_email(params[:email])
  	competitor = user.try(:competitor)
  	random_password = (0...8).map { (65 + rand(26)).chr }.join
  	unless user.present?
  		user = User.create(:email=>params[:email], :password=>random_password, :password_confirmation=>random_password,:first_name=>params[:create_first_name],:last_name=>params[:create_last_name],:user_type_id=>2, :country=>"United States")
  		Thread.new {UserMailer.create_user(user,random_password).deliver}
      	competitor = create_competitor(user,params[:category_id]) if competitor.nil?
  	end
  	evnt_cat_reg = register_competitor(params[:mobile_checkin_id],competitor,params[:category_id])

  	redirect_to mobile_checkin_take_photo_path(:evnt_cat_reg_id=>evnt_cat_reg.try(:id))
  end

  def validate_email_on_create_new_user
  	user = User.find_by_email(params[:email])
	if user.present?
		render :json => {"status"=>true,"message"=>"Email has already been taken"}
	else
		render :json => {"status"=>false}
	end

  end

  def register_competitor(event_id,competitor,category_id)
  	event = Event.find_by_id(event_id)
  	categories,event_number,no_event = populate_badge_id_for_an_event(event.try(:id))
  	evnt_cat_reg = EventCategoryRegistration.find_or_create_by_event_id_and_competitor_id(event.try(:id), competitor.try(:id))
    evnt_cat_reg.update_attributes(:event_registration_id=>event.try(:event_registration).try(:id), :category_id=>category_id, :user_id=>competitor.user.try(:id), :number=>(evnt_cat_reg.try(:number).present? ? evnt_cat_reg.try(:number) : event_number+1)) if evnt_cat_reg
    #Add the currently registered member(through checkin manager web) to the GroupMe group.
    # groupme_response = GroupMe.add_members_to_group(GroupMeConfig.access_token,'groups',evnt_cat_reg) if evnt_cat_reg.present?

    event_category = EventCategory.find_by_event_id_and_category_id(event.try(:id),category_id)
    evnt_comp = EventCompetitor.find_or_create_by_event_category_registration_id(evnt_cat_reg.try(:id))
    evnt_comp.update_attributes(:competitor_id=>evnt_cat_reg.try(:competitor_id), :event_category_id=>event_category.try(:id)) if evnt_cat_reg.present?
    competitor.update_attributes(:event_id=>event.try(:id),:number=>evnt_cat_reg.try(:number), :compete_category_id=>category_id)
    EventCompetitor.where("event_category_id is null").delete_all
    EventCategoryRegistration.where("event_id is null").delete_all
    return evnt_cat_reg
  end

  def create_competitor(user,category_id)
  	user.create_competitor(:nick_name=>user.try(:first_name),:category_id=>category_id,:city=>"City", :state=>"State",:charity_id=>54,:club_id=>212, :edited_by=>"admin")
  end

end
