class EventRequestsController < ApplicationController
	layout "mobile_checkin_layout"

	before_filter :find_event, :only=> [:edit,:update, :show, :destroy]
	before_filter :assign_uuid
	before_filter :check_user_by_uuid #, :only=> [:index]
	before_filter :find_clubs, :only=> [:new,:create,:edit,:update]

	def index
		@events = Event.where(:status=>"Pending", :user_id=>@user.try(:id))
	end

	def new
		@event = Event.new
		@event.build_event_requester_shipping_detail
	end

	def create
		@event = Event.new(params[:event])
		if @event.save
			redirect_to event_requests_path("user[uuid]"=>@uuid)
		else
			@errors = @event.errors.messages
			@shipping_errors = @event.try(:event_requester_shipping_detail).errors.messages
        	find_state(params[:event][:country]) if @errors.present?
        	find_shipping_state(params[:event][:event_requester_shipping_detail_attributes][:shipping_country])  if @shipping_errors.present?
			render "new"
		end
	end

	def edit
		@event.event_requester_shipping_detail
		find_state(@event.try(:country))
		find_shipping_state(@event.try(:event_requester_shipping_detail).try(:shipping_country))
	end

	def update
		if @event.update_attributes(params[:event])
			redirect_to event_requests_path("user[uuid]"=>@uuid)
		else
			@errors = @event.errors.messages
			@shipping_errors = @event.try(:event_requester_shipping_detail).errors.messages
        	find_state(params[:event][:country]) if @errors.present?
        	find_shipping_state(params[:event][:event_requester_shipping_detail_attributes][:shipping_country])  if @shipping_errors.present?
			render "edit"
		end
	end

	def show
		@uuid = params[:user][:uuid] if params[:user].present?
	end

	def destroy
		@event.destroy
		redirect_to event_requests_path("user[uuid]"=>@uuid)
	end

	private

	def find_state(country)
		country_obj = Carmen::Country.named(country)
    	# @states = country_obj.try(:subregions)
    	@states = country_obj.subregions.map do |state| 
			state_code = state.code
		end
    	@state = @event.try(:state)
	end

	def find_shipping_state(country)
		country_obj = Carmen::Country.named(country)
    	# @states = country_obj.try(:subregions)
    	@shipping_states = country_obj.subregions.map do |state| 
			state_code = state.code
		end if country_obj.present?
    	@shipping_state = @event.try(:event_requester_shipping_detail).try(:shipping_state)
	end

	def find_event
		@event = Event.find_by_id(params[:id])
	end

	def assign_uuid
		@uuid = params[:user][:uuid] if params[:user].present?
	end

	def find_clubs
		clubs = Club.order(:name)
		clubs_name = clubs.pluck(:name)
		clubs_id = clubs.pluck(:id)
		@clubs = clubs_name.zip(clubs_id)
	end
end