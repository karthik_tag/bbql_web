=begin
This module will be used for write the custom methods. This will be used for through out the controllers by calling.

Created On : Oct 1,2014
@author : TAG
=end

require "push_notifications_bulk_import.rb"
class ApplicationController < ActionController::Base
  protect_from_forgery

  # Override build_footer method in ActiveAdmin::Views::Pages
  require 'active_admin_views_pages_base.rb'
  
  include OpenCipher

  # This method will give the error response to client.
  def error_response(message, code)
  	return :json => {:status => false, :message => message, :code => code}
  end

  # This method will give the success response to client.
  def success_response(message, code)
  	return :json => {:status => true, :message => message, :code => code}
  end
  
  # This method will give the success message with object response to client.
  def success_response_with_object(message, code, json_object)
    return :json => {:status => true, :message => message, :code => code, :data => json_object}
  end

  # This method used for define new hash
  def new_hash
    return Hash.new
  end

  # This method will be used for check the user type based on the value choosen by client while registration
  def check_user_type
    params[:user]= ActiveSupport::JSON.decode(params[:user]) if params[:file]
  	if params[:is_competitor]=='true' || params[:is_competitor]== "1"
  		params[:user][:user_type_id] = UserType.get_id("Competitor").first.id
  	elsif params[:judge_code].present?
  	 	judge_code = JudgeCode.get_status(params[:judge_code]).first
      if judge_code.nil?
        render error_response("Judge code is invalid", 401)
      else
    		unless judge_code.status  #jugde code not yet assigned
    			params[:user][:user_type_id] = UserType.get_id("Judge").first.id
          params[:user][:judge_code_id] = judge_code.id
          judge_code.update_attribute(:status, true)
    		else
    			params[:judge_code_assigned] = true
    		end
      end
  	else
  		params[:user][:user_type_id] = UserType.get_id("Visitor").first.id
  	end
  end

  # This method used for check the user by decryption with encrypted uuid.
  def check_user_by_uuid      
      params[:user]= ActiveSupport::JSON.decode(params[:user]) if params[:file]
      if params[:user].present? && params[:user][:uuid].present? && params[:user][:uuid].include?(" ")
        params[:user][:uuid] = params[:user][:uuid].gsub(" ","+")
      end
      if params[:user] && params[:user][:uuid]
        user_id = decrypt(params[:user][:uuid])
        @user = User.get_user_by_id(user_id).first
        if @user.nil?
          render error_response("Unauthorized User", 401)
        else
          @user_type = UserType.get_user_type(@user.user_type_id).first.try(:name)
        end
      else
        render error_response("Unauthorized User", 401)
      end
  end

  # This method used for get judge results
  def get_judge_results(offset_count,limit_count,event)    
    results_array = []
    before_list = event.event_categories.order('sort_order IS NULL, sort_order ASC').includes(:event_competitor_results)
    before_list.limit(limit_count).offset(offset_count).each do |event_category|
      category_results_array = []
      # Check the event categories published or not
      if event_category.is_published && event_category.event_competitor_results.present?
        # Get the winner competitor
        # event_competitor = event_category.event_competitor_results.sort_by(&:avg_judge_results).last.event_competitor if event_category.judge_votes.present?
        # competitor = event_competitor.competitor if event_competitor.present?
        
        if event_category.judge_votes.present?
          all_judge_results = event_category.event_competitor_results.where(:avg_visitor_results=>0).sort_by(&:position).first(3)            
        end
        all_judge_results.each_with_index do |all_judge_result,index|
          event_competitor = all_judge_result.try(:event_competitor)
          competitor = event_competitor.try(:competitor)
          if competitor.present?
            results = new_hash
            # Get the category
            results["category"] = event_category.category.name
            # Get the winner image url
            # results["competitor_image_url"] = competitor.user.attachment ? "#{AppConfig.image_url}" + competitor.user.attachment.attachment.url : nil
            
            # Get the competitor image url
            evnt_cat = event_competitor.event_category
            comp_img_url = EventCategoryRegistration.where(:user_id=>competitor.user.id, :category_id=>evnt_cat.category_id, :event_id=>evnt_cat.event_id)
            # img_url = (comp_img_url.first.try(:event_photo).to_s.include?("missing") || nil) ? (competitor.user.attachment ? "#{AppConfig.image_url}" + competitor.user.attachment.attachment.url(:original) : nil) : "#{AppConfig.image_url}" + comp_img_url.last.try(:event_photo).url(:original)
            img_url = (comp_img_url.first.try(:event_photo).to_s.include?("missing") || comp_img_url.empty?) ? 
            ( competitor.try(:user).try(:attachment).try(:attachment).present? ? competitor.user.attachment.attachment.url(:original) : nil) : 
            comp_img_url.last.try(:event_photo).present? ? comp_img_url.last.try(:event_photo).url(:medium) : nil

            results["competitor_image_url"] = img_url.present? ? img_url : nil

            # Get the winner nick name
            results["nick_name"] = competitor.nick_name.try(:capitalize)
            # Get the winner name
            results["name"] = competitor.user ? competitor.user.first_name.try(:capitalize) : nil
            results["first_name"] = competitor.user ? competitor.user.first_name.try(:capitalize) : nil
            results["last_name"] = competitor.user ? competitor.user.last_name.try(:capitalize) : nil
            # Get winner rank
            results["rank"] = ((index + 1 ) == 1 ? "#{index + 1}ST PLACE" : (index + 1 ) == 2 ? "#{index + 1}ND PLACE" : "#{index + 1}RD PLACE") 

            results["charting_data"] = EventCompetitor.charting_data(all_judge_result.try(:event_competitor_id),all_judge_result.try(:event_category_id))
            results["avg_judge_results"] = all_judge_result.try(:avg_judge_results)

            category_results_array.push(results)
          end
        end if all_judge_results.present?

      end
      results_array.push(category_results_array) if category_results_array.present?
    end if event.present?
    return results_array
  end

  def get_event_judge_results(event)    
    results_array = []
    event.event_categories.order('sort_order IS NULL, sort_order ASC').each do |event_category|
      results = []
      # Check the event categories published or not
      if event_category.is_published && event_category.event_competitor_results.present?
        # Get the winner competitor
        event_competitor = event_category.event_competitor_results.where(:avg_visitor_results=>0).sort_by(&:position).first.try(:event_competitor) if event_category.judge_votes.present?
        competitor = event_competitor.competitor if event_competitor.present?        

        # if event_category.judge_votes.present?
        #   all_judge_results = event_category.event_competitor_results.sort_by(&:avg_judge_results).reverse.first(3)            
        # end
        # all_judge_results.each_with_index do |all_judge_result,index|
          # competitor = all_judge_result.try(:event_competitor).try(:competitor)
          if competitor.present?
            results = new_hash
            # Get the category
            results["category"] = event_category.category.name
            
            # Get the winner image url
            # results["competitor_image_url"] = competitor.user.attachment ? "#{AppConfig.image_url}" + competitor.user.attachment.attachment.url : nil
            
            # Get the competitor image url
            evnt_cat = event_competitor.event_category
            comp_img_url = EventCategoryRegistration.where(:user_id=>competitor.user.id, :category_id=>evnt_cat.category_id, :event_id=>evnt_cat.event_id)
            img_url = (comp_img_url.first.try(:event_photo).to_s.include?("missing") || comp_img_url.empty?) ? 
            ( competitor.try(:user).try(:attachment).try(:attachment).present? ? competitor.user.attachment.attachment.url(:medium) : nil) : 
            comp_img_url.last.try(:event_photo).present? ? comp_img_url.last.try(:event_photo).url(:medium) : nil
            results["competitor_image_url"] = img_url.present? ? img_url : nil

            # Get the winner nick name
            results["nick_name"] = competitor.nick_name.try(:capitalize)
            # Get the winner name
            results["name"] = competitor.user ? competitor.user.first_name : nil
            results["first_name"] = competitor.user ? competitor.user.first_name.try(:capitalize) : nil
            results["last_name"] = competitor.user ? competitor.user.last_name.try(:capitalize) : nil
            # Get winner rank
            # results["rank"] = ((index + 1 ) == 1 ? "#{index + 1}ST PLACE" : (index + 1 ) == 2 ? "#{index + 1}ND PLACE" : "#{index + 1}RD PLACE") 
            # category_results_array.push(results)
          end
        # end if all_judge_results.present?

      end
      results_array.push(results) if results.present?
    end if event.present?
    return results_array
  end

  # This method will be used for get contestent deatails, events participated and points
  def get_contestent_details(id, data_hash)
    competitor = Competitor.where(:id => id).first
      if competitor.present?
        data_hash = competitor
          data_hash["category_name"] = competitor.category.try(:name)
          data_hash["charity_name"] = competitor.charity.try(:name)
          data_hash["club_name"] = competitor.club.try(:name)
          data_hash["club_url"] = competitor.club.try(:attachment) ? competitor.club.try(:attachment).try(:attachment).url(:medium) : ''
          # Get contestent details
        data_hash["user_details"] = competitor.user

        # Get contestent details
          data_hash["competitor_image_url"] = competitor.user.try(:attachment) ? competitor.user.try(:attachment).try(:attachment).url : ''
          competitor_images_array = []
          
          competitor.try(:attachments).each_with_index do |attachment,index|
            image_hash = Hash.new
            image_url = attachment ? attachment.try(:attachment).url : nil
            g_width = 0.0
            g_height = 0.0
            g_proportion = 0.0
            unless attachment.try(:attachment)
              geo = Paperclip::Geometry.from_file(attachment.try(:attachment).to_s)
              g_width = geo.width
              g_height = geo.height
              g_proportion = g_height / g_width
            end
            image_hash["image_url"] = image_url
            image_hash["proportion"]  = g_proportion
            competitor_images_array.push(image_hash)
          end
          data_hash["competitor_images"] = competitor_images_array.sort_by{|hsh| hsh[:proportion]}

          events_array = []
          event_hash  = Hash.new
          contestent_events = EventCompetitor.where(:competitor_id => id)
          total_points = 0
          contestent_events.each do |event_competitor|
            # Get events participated by the contestent
            event = event_competitor.event_category.event
            if event.status == 'Completed'
              event_hash = event
              event_hash["event_date"] = event.from_date.strftime("%B %d, %Y") unless event.from_date.nil?
              # Get the results and points
              event_hash["results"] = EventCompetitorResult.where(:event_competitor_id => event_competitor.id, :avg_visitor_results => 0)
              points = event_competitor.try(:points) || 0
              event_hash["points"] = points
              events_array.push(event_hash)
              total_points += points
            end
          end
        data_hash["events"] = events_array
        # Total points
        data_hash["total_points"] = total_points
      end
      return data_hash
  end

  def dynamic_index_title(params)
    if params[:q].present? && (params[:q][:event_id_eq].present? || params[:q][:event_filter_equals].present?)
        event = params[:q][:event_id_eq].present? ? Event.find_by_id(params[:q][:event_id_eq]) : Event.find_by_name(params[:q][:event_filter_equals])
        dots = (event.try(:name).present? && event.try(:name).try(:size) >= 40) ? "..." : ""
        title = "#{event.try(:name).slice(0, 40)}#{dots} : #{event.from_date.to_date.strftime('%d, %b %y')}"
    else
        title = ""
    end
    return title
  end

# This mehtod used for get event details
  def get_events_details(events, data_hash)
    events_array = []
    unless events.empty?
      events.each do |event|
        data_hash = event
        data_hash["event_date"] = event.from_date.strftime("%B %d, %Y") unless event.from_date.nil?
        data_hash["event_image_url"] = event.image.url
        events_array.push(data_hash)
      end
    end
    return events_array
  end

  def group_events_by_monthwise(resultant_events)
    resultant_events.find_all {|x| x.destroy if x.name.nil?}
    resultant_events.group_by{ |u| u.from_date.beginning_of_month.strftime("%B") }
  end

  def group_events_by_yearwise(resultant_events)
    resultant_events.find_all {|x| x.destroy if x.name.nil?}
    resultant_events.group_by{ |u| u.from_date.beginning_of_year.strftime("%Y") }
  end
  
  def populate_badge_id_for_an_event(event_id)
    event = Event.find_by_id(event_id)
    evt_cat = event.try(:categories)
    categories = evt_cat.present? ? evt_cat.order('event_categories.sort_order IS NULL, event_categories.sort_order ASC') : []
    no_event = event_id.present? ? false : true
    evnt_cat = EventCategoryRegistration.where("event_id = ? and number != ?", event_id,0)
    if evnt_cat.present?
      max_numb = evnt_cat.maximum(:number)
      rest_numb_arr = evnt_cat.pluck(:number)
      max_numb_range = (1..max_numb).to_a
      arr = max_numb_range - rest_numb_arr
    end
    number = arr.present? ? (arr.first - 1) : max_numb
    event_number = number || 0
    return categories,event_number,no_event
  end

  def update_state_code(user)
    country = Country.named(user.try(:country))
    state_code = country.subregions.named(user.try(:state)).try(:code)
    user.competitor.update_attributes(:state=>state_code)
    return
  end

  # Push Notification to all users of the application for Android devices. 
  def android_push_notification(title,message,users_ids,event_id)
    gcm = GCM.new(PushNotifConfig.api_key)
    android_user_reg_ids = UserRegId.where(:user_id=>users_ids,:device_os=>"Android",:status=>true).pluck(:user_gcm_reg_id).uniq # an array of reg ids of app clients
    options = {data: {title: "#{title}", message: "#{message}", notId: "#{Time.now.to_i}", event_id: event_id}, collapse_key: "updated_score"}
    response = gcm.send(android_user_reg_ids, options)
    return
  end

  # Push Notification to all users of the application for IOS devices.
  def ios_push_notification(message,users_ids,event_id)
    bulk_data = []    
    ios_user_reg_ids = UserRegId.where(:user_id=>users_ids,:device_os=>"iOS",:status=>true).pluck(:user_gcm_reg_id).uniq
    ios_user_reg_ids.each do |device_token|
      n = Rpush::Apns::Notification.new
      n.app = Rpush::Apns::App.find_by_name("ios_app")
      n.device_token = device_token
      n.alert = message.gsub("'", %q(\\\'))
      n.url_args = {"event_id"=>event_id,"key"=>""} # Here we are passing an empty event_id from calling function, coz, this is admin push notif and no need of event_id
      bulk_data << n
    end
    Rpush::Apns::Notification.import bulk_data # Calls the method written in lib for this class. This class is created by us.
    ios_apns_push
  end

    # Push Notification to all users of the application for Android devices. 
  def ffhl_android_push_notification(title,users_ids,event_id,overall_results_hash)
    gcm = GCM.new(PushNotifConfig.api_key)
    android_user_reg_ids = UserRegId.where(:user_id=>users_ids,:device_os=>"Android",:status=>true).try(:uniq) #.pluck(:user_gcm_reg_id).uniq # an array of reg ids of app clients
    # message = "Voting for the category '#{event_category.try(:category).try(:name).try(:capitalize)}' is Closed and your FFH rank is #{} out of #{} with the score #{}. Click here to see the detailed results."
    android_user_reg_ids.each do |android_user_reg_id|
      user_id = android_user_reg_id.try(:user_id)
      your_score, tie, position = fan_results_tie_calculation(user_id,overall_results_hash)
      message = "The Fantasy Facial Hair results are in! You scored #{your_score} points and placed #{EventCompetitor.ordinal_suffix_of(position)} out of #{overall_results_hash.size}!"
      options = {data: {title: "#{title}", message: "#{message}", notId: "#{Time.now.to_i}", event_id: event_id}, collapse_key: "fan_results"}
      response = gcm.send([android_user_reg_id.try(:user_gcm_reg_id)], options)
    end
    return
  end

  # Push Notification to all users of the application for IOS devices.
  def ffhl_ios_push_notification(users_ids,event_id,overall_results_hash)
    bulk_data = []    
    ios_user_reg_ids = UserRegId.where(:user_id=>users_ids,:device_os=>"iOS",:status=>true).try(:uniq) #.pluck(:user_gcm_reg_id).uniq
    ios_user_reg_ids.each do |device_token|
      user_id = device_token.try(:user_id)
      your_score, tie, position = fan_results_tie_calculation(user_id,overall_results_hash)
      message = "The Fantasy Facial Hair results are in! You scored #{your_score} points and placed #{EventCompetitor.ordinal_suffix_of(position)} out of #{overall_results_hash.size}!"
      n = Rpush::Apns::Notification.new
      n.app = Rpush::Apns::App.find_by_name("ios_app")
      n.device_token = device_token.try(:user_gcm_reg_id)
      n.alert = message.gsub("'", %q(\\\'))
      n.collapse_key = "fan_results"
      n.url_args = {"event_id"=>event_id,"key"=>"fan_results"} #the above collapse key does not work for ios, so changed this param with hash to have both event_id & collapse key
      bulk_data << n
    end
    Rpush::Apns::Notification.import bulk_data # Calls the method written in lib for this class. This class is created by us.
    ios_apns_push
  end

  # Push Notification to all users of the application for Android devices when event is launched.
  def android_push_notification_on_launch(title,message,users_ids,event_id)
    gcm = GCM.new(PushNotifConfig.api_key)
    android_user_reg_ids = UserRegId.where(:user_id=>users_ids,:device_os=>"Android",:status=>true).pluck(:user_gcm_reg_id).uniq # an array of reg ids of app clients
    options = {data: {title: "#{title}", message: "#{message}", notId: "#{Time.now.to_i}", event_id: event_id}, collapse_key: "event_launch"}
    response = gcm.send(android_user_reg_ids, options)
    return
  end

  # Push Notification to all users of the application for IOS devices when event is launched.
  def ios_push_notification_on_launch(message,users_ids,event_id)
    bulk_data = []
    ios_user_reg_ids = UserRegId.where(:user_id=>users_ids,:device_os=>"iOS",:status=>true).pluck(:user_gcm_reg_id).uniq
    ios_user_reg_ids.each do |device_token|
      n = Rpush::Apns::Notification.new
      n.app = Rpush::Apns::App.find_by_name("ios_app")
      n.device_token = device_token
      n.alert = message.gsub("'", %q(\\\'))
      n.collapse_key = "event_launch"
      n.url_args = {"event_id"=>event_id,"key"=>"event_launch"}
      bulk_data << n
    end
    Rpush::Apns::Notification.import bulk_data # Calls the method written in lib for this class. This class is created by us.
    ios_apns_push
  end

  def fan_results_tie_calculation(user_id,hash)
    score = hash[user_id]
    hash_values = hash.values
    count = 0
    is_tie = hash_values.count(hash[user_id])
    tie = (is_tie > 1) ? "(T#{is_tie})" : ""
    is_position = hash_values.index(hash[user_id])
    position = is_position.present? ? (is_position + 1) : hash.length
    return score, tie, position
  end

  #It pushes to all the IOS users once the above contents are set
  def ios_apns_push
    if defined?(Rails)
      ActiveSupport.on_load(:after_initialize) do
        Rpush.embed
      end
    else
      Rpush.embed
    end
  end
end