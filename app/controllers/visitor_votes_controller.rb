class VisitorVotesController < ApplicationController
	def load_visitor_votes
		@visitor_votes = VisitorVote.where("position1 = ? or position2 = ? or position3 = ?", params[:format],params[:format],params[:format])#.page(1).per(50)
		if params[:csv_param]
    		respond_to do |format|
    		   format.html
    		   format.csv { send_data @visitor_votes.to_csv, :filename => 'visitor_votes.csv' }
    		end
		else        
			render :json => { :visitorResultsPartial => render_to_string('visitor_votes/_load_visitor_votes', :layout => false, :locals => { :visitor_votes => @visitor_votes, :format => params[:format]})}
		end		
	end
end
