class JudgeCodesController < ApplicationController
	def generate_judge_code
		generated_random_judge_code = (0...8).map { (65 + rand(26)).chr }.join
		render :json => {:judge_code => generated_random_judge_code}
	end

	def validate_judge_code
		judge_codes = JudgeCode.where(:name=>params[:judge_code])
		event_judge_code = EventJudgeCode.where(:event_id=>params[:event_id], :judge_code_id=>judge_codes.pluck(:id))
		if (event_judge_code.count > 0)
			render :json => {"status"=>true,"message"=>"This judge code has already been taken for this event"}
		else
			render :json => {"status"=>false}
		end
	end
end
