class UserRegIdsController < ApplicationController
	before_filter :check_user_by_uuid

	def gcm_register
		@user.user_reg_ids.find_or_create_by_user_gcm_reg_id_and_device_os(params[:user][:reg_id],params[:user][:device_os]) if params[:user][:reg_id].present? && params[:user][:uuid].present? && params[:user][:device_os].present?
		render :nothing => true
	end

	def toggle_push_notification
		if params[:user].present?
			user_reg_id = UserRegId.find_by_user_gcm_reg_id_and_user_id_and_device_os(params[:user][:reg_id],@user.try(:id),params[:user][:device_os]) if params[:user][:reg_id].present? && params[:user][:uuid].present? && params[:user][:device_os].present?
			if user_reg_id.present?
				user_reg_id.update_attributes(:status=>params[:user][:status])
				render :json => {"status"=>"true", :push_status => user_reg_id.try(:status), "message"=>"Push Notification status has been updated","status_code"=>200}
			else
				render :json => {"status"=>"false", :push_status => user_reg_id.try(:status), "message"=>"Push Notification is already enabled","status_code"=>400}
			end
		else
			render :json => {"status"=>"false","message"=>"Internal server error has occured","status_code"=>500}
		end
	end
end
