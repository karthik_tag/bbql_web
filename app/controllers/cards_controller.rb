=begin
	This module will be used for generate cards. This will have the BBQL database contestents details.

	Created On : Oct 23,2014
	@author : TAG
=end
class CardsController < ApplicationController
	before_filter :check_user_by_uuid #, :except => [:my_card_details]
	# This method used to get all contestents in BBQL database
	def index
		if params[:limit_count].present? || params[:offset_count].present?
			offset_count = params[:offset_count] || 0
			limit_count = params[:limit_count] || 10
			side_bar = params[:side_bar] || false
			search_text = params[:search_text] || ""
		end		
		data = new_hash
		contestents = (params[:limit_count].present? || params[:offset_count].present?) ? EventCompetitor.get_card_details(data,offset_count,limit_count,side_bar,search_text) : EventCompetitor.get_card_details(data,nil,nil,nil,nil)
		data["contestents"] = contestents
		render success_response_with_object("Success", 200, data)
	end
	# This method will be used to get particular contestent card details
	def show
		data = new_hash
		data["contestent_details"] = get_contestent_details params[:id], data
		render success_response_with_object("Success", 200, data)
	end

	def get_competitor_details
		competitor = @user.competitor
		data_hash = new_hash
		competitor_attachments_array = []
		data_hash["competitor_details"] = competitor
		data_hash["main_category_id"] = competitor.try(:category).try(:main_category).try(:id)
		competitor.attachments.last(5).each do |attach|
			data = new_hash
			data["competitor_attachment_url"] = attach.try(:attachment).present? ? attach.try(:attachment).url(:small) : nil
			competitor_attachments_array.push(data)
		end if competitor.present?
		data_hash["attachments"] = competitor_attachments_array
		render competitor.present? ? success_response_with_object("Success", 200, data_hash) : error_response("There is no competitor for this user", 400)
	end

	def update_competitor_details
		competitor = @user.competitor
		competitor_updated = competitor.update_attributes(params[:user][:competitor])
		update_state_code(@user)
		# competitor.update_attributes(:state=>@user.try(:state))
		competitor.attachments.create(:attachment=>params[:file]) if params[:file].present?
		render competitor_updated ? success_response_with_object("Your details have been successfully updated.", 200, competitor) : error_response("Update failed", 400)
	end

	def my_card_details
		data_hash = new_hash
		if params[:id].present?
			competitor = Competitor.find_by_id(params[:id])
			user = competitor.try(:user)
		else
			user = @user
			competitor = user.try(:competitor)
		end
		data_hash["user_details"] = EventCompetitor.profile_details(data_hash,user,competitor)
		data_hash["history_details"] = EventCompetitor.history_details(data_hash,user,competitor)
		data_hash["gallery_details"] = EventCompetitor.gallery_details(data_hash,user,competitor)
		render success_response_with_object("Success", 200, data_hash)
	end
end
