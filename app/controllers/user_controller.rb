=begin
This module will be used for manipulating user sign_in and sign_up process and it will get the requests from client side(Could be mobile devices such as iphone, android, ipad).

Created On : Oct 1,2014
@author : TAG
=end
require 'gcm'
class UserController < ApplicationController
	before_filter :check_user_type, :only=>['sign_up']
	# This method will be used for authenticate the user by using credentials given by user.
	def sign_in
		existing_user = User.includes(:competitor).get_user_by_email(params[:user][:email]).first
		passwd = existing_user.valid_password?(params[:user][:password]) if existing_user
		if existing_user && passwd
			existing_user_hash = new_hash
			existing_user_hash["uuid"] = encryption(existing_user.id)
			existing_user_hash["user_details"] = existing_user
			existing_user_hash["user_details"]["user_type"] = existing_user.user_type.name
			existing_user_hash["user_details"]["nick_name"] = existing_user.try(:competitor).present? ? existing_user.try(:competitor).try(:nick_name).try(:capitalize) : ""
			existing_user_hash["user_details"]["comp_id"] = existing_user.try(:competitor).present? ? existing_user.try(:competitor).try(:id) : ""
			existing_user_hash["user_details"]["image_url"] = existing_user.try(:attachment).try(:attachment).url(:small)
			if (existing_user.try(:user_type).try(:name) == "Competitor" && existing_user.try(:competitor).nil?)
				render success_response_with_object("Please enter competitor details", 300, existing_user_hash)
			else				
				render success_response_with_object("Successfully Logged in", 200, existing_user_hash)
			end
		else
			render error_response("Username or Password entered is incorrect. Please try again.", 401)
	    end
	end

    # This method will be used for create a user to appilication
	def sign_up
		user = User.new(params[:user])
		invalid_judge_code = false
		unless user.judge_code_id.nil?
		    valid_event_judge_code = EventJudgeCode.where("judge_code_id=? and user_id is NULL",user.judge_code_id)
		    if valid_event_judge_code.size == 0
		      invalid_judge_code = true
		    end
		  end
		if params[:judge_code_assigned] || invalid_judge_code
			render error_response("Invalid Judge Code", 401)
		else
			if user.save
				new_user = new_hash
				user.create_attachment(:attachment=>params[:file]) if params[:file]
				new_user["uuid"] = encryption(user.id)
				new_user["user_details"] = user
				new_user["user_details"]["nick_name"] = ""
				new_user["user_details"]["image_url"] = user.try(:attachment).try(:attachment).try(:url)
				new_user["user_details"]["comp_id"] = user.try(:competitor).present? ? user.try(:competitor).try(:id) : ""
				new_user["user_details"]["user_type"] = user.user_type.name
				if user.judge_code_id.present?
					event_judge_code = EventJudgeCode.find_by_judge_code_id(user.judge_code_id)
					event_judge_code.update_attributes(:user_id=>user.id) unless event_judge_code.nil?
				end
				#Thread.new{UserMailer.send_registration_success(user).deliver}
				Thread.new{UserMailer.create_user(user,nil).deliver}
				render success_response_with_object("Successfully registered", 200, new_user)
			else
				render error_response(user.errors.full_messages[0], 401)
			end
		end
	end

	def app_landing_page
		rand_login_images = LoginImage.where("id > ?",0).sample(5)
		data = get_login_images_info rand_login_images
		render :json => {"status"=>"true","login_images"=>rand_login_images, "donation_url" => NonProfit.first.present? ? NonProfit.first.try(:url) : "","status_code"=>200}
	end

	def create_non_profit_url
		NonProfit.destroy_all
		NonProfit.create(:url=>params[:url] ,:status=>true)
		redirect_to admin_admin_users_path
	end

	def delete_non_profit_url
		if (params[:url_param] != "undefined" && params[:url_param] != "")
			non_profit_url = NonProfit.find_by_url(params[:url_param])
			non_profit_url.destroy
		end
		render :partial=> "/admin/admin_users/create_non_profit_url"
	end

	# def create_admin_push_notification
	# 	AdminPushNotification.destroy_all
	# 	admin_notif = AdminPushNotification.create(:content=>params[:content] ,:status=>true)

	# 	# Android Push Notification to be sent first . Before IOS notifications. because IOS creates table records.
	# 	title = "Facial Hair League"
	# 	message = admin_notif.try(:content)
 #    	users_ids = UserRegId.where(:status=>true).pluck(:user_id).uniq
 #    	# Android push notification code
	# 	android_push_notification(title,message,users_ids)

	# 	# One time code to be run when the app server starts
	# 	# app = Rpush::Apns::App.new
	# 	# app.name = "ios_app"
	# 	# #openssl pkcs12 -clcerts -in cert.p12 -out <environment>.pem
	# 	# app.certificate = File.read(Rails.root + "config/development.pem")
	# 	# app.environment = "sandbox" # APNs environment.
	# 	# app.password = "123456"
	# 	# app.connections = 1
	# 	# app.save!

	# 	# IOS push notification code
	# 	ios_push_notification(message,users_ids)
	# 	ios_apns_push
	# 	redirect_to admin_admin_users_path
	# end	

	# def delete_admin_push_notification
	# 	if (params[:content_param] != "undefined" && params[:content_param] != "")
	# 		admin_push_notif = AdminPushNotification.find_by_content(params[:content_param])
	# 		admin_push_notif.destroy
	# 	end
	# 	render :partial=> "/admin/admin_users/create_push_notification"
	# end

	def get_login_images_info(rand_login_images)
		login_image_array = []
		rand_login_images.each do |rand_login_image|
			login_image_hash = new_hash
			login_image_hash = rand_login_image
			login_image_hash["charity_name"] = rand_login_image.try(:charity).try(:name)
			login_image_hash["login_image_url"] = rand_login_image.try(:login_image) ? rand_login_image.try(:login_image).url(:original) : nil
			login_image_array.push(login_image_hash)
		end
		return login_image_array
	end

	def forgot_password # Can also use the same method for password resend too
    	if params[:user][:email].present?
    	  user = User.find_by_email(params[:user][:email])
    	  secure_code = rand.to_s[2..6]
    	  # random_password = (0...8).map { (65 + rand(26)).chr }.join
    	  begin
    	    # user.update_attributes(:password=>random_password,:password_confirmation=>random_password)
    	    user.user_password_codes.create(:security_code=>secure_code)
    	    expired_codes = UserPasswordCode.where("created_at < ? and status =?", (Time.now.utc - 1.hour), true)
    	    expired_codes.update_all(:status=>false)
    	    Thread.new {UserMailer.reset_password(user,secure_code).deliver}
    	    render :json => {"status"=>"true","message"=>"Verification Code has been sent to your email","status_code"=>200}
    	  rescue
    	    render :json => {"status"=>"false","message"=>"Invalid email address","status_code"=>401}     
    	  end
    	else
    		render :json => {"status"=>"false","message"=>"Please enter an email","status_code"=>401}
    	end
  	end

  	def validate_security_code
  		user = User.find_by_email(params[:user][:email]) if params[:user].present? && params[:user][:email].present?
  		if user.present?
  			security_code = UserPasswordCode.where(:security_code=> params[:user][:security_code],:user_id=>user.try(:id)).first
  			if security_code.present? && (((Time.now.utc - security_code.created_at)/60) > 60)
  				security_code.update_attributes(:status=>false)
  				render :json => {"status"=>"false","status_code"=>401,"message"=>"Security Code is expired. Please try again."}
  			elsif security_code.present? && (((Time.now.utc - security_code.created_at)/60) < 60)
  				security_code.update_attributes(:status=>false)
  				render :json => {"status"=>"true","message"=>"Please enter your password details.","status_code"=>200}
  			else
  				render :json => {"status"=>"false","status_code"=>401,"message"=>"Security Code is not valid."}
  			end
  		else
  			render :json => {"status"=>"false","message"=>"Invalid email address","status_code"=>401}
  		end
  	end

  	def update_password_details
  		if params[:user][:email].present?
    	  user = User.find_by_email(params[:user][:email])
    	  begin
    	    user.update_attributes(:password=>params[:user][:password],:password_confirmation=>params[:user][:password_confirmation]) if params[:user].present? && params[:user][:password].present? && params[:user][:password_confirmation].present?
    	    Thread.new {UserMailer.send_mail_after_password_reset(user).deliver}                      
    	    render :json => {"status"=>"true","message"=>"Your Password has been updated","status_code"=>200}
    	  rescue
    	    render :json => {"status"=>"false","message"=>"Invalid email id","status_code"=>401}     
    	  end
    	else
    		render :json => {"status"=>"false","message"=>"Please enter an email","status_code"=>401}
    	end
  	end

  	def validate_number
  		# user = User.find_by_email(params[:email])
  		evnt_cat_reg = EventCategoryRegistration.where(:event_id=>params[:event_id],:number=>params[:number])
  		user_evnt_cat_reg = EventCategoryRegistration.where(:event_id=>params[:event_id],:number=>params[:number], :user_id=>params[:user_id])
  		if (evnt_cat_reg.count > 0 && user_evnt_cat_reg.count < 1)
  			render :json => {"status"=>true,"message"=>"Number has already been taken for this event"}  			
  		elsif params[:number].to_i <= 0
  			render :json => {"status"=>true,"message"=>"Please enter the badge number greater than 0"}  			
  		else
  			render :json => {"status"=>false}
  		end
  	end

  	def generate_users_list
  		case params[:selected_value].to_i
		when 1,3
		  users = User.select("id,first_name,last_name").order("last_name asc, first_name asc")
		when 2
		  users_reg_ids = UserRegId.pluck(:user_id).try(:uniq)
		  users = User.where("id not in (?)",users_reg_ids).select("id,first_name,last_name").order("last_name asc, first_name asc")
		else
		  users = []
		end
  		render :json => { :resultsPartial => render_to_string('user/_generate_users_list', :layout => false, :locals => { :users => users, :selected_value=>params[:selected_value].to_i})}
  	end

  	def validate_app_user_by_email
  		user = User.find_by_email(params[:email]) if params[:email].present?
  		if user.present?
  			render :json => {"status"=>true,"message"=>"Email has already been taken"}
  		else
  			render :json => {"status"=>false}
  		end
  	end

	# This method used for encrypt the user id
	def encryption(user_id)
		return encrypt(user_id)
	end

	def populate_user_details
		user = User.find_by_id(params[:user_id])
		if (user.present?)
  			render :json => {:status=>true, :first_name => user.try(:first_name).try(:capitalize),:last_name => user.try(:last_name).try(:capitalize), :nick_name => user.try(:competitor).try(:nick_name).try(:capitalize), :charity_id => user.try(:competitor).try(:charity).try(:id)}
  		else
  			render :json => {:status=>false}
  		end
	end

	def get_states
		country_obj = Carmen::Country.named(params[:country])
		states = country_obj.subregions.map do |state| 
			state_code = state.code
		end
		render :partial=> "user/populate_states", :locals => {:states => states}
	end
end
