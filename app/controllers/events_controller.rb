=begin
This module will be used for manipulating events  and it will get the requests from client side(Could be mobile devices such as iphone, android, ipad).

Created On : Oct 9,2014
@author : TAG
=end

class EventsController < ApplicationController
	before_filter :check_user_by_uuid, :except => [:populate_user_not_participated_in_this_event, :check_if_events, :pull_event_registrants, :get_all_events_details, :check_if_events_for_tabs]
	# This method used for display the upcoming events and past events
	def index
		data = new_hash
		upcoming_events = Event.get_upcoming_events_list
		data["upcoming_events_list"] = get_events_details upcoming_events, data
		past_events = Event.get_past_events_list
		data["past_events_list"] = get_events_details past_events, data

		render success_response_with_object("Success", 200, data)
	end

	# def motta_sathu
	# 	MultipleTy.delete_all
	# 	EventCategory.find(613).update_attributes(:tie_position=>0)
	# 	ecr = EventCompetitorResult.where(:id=>[10271,10272])
	# 	ecr.each do |e|
	# 		e.update_attributes(:avg_judge_results=>9.75,:position=>1)
	# 	end
	# 	ecr1 = EventCompetitorResult.where(:id=>[10273,10274])
	# 	ecr1.each do |e|
	# 		e.update_attributes(:avg_judge_results=>8.75,:position=>3)
	# 	end
	# 	## event = Event.find_by_id(101)
	# 	## event.update_attributes(:status=>params[:status])
	# 	render :json => {:success=>true}
	# end

	# This method used for get the event and its related all datas
	def show
		data = new_hash
		event = Event.get_event(params[:id]).first
		# Get the event
		data["event"] = event
		data["allow_participation"] = (event.status == 'Upcoming' && @user.user_type.name == 'Competitor')
		data["event"]["event_date"] = event.from_date.strftime("%B %d, %Y") unless event.from_date.nil?
		data["event"]["event_image_url"] = event.image.url(:original)
		#Is Event MC
		is_event_mc = @user.event_mcs.pluck(:event_id).include?(event.try(:id))
		data["event"]["is_event_mc"] = is_event_mc
		if is_event_mc
			event_date = event.try(:from_date)
			bd = DateTime.now.try(:beginning_of_day)
			ed = DateTime.now.try(:end_of_day)
			e_t = event_date
			data["event"]["enable_launch_button"] = (bd..ed).cover?(e_t)
		end		

		# Check the any event categories has published its result or not for a event
		data["event"]["results_published"] = event.event_categories.where(:is_published=>true).present?
		#Check if user participated in this event
		data["event"]["is_participated"] = @user.try(:user_type).try(:name) == "Competitor" ? is_participated(event.try(:id),@user.try(:id)) : false
		# Is Checked-in or not?
		evnt_cat_reg = EventCategoryRegistration.find_by_event_id_and_user_id(event.try(:id),@user.try(:id))
		data["event"]["is_checked_in"] = evnt_cat_reg.try(:is_checked_in).present? ? evnt_cat_reg.try(:is_checked_in) : false
		data["event"]["nonProfit"] = ActionController::Base.helpers.number_to_currency(event.try(:non_profit))
		data["event"]["participated_category"] = evnt_cat_reg.try(:category).try(:name)
		#Get the already voted categories
		array_event_category = event.event_categories.map(&:id)
		is_event_judge = EventJudgeCode.where(:event_id => event.id, :user_id => @user.id).present?

		array_of_voted_event_category = []
		if is_event_judge
			array_of_voted_event_category = JudgeVote.where(:user_id=>@user.id,:event_category_id=>array_event_category).pluck(:event_category_id)
		else
			array_of_voted_event_category = VisitorVote.where(:user_id=>@user.id,:event_category_id=>array_event_category).pluck(:event_category_id)
		end
		
		data["event"]["voted_categories"] = array_of_voted_event_category
		
		# Get details if the current user as Judge for this event
		data["is_judge"] = is_event_judge
		unless @user.competitor.nil?
			data["participating_event_category_id"] = EventCompetitor.where("competitor_id=? and event_category_id in (?)",@user.competitor.id,array_event_category).try(:first).try(:event_category_id) || 0
		else
			data["participating_event_category_id"] = 0
		end
		# Get the event clubs
		data["club_affiliations"] = get_event_clubs event.clubs
		# Get the upcomming events
		
		# Removed this code since it is unusable for event details page
		# upcoming_events = Event.get_upcoming_events
		# data["upcoming_events"] = get_events_details upcoming_events, data


		# Get the event photos
		data["email_id"] = @user.try(:email)
		data["event_photos"] = get_event_photos_list event.get_event_photos
		data["event_results"] = nil
		data["ind_cat_res"] = []
		data["overall_ffh_results"] = []
		if event.status == "Completed"
			data["event_results"] = get_result(event.id)
			ind_cat_res, overall_ffh_results = track_FFH_results(event.id)
			data["ind_cat_res"] = ind_cat_res
			data["overall_ffh_results"] = overall_ffh_results
		end
		if event.status == "OnGoing"
			contains = event.try(:event_categories).pluck(:is_published).try(:uniq).include?(nil)
			data["is_all_category_closed"] = !contains
			data["yet_to_announce_result"] = contains
		end
		# Get the event categories
		data["event_categories"] = get_event_categories_list event.get_event_categories
		data["event_sponsors"] = get_event_sponsors_list event.get_event_sponsors
		data["event_judges"] = get_event_judges event.id
		data["event_mcs"] = get_event_mcs event.id
		data["event_banner_url"] = event.try(:event_banner).present? ? event.try(:event_banner).try(:event_banner).url(:original) : ""
		render success_response_with_object("Success", 200, data)
		rescue => error    
      		render :json => {"status"=>"false","message"=>"Internal server error has occured","status_code"=>500}
	end

    # This method used for get event results
	def results		
		data = get_event_results(params[:id])
		render success_response_with_object("Success", 200, data)
	end

	def launch_event
		event = Event.find params[:id]
		if params[:app_version] == "v1.0" && !params[:evt_cat_ids].present?
			# non_chkdin_comps_catgs = EventCompetitor.check_all_registered_members_checkd_in(event)
			# array = []
			# non_chkdin_comps_catgs.collect {|x| array << x.try(:category).try(:name)}
			# if (array.try(:uniq).try(:length) > 0)
				# render :json => {"status"=>false,"status_code"=>400, "message" => "Please make sure to check-in all the competitors in these categories '#{array.try(:uniq).join(',')}'."}
			# else
				main_arr = []
				event_categories = event.try(:event_categories)
				switch = event_categories.present? ? true : false
				event_categories.includes(:category,:event_competitors).each do |evt_cat|
				event_competitors = evt_cat.event_competitors
					if event_competitors.blank?
						arr = [evt_cat.try(:id),evt_cat.try(:category).try(:name)]
						main_arr.push(arr)
					end
				end if switch
				bool = main_arr.present? ? true : false
				if switch && bool
					render :json => {"status"=>true,"status_code"=>200, "category_arr"=>main_arr, "bool" => bool, "message" =>"Success"}				
				elsif switch && !bool
					event.update_attributes(:status => "OnGoing")
					push_notification_on_event_launch(event)
					render :json => {"status"=>true,"status_code"=>200, "category_arr"=>[], "bool" => true, "message" => "Event has been launched"}
				elsif !switch
					render :json => {"status"=>false,"status_code"=>400, "category_arr"=>[], "bool" => bool, "message" => "There are no categories for an event. Please add categories first."}
				end
			# end
		elsif params[:app_version] == "v1.0" && params[:evt_cat_ids].present?
			# non_chkdin_comps_catgs = EventCompetitor.check_all_registered_members_checkd_in(event)
			# array = []
			# non_chkdin_comps_catgs.collect {|x| array << x.try(:category).try(:name)}
			# if (array.try(:uniq).try(:length) > 0)
				# render :json => {"status"=>false,"status_code"=>400, "category_arr"=>[], "bool" => bool, "message" => "Please make sure to check-in all the competitors in these categories '#{array.try(:uniq).join(',')}'."}
			# else
				# This block will auto adjust the event category id if empty comps are deleted by MC.
				event_categories = EventCategory.where(:id=>params[:evt_cat_ids]).destroy_all
				non_deleted_cats = event.try(:event_categories).order(:sort_order)
				non_deleted_cats.each_with_index do |non_deleted_cat,index|
					non_deleted_cat.update_attributes(:sort_order=>index+1)
				end

				event.update_attributes(:status => "OnGoing")
				push_notification_on_event_launch(event)
	 	  		render :json => {"status"=>true,"status_code"=>200, "category_arr"=>[], "bool" => true, "message" => "Successfully deleted empty categories & launched the event"}
	 	  	# end
		
		else
			event.update_attributes(:status => "OnGoing")
			push_notification_on_event_launch(event)
	 	  	render :json => {"status"=>true,"status_code"=>200}
		end 	  	
		rescue => error
      		render :json => {"status"=>"false","message"=>"Internal server error has occured","status_code"=>500}
	end

	def event_stats
		event = Event.find_by_id(params[:id])
		event_category_ids = event.event_categories.pluck(:id)
		# competitor_ids = EventCompetitor.where(:event_category_id=>event_category_ids).pluck(:competitor_id)
		competitor_ids = EventCategoryRegistration.where(:event_id=>event.try(:id),:is_checked_in=>true).pluck(:competitor_id)
		competitors = Competitor.where(:id=>competitor_ids)
		result = competitors.group(:club_id).order("count(club_id) desc").count
		required_clubs = Club.where(:id=>competitors.pluck(:club_id).uniq)
		arr = []
		main_hash = new_hash
		default_value = result.first.present? ? result.first[1] : 0
		default_rank = 1
		rank = 1
		result.each_with_index do |(k,v),index|
			if default_value > v #Comparing previous entries and incrementing rank if it differs
				rank = default_rank + index
				default_value = v
			end
			hash = new_hash
			club_name = required_clubs.find_all{|club| club.id==k}.try(:first).try(:name)
			hash["rank"] = rank
			hash["entries"] = v
			hash["club"] = club_name
			arr.push(hash)
		end

		cat_arr = []
		evt_cats = event.get_event_categories
		evt_cats.each do |evt_cat|
			cat_hash = new_hash
			cat_hash["Category"] = evt_cat.try(:category).try(:name)
			cat_hash["Competitors"] = evt_cat.get_checked_in_event_competitors(nil).count
			total_comps = EventCategoryRegistration.where(:event_id=>evt_cat.try(:event_id), :category_id=>evt_cat.try(:category_id))
			cat_hash["total_reg_comps"] = total_comps.count
			cat_arr.push(cat_hash)
		end

		main_hash["Most Club Members Competing"] = arr
		main_hash["Total number of Competitors"] = cat_arr
		# Event banner to display in all event related pages.
		# is_event_banner = event.try(:is_event_banner)
		# event_banner_url = event.try(:event_banner).present? ? event.try(:event_banner).try(:event_banner).url(:original) : ""
		render :json => {"data" => main_hash}
	end

	def close_event
		event = Event.find params[:id]
		Event.close_event(event)
 	  	render :json => {"status"=>true,"status_code"=>200}
		rescue => error
      		render :json => {"status"=>false,"message"=>"Internal server error has occured","status_code"=>500}
	end

	def get_non_closed_event_category
		data = new_hash
		event = Event.find_by_id(params[:id])
		contains = event.try(:event_categories).where("is_published IS NULL")
		data["is_all_category_closed"] = !contains.present?
		data["non_closed_category"] = contains.try(:first).try(:id)
		data["is_event_closed"] = event.try(:status).eql?("Completed")
		# data["best_in_show"] = []
		# if !contains.present?
		#	best_in_show = Event.best_in_show(event)
		#	@event_competitor_results = EventCompetitorResult.where(:id => best_in_show).includes(:event_category=>[:category], :event_competitor=>[:competitor,:event_category_registration]).order("avg_judge_results desc")
		# 	@event_competitor_results.each do |evnt_comp_result|
		# 		hsh = {}
		# 		hsh["rank"] = evnt_comp_result.try(:position)
		# 		hsh["nick_name"] = evnt_comp_result.try(:event_competitor).try(:competitor).try(:nick_name)
		# 		hsh["badge_id"] = evnt_comp_result.try(:event_competitor).try(:event_category_registration).try(:number)
		# 		hsh["avg_judge_results"] = evnt_comp_result.try(:avg_judge_results)
		# 		hsh["category_name"] = evnt_comp_result.try(:event_category).try(:category).try(:name)
		# 		data["best_in_show"].push(hsh)
		# 	end			
		# end
		render :json => {"status"=>true,"status_code"=>200, "data"=>data}
		rescue => error
      		render :json => {"status"=>false,"message"=>"Internal server error has occured","status_code"=>500}
	end

	def track_FFH_results(event_id)
		#Overall Results
		event = Event.find_by_id(event_id)
		main_hash = EventFanVotingResult.track_overall_fan_voting_results(event_id)
		@users = User.where(:id=>main_hash.try(:keys))
		ffh_results = []
		if main_hash.present?
	    	current_max=main_hash.first[1]
	    	current_rank=1
	    	main_hash.each_with_index do |hsh,index|
	    		data_hash = new_hash
				user = @users.find_all{|user| user.id==hsh[0]}.first
				if hsh[1] < current_max
					current_rank = index+1
					current_max=hsh[1]
				end
				data_hash["rank"] = current_rank
				data_hash["score"] = hsh[1]
				data_hash["first_name"] = user.try(:first_name).try(:capitalize)
				data_hash["last_name"] = user.try(:last_name).try(:capitalize)
				data_hash["email_id"] = user.try(:email)
				ffh_results.push(data_hash)
			end
		end
		#Individual Categories Results
		ind_results = []

		event_categories = event.try(:event_categories).order('event_categories.sort_order IS NULL, event_categories.sort_order ASC')
		is_fan_vote_present = VisitorVote.where(:event_category_id=>event_categories.pluck(:id),:user_id=>@user.try(:id))
		# @user = User.find_by_id(1600)
		if is_fan_vote_present.present?
			event_categories.includes(:event, :category, :event_competitor_results=>[:event_competitor=>:event_category_registration]).each do |event_category|
				total_evnt_comp_results = event_category.try(:event_competitor_results).where(:avg_visitor_results => 0).order(:position)
				judges_event_competitor_results = total_evnt_comp_results.limit(3)
				fan_event_competitor_result = event_category.try(:visitor_votes).find_by_user_id(@user.try(:id)) #.where(:user_id=>@user.try(:id)).first
				hash = new_hash
				hash["category_name"] = event_category.try(:category).try(:name)
				total_score = 0
				hash["results"] = []
				first, second, third = EventFanVotingResult.track_individual_scores(event_category.try(:event_id),event_category.try(:category_id),@user.try(:id))
				judges_event_competitor_results.includes(:event_competitor=>[:event_category_registration]).each_with_index do |judges_event_competitor_result,index|
					inner_hash = new_hash
					inner_hash["index"] = index + 1
					judge_pick = judges_event_competitor_result.try(:event_competitor).try(:event_category_registration).try(:event_photo)
					inner_hash["judge_pick"] = judge_pick.present? ? judge_pick.url(:medium) : "#{AppConfig.image_url}" + "/images/mobile_icon.png"
					position = (index == 0) ? fan_event_competitor_result.try(:position1) : (index == 1) ? fan_event_competitor_result.try(:position2) : fan_event_competitor_result.try(:position3)
					score = (index == 0) ? first : (index == 1) ? second : third
					your_pick = EventCompetitor.find_by_id(position).try(:event_category_registration).try(:event_photo)
					inner_hash["your_pick"] = your_pick.present? ? your_pick.url(:medium) : "#{AppConfig.image_url}" + "/images/mobile_icon.png"
					inner_hash["score"] = score
					total_score +=score.present? ? score : 0
					hash["results"].push(inner_hash)
				end
				
				# tie_count = total_evnt_comp_results.present? ? total_evnt_comp_results.pluck(:position).count(result.try(:position)) : 0
				ind_res_hash = EventFanVotingResult.do_calculations(judges_event_competitor_results,event_category)
				score, tie, position = fan_results_tie_calculation(@user.try(:id),ind_res_hash)
				if score.nil? # || score.eql?(0)
					hash["your_score"] = "0 Points"
				else
					hash["your_score"] = "#{score} Pts - #{tie} #{EventCompetitor.ordinal_suffix_of(position)} Place"
				end
				ind_results.push(hash)
			end
		end
		return ind_results, ffh_results
		# render :json => {:ind_cat_res=>ind_results, :overall_ffh_results => ffh_results}
	end

	def show_season_wise_donations
		data = new_hash
		yearly_events = Event.select("name, from_date, non_profit, status").event_get(["Upcoming","Completed","OnGoing"],"ASC") #
		data["yearly_events"] = group_events_by_monthwise(yearly_events)
		render success_response_with_object("Success", 200, data)
		rescue => error    
      		render :json => {"status"=>"false","message"=>"Internal server error has occured","status_code"=>500}
	end

	def is_participated(event_id,user_id)
		# evt_cat_ids = EventCategory.where(:event_id=>event_id).map(&:id)
		# evnt_comp = User.find_by_id(user_id).try(:competitor).try(:event_competitors)
		# is_evt_comp = evnt_comp.present? ? evnt_comp.where(:event_category_id=>evt_cat_ids) : []
		# return is_evt_comp.length > 0 ? true : false
		evnt_cat_reg = EventCategoryRegistration.find_by_event_id_and_user_id(event_id,user_id)
		return evnt_cat_reg.present? ? true : false
	end

	def populate_user_not_participated_in_this_event		
		user_ids = EventCategoryRegistration.where(:event_id=>params[:event_id]).pluck(:user_id)
		# if params[:obj_name] == "event_mc"
			mc_user_ids = EventMc.where(:event_id=>params[:event_id]).pluck(:user_id)
		# else
			judge_user_ids = EventJudgeCode.where(:event_id=>params[:event_id]).pluck(:user_id)
		# end
		user_ids = user_ids + mc_user_ids + judge_user_ids
		user_ids = user_ids.try(:compact).present? ? user_ids.try(:compact) : [0]
		users = User.where("id not in (?) and last_name != ?",user_ids.compact.uniq,"").order("last_name asc,first_name asc") + User.where("id not in (?) and last_name = ?",user_ids.compact.uniq,"").order("last_name asc,first_name asc") #.order(:first_name)
		render :partial=> "users_drop_down", :locals => {:users => users}
	end

	def self_check_in
		evt_cat_reg = EventCategoryRegistration.find_by_event_id_and_user_id(params[:event_id],@user.id)
		if params[:file].present? && evt_cat_reg.present?
			evt_cat_reg.update_attributes(:event_photo=>params[:file], :is_checked_in=>true)
		end
		render :json => {"status"=>true,"message"=>"You are checked in. Good Luck!","status_code"=>200}
		rescue => error    
      		render :json => {"status"=>false,"message"=>"Internal server error has occured","status_code"=>500}
	end

	def get_event_results(event_id)
		data = new_hash
		event = Event.get_event(event_id).first
		# Get the event
		data["event"] = event
		data["event"]["event_date"] = event.from_date.strftime("%b %d, %Y") unless event.from_date.nil?
		data["event"]["event_image_url"] = event.image.url(:original)
		#Get event judge results
		data["judge_results"] = get_event_judge_results event
		#Get event visitor results
		data["visitor_results"] = [] #get_visitor_results event # Unhide this when client asks visitor results in app
		return data
	end

	def get_result(event_id)
		data = new_hash
		event = Event.get_event(event_id).first
		# Get the event
		data["event"] = event
		data["event"]["event_date"] = event.from_date.strftime("%b %d, %Y") unless event.from_date.nil?
		data["event"]["event_image_url"] = event.image.url(:original)
		#Get event judge results
		if params[:app_version].present?
			data["judge_results"] = get_judge_results(0,1,event)
		else
			data["judge_results"] = get_judge_results(0,nil,event)
		end
		#Get event visitor results
		data["visitor_results"] = [] #get_visitor_results event # Unhide this when client asks visitor results in app
		return data
	end

	def load_more_judge_results_data
		offset_count = params[:offset_count] || 0
		limit_count = 1
		event = Event.find_by_id(params[:id])

		data = new_hash		
		unless params[:offset_count].nil?
			data["judge_results"] = get_judge_results(offset_count,limit_count,event)
			render success_response_with_object("Success", 200, data)
		else
			data["judge_results"] = get_judge_results(nil,nil,event)
			render success_response_with_object("Success", 200, data)
		end
	end


	# This method used for get event photos
	def photos
		data = new_hash
		event = Event.find_by_id(params[:id])
		data["event"] = event
		data["event"]["event_date"] = event.from_date.strftime("%B %d, %Y") unless event.from_date.nil?
		data["event"]["event_image_url"] = event.image.url(:original)
		data["event_photos"] = get_event_photos_list event.get_event_photos
		render success_response_with_object("Success", 200, data)
	end

	def participate_event_category
		data = new_hash
		events_all_categories = EventCategory.find(params[:participation][:event_category_id]).event.event_categories.pluck(:id)
		EventCompetitor.where("competitor_id = ? and event_category_id in (?)",@user.competitor.id,events_all_categories).delete_all
		EventCompetitor.create(:competitor_id=>@user.competitor.id,:event_category_id=>params[:participation][:event_category_id])
		data["participating_event_category_id"] = params[:participation][:event_category_id]
		render success_response_with_object("Success", 200, data)
	end


   # This method used for get event categories detail
	def get_event_categories_list(event_categories)
		event_category_array = []
		event_categories.each do |event_category|
			event_category_hash = new_hash
			event_category_hash["event_category_id"] = event_category.id
			# Get the category title
			event_category_hash["category_title"] = event_category.category.name
			event_category_hash["is_published"] = event_category.try(:is_published) ? true : false
			# Get the category image
			event_category_hash["category_image_url"] = event_category.category.attachment ? event_category.category.attachment.attachment.url(:small) : nil

			#Is all judges votes
			total_judges,voted_judges = EventCategory.total_judges_voted(event_category)
			event_category_hash["is_all_judges_voted"] = (total_judges.to_i == voted_judges.to_i)

			if (params[:tie_position].present? && params[:tie_category_id].present? && (params[:tie_category_id].to_i == event_category.try(:id)))
				#Is all judges broken ties or not
				total_judges_count = total_judges
				tie_position = params[:tie_position].to_i
				if (EventCategory.find_by_id(params[:tie_category_id]).try(:tie_position) < tie_position)
					tie_broken_by_judges_count = event_category.multiple_ties.where(:position=>tie_position).pluck(:user_id).uniq.try(:count)
					event_category_hash["is_all_judges_voted"] = !(total_judges_count > tie_broken_by_judges_count)
				end
			end

			# Greater than tie_position in event_category table
			is_event_judge = EventJudgeCode.where(:event_id => event_category.event_id, :user_id => @user.id).present?
			is_j_voted = false
			is_f_voted = false
			if is_event_judge
				is_judge_voted = JudgeVote.find_by_user_id_and_event_category_id(@user.try(:id),event_category.try(:id))
				is_j_voted = is_judge_voted.present? ? true : is_j_voted
			else
				is_fan_voted = VisitorVote.find_by_user_id_and_event_category_id(@user.try(:id),event_category.try(:id))
				is_f_voted = is_fan_voted.present? ? true : is_f_voted
			end
			event_category_hash["is_judge_voted"] = is_j_voted
			event_category_hash["is_fan_voted"] = is_f_voted

			if params[:app_version] == "v1.1.0"
				tie_position = params[:tie_position].to_i || event_category.multiple_ties.pluck(:position).try(:uniq).try(:first).to_i
				multiple_ties_of_judges = event_category.multiple_ties.where(:position=>tie_position)
				random_judge_as_deciders = multiple_ties_of_judges.pluck(:user_id).try(:uniq)
				if ((event_category.try(:tie_position).to_i < tie_position && random_judge_as_deciders.include?(@user.try(:id))))
					event_category_hash["is_tie"] = false
				else
					is_tie, ties_arr = EventCompetitor.fetch_tied_competitors(event_category.id)
					event_category_hash["is_tie"] = is_tie
				end
			else
				is_tie, ties_arr = EventCompetitor.fetch_tied_competitors(event_category.id)
				event_category_hash["is_tie"] = is_tie
			end

			# This has added to make "Voting Closed", when any of two judges not voted but the judging is closed already.
			if !is_j_voted && event_category.event_competitor_results.where(:avg_visitor_results=>0).present? && !is_tie
				if is_event_judge
					event_category_hash["is_judge_voted"] = true
				end
			end
			
			if (!total_judges_count.nil? && !tie_broken_by_judges_count.nil?) && (total_judges_count == tie_broken_by_judges_count) && !is_tie
				if is_event_judge
					event_category_hash["is_all_judges_voted"] =  true
				end
			end

			competitors_length =  event_category.get_checked_in_event_competitors(nil).length
			event_category_hash["competitors_length"] = competitors_length
			event_category_array.push(event_category_hash)
		end
		return event_category_array
	end

	# This method used for get event sponsors detail
	def get_event_sponsors_list(event_sponsors)
		event_sponsor_array = []
		event_sponsors.each do |event_sponsor|
			event_sponsor_hash = new_hash
			event_sponsor_hash["sponsor_name"] = event_sponsor.sponsor_name
			event_sponsor_hash["sponsor_url"] = event_sponsor.sponsor_url
			event_sponsor_hash["sponsor_image_url"] = event_sponsor.try(:attachment) ? event_sponsor.try(:attachment).try(:attachment).url(:original) : nil
			event_sponsor_array.push(event_sponsor_hash)
		end
		return event_sponsor_array
	end

	# This method used for get the event photos
	def get_event_photos_list(event_images)
		event_photo_array = []
		event_images.each do |event_image|
			data_hash = new_hash
			# Get the event photo
			data_hash["event_photo_url"] = event_image.attachment ? event_image.attachment.url(:original) : nil
			event_photo_array.push(data_hash)
		end
		return event_photo_array
	end


	# This method used for get the event associated clubs
	def get_event_clubs(event_clubs)
		event_club_array = []
		event_clubs.each do |event_club|
			data_hash = new_hash
			data_hash["club_name"] = event_club.name
			# Get the club photo
			data_hash["event_club_image_url"] = event_club.attachment ? event_club.attachment.attachment.url(:original) : "#{AppConfig.image_url}" + "/images/noClubIcon.png"
			event_club_array.push(data_hash)
		end
		return event_club_array
	end


	# This method used for get visitor results
	  def get_visitor_results(event)
	    results_array = []
	    event.event_categories.each do |event_category|
	      # Check the event categories published or not
	      if event_category.is_published && event_category.event_competitor_results.present?
	        # Get the winner competitor
	        event_competitor = event_category.event_competitor_results.sort_by(&:avg_visitor_results).last.event_competitor if event_category.visitor_votes.present?
	        competitor = event_competitor.competitor if event_competitor.present?
	        if competitor.present?
				results = new_hash
				# Get the category
				results["category"] = event_category.category.name
				# Get the winner image url
				results["competitor_image_url"] = competitor.user.attachment ? competitor.user.attachment.attachment.url(:original) : nil
				# Get the winner nick name
				results["nick_name"] = competitor.nick_name.try(:capitalize)
				# Get the winner name
				results["name"] = competitor.user ? competitor.user.first_name.try(:capitalize) : nil
				results_array.push(results)
	        end
	      end
	    end if event.present?
	    return results_array
	  end

	  def get_event_judges(event_id)
	  	judge_user_type = 1
	  	# judges = User.where("user_type_id=1").includes(:attachment)
	  	judge_users = EventJudgeCode.where(:event_id => event_id).pluck(:user_id)
	  	judges = User.where(:id=>judge_users).includes(:attachment)

	  	judges_array = []
		judges.each do |judge|
			data_hash = new_hash
			data_hash["name"] = "#{judge.first_name.try(:capitalize)} #{judge.last_name.try(:capitalize)}"
			# Get the judge user photo
			data_hash["judge_image_url"] = judge.attachment ? judge.attachment.attachment.url(:medium) : nil
			judges_array.push(data_hash)
		end
		return judges_array
	  end

	  def get_event_mcs(event_id)
	  	mc_users = EventMc.where(:event_id => event_id).pluck(:user_id)
	  	mcs = User.where(:id=>mc_users).includes(:attachment)
	  	mcs_array = []
		mcs.each do |mc|
			data_hash = new_hash
			data_hash["name"] = "#{mc.first_name.try(:capitalize)} #{mc.last_name.try(:capitalize)}"
			# Get the mc user photo
			data_hash["mc_image_url"] = mc.attachment ? mc.attachment.attachment.url(:medium) : nil
			mcs_array.push(data_hash)
		end
		return mcs_array
	  end

	  def check_if_events
	  	# events = Event.where("from_date > ? AND from_date < ? and status = ?", Time.now.beginning_of_year, Time.now.end_of_year,params[:event_type])
	  	events = Event.where(:status=>params[:event_type])
	  	unless events.present?
	  		event_type = params[:event_type] == "OnGoing" ? "In Progress" : params[:event_type].capitalize
	  		render :json => {"status"=>true,"message"=>"No '#{event_type}' events found","status_code"=>200}
		else
			render :json => {"status"=>false}
		end
	  end

	  def check_if_events_for_tabs
	  	total_events_sorted = []
	  	if params[:event_type].present?
	  		event_status = (params[:event_type] == "all") ? ['Upcoming','OnGoing','Completed'] : params[:event_type]
	  		total_events_sorted = Event.ordered_events(event_status)
	  	end
	  	if total_events_sorted.present?
	  		render :json => {:status=>true,:status_code=>200, :eventsPartial => render_to_string('admin/all_events/_all_events', :layout => false, :locals => {:total_events_sorted => total_events_sorted})}
		else
			event_type = params[:event_type] == "OnGoing" ? "In Progress" : params[:event_type].capitalize
			render :json => {:status=>false,:status_code=>404,:message=>"No '#{event_type}' events found", :eventsPartial=>nil}
		end
	  end

	  def pull_event_registrants
	  	event_cat_regs = params[:event_id].present? ? EventCategoryRegistration.where(:event_id => params[:event_id], :is_checked_in=>true).pluck(:user_id) : [] #all comps in a selected event
	  	participated_users = params[:current_event_id].present? ? EventCategoryRegistration.where(:event_id => params[:current_event_id]).pluck(:user_id) : [] # participated comps in a current event
	  	non_participated_users = event_cat_regs - participated_users # Non participated comps
		users = non_participated_users.present? ? User.where(:id=>non_participated_users).order('last_name asc, first_name asc') : []
	  	render :partial=> "pull_event_registrants", :layout => false, :locals => {:users => users}
	  end

	  def get_all_events_details
	  	responseData = all_events_in_event_categories
	  	render :json => JSON.pretty_generate({"status"=>true,"message"=>"Success","status_code"=>200, "responseData" => responseData})
	  end

	  def all_events_in_event_categories
	  	responseData = {}
	  	eventDetails = []
	  	events = Event.order("name ASC")
	  	events.includes(:categories,:event_categories).each do |event|
	  		event_hash = {}
	  		event_hash["event_name"] = event.try(:name)
	  		event_hash["event_date"] = event.try(:from_date).strftime("%B %d, %Y")
	  		event_categories = event.try(:event_categories).includes(:event,:category, :event_competitor_results, :judge_votes, :visitor_votes)
	  		categories = []
	  		event_categories.each do |event_category|
	  			category_hash = {}
	  			results = {}
	  			judges_results = []
	  			fans_results = []

	  			category_hash["category_name"] = event_category.try(:category).try(:name)
	  			category_hash["category_status"] = event_category.try(:is_published) ? "Published" : "Not Published"
	  			event_results = event_category.try(:event_competitor_results)
	  			judge_results = event_results.where(:avg_visitor_results => 0).order("position ASC")
	  			visitor_results = event_results.where(:avg_judge_results => 0).order("position ASC")

	  			judge_results.includes(:event_competitor => [:competitor => :user]).each do |judge_result|
	  				judge_results_hash = {}
	  				judge_results_hash["rank"] = judge_result.try(:position)
	  				competitor = judge_result.try(:event_competitor).try(:competitor)
	  				judge_results_hash["first_name"] = competitor.try(:user).try(:first_name).try(:capitalize)
	  				judge_results_hash["last_name"] = competitor.try(:user).try(:last_name).try(:capitalize)
	  				judge_results_hash["badge_id"] = EventCategoryRegistration.where(:user_id=>competitor.try(:user_id), :event_id =>event_category.event_id).first.try(:number)
	  				judge_results_hash["nick_name"] = competitor.try(:nick_name).try(:capitalize)
	  				judge_results_hash["avg_judge_results"] = judge_result.try(:avg_judge_results)
	  				judges_results.push(judge_results_hash)
	  			end


	  			visitor_results.includes(:event_competitor => [:competitor => :user]).each do |visitor_result|
	  				visitor_results_hash = {}
	  				visitor_results_hash["rank"] = visitor_result.try(:position)
	  				competitor = visitor_result.try(:event_competitor).try(:competitor)
	  				visitor_results_hash["first_name"] = competitor.try(:user).try(:first_name).try(:capitalize)
	  				visitor_results_hash["last_name"] = competitor.try(:user).try(:last_name).try(:capitalize)
	  				visitor_results_hash["badge_id"] = EventCategoryRegistration.where(:user_id=>competitor.try(:user_id), :event_id =>event_category.event_id).first.try(:number)
	  				visitor_results_hash["nick_name"] = competitor.try(:nick_name).try(:capitalize)
	  				visitor_results_hash["avg_visitor_results"] = visitor_result.try(:avg_visitor_results)
	  				fans_results.push(visitor_results_hash)
	  			end
	  			results["judges_results"] = judges_results
	  			results["fans_results"] = fans_results
	  			category_hash["results"] = results
	  			categories.push(category_hash)
	  		end
	  		event_hash["categories"] = categories
	  		eventDetails.push(event_hash)
	  	end

	  	responseData["eventDetails"] = eventDetails
	  	return responseData
	  end

	def push_notification_on_event_launch(event)
		if event.enable_notification_on_launch
			# Android Push Notification to be sent first . Before IOS notifications. because IOS creates table records.
			title = "Facial Hair League"
			event_name = event.try(:name).gsub("'", %q(\\\'))
			message = "The Event '#{event_name.try(:capitalize).gsub("'", %q(\\\'))}' is live now. Please visit the app to vote for your favourite competitors."
			users_ids = UserRegId.where(:status=>true).pluck(:user_id).uniq

			#All mcs of the event
			event_mcs_users_ids = event.try(:event_mcs).pluck(:user_id).try(:uniq).try(:compact)
			#All judges of the event
			event_judges_users_ids = event.try(:event_judge_codes).pluck(:user_id).try(:uniq).try(:compact)
			#All competitors of the event
			event_comps_users_ids = EventCategoryRegistration.where(:event_id=>event.try(:id)).pluck(:user_id).try(:uniq).try(:compact)
			#Only fans of the application
			push_notif_send_users_ids = users_ids - event_mcs_users_ids - event_judges_users_ids - event_comps_users_ids
			# Android push notification code
			android_push_notification_on_launch(title,message,push_notif_send_users_ids,event.try(:id))
			# IOS push notification code
			ios_push_notification_on_launch(message,push_notif_send_users_ids,event.try(:id))
			# ios_apns_push
		end
	end
end