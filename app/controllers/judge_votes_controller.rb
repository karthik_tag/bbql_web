class JudgeVotesController < ApplicationController
	before_filter :check_user_by_uuid

	def get_judges_votes
		judge_votes = @user.judge_votes.includes(:user, :category_question, :event_category => [:category], :event_competitor => [:event_category_registration, :competitor=>[:user=>[:attachment]]]).where(:event_category_id=>params[:id]).order("rating DESC")
		event_category = EventCategory.find_by_id(params[:id])
		vote_collection = []
		data = Hash.new
		category_name = event_category.try(:category).try(:name)
		data["category_name"] = category_name
		judge_votes.each_with_index do |judge_vote,index|
			hash = new_hash
			hash["rank"] = index+1
			event_competitor = judge_vote.try(:event_competitor)
			event_category_registration = event_competitor.try(:event_category_registration)
			comp_user = event_competitor.try(:competitor).try(:user)
			img_url = (event_category_registration.try(:event_photo).to_s.include?("missing") || event_category_registration.blank?) ? (comp_user.try(:attachment).try(:attachment).present? ? comp_user.try(:attachment).try(:attachment).url(:small) : nil) : event_category_registration.try(:event_photo).url(:small)
			hash["competitor_image_url"] = img_url.present? ? img_url : nil
			hash["judge_vote"] = judge_vote.try(:rating)
			hash["badge_id"] = event_category_registration.try(:number)
			vote_collection.push(hash)
		end
		data["vote_collection"] = vote_collection

		if (vote_collection.present?)
			render :json => {"status"=>true,"message"=>"Success", "data"=>data}
		else
			render :json => {"status"=>false,"category_name"=>category_name, :message=>"No voting available"}
		end
	end
end
