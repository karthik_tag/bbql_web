=begin
This module will be used for getting clubs and its contestents. This will get the requests from client side(Could be mobile devices such as iphone, android, ipad).

Created On : Nov 3,2014
@author : TAG
=end
class ClubsController < ApplicationController
	before_filter :check_user_by_uuid,:except => [:index]
	# This method used for get the clubs for user select while sign up as competitor
	def index
		offset_count = params[:offset_count] || 0
		limit_count = params[:limit_count] || 10
		side_bar = params[:side_bar] || false
		search_text = params[:search_text] || ""
		data = new_hash		
		unless params[:search_text].nil?
			data["clubs"] = Club.get_club_details(data,offset_count,limit_count,side_bar,search_text)
			render success_response_with_object("Success", 200, data)
		else
			club_array = Club.fetch_all_clubs
			render success_response_with_object("Success", 200, club_array)
		end
		# club_array = Club.fetch_all_clubs
		# render success_response_with_object("Success", 200, club_array)
	end
	# This method used for get the particular club deatils and its contestent details
	def show		
      	if params[:appversion].present? && params[:appversion] == "v.1.0"
      		if params[:limit_count].present? || params[:offset_count].present?
				offset_count = params[:offset_count] || 0
				limit_count = params[:limit_count] || 10
				tab_bar = params[:infinite_type] || "all"
			end
      		competitor_arr = []
			club = Club.where(:id => params[:id]).first
			club_data = new_hash
			club_data_hash = new_hash
			if club.present?
				club_data = club
				club_competitors = club.try(:competitors)
				if tab_bar == "all"
					club_data["club_image_url"] = club.attachment ? club.attachment.attachment.url(:original) : "#{AppConfig.image_url}" + "/images/noClubIcon.png"			
					#Newly added params
			      	club_data["competitors_count"] = club_competitors.count
			      	event_competitors = EventCompetitor.where(:competitor_id=>club_competitors.present? ? club_competitors.pluck(:id) : []).joins(:event_category=>[:event]).where("events.status = ?","Completed")
			      	club_data["total_events_participated_in"] = event_competitors.count
			      	
			      	results = EventCompetitorResult.joins(:event_category=>[:event]).where("events.status = ? and event_categories.is_published = ? and event_competitor_results.event_competitor_id in (?) and event_competitor_results.avg_visitor_results = ?","Completed",true,event_competitors.present? ? event_competitors.pluck(:id) : [],0)
					result_positions = results.present? ? results.pluck(:position) : []
					club_data["wins"] = result_positions.count(1)
		    		club_data["places"] = result_positions.count(2)
		    		club_data["shows"] = result_positions.count(3)
		    		club_data["event_year"] = Date.today.year
	    		end
	    		if ["team","all"].include?(tab_bar) && tab_bar != "history"
	    			before_list = club_competitors
	    			filtered_club_competitors = before_list.limit(limit_count).offset(offset_count)
					filtered_club_competitors.each do |competitor|
						if competitor.present?
							data_hash = new_hash
					        data_hash = competitor
					        data_hash["category_name"] = competitor.category.try(:name)
					        data_hash["charity_name"] = competitor.charity.try(:name)
					        data_hash["competitor_image_url"] = competitor.user.try(:attachment) ? competitor.user.try(:attachment).try(:attachment).url(:medium) : "#{AppConfig.image_url}" + "/images/noClubIcon.png"
					        data_hash["nick_name_index"] = competitor.try(:nick_name).slice(0)

					        data_hash["user_details"] = competitor.user
				        	competitor_arr.push(data_hash)
				    	end
		      		end if filtered_club_competitors.present?
			      	club_data["club_contestents"]=competitor_arr
		      	end
		    	if ["history","all"].include?(tab_bar) && tab_bar != "team"
	    			before_list = EventCompetitor.where(:competitor_id=>club_competitors.present? ? club_competitors.pluck(:id) : []).joins(:event_category=>[:event]).where("events.status = ?","Completed")
	    			event_competitors = before_list.limit(limit_count).offset(offset_count)
	      			club_data["club_history_details"] = EventCompetitor.club_history_details(club_data_hash,event_competitors)
	      		end
	      	end
      	else
      		competitor_arr = []
			club = Club.where(:id => params[:id]).first
			club_data = new_hash
			if club.present?
				club_data = club
				club_data["club_image_url"] = club.attachment ? club.attachment.attachment.url(:original) : "#{AppConfig.image_url}" + "/images/noClubIcon.png"			
				club_data["club_group_photo_url"] = club.try(:group_image).to_s.include?("missing") ? nil : club.try(:group_image).present? ? club.try(:group_image).url(:original) : nil
				club_competitors = club.try(:competitors)			
				club_competitors.each do |competitor|
					if competitor.present?
						data_hash = new_hash
				        data_hash = competitor
				        data_hash["category_name"] = competitor.category.try(:name)
				        data_hash["charity_name"] = competitor.charity.try(:name)
				        #data_hash["club_name"] = competitor.club.try(:name)
				        #data_hash["club_url"] = competitor.club.try(:attachment) ? "#{AppConfig.image_url}" +competitor.club.try(:attachment).try(:attachment).try(:url) : ''
				        data_hash["competitor_image_url"] = competitor.user.try(:attachment) ? competitor.user.try(:attachment).try(:attachment).url(:medium) : "#{AppConfig.image_url}" + "/images/noClubIcon.png"
				        data_hash["nick_name_index"] = competitor.try(:nick_name).slice(0)

				        data_hash["user_details"] = competitor.user
			        	competitor_arr.push(data_hash)
			    	end
	      		end if club_competitors.present?
		      	club_data["club_contestents"]=competitor_arr
		      	#Newly added params
		      	event_competitors = EventCompetitor.where(:competitor_id=>club_competitors.present? ? club_competitors.pluck(:id) : []).joins(:event_category=>[:event]).where("events.status = ?","Completed")
		      	club_data["competitors_count"] = competitor_arr.count
		      	club_data["total_events_participated_in"] = event_competitors.count
		      	
		      	results = EventCompetitorResult.joins(:event_category=>[:event]).where("events.status = ? and event_categories.is_published = ? and event_competitor_results.event_competitor_id in (?) and event_competitor_results.avg_visitor_results = ?","Completed",true,event_competitors.present? ? event_competitors.pluck(:id) : [],0)
				result_positions = results.present? ? results.pluck(:position) : []
				club_data["wins"] = result_positions.count(1)
	    		club_data["places"] = result_positions.count(2)
	    		club_data["shows"] = result_positions.count(3)
	    		club_data["event_year"] = Date.today.year
	      	end
      	end
      	render success_response_with_object("Success", 200, club_data)
	end
end