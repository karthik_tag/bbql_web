=begin
This module will be used for give the dashboard details to client and it will get the requests from client side(Could be mobile devices such as iphone, android, ipad).

Created On : Oct 6,2014
@author : TAG
=end

class HomeController < ApplicationController
	before_filter :check_user_by_uuid, :except => [:get_charities, :get_clubs, :get_categories, :signup_clubs, :news, :get_all_dropdown_values]
    
    # This method will return the dashboard page details.
	def index
		data = new_hash
		# @user.user_reg_ids.find_or_create_by_user_gcm_reg_id_and_device_os(params[:user][:reg_id],params[:user][:device_os]) if params[:user][:reg_id].present? && params[:user][:uuid].present? && params[:user][:device_os].present?
		#Get the upcoming events limit 3
		upcoming_events = Event.get_upcoming_events
		up_comng_evnts = get_events_details upcoming_events, data
		# data["upcoming_events"] = group_events_by_monthwise(up_comng_evnts) #Need to add condition for new API request coz, old request should'nt break.
		if params[:appversion] == "v.2.0"
			yearswise_events = group_events_by_yearwise(up_comng_evnts)
			year_and_monthwise_events = {}
			yearswise_events.each do |key,yearwise_events|
				year_and_monthwise_events[key] = group_events_by_monthwise(yearwise_events)
			end
			data["upcoming_events"] = year_and_monthwise_events
		elsif params[:appversion] == "v.1.0"
			data["upcoming_events"] = group_events_by_monthwise(up_comng_evnts)
		else
			data["upcoming_events"] = up_comng_evnts
		end
		# Get all live events details
		live_events = Event.get_live_event
		liv_evnts = get_events_details live_events, data
		# data["live_events"] = group_events_by_monthwise(liv_evnts) #Need to add condition for new API request coz, old request should'nt break.
		if (params[:appversion] == "v.1.0" || params[:appversion] == "v.2.0")
			data["live_events"] = group_events_by_monthwise(liv_evnts)
		else
			data["live_events"] = liv_evnts
		end
		# Get all the past events details
		past_events = Event.get_past_events_list
		past_evnts = get_events_details past_events, data

		if params[:appversion] == "v.2.0"
			yearswise_events = group_events_by_yearwise(past_evnts)
			year_and_monthwise_events = {}
			yearswise_events.each do |key,yearwise_events|
				year_and_monthwise_events[key] = group_events_by_monthwise(yearwise_events)
			end
			data["past_events"] = year_and_monthwise_events
		else
			data["past_events"] = group_events_by_monthwise(past_evnts)
		end

		# # Get live event details
		live_event = Event.get_live_event.first
		data["live_event"] = get_event_info live_event, data

		#Get the last event details
		last_event = Event.get_last_event.first
		data["last_event"] = get_event_info last_event, data		

		update_judge_code_required = false
		if @user.judge_code_id?
			event_ids = Event.get_live_events_id + Event.get_upcoming_events_id
			update_judge_code_required = EventJudgeCode.where(:event_id => event_ids, :user_id => @user.id).empty?
		end
		data["update_judge_code_required"] = update_judge_code_required
		
		data["total_past_events"] = past_events.count
		data["total_live_events"] = live_events.count
		data["total_upcoming_events"] = upcoming_events.count
		data["event_year"] = Date.today.year
		#Total count of the competitors, who have participated atleast one event.
		# data["total_active_competitors"] = EventCategoryRegistration.pluck(:competitor_id).compact.uniq.count.to_s.reverse.gsub(/(\d{3})(?=\d)/, '\\1,').reverse
		data["total_active_competitors"] = User.where(:user_type_id=>2).count.to_s.reverse.gsub(/(\d{3})(?=\d)/, '\\1,').reverse
		#Total count of the competitors in the database.
		# data["total_competitors"] = Competitor.where("nick_name IS NOT NULL and nick_name != ''").compact.uniq.count
		data["total_donations_raised"] = ActionController::Base.helpers.number_to_currency(Event.total_donations_raised)
		competitors = Competitor.joins(:attachments).where("nick_name != '' and attachments.attachment_file_name IS NOT NULL").includes(:charity, :category, :user=>:attachment, :club=>:attachment)
		# random_contestent_id = competitors.offset(rand(competitors.count)).first.try(:id)
		random_contestent_id = Competitor.find_by_id("1").try(:id)
		data["random_contestent_details"] = get_contestent_details random_contestent_id, data

		event_judge_codes = @user.event_judge_codes.joins(:event).where("status = ?", "OnGoing")
		data["event_judge_codes"]  = {}
		if event_judge_codes.present?
			data["event_judge_codes"]["welcome_judge_name"]  = "Welcome Judge #{@user.try(:last_name).try(:strip)}!"
			data["event_judge_codes"]["message"]  = "Let me take you to your judges seat. We're getting ready to get started!"
			data["event_judge_codes"]["event_id"] = event_judge_codes.try(:first).try(:event_id)
		end

		data["appImages"] = {}
		if (live_events.count <= 0)
			rand_app_images = AppImage.where("id > ?",0).sample(5)		  	
		  	app_image_details = []
		  	rand_app_images.each do |app_image|
		  		new_hash = {}
		  		new_hash["content"] = app_image.content
		  		new_hash["image_url"] = app_image.try(:app_image).try(:url)
		  		app_image_details << new_hash
		  	end
		  	data["appImages"]["app_image_details"] = app_image_details
		  	elapsed_time, next_event = Event.check_for_next_event
		  	data["appImages"]["elapsed_time"] = elapsed_time
		  	data["appImages"]["event_name"] = next_event.try(:name) #.try(:capitalize)
	  	else
	  		data["appImages"]["app_image_details"] = []
	  		data["appImages"]["elapsed_time"] = ""
	  		data["appImages"]["event_name"] = ""
	  	end

		#Get last event results
		data["judge_results"] = get_event_judge_results last_event
		data["ad_url"] = "#{AppConfig.ad_url}"		
		p success_response_with_object("Success", 200, data)
		render success_response_with_object("Success", 200, data)
	end

	# We are integrating the methods "get_categories", "get_charities" & "signup_clubs" into one to load at a time in competitor create page in UI.
	def get_all_dropdown_values
		data = new_hash
		categories = get_all_categories MainCategory.includes(:categories).all, data
		charities = Charity.order('name ASC')
		clubs = Club.select('id, name').order('name ASC')
		data["main_categories"] = categories
		data["charities"] = charities
		data["clubs"] = clubs
		render success_response_with_object("Success", 200, data)
	end

	# This method used for get the categories for user select while sign up as competitor
	def get_categories
		data = new_hash
		data = get_all_categories MainCategory.includes(:categories).all, data
		render success_response_with_object("Success", 200, data)
	end

	# This method used for get the charities for user select while sign up as competitor
	def get_charities
		categories = Charity.order('name ASC')
		render success_response_with_object("Success", 200, categories)
	end
	# This method used for get the clubs 
	def get_clubs
		offset_count = params[:offset] || 0
		limit_count = params[:limit] || 10
		clubs_set = Club.limit(limit_count).offset(offset_count).includes(:attachment)
		club_data = new_hash
		club_array = []
		clubs_set.each do |club|
			club_data = club
			club_data["club_image_url"] = club.attachment ? club.attachment.attachment.url(:original) : "#{AppConfig.image_url}" + "/images/noClubIcon.png"
			club_array.push(club_data)
		end
		render success_response_with_object("Success", 200, club_array)
	end

	def get_all_categories(main_categories, data_hash)
		main_category_array = []
		main_categories.each do |main_category|
			data_hash = main_category
			data_hash["categories"] = main_category.categories.select("id,name")
			main_category_array.push(data_hash)
		end
		return main_category_array
	end

	# This method used for get live event details
	def get_event_info(event, data)
		data = event
		unless event.nil?
			data["event_date"] = event.from_date.strftime("%B %d, %Y")  unless event.from_date.nil?
			data["event_image_url"] = event.image ? event.image.url(:original) : nil
		end
		return data
	end
	# This method used for update the judge code
	def update_judge_code
		judge_code = JudgeCode.get_status(params[:user][:judge_code]).first
		
		if judge_code

			is_comp_to_judge_shift = validate_comp_to_judge_shift(@user,judge_code)
			unless (is_comp_to_judge_shift)
				judge_code.update_attribute(:status, true)
				# @user.update_attributes(:judge_code_id => judge_code.id, :user_type_id => 1)
				@user.try(:competitor).update_attributes(:edited_by => "user") if @user.competitor.present?
				@user.update_attributes(:judge_code_id => judge_code.id)
				event_judge = EventJudgeCode.where(:judge_code_id => judge_code.id, :user_id => nil).first
				if event_judge
					event_judge.update_attribute(:user_id, @user.id)
					return render success_response("Successfully Updated", 200)
				else
					return render error_response("Code has been already assigned", 401)
				end
			else
				return render error_response("You cannot become a judge for this event since you are participating in this event.", 401)
			end

		else
			render error_response("Invalid code", 401)
		end

	end

	def validate_comp_to_judge_shift(user,judge_code) # This method validates if a competitor participated to an event can become a judge of the same event. Returns true if not, else false.
		competitor = user.try(:competitor)
		if competitor.present?
			evt_comp = competitor.event_competitors
			evt_catgry_ids = evt_comp.pluck(:event_category_id)
			evt_cats = EventCategory.where(:id=>evt_catgry_ids)
			evnt_ids = evt_cats.pluck(:event_id)		
			
			evt_jc = judge_code.event_judge_codes.first
			evnt = evt_jc.try(:event_id)
			return evnt_ids.include?(evnt)
		else
			return false
		end
	end

	# This method used for get clubs for user select while sign up as competitor
	def signup_clubs
		data = new_hash
		data = Club.select('id, name').order('name ASC')
		render success_response_with_object("Success", 200, data)
	end
	# This method used for get news from given API
	def news
		data = new_hash
		news_data = Net::HTTP.get(URI.parse("#{AppConfig.news_url}"))
		data = ActiveSupport::JSON.decode(news_data)
		render success_response_with_object("Success", 200, data)
	end
end
