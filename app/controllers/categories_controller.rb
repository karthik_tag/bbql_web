class CategoriesController < ApplicationController
	def populate_category_by_main_category
		categories = Category.where(:main_category_id => params[:id]).order("name asc")
		render :partial=> "categories_drop_down", :locals => {:categories => categories}
	end

	def populate_event_categories
		categories,event_number,no_event = populate_badge_id_for_an_event(params[:event_id])
		# render :partial=> "categories_drop_down", :locals => {:categories => categories}
		render :json => { :categoriesPartial => render_to_string('categories/_categories_drop_down', :layout => false, :locals => { :categories => categories}), :cat_count =>categories.count, :eventNumber=>event_number+1, :noEvent => no_event}
	end
end
