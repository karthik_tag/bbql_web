class EventFanVotingResultsController < ApplicationController
	require 'csv'
	before_filter :check_user_by_uuid, :only => [:track_FFH_results]
	def track_category_fan_voting_results
		hash = EventFanVotingResult.track_category_fan_voting_results(params[:event_id],params[:category_id])
		if params[:csv_param]
    		respond_to do |format|
    		   format.html
    		   format.csv { send_data EventFanVotingResult.to_csv(hash), :filename => 'Event Category Fan Voting Results.csv' }
    		end
		else
			@users = User.where(:id=>hash.try(:keys))
			# render :json => { :resultsPartial => render_to_string('event_fan_voting_results/_fan_voting_results', :layout => false, :locals => { :hash => hash, :event_id=>params[:event_id], :category=>event_category.try(:category)}), :sidebarPartial => render_to_string('event_fan_voting_results/_sidebar_filters', :layout => false)}
			render :json => { :resultsPartial => render_to_string('event_fan_voting_results/_fan_voting_results', :layout => false, :locals => { :hash => hash, :event_id=>params[:event_id], :category_id=>params[:category_id]})}
		end
	end

	def do_calculations(avg_jud_res,event_category)
		hash = EventFanVotingResult.do_calculations(avg_jud_res,event_category)
		return hash
	end

	def track_overall_fan_voting_results
		main_hash = EventFanVotingResult.track_overall_fan_voting_results(params[:event_id])

		@users = User.where(:id=>main_hash.try(:keys))		
		if params[:csv_param]
    		respond_to do |format|
    		   format.html
    		   format.csv { send_data EventFanVotingResult.to_csv(main_hash), :filename => 'Event Overall Fan Voting Results.csv' }
    		end
		else        
			# render :json => { :resultsPartial => render_to_string('event_fan_voting_results/_fan_voting_results', :layout => false, :locals => { :hash => main_hash, :event_id=>params[:event_id], :category=>nil}), :sidebarPartial => render_to_string('event_fan_voting_results/_sidebar_filters', :layout => false)}
			render :json => { :resultsPartial => render_to_string('event_fan_voting_results/_fan_voting_results', :layout => false, :locals => { :hash => main_hash, :event_id=>params[:event_id], :category_id=>nil})}
		end
	end
end