=begin
This module will be used for give the profile details to client and it will get the requests from client side(Could be mobile devices such as iphone, android, ipad).

Created On : Oct 7,2014
@author : TAG
=end

class ProfilesController < ApplicationController
	before_filter :check_user_by_uuid
	# This method will be used for give the details of user to view the profile.
	def show
		profile = new_hash
		if @user_type=='Competitor'
			profile["user"] = @user
			profile["competitor"] = @user.competitor
		else
			profile["user"] = @user
		end		
		s3url = @user.attachment ? @user.attachment.attachment.url(:small) : nil
		profile["user"]["image_url"] = s3url
		country = Carmen::Country.named(@user.country)
		subregions = country.subregions
		state = subregions.named(@user.state)
		if state.nil?
			state = subregions.coded(@user.state)
		end
		profile["user"]["state"] = state.try(:name)
		user_reg_id = UserRegId.find_by_user_gcm_reg_id_and_user_id(params[:user][:reg_id],@user.try(:id)) if params[:user][:reg_id].present? && params[:user][:uuid].present?
		profile["user"]["push_status"] = user_reg_id.try(:status)
		
		render success_response_with_object("Success", 200, profile)
	end

	# This method will be used for update the details of user profile.
	def update_profile
		params[:user_profile] = ActiveSupport::JSON.decode(params[:user_profile]) if params[:file]
		user = @user		
		if params[:user_profile][:password].blank? && params[:user_profile][:password_confirmation].blank?
        	params[:user_profile].delete("password")
        	params[:user_profile].delete("password_confirmation")
      	end
		if @user_type=='Competitor'
			if user.update_attributes(params[:user_profile])
				update_state_code(user)
				# user.competitor.update_attributes(:state=>user.try(:state))
				user.attachment.update_attributes(:attachment => params[:file]) if params[:file]
				if user.try(:competitor).nil?
					existing_user_hash = find_if_competitor_record(user)
					return render success_response_with_object("Please enter competitor details", 300, existing_user_hash)
				end
			else
				err_msg = find_err_msg(user)
				return render error_response(err_msg, 401)
			end
			#@user.competitor.update_attributes(params[:competitor]) if params[:competitor]
		else
			# user = @user
			is_visitor = (user.try(:user_type).try(:name) == "Visitor")
			if is_visitor && (params[:is_competitor] == true || params[:is_competitor] == "true")
				if user.update_attributes(params[:user_profile])
					user.attachment.update_attributes(:attachment => params[:file]) if params[:file]
					user.update_attributes(:user_type_id => UserType.find_by_name("Competitor").try(:id))
					if user.try(:competitor).nil?
						existing_user_hash = find_if_competitor_record(user)
						return render success_response_with_object("Please enter competitor details", 300, existing_user_hash)
					end
				else
					err_msg = find_err_msg(user)
					return render error_response(err_msg, 401)
				end
			end

			if user.update_attributes(params[:user_profile])
				user.attachment.update_attribute(:attachment, params[:file]) if params[:file]
			else
				return render error_response(err_msg, 401)
			end
		end
		render success_response_with_object("Your details have been successfully updated.", 200, find_if_competitor_record(user))
	end

	def find_err_msg(user)
		err_msgs = user.errors.messages
		err_msg = err_msgs[err_msgs.keys.first].first
		err_msg = err_msgs.keys.first.to_s.capitalize + " " + err_msg.to_s
		return err_msg
	end


	def find_if_competitor_record(user)
		existing_user_hash = new_hash
		existing_user_hash["uuid"] = encrypt(user.id)
		existing_user_hash["user_details"] = user
		existing_user_hash["user_details"]["user_type"] = user.try(:user_type).try(:name)
		existing_user_hash["user_details"]["image_url"] = user.try(:attachment).try(:attachment).url(:small) if user.try(:attachment).present?
		existing_user_hash["user_details"]["nick_name"] = user.try(:first_name)
		existing_user_hash["user_details"]["comp_id"] = user.try(:competitor).present? ? user.try(:competitor).try(:id) : ""		
		return existing_user_hash
	end

	# This mehtod used for cretae competitor profile while sign up
	def create
		if @user.competitor.nil?
			competitor = create_default_comp_record(@user,params[:user][:competitor]) #@user.create_competitor(params[:user][:competitor])
			update_state_code(@user)
			# competitor.update_attributes(:state=>@user.try(:state)) # This can be replaced by writting in after_save in competitor meodel. Also, delete the line in cards controller 'update_competitor_details' method and this file line no 32.
	    end
	    @user.competitor.attachments.create(:attachment=>params[:file])
		render success_response("Successfully created", 200)
	end

	def create_default_comp_record(user,comp_params)
		nick_name = comp_params[:nick_name].present? ? comp_params[:nick_name] : user.try(:first_name)
		category_id = comp_params[:category_id].present? ? comp_params[:category_id] : 14
		club_id = comp_params[:club_id].present? ? comp_params[:club_id] : 212
		charity_id = comp_params[:charity_id].present? ? comp_params[:charity_id] : 54
		city = comp_params[:city].present? ? comp_params[:city] : "City"
		user.create_competitor(:nick_name=>nick_name,:category_id=>category_id,:city=>city, :charity_id=>charity_id,:club_id=>club_id, :edited_by=>"user")
		return user.competitor
	end
end
