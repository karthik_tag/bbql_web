=begin
This module will be used for display categories and its competitors  and it will get the requests from client side(Could be mobile devices such as iphone, android, ipad).

Created On : Oct 9,2014
@author : TAG
=end

class EventCategoriesController < ApplicationController
	before_filter :check_user_by_uuid, :except => [:generate_competitor_position, :validate_category_name]
	
	# This method will be used for diaplay event category details and contestens for voting
	def show
		data = new_hash
		# Get a category detail
		event_category = EventCategory.get_event_category(params[:id]).first
		data["category"]= get_category_details event_category.category, data
		# Get the competitors for a category based on view_mode/edit mode
		if params[:mode] == "vote_mode"
			old_event_competitor_ids = params[:event_competitor_ids].present? ? params[:event_competitor_ids] : nil
			data["contestants"] = get_event_competitors_list event_category.get_checked_in_event_competitors(old_event_competitor_ids), data, "medium"
		else
			data["contestants"] = get_event_competitors_list event_category.get_event_competitors, data, "medium"
		end
		# Get Questions
		data["category_questions"] = event_category.category.get_category_questions if EventJudgeCode.where(:event_id => event_category.event_id, :user_id => @user.id).present?
		# Event banner to display in all event related pages.
		event_obj = event_category.try(:event)
		data["is_event_banner"] = event_obj.try(:is_event_banner)
		data["event_banner_url"] = event_obj.try(:event_banner).present? ? event_obj.try(:event_banner).try(:event_banner).url(:original) : ""

		render success_response_with_object("Success", 200, data)
	end

	def get_event_category_competitors
		data = new_hash
		event_category = EventCategory.find_by_id(params[:id])
		ordered_comp_list = get_event_competitors_list event_category.get_checked_in_event_competitors(nil), data, "small"
		data["contestants"] = ordered_comp_list.present? ? ordered_comp_list : []
		data["category_name"] = event_category.try(:category).try(:name)
		judge_voted_array = find_voted_judges(params[:id])
		data["voted_judges"] = judge_voted_array
		# Event banner to display in all event related pages.
		# event_obj = event_category.try(:event)
		# data["is_event_banner"] = event_obj.try(:is_event_banner)
		# data["event_banner_url"] = event_obj.try(:event_banner).present? ? event_obj.try(:event_banner).try(:event_banner).url(:original) : ""
		render success_response_with_object("Success", 200, data)
	end

	# This method used for vote the results
	def voting
=begin
		case @user_type
			when 'Visitor'
				# Check visitor already voted for event category or not
				if VisitorVote.where(:event_category_id => params[:visitor_vote][:event_category_id],:user_id => params[:visitor_vote][:user_id]).present?
					return render error_response("Already voted", 401)
				else
					VisitorVote.create(params[:visitor_vote])
				end
			when 'Judge'
				JudgeVote.where(:event_category_id => params[:event_category_id], :user_id => params[:user_id]).delete_all
				JudgeVote.create(params[:judge_vote])				
		end
=end
		if params.has_key?("visitor_vote")
			event_category_id = params[:visitor_vote][:event_category_id] 
		else
	 		event_category_id = params[:event_category_id]
		end
		event_category = EventCategory.find_by_id(event_category_id)
		if EventJudgeCode.where(:event_id => event_category.event_id, :user_id => @user.id).present?
			JudgeVote.where(:event_category_id => params[:event_category_id], :user_id => params[:user_id]).delete_all
			JudgeVote.create(params[:judge_vote])
			
			total_judges,voted_judges = EventCategory.total_judges_voted(event_category)
			params.slice!("id", "user")
			event_competitor_result_present = EventCompetitorResult.where(:event_category_id=>event_category.id,:avg_visitor_results=>0)
			if (total_judges.to_i == voted_judges.to_i) && !event_competitor_result_present.present?
				EventCategory.close_judge_voting(event_category)
			end
			redirect_to event_path(event_category.try(:event_id),params)
		else
			if event_category.is_published==true
				return render error_response("Category has been published", 401)
			else
				# Check visitor already voted for event category or not
				if VisitorVote.where(:event_category_id => params[:visitor_vote][:event_category_id],:user_id => params[:visitor_vote][:user_id]).present?
					return render error_response("Already voted", 401)
				else
					VisitorVote.create(params[:visitor_vote])
				end
			end
			render success_response("Succesfully voted", 200)
		end		
	end

	def announce_event_category_results
		event_category = EventCategory.find params[:id]
		data = new_hash
		main_array = []		
		data["id"] = event_category.try(:id)
		data["category_name"] = event_category.try(:category).try(:name)
		data["event_id"] = event_category.try(:event_id)
		data["is_published"] = event_category.try(:is_published).present? ? event_category.try(:is_published) : false
		if event_category.try(:is_published)
			main_array = close_event_category_method(params[:id], "view_results")
		end
		judge_voted_array = find_voted_judges(params[:id])
		data["category_image_url"] = event_category.try(:category).try(:attachment).try(:attachment).url(:small)
		data["announced_results"] = main_array
		data["voted_judges"] = judge_voted_array
		# Event banner to display in all event related pages.
		# event_obj = event_category.try(:event)
		# data["is_event_banner"] = event_obj.try(:is_event_banner)
		# data["event_banner_url"] = event_obj.try(:event_banner).present? ? event_obj.try(:event_banner).try(:event_banner).url(:original) : ""

		render success_response_with_object("Success", 200, data)
	end

	def find_voted_judges(event_category_id)
		judge_voted_array = []
		event_category = EventCategory.find_by_id(event_category_id)
		total_judges = event_category.try(:event).try(:event_judge_codes)
		judges_voted = event_category.try(:judge_votes).where("user_id IS NOT NULL").pluck(:user_id).uniq.compact
		non_voted_judge_ids = (total_judges.pluck(:user_id).uniq - judges_voted).compact
		all_judges = User.includes(:attachment).where(:id=>(judges_voted + non_voted_judge_ids).sort)
        vote_status = ""
        # Check if any ties for a category
        top_three_results, is_tie = fetch_top_results(event_category)
        all_judges.includes(:attachment).each do |judge_user|
          hash = new_hash
          title = "#{judge_user.try(:last_name)}, #{judge_user.try(:first_name)}"
          if judges_voted.include?(judge_user.try(:id)) && !is_tie
          	hash["color"] = "green"
          else
          	hash["color"] = "red"
          end
          hash["image_url"] = judge_user.try(:attachment).try(:attachment).url(:small)
          hash["title"] = title
          judge_voted_array.push(hash)
        end
        return judge_voted_array
        # vote_status.present? ? vote_status.html_safe : "There are no judges for this event"
	end

	def validate_category_name
		event = Event.find_by_id(params[:event_id])
		is_present = event.categories.pluck(:id).include?(params[:id].to_i)
		if is_present
			render :json => {"status"=>"true","message"=>"This category has already been taken for this event","status_code"=>400}
		else
			render :json => {"status"=>"false","message"=>"Success","status_code"=>200}
		end
		rescue => error    
      		render :json => {"status"=>"true","message"=>"Internal server error has occured","status_code"=>500}
	end

	def close_event_category
		if params[:app_version] == "v1.1"
			event_category = EventCategory.find_by_id(params[:id])
			tot_judges_voted = event_category.try(:judge_votes).pluck(:user_id).uniq
			tot_event_judges = event_category.try(:event).try(:event_judge_codes)

			if (tot_judges_voted.try(:count).to_i <= 0)
				render :json => {"status"=>"false","message"=>"No judge has voted for this category","status_code"=>400}
			elsif (tot_judges_voted.try(:count).to_i > 0) && (tot_judges_voted.try(:count).to_i < tot_event_judges.try(:count).to_i)
				left_judges = tot_event_judges.try(:count).to_i - tot_judges_voted.try(:count).to_i
				if (left_judges == 1)
					render :json => {"status"=>"true","message"=>"Are you sure you want to close this category? #{left_judges} judge has not finished voting.","status_code"=>201}
				else
					render :json => {"status"=>"true","message"=>"Are you sure you want to close this category? #{left_judges} judges have not finished voting.","status_code"=>201}
				end
			else
				main_array = close_event_category_method(params[:id], "close_results")
				if main_array.present?
					render :json => {"status" => "true", "message" => "Success", "status_code" => 200, "data" => main_array}
				else
					render :json => {"status"=>"false","message"=>"Judging is not closed yet","status_code"=>400}
				end
			end
		else
			main_array = close_event_category_method(params[:id], "close_results")
			if main_array.present?
				render success_response_with_object("Success", 200, main_array)
			else
				render :json => {"status"=>"false","message"=>"Judging is not closed yet","status_code"=>400}
			end
		end
		rescue => error    
      		render :json => {"status"=>"false","message"=>"Internal server error has occured","status_code"=>500}
	end

	def force_close_judging_by_mc
		# event_category = EventCategory.find_by_id(params[:id])		
		main_array = close_event_category_method(params[:id], "close_results")
		if main_array.present?
			render :json => {"status" => "true", "message" => "Success", "status_code" => 200, "data" => main_array}
			# render success_response_with_object("Success", 200, main_array)
		else
			render :json => {"status"=>"false","message"=>"There is a tie. You can not close a category until the tie is broken by the judges.","status_code"=>400}
		end
	end

	def close_event_category_method(event_category_id,result_status)
		event_category = EventCategory.find event_category_id
		main_array = []		
		top_three_results, is_tie = fetch_top_results(event_category)
		if params[:action] == "force_close_judging_by_mc" && !top_three_results.present? && !is_tie
			EventCategory.close_judge_voting(event_category)
		end
		if result_status == "close_results" && top_three_results.present? && !is_tie
			cat_event = EventCompetitorResult.close_event_category(event_category)
		end
		# We are calling this method twice (7 lines above), because after implementing confirm message for force close th judging if any judge has not voted.
		top_three_results, is_tie = fetch_top_results(event_category)
		if params[:action] == "force_close_judging_by_mc" && top_three_results.present? && !is_tie
			cat_event = EventCompetitorResult.close_event_category(event_category)
		end

		top_three_results.includes(:event_competitor=>[:event_category_registration,:competitor=>[:user]]).rotate(1).each do |top_three_result|
			data = new_hash
			event_category_registration = top_three_result.try(:event_competitor).try(:event_category_registration)
			event_competitor = top_three_result.try(:event_competitor)
			user = top_three_result.try(:event_competitor).try(:competitor).try(:user)
			data["badge_id"] = event_category_registration.try(:number)
			data["first_name"] = user.try(:first_name)
			data["last_name"] = user.try(:last_name)

			img_url = (event_category_registration.try(:event_photo).to_s.include?("missing") || event_category_registration.blank?) ? (user.try(:attachment).try(:attachment).present? ? user.try(:attachment).try(:attachment).url(:small) : nil) : event_category_registration.try(:event_photo).url(:small)
			data["competitor_image_url"] = img_url.present? ? img_url : nil

			data["rank"] = EventCompetitor.ordinal_suffix_of(top_three_result.try(:position))
			data["score"] = top_three_result.try(:avg_judge_results)
			main_array.push(data)
		end if (top_three_results.present? && !is_tie)
		return main_array
	end

	def fetch_top_results(event_category)
		top_results = EventCompetitorResult.where(:event_category_id=>event_category.try(:id), :avg_visitor_results => 0).order(:position)
		is_tie = false
		if top_results.present?
			top_three_results = top_results.limit(3)
			top_four_ranks = top_results.limit(4).pluck(:position)
		else
			top_three_results = nil
			top_four_ranks = nil
		end
		is_tie = top_four_ranks.present? ? (top_four_ranks.uniq.length != top_four_ranks.length) : is_tie
		return top_three_results, is_tie
	end

	def all_judges_broken_ties(all_judges_multiple_ties,tie_category,tie_position)
		delete_later_ids = all_judges_multiple_ties.pluck(:id)
		all_comps = all_judges_multiple_ties.pluck(:event_competitor_id)
		all_comp_ids = all_comps.uniq
		occurance = all_comp_ids.max_by{ |i| all_comps.count( i ) }

		all_comp_ids.each do |comp_id|
			all_comp_tie_records = all_judges_multiple_ties.where(:event_competitor_id=>comp_id,:position=>tie_position)
			single_record = all_comp_tie_records.try(:first)
			avg_rank = all_comp_tie_records.average(:rank)
			avg_score = single_record.try(:score)
			ActiveRecord::Base.transaction do
				total_juds_count = tie_category.try(:event).try(:event_judge_codes).try(:count)
				if (tie_position == 3 && occurance == comp_id && total_juds_count.odd?)
					avg_rank = avg_rank + 0.1
					avg_score = avg_score + 0.03
				end
				tie_category.multiple_ties.create!(:rank=>avg_rank,:score=>avg_score,:event_competitor_id=>comp_id,:user_id=>@user.try(:id), :position=>single_record.try(:position))
			end
		end

		# if tie_position == 3
		# 	max_occurance = all_judges_multiple_ties.max_by { |v| freq[v] }
		# 	final_update = tie_category.multiple_ties.find_by_event_competitor_id_and_position(max_occurance,tie_position)
		# 	final_update.update_attributes(:rank=>)
		# end

		delete_now = all_judges_multiple_ties.where(:id=>delete_later_ids)
		delete_now.delete_all
		tie_category.update_attributes(:tie_position=>tie_position)
	end

	def submit_tie_breaker
		#NEW CODE STARTS HERE
		begin
			tie_category = EventCategory.find_by_id(params[:event_category_id])
			tie_breaker_results = params[:tie_breaker_results].sort_by { |k| k["position"] }
			
			if params[:user][:app_version] == "v1.1.0"
				# multiple_ties_already_present = tie_category.multiple_ties.where(:position=>tie_breaker_results[0]['position'].to_i)
				if tie_category.tie_position >= 0 && tie_category.try(:tie_position) != tie_breaker_results[0]['position'].to_i # If round 1 IS DONE for any tie position
					tie_breaker_results.each do |obj|
						evt_comp_result = EventCompetitorResult.find_by_id(obj[:id])
						# tie_category.multiple_ties.create!(:rank=>obj[:position],:score=>obj[:avg_judge_results],:event_competitor_id=>evt_comp_result.try(:event_competitor_id),:user_id=>@user.try(:id), :position=>tie_breaker_results[0]['position'].to_i)
						mul_ti_obj = tie_category.multiple_ties.find_or_create_by_event_competitor_id_and_user_id_and_position(evt_comp_result.try(:event_competitor_id),@user.try(:id),tie_breaker_results[0]['position'].to_i)
						mul_ti_obj.update_attributes(:rank=>obj[:position],:score=>obj[:avg_judge_results])
					end

					# Adds new fields at the last automatically when the last judges finishes Tie breaker and deletes the old multiple ties values
					total_judges_count = tie_category.try(:event).try(:event_judge_codes).try(:count)
					
					tie_broken_by_judges_count = tie_category.multiple_ties.where(:position=>tie_breaker_results[0]['position'].to_i).pluck(:user_id).uniq.try(:count)
					# is_judge_voted = false

					is_j_voted = false
					is_judge_voted = MultipleTy.find_by_user_id_and_event_category_id_and_position(@user.try(:id),tie_category.try(:id),tie_breaker_results[0]['position'].to_i)
					
					if (total_judges_count == tie_broken_by_judges_count)
						all_judges_multiple_ties = tie_category.multiple_ties.where(:position=>tie_breaker_results[0]['position'].to_i)
						all_judges_broken_ties(all_judges_multiple_ties,tie_category,tie_breaker_results[0]['position'].to_i)
						is_all_judges_voted = true
						is_j_voted = is_judge_voted.present? ? true : is_j_voted
						is_tie,ties_arr = EventCompetitor.fetch_round_1_tied_competitors(tie_category,tie_breaker_results[0]['position'].to_i)
					else #if (total_judges_count > tie_broken_by_judges_count)
						# is_judge_voted = true
						is_j_voted = is_judge_voted.present? ? true : is_j_voted
						is_tie = false
						ties_arr = []
						is_all_judges_voted = false
					# else
					# 	is_tie = false
					# 	ties_arr = []
					# 	is_all_judges_voted = true						
					end

					is_judge_voting_done = JudgeVote.find_by_user_id_and_event_category_id(@user.try(:id),tie_category.try(:id))
					if is_j_voted && !is_judge_voting_done.present? && tie_category.event_competitor_results.where(:avg_visitor_results=>0).present? && !is_tie
						is_j_voted = true
					end

					comp_arr = get_tied_user_results(ties_arr,tie_category)
					render :json => {"status"=>"true","message"=>"Tied Competitor Details","status_code"=>200, "is_judge_voted"=>is_j_voted, "is_published"=>false, "is_all_judges_voted"=>is_all_judges_voted, "is_tie"=>is_tie, "ties_arr" => comp_arr, :tie_position=>tie_breaker_results[0]['position'].to_i}
				else # If round 1 IS NOT DONE for any tie position
					tie_category.update_attributes(:tie_position=>tie_breaker_results[0]['position'])
					tie_breaker_results.each do |obj|
						evt_comp_result = EventCompetitorResult.find_by_id(obj[:id])
						evt_comp_result.update_attributes(:avg_judge_results=>obj[:avg_judge_results],:position=>obj[:position])			
						#if obj[:position]==3 or obj[:position]==2
						if obj[:position]<=3
							rest_first_place_comps = EventCompetitorResult.where(:id=>params[:all_ids] - [obj[:id].to_i]).where(:position=>obj[:position])
							rest_first_place_comps.update_all(:position=>obj[:position]+1)
						end
					end
					is_tie, ties_arr = EventCompetitor.fetch_tied_competitors(params[:event_category_id])
					if !is_tie
						is_j_voted = true
						is_all_judges_voted = true
					end
					render :json => {"status"=>"true","message"=>"Tie has been broken","is_tie"=>is_tie, "is_judge_voted"=>is_j_voted, "is_all_judges_voted"=>is_all_judges_voted, "is_published"=>false, "ties_arr" => ties_arr,"status_code"=>200, :tie_position=>tie_breaker_results[0]['position'].to_i}
				end

			else
				if tie_category.tie_position < tie_breaker_results[0]['position'].to_i
					tie_category.update_attributes(:tie_position=>tie_breaker_results[0]['position'])
					tie_breaker_results.each do |obj|
						evt_comp_result = EventCompetitorResult.find_by_id(obj[:id])
						evt_comp_result.update_attributes(:avg_judge_results=>obj[:avg_judge_results],:position=>obj[:position])			
						#if obj[:position]==3 or obj[:position]==2
						if obj[:position]<=3
							rest_first_place_comps = EventCompetitorResult.where(:id=>params[:all_ids] - [obj[:id].to_i]).where(:position=>obj[:position])
							rest_first_place_comps.update_all(:position=>obj[:position]+1)
						end
					end
					is_tie, ties_arr = EventCompetitor.fetch_tied_competitors(params[:event_category_id])
					render :json => {"status"=>"true","message"=>"Tie has been broken","is_tie"=>is_tie,"status_code"=>200}
				else
					is_tie, ties_arr = EventCompetitor.fetch_tied_competitors(params[:event_category_id])
					render :json => {"status"=>"true","message"=>"Tie has already been broken by another judge","is_tie"=>is_tie,"status_code"=>200}
				end
			end

	    # HAVE IMPOR # have to revert the hide #
		rescue => exception 
			render :json => {"status"=>"false","message"=>"Internal server error","status_code"=>500}
		end
		#NEW CODE ENDS HERE

		# PREVIOUS CODE STARTS HERE
		# evt_comp_result = EventCompetitorResult.find_by_id(params[:id])
		# if evt_comp_result.present?
		# 	evt_comp_result.update_attributes(:avg_judge_results=>params[:score])
		# 	rest_first_place_comps = EventCompetitorResult.where(:id=>params[:all_ids] - [params[:id].to_i])
		# 	rest_first_place_comps.update_all(:position=>2)
		# 	render :json => {"status"=>"true","message"=>"Tie has been broken","status_code"=>200}
		# else
		# 	render :json => {"status"=>"true","message"=>"Empty values have been submitted","status_code"=>200}
		# end
		# PREVIOUS CODE ENDS HERE
	end

	# Fetch all the competitors who have become tie for an event category
	def fetch_tied_competitors
		event_category_id = event_category_id || params[:event_category_id]
		evnt_cat = EventCategory.find_by_id(event_category_id)
		is_tie, ties_arr = EventCompetitor.fetch_tied_competitors(event_category_id)
		comp_arr = get_tied_user_results(ties_arr,evnt_cat)

		# event_obj = evnt_cat.try(:event)
		# is_event_banner = event_obj.try(:is_event_banner)
		# event_banner_url = event_obj.try(:event_banner).present? ? event_obj.try(:event_banner).try(:event_banner).url(:original) : ""
		# render :json => {"status"=>"true","message"=>"Tied Competitor Details","status_code"=>200, "is_tie"=>is_tie, "ties_arr" => comp_arr, "is_event_banner"=>is_event_banner,"event_banner_url"=>event_banner_url, "category_name"=>evnt_cat.try(:category).try(:name)}
		render :json => {"status"=>"true","message"=>"Tied Competitor Details","status_code"=>200, "is_tie"=>is_tie, "ties_arr" => comp_arr, "category_name"=>evnt_cat.try(:category).try(:name)}
	end

	def get_tied_user_results(ties_arr,evnt_cat)
		comp_arr = []
		ties_arr.each do |evnt_comp_res|
			competitor = evnt_comp_res.try(:event_competitor).try(:competitor)
			user = competitor.try(:user)
			tie_hsh = evnt_comp_res
			tie_hsh["user_details"] = new_hash
			tie_hsh["user_details"]["first_name"] = user.try(:first_name)
			tie_hsh["user_details"]["last_name"] = user.try(:last_name)			
			comp_img_url = EventCategoryRegistration.where(:user_id=>user.try(:id), :category_id=>evnt_cat.try(:category_id), :event_id=>evnt_cat.try(:event_id))
			tie_hsh["user_details"]["number"] = comp_img_url.try(:first).try(:number)
			tie_hsh["user_details"]["member_id"] = comp_img_url.try(:first).try(:number)
			img_url = (comp_img_url.try(:first).try(:event_photo).to_s.include?("missing") || comp_img_url.empty?) ? (competitor.try(:user).try(:attachment).try(:attachment).present? ? competitor.try(:user).try(:attachment).try(:attachment).url(:small) : nil) : comp_img_url.try(:last).try(:event_photo).present? ? comp_img_url.try(:last).try(:event_photo).url(:small) : nil
			tie_hsh["user_details"]["competitor_image_url"] = img_url.present? ? img_url : nil
			comp_arr.push(tie_hsh)
		end
	end

    # This method used for get the competitors list for particular event
	def get_event_competitors_list(get_event_competitors, data, img_size)
		event_competitor_array = []
		get_event_competitors.includes(:event_category,:event_category_registration,:competitor=>[:user,:club=>[:attachment]]).where("event_competitors.competitor_id IS NOT NULL").each do |event_competitor|
			# Get the competitor
			competitor = event_competitor.try(:competitor)
				data = competitor
				evnt_cat = event_competitor.try(:event_category)
				# comp_img_url = EventCategoryRegistration.where(:user_id=>competitor.user.id, :category_id=>evnt_cat.category_id, :event_id=>evnt_cat.event_id)
				comp_img_url = event_competitor.try(:event_category_registration)
				user = competitor.try(:user)
				# Get the first name of competitor
				data["first_name"] = user.try(:first_name).try(:capitalize)
				data["last_name"] = user.try(:last_name).try(:capitalize)
				data["member_id"] = comp_img_url.try(:number) #competitor.user.try(:card_id)				
				data["number"] = comp_img_url.try(:number) if comp_img_url.present?
				
				# Get the competitor image url
				checkin_img = (comp_img_url.try(:event_photo).to_s.include?("missing") || comp_img_url.nil?)
				user_dp = competitor.try(:user).try(:attachment).try(:attachment)				
				if (img_size == "medium")
					img_url = checkin_img ? (user_dp.present? ? user_dp.url(:medium) : nil) : comp_img_url.try(:event_photo).url(:medium)
				else
					img_url = checkin_img ? (user_dp.present? ? user_dp.url(:medium) : nil) : comp_img_url.try(:event_photo).url(:small)
				end
				data["competitor_image_url"] = img_url.present? ? img_url : nil
				data["comp_hometown"] = competitor.try(:city)

				# Old code for pulling competitor image url always
				# Get the competitor image url
				# data["competitor_image_url"] = competitor.user.attachment ? "#{AppConfig.image_url}" + competitor.user.attachment.attachment.url(:original) : nil

				# Get the competitor club image url
				user_club = competitor.try(:club)
				data["competitor_club_affiliation"] = user_club.try(:name)
				data["competitor_club_image_url"] = user_club.try(:attachment).present? ? user_club.try(:attachment).try(:attachment).url(:small) : "#{AppConfig.image_url}" + "/images/noClubIcon.png"
				# Get the event competitor id
				data["event_competitor_id"] = event_competitor.try(:id)
				data["id"] = event_competitor.try(:id)
			event_competitor_array.push(data)
		end
		return event_competitor_array
	end

	# This method used for get the event category details
	def get_category_details(category, data_hash)
		# Get the category
		data_hash=category
		# Get the category image url
		data_hash["category_image_url"]=category.attachment ? category.attachment.attachment.url(:small) : nil
		# Get the parent category name
		data_hash["parent_category_name"] = category.main_category.name
		return data_hash
	end

	def generate_competitor_position
		avg_jud_res = params[:avg_jud_res]
		evt_cat_id = params[:evt_cat_id]
		event_category = EventCategory.find_by_id(params[:evt_cat_id])
		all_judge_results = event_category.event_competitor_results.where(:avg_visitor_results=>0).order("position ASC")
		current_user_rank = all_judge_results.where("avg_judge_results > ?",avg_jud_res).size + 1
		if (current_user_rank > 0)
			render :json => {"status"=>true, :position=>current_user_rank}
		else
			render :json => {"status"=>false}
		end
	end

	# def delete_sathu
	# 	EventCompetitorResult.where(:event_category_id=>[414,412]).delete_all
	# 	JudgeVote.where(:user_id=>[3359,807]).delete_all
	# 	VisitorVote.where(:event_category_id=>[414,412]).delete_all
	# 	EventCategory.where(:id=>[414,412]).update_all(:is_published=>nil)
	# 	render :nothing=>true
	# end
end