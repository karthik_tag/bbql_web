ActiveAdmin.register Charity do
	menu :parent => "Administration", :url => "#{AppConfig.url}/charities", :if => proc{ User.toggle_main_menu(current_admin_user) }
    config.sort_order = "name_asc"
	index do
		column :name
		column :link
        column "ABN Number", :abn_number
		default_actions
        paginated_collection(collection, download_links: false, entry_name: "Charities")
	end
    
    csv force_quotes: true, col_sep: ',' do
        column :name
        column :link
        column :abn_number
    end

	sidebar "Charity Image", :only => :show do
    	attributes_table_for charity do
    	  row(" ") do
    	    ul :class=>"left-algin-fix" do
    	      img = charity.attachment
    	      unless img.try(:attachment_file_name).nil?
    	        image_tag ("#{AppConfig.s3_url}/attachments/#{img.id}/small/"+img.attachment_file_name)
    	      else
                "There is no charity image"
              end
    	    end
    	  end
    	end
  	end
	show do
    	panel "Charity Details" do
    		attributes_table_for charity, :name, :link, :contact_name, :contact_phone_number, :abn_number

            attributes_table_for charity do                
                competitors = Competitor.where(:charity_id => charity.id)
                competitors_list = competitors.map { |competitor| link_to(competitor.try(:nick_name).try(:capitalize), admin_user_path(competitor.user))} if competitors.present?
                row("Users associated with this Charity") {                    
                    if (competitors_list.present?)
                        safe_join competitors_list, '<br>'.html_safe
                    else
                      "None"
                    end
                }             
            end

    	end
  	end

	filter :name
	filter :link
    filter :abn_number

	form do |f|
    	f.inputs "Charity Details" do
			f.input :name
			f.input :link
            f.input :contact_name
            f.input :contact_phone_number
            f.input :abn_number, :label => "ABN Number"
			f.inputs for: [:attachment, f.object.attachment || Attachment.new] do |charity|
        		charity.input :attachment, as: :file, :input_html => {:onchange => "validate_file_field($(this).attr('id'))"}, :hint => charity.template.image_tag(charity.try(:object).try(:attachment_file_name).present? ? "#{AppConfig.s3_url}/attachments/#{charity.object.try(:id)}/small/"+charity.try(:object).try(:attachment_file_name) : "#{AppConfig.image_url}" + "/attachments/small/missing.png")
        		charity.input :_destroy, as: :boolean, :label => "Do you want to remove attachment?"
			end
    	end    	  
    	f.actions
  	end
end
