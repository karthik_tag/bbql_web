ActiveAdmin.register EventFanVoting do
	# menu :parent => "events", :url => "#{AppConfig.url}/event_fan_votings"
  menu :parent => "events", :url => "#{AppConfig.url}/all_events?order=events.from_date_asc&route=evt_fan_vot&event_type=all"
	actions :all, except: [:new, :edit, :destroy, :show]
	config.clear_sidebar_sections!
	scope :joined, :default => true do |event_fan_voting|
  		event_fan_voting.includes(:event).where("events.status not in ('Pending','Rejected')")
	end

	index :download_links => false do
		column "Events", :event do |event_fan_voting|
    	  link_to event_fan_voting.event.try(:name), admin_event_fan_voting_results_path("event_id" => event_fan_voting.try(:event))
    end

    column "Event Date", :event_date do |event_fan_voting|
      event_fan_voting.try(:event).try(:from_date).strftime('%A, %B %d, %Y') if event_fan_voting.try(:event).try(:from_date).present?
    end

    # default_actions
    paginated_collection(collection, download_links: false, entry_name: "Event Fan Votings")
	end

	show do
    panel "Event Fan Voting" do
    	attributes_table_for event_fan_voting, :id
    	attributes_table_for event_fan_voting do
      	row("Event") { link_to(event_fan_voting.try(:event).try(:name), admin_event_path(event_fan_voting.try(:event))) }
    	end
      attributes_table_for event_fan_voting do
        row("Event Date") { event_fan_voting.try(:event).try(:from_date).strftime('%A, %B %d, %Y') }
      end
    end
  end
end
