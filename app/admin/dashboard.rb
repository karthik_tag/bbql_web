ActiveAdmin.register_page "Dashboard" do

  menu :label => proc{ I18n.t("active_admin.dashboard") }
  menu :priority => 1, :url => "#{AppConfig.url}/dashboard", :if => proc{ User.toggle_main_menu(current_admin_user) }


  # menu :priority => 1, :label => proc{ I18n.t("active_admin.dashboard") }

  content :title => proc{ I18n.t("active_admin.dashboard") } do
    # div :class => "blank_slate_container", :id => "dashboard_default_message" do
    #   span :class => "blank_slate" do
    #     span I18n.t("active_admin.dashboard_welcome.welcome")
    #     small I18n.t("active_admin.dashboard_welcome.call_to_action")
    #   end
    # end

    all_charities = Charity.order("name ASC")
    all_clubs = Club.order("name ASC")
    # all_events = Event.order("name ASC")
    scheduled_events = Event.where(:status=>"Upcoming").order("from_date ASC")
    historical_events = Event.where(:status=>"Completed").order("from_date DESC")
    all_main_categories = MainCategory.order("name ASC")
    all_categories = Category.order("name ASC")


    columns do

      column do
        panel "Charities" do
          ul do
            all_charities.map do |charity|
              li link_to(charity.name, admin_charity_path(charity))
            end
          end
        end
      end if all_charities.present?

      column do
        panel "Clubs" do
          ul do
            all_clubs.map do |club|
              li link_to(club.name, admin_club_path(club))
            end
          end
        end
      end if all_clubs.present?

      column  do
        panel "Scheduled Events", class: "panel-dateLink" do
          ul do
            li "<u><b>Event Date</u> &nbsp;&nbsp;<u>Event Name</b></u><br>".html_safe 
            scheduled_events.map do |event|
            li ("<label> #{event.from_date.strftime('%m/%d/%Y')} </label> &nbsp;&nbsp;" + "#{link_to(event.name, admin_event_path(event))}").html_safe              
            end
          end
        end
      end if scheduled_events.present?

      column do
        panel "Completed Events" do
          ul do
            historical_events.map do |event|
              li link_to(event.name, admin_event_path(event))
            end
          end
        end
      end if historical_events.present?

      column do
        panel "Main Categories" do
          ul do
            all_main_categories.map do |main_category|
              li link_to(main_category.name, admin_main_category_path(main_category))
            end
          end
        end
      end if all_main_categories.present?

      # column do
      #   panel "Categories" do
      #     ul do
      #       all_categories.map do |category|
      #         li link_to(category.name, admin_category_path(category))
      #       end
      #     end
      #   end
      # end if all_categories.present?

    end

    # Here is an example of a simple dashboard with columns and panels.
    #
    # columns do
    #   column do
    #     panel "Recent Posts" do
    #       ul do
    #         Post.recent(5).map do |post|
    #           li link_to(post.title, admin_post_path(post))
    #         end
    #       end
    #     end
    #   end

    #   column do
    #     panel "Info" do
    #       para "Welcome to ActiveAdmin."
    #     end
    #   end
    # end
  end # content

  controller do
        def index
          if current_admin_user.try(:is_checkin_manager) && !["admin@bbql.com","brett@facialhairleague.com","brett@fhl.com"].include?(current_admin_user.try(:email))
            path = current_admin_user.try(:event_ids).reject(&:empty?).count.eql?(1) ? event_checkin_mobile_checkin_path(current_admin_user.try(:event_ids).try(:last)) : mobile_checkins_path
            redirect_to path
          end
        end
    end
end
