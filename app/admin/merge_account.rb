ActiveAdmin.register MergeAccount do
	menu :parent => "Administration", :url => "#{AppConfig.url}/merge_accounts", :if => proc{ User.toggle_main_menu(current_admin_user) }
	config.clear_sidebar_sections!
	actions :all, except: [:show,:edit]

	index do
		column :actual_account do |merge_account|
			if merge_account.try(:actual_account).present?
				link_to merge_account.try(:actual_account).try(:email), admin_user_path(merge_account.try(:actual_account))
			else
				"None"
			end
    	end
		# column :duplicate_account
    	default_actions
      	paginated_collection(collection, download_links: false, entry_name: "Merge Accounts")
	end

	csv force_quotes: true, col_sep: ',' do
	    column :actual_account
	    # column :duplicate_account
  	end

  #   show do
		# panel "Merge Account Details" do
  #       	attributes_table_for merge_account, :actual_account, :duplicate_account
  #       end
  #   end

	form do |f|
	  	f.inputs "Event Judge Details" do
	  	  users_array = User.where(:user_type_id=>2).joins(:competitor).order("last_name asc,first_name asc,email asc").collect{|obj| ["#{obj.try(:last_name).try(:capitalize)}, #{obj.try(:first_name).try(:capitalize)}, #{obj.try(:email)}",obj.id]}
	      f.input :actual_account_id, :as => :select, :collection => users_array, :include_blank => "Please select actual account"
	      f.input :duplicate_account_id, :as => :select, :collection => users_array, :include_blank => "Please select duplicate account"
	  	end
	  	f.actions
  	end

  	controller do |merge_account|
	    def create
			super do |format|
				actual_account_user = User.find_by_id(params[:merge_account][:actual_account_id])
				actual_account_competitor = actual_account_user.try(:competitor)
				duplicate_account_user = User.find_by_id(params[:merge_account][:duplicate_account_id])
				duplicate_account_competitor = duplicate_account_user.try(:competitor)

				ActiveRecord::Base.transaction do
					# event_category_registration -> user_id, competitor_id
					dup_evt_cat_regs = EventCategoryRegistration.where(:user_id=>duplicate_account_user.try(:id),:competitor_id=>duplicate_account_competitor.try(:id))
					dup_evt_cat_regs.update_all(:user_id=>actual_account_user.try(:id),:competitor_id=>actual_account_competitor.try(:id)) if dup_evt_cat_regs.present?
					
					# event_competitors -> competitor_id
					dup_evt_comps = EventCompetitor.where(:competitor_id=>duplicate_account_competitor.try(:id))
					dup_evt_comps.update_all(:competitor_id=>actual_account_competitor.try(:id)) if dup_evt_comps.present?

					# event_judge_codes -> user_id
					dup_evt_jud_codes = EventJudgeCode.where(:user_id=>duplicate_account_user.try(:id))
					dup_evt_jud_codes.update_all(:user_id=>actual_account_user.try(:id)) if dup_evt_jud_codes.present?

					# event_mcs -> user_id
					dup_evt_mcs = EventMc.where(:user_id=>duplicate_account_user.try(:id))
					dup_evt_mcs.update_all(:user_id=>actual_account_user.try(:id)) if dup_evt_mcs.present?
					
					# events -> user_id (event_created_by)
					dup_events = Event.where(:user_id=>duplicate_account_user.try(:id))
					dup_events.update_all(:user_id=>actual_account_user.try(:id)) if dup_events.present?

					# If the crash occurs in judge vote and fan vote, it may be because the merged votes by dupli and orig accounts
					# judge_votes -> user_id (judges voting by the duplicate user)
					dup_judge_votes = JudgeVote.where(:user_id=>duplicate_account_user.try(:id))
					dup_judge_votes.update_all(:user_id=>actual_account_user.try(:id)) if dup_judge_votes.present?

					# visitor_votes -> user_id (fan voting by the duplicate user)
					dup_fan_votes = VisitorVote.where(:user_id=>duplicate_account_user.try(:id))
					dup_fan_votes.update_all(:user_id=>actual_account_user.try(:id)) if dup_fan_votes.present?

					# #Move all the competitor images of duplicate account to actual account
					dup_comp_images = duplicate_account_user.try(:competitor).try(:attachments)
					image_updated = dup_comp_images.update_all(:attachable_id=>actual_account_competitor.try(:id)) if dup_comp_images.present?					
					
					# # #Delete the duplicate account
					duplicate_email_id = duplicate_account_user.try(:email)
					duplicate_account_user.delete

					# #Sending mail to actual account after the deleting the duplicate account
					Thread.new{UserMailer.duplicate_account_deletion(actual_account_user,duplicate_email_id).deliver}
					
					
				end
				redirect_to admin_merge_accounts_path and return if resource.valid?
			end
	    end
	end

end
