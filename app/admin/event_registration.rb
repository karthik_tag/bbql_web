ActiveAdmin.register EventRegistration do
	# menu :parent => "events", :url => "#{AppConfig.url}/event_registrations"
  menu :parent => "events", :url => "#{AppConfig.url}/all_events?order=events.from_date_asc&route=evt_reg&event_type=all"
	actions :all, except: [:show, :new, :edit, :destroy]
	config.clear_sidebar_sections!
	scope :joined, :default => true do |event_registration|
  		# event_registration.includes [:event]
  		event_registration.includes(:event).where("events.status not in ('Pending','Rejected')")
	end

	index :download_links => false do
		column "Events", :event do |event_registration|
		  val = EventCategoryRegistration.find_checkedin_val_or_not(event)
    	link_to event_registration.try(:event).try(:name), admin_event_category_registrations_path("q[event_id_eq]" => event_registration.try(:event), "q[is_checked_in_eq]" =>val, :order =>"event_category_registrations.number_asc")
    	end

    	column "Event Date" do |event_registration|
		  event_registration.try(:event).try(:from_date).strftime('%A, %B %d, %Y')
    	end

    	actions defaults: false do |event_registration|
          link_to 'Register A User For This Event', admin_users_path(:event_id=>event_registration.event)
        end
        paginated_collection(collection, download_links: false, entry_name: "Event Registrations")
	end
end