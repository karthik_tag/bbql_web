ActiveAdmin.register Event do
    menu :url => "#{AppConfig.url}/events?q[status_eq]=#{Event.where(:status=>'Upcoming').count > 0 ? 'Upcoming' : Event.where(:status=>'OnGoing').count > 0 ? 'OnGoing' : 'Completed'}", :if => proc{ User.toggle_main_menu(current_admin_user) }
	config.sort_order = "from_date_asc"
  scope :joined, :default => true do |event|
      event.includes [:user,:competitors]
  end
    # To Make the events Launch Event
 	member_action :go_live do #, :method => :put do
 	  event = Event.find params[:id]
      params.merge!({"q[status_eq]" => "Upcoming"})
 	  event.update_attributes(:status => "OnGoing")
    ActionController::Base::EventsController.new.push_notification_on_event_launch(event)
 	  redirect_to admin_events_path(params)
 	end
 	# To Make the events completed
 	member_action :wind_up do #, :method => :put do
 	  event = Event.find params[:id]
      params.merge!({"q[status_eq]" => "OnGoing"})
 	  # event.update_attributes(:status => "Completed")

    #   ##Push Notification to all app users to announce the event close notification and the judges results.
    #     users_ids = UserRegId.where(:status=>true).pluck(:user_id).uniq
    #     # Android Push Notification to be sent first . Before IOS notifications. because IOS creates table records.
    #     title = "Facial Hair League"
    #     message = "#{event.try(:name).try(:strip).try(:upcase).gsub("'"){"\\'"}} has just now finished and the Official Results are live! Click here to see pictures of the winners."
    #     # users_ids = all_users
    #     # Android push notification code
    #     android_push_notification(title,message,users_ids,event.try(:id))
    #     # IOS push notification code
    #     ios_push_notification(message,users_ids,event.try(:id))

    #   #Push notification for FFH results. Will send only to the Fans who have voted for the event categories. Will send when an event is closed.
    #     event_categories_ids = event.try(:event_categories).pluck(:id)
    #     ffhl_users_ids = VisitorVote.where(:event_category_id=>event_categories_ids).pluck(:user_id).try(:compact).try(:uniq)
    #     # Find the overall fan voting results of an event
    #     overall_results_hash = EventFanVotingResult.track_overall_fan_voting_results(event.try(:id))
    #     ffhl_android_push_notification(title,ffhl_users_ids,event.try(:id),overall_results_hash)
    #     ffhl_ios_push_notification(ffhl_users_ids,event.try(:id),overall_results_hash)
    #   # ios_apns_push #hidden here and in application_controller. Is is enough to initialize once. So, moved this to initializers.rb at the EOL.

    #   event.event_registration.delete if event.try(:event_registration).present? # Need to run onetime rake to generate
 	  Event.close_event(event)
    redirect_to admin_events_path(params)
 	end
    # To Make the events Reverted to ongoing status
    member_action :postpone, :method => :put do
      event = Event.find params[:id]
      params.merge!({"q[status_eq]" => "OnGoing"})
      event.update_attributes(:status => "Upcoming")
      redirect_to admin_events_path(params)
    end
    # To accept the user requested events. From pending status to upcoming status
    member_action :accept, :method => :put do
      event = Event.find params[:id]
      params.merge!({"q[status_eq]" => "Pending"})
      event.update_attributes(:status => "Upcoming")
      redirect_to admin_events_path(params)
    end

    # To reject the user requested events. From pending status to rejected status
    member_action :reject, :method => :put do
      event = Event.find params[:id]
      params.merge!({"q[status_eq]" => "Pending"})
      event.update_attributes(:status => "Rejected")
      redirect_to admin_events_path(params)
    end

	index do
        section :style => 'float:left;margin-right:20px;' do
          div do
              render "upcoming_events"
          end
        end
        section :style => 'float:left;margin-right:20px;' do
          div do
            render "inprogress_events"
          end
        end
        section :style => 'float:left;margin-right:20px;' do
          div do
            render "completed_events"
          end
        end
        section :style => 'float:left;margin-right:20px;' do
          div do
            render "pending_events"
          end
        end
		column :name
		column "Event Date", :from_date
		column "Venue<br>Name".html_safe, :venue_name
    column "Address", :address_line_1
		column :city
		column :state
		column :country
		column :status, :sortable => 'events.status' do |event|
    	status_tag (event.status == "OnGoing" ? "In Progress" : event.status), ((event.status == "OnGoing" || event.status == "Upcoming") ? :ok : :error)
    end
    column "Requested<br>By".html_safe, :user_id, :sortable => 'users.last_name' do |event|
      event.try(:user_id).present? ? "#{event.try(:user).try(:last_name)}, #{event.try(:user).try(:first_name)}" : "None"
    end
    column "Total<br>Competitors".html_safe, :total_comps, :sortable => 'events.total_comps' do |event|
      evt_comps = EventCategoryRegistration.where(:event_id=>event.try(:id))
      evt_comps.present? ? "#{evt_comps.length}" : "0"
    end
    	events_statuses = Event.pluck(:status)
    	if (events_statuses.include?("Upcoming") || events_statuses.include?("OnGoing") || events_statuses.include?("Pending"))
    		# Launch Event links for events
			actions defaults: false do |event|
				if (event.status == "Upcoming") # && (event.from_date.to_date == DateTime.now.in_time_zone.to_date))
                    # non_chkdin_comps_catgs = EventCompetitor.check_all_registered_members_checkd_in(event)
                    # array = []
                    # non_chkdin_comps_catgs.collect {|x| array << x.try(:category).try(:name)}
                    # if (array.try(:uniq).try(:length) > 0)
                        # link_to 'Launch Event', "#", :confirm => "Please make sure to check-in all the competitors in these categories '#{array.try(:uniq).join(',')}'."
                    if (event.try(:competitors).try(:length)<=0)
                      link_to 'Launch Event', "#", :confirm => "There are no competitors registered for this event yet"
                    else
                        link_to 'Launch Event', go_live_admin_event_path(event.id,params), method: :get, :confirm => "Are you sure you want to launch the event?"
                    end
				elsif event.status == "OnGoing"
					link_to 'Close Event', wind_up_admin_event_path(event.id,params), method: :get, :confirm => "Are you sure you want to close the event?"
                elsif event.status == "Pending"
                    link_to 'Accept', accept_admin_event_path(event.id,params), method: :put, :confirm => "Are you sure you want to accept the event?"				end
			end
            actions defaults: false do |event|
                evt_cat = event.event_categories.pluck(:event_id).uniq
                jud_vot = JudgeVote.where(:event_category_id => evt_cat)
                
                if (event.status == "OnGoing" && (event.from_date.to_date >= DateTime.now.in_time_zone.to_date)) && jud_vot.present?
                    link_to 'Postpone Event', '#', :confirm => "Event has already occured & you can't postpone it."
                elsif (event.status == "OnGoing" && (event.from_date.to_date >= DateTime.now.in_time_zone.to_date))
                    link_to 'Postpone Event', postpone_admin_event_path(event.id,params), method: :put, :confirm => "Are you sure you want to postpone the event?"
                elsif event.status == "Pending"
                    link_to 'Reject', reject_admin_event_path(event.id,params), method: :put, :confirm => "Are you sure you want to Reject the event?"
                end
            end

            column :actions do |object|
                raw( %(#{link_to "View", [:admin, object]} 
                #{(link_to "Edit", [:edit, :admin, object]) }
                ))
            end
		end
		# default_actions
        paginated_collection(collection, download_links: false, entry_name: "Events")
	end

    csv force_quotes: true, col_sep: ',' do
        column :name
        column("Event Date")  do |event|
          event.from_date
        end
        column :venue_name
        column("Address")  do |event|
          event.address_line_1
        end
        column :city
        column :state
        column :country
        column("status")  do |event|
          event.status == "OnGoing" ? "In Progress" : event.status
        end
    end

	show do
        panel "What would you like to do?" do
            div do
                render "events_links_dropdowns"
            end
        end
        panel "Event Details" do
            attributes_table_for event do
                row :name
                row("Event Date") {
                  "#{event.try(:from_date).strftime("%A, %B %d, %Y %H:%M")} #{event.try(:meridian)}"
                }
                row("Time Zone") {
                    event.try(:time_zone)
                }
                row :venue_name
                row("Address") {
                    event.try(:address_line_1)
                }
            end
            attributes_table_for event, :city, :state, :country, :latitude, :longitude
            attributes_table_for event do
                row("Status") {
                    event.status == "OnGoing" ? "In Progress" : event.status
                }
                row("Categories") {
                    event_categories = event.try(:categories).order('event_categories.sort_order IS NULL, event_categories.sort_order ASC').map{ |category| link_to(category.name, admin_category_path(category)) } if event.categories.present?
                    if (event_categories.present?)
                        safe_join event_categories, '<br>'.html_safe
                    else
                      "None"
                    end
                }
                # row("Clubs") {
                #     event_clubs = event.clubs.map{ |club| link_to(club.name, admin_club_path(club)) } if event.clubs.present?
                #     if (event_clubs.present?)
                #         safe_join event_clubs, '<br>'.html_safe
                #     else
                #       "None"
                #     end
                # }
                row("Judges") {
                    event_judges = event.try(:event_judge_codes).where("user_id IS NOT NULL").map{ |event_jud| link_to("#{event_jud.try(:user).try(:last_name)}, #{event_jud.try(:user).try(:first_name)}", admin_user_path(event_jud.try(:user))) } if event.try(:event_judge_codes).present?
                    if (event_judges.present?)
                        safe_join event_judges, '<br>'.html_safe
                    else
                      "None"
                    end
                }
                row("MC") {
                    event_mcs = event.try(:event_mcs).where("user_id IS NOT NULL").map{ |event_mc| link_to("#{event_mc.try(:user).try(:last_name)}, #{event_mc.try(:user).try(:first_name)}", admin_user_path(event_mc.try(:user))) } if event.try(:event_mcs).present?
                    if (event_mcs.present?)
                        safe_join event_mcs, '<br>'.html_safe
                    else
                      "None"
                    end
                }
                row("Participate Url") {
                  if event.paticipate_url.present?
                    link_to(event.paticipate_url, "#{event.paticipate_url}", target: "_blank")
                  else
                    "None"
                  end
                }
                row("Non Profit") {
                    "#{ActionController::Base.helpers.number_to_currency(event.try(:non_profit))}"
                }
                row("Has Olympic Rules") {
                    event.is_olympic_rules ? "Yes" : "No"
                }
                row("Is Self Registration Enabled?") {
                    event.is_self_registartion_on ? "Yes" : "No"
                }
                row("Is Event Banner Enabled?") {
                    event.is_event_banner ? "Yes" : "No"
                }
                row("Is Event Launch Notification Enabled?") {
                    event.enable_notification_on_launch ? "Yes" : "No"
                }                
                row("Requester's Name") {
                    if event.try(:user).present?
                      link_to "#{event.try(:user).try(:last_name)}, #{event.try(:user).try(:first_name)}", admin_user_path(event.try(:user))
                    else
                      "None"
                    end
                }
                row("Requester's Mobile") {
                    event.try(:requester_mobile)
                }
                row("Number of categories") {
                    !event.try(:no_of_categories).present? ? event.try(:categories).count : event.try(:no_of_categories)
                }
                row("Number of Judges") {
                    event.try(:no_of_judges)
                }
                row("Number of Expected Competitors") {
                    event.try(:no_of_expected_competitors)
                }
                row("Nonprofit Cause") {
                    event.try(:nonprofit_cause)
                }
                if (current_admin_user.email == "brett@facialhairleague.com" || current_admin_user.email == "brett@fhl.com")
                  row("DELETE EVENT") {
                      link_to("Click here to delete this event", "/admin/events/#{event.id}", method: :delete, :confirm=>"All the event data will be deleted. Are you sure you want to delete this?")
                  }
                end
            end
            
        end
        panel "Requester Shipping Details" do
          if event.try(:event_requester_shipping_detail).present?
            attributes_table_for shipping_contact_details = event.try(:event_requester_shipping_detail) do
              row("Shipping Contact Name") {
                  shipping_contact_details.try(:shipping_contact_name)
              }
              row("Shipping Address Line 1") {
                  shipping_contact_details.try(:shipping_address_line_1)
              }
              row("Shipping Address Line 2") {
                  shipping_contact_details.try(:shipping_address_line_2)
              }
              row("Shipping City") {
                  shipping_contact_details.try(:shipping_city)
              }
              row("Shipping Country") {
                  shipping_contact_details.try(:shipping_country)
              }
              row("Shipping State") {
                  shipping_contact_details.try(:shipping_state)
              }
              row("Shipping Zip") {
                  shipping_contact_details.try(:shipping_zip)
              }
            end
          else
            "No data found"
          end
        end
    end

	filter :name
	filter :from_date, :label => "Event Date"
	filter :venue_name
  filter :address_line_1, :label => "Address"
	filter :city
	filter :state
	filter :country #, as: :country
	filter :status, as: :Select, collection: [['In Progress', "OnGoing"],['Upcoming', "Upcoming"],['Completed', "Completed"]]
  filter :is_olympic_rules, as: :Select, collection: [['Yes', true],['No', false]], :label => "Olympic Rules"
  filter :is_self_registartion_on, as: :Select, collection: [['Enabled', true],['Disabled', false]], :label => "Self Registration"
  filter :is_event_banner, as: :Select, collection: [['Enabled', true],['Disabled', false]], :label => "Event Banner"
  filter :enable_notification_on_launch, as: :Select, collection: [['Enabled', true],['Disabled', false]], :label => "Event Launch Notification"

	sidebar "Event Image", :only => :show do
    	attributes_table_for event do
    	  row(" ") do |sub_cat|
    	    ul :class=>"left-algin-fix" do          
    	      unless event.try(:image_file_name).nil?
    	        image_tag ("#{AppConfig.s3_url}/attachments/#{event.id}/small/"+event.image_file_name)
              else
                "There is no event image"
    	      end
    	    end         
    	  end
    	end
  	end

  	sidebar "Event Photos", :only => :show do
    	attributes_table_for event do
    	    row(" ") do |evt|
    	      ul :class=>"left-algin-fix" do          
    	        imgs = event.attachments
    	        if imgs.present?
                    imgs.each do |img|
    	                li do
    	                  unless img.try(:attachment_file_name).nil?
    	                    image_tag ("#{AppConfig.s3_url}/attachments/#{img.id}/small/"+img.attachment_file_name)
    	                  end
    	                end
    	            end
                else
                    "There are no event photos"
                end
    	      end         
    	    end
    	end
        render "add_event_photos"
  	end

    # sidebar "Club logos", :only => :show do
    #     images_arr = []
    #     attributes_table_for event do
    #         row(" ") do |evt|
    #           ul do          
    #             clubs = event.clubs
    #             if clubs.present?
    #                 clubs.each do |club|
    #                     img = club.try(:attachment)
    #                     if img.present?
    #                         unless img.try(:attachment_file_name).nil?
    #                           images_arr << "#{AppConfig.s3_url}/attachments/#{img.id}/small/"+img.attachment_file_name
    #                         end
    #                     end
    #                 end
    #             end
    #             images_arr = images_arr.compact
    #             if images_arr.present?
    #                 images_arr.each do |image_arr|
    #                     li do
    #                         image_tag (image_arr)
    #                     end
    #                 end
    #             else
    #                 "There are no club logos"
    #             end
    #           end         
    #         end
    #     end
    #     render "add_event_clubs"
    # end

    sidebar "Event Judges", :only => :show do
        images_arr = []
        attributes_table_for event do
            row(" ") do |evt|
              ul do          
                event_judges = event.event_judge_codes.pluck(:user_id)
                judges_users = User.where(:id=>event_judges)
                if judges_users.present?
                    judges_users.each do |judges_user|
                        img = judges_user.try(:attachment)
                        if img.present?
                            unless img.try(:attachment_file_name).nil?
                              images_arr << "#{AppConfig.s3_url}/attachments/#{img.id}/small/"+img.attachment_file_name
                            end
                        end
                    end
                end
                images_arr = images_arr.compact
                if images_arr.present?
                    images_arr.each do |image_arr|
                        li do
                            image_tag (image_arr)
                        end
                    end
                else
                    "There are no Judges for this event"
                end
              end
            end
        end
    end

    sidebar "Event Mcs", :only => :show do
        images_arr = []
        attributes_table_for event do
            row(" ") do |evt|
              ul do          
                event_mcs = event.event_mcs.pluck(:user_id)
                mcs_users = User.where(:id=>event_mcs)
                if mcs_users.present?
                    mcs_users.each do |mc_user|
                        img = mc_user.try(:attachment)
                        if img.present?
                            unless img.try(:attachment_file_name).nil?
                              images_arr << "#{AppConfig.s3_url}/attachments/#{img.id}/small/"+img.attachment_file_name
                            end
                        end
                    end
                end
                images_arr = images_arr.compact
                if images_arr.present?
                    images_arr.each do |image_arr|
                        li do
                            image_tag (image_arr)
                        end
                    end
                else
                    "There are no Mcs for this event"
                end
              end
            end
        end
    end

	form do |f|
  	f.inputs "Event Details" do
			f.input :name
      f.input :is_olympic_rules, :label => "Has Olympic Rules"
      f.input :is_self_registartion_on, :label => "Enable Self Registration?"
      f.input :is_event_banner, :label => "Enable Event Banner?"
      f.input :enable_notification_on_launch, :label => "Enable Notification on Launch?"
			# f.input :from_date, :as => :datepicker, :label => "Event Date" #, :as => :just_datetime_picker, :label => "Event Date"
      f.input :from_date, :as => :just_datetime_picker, :label => "Event Date"
      f.input :meridian, :as => :select, :include_blank => false, :collection => ["AM", "PM"], :input_html => {:style=>"left:450px; position:relative; top:-49px;height:29px;"}
			f.input :time_zone, :as => :time_zone, :include_blank => "Please Select a Timezone"
      f.input :venue_name
      f.input :address_line_1, :label => "Address"
			# f.input :address_line_2
			f.input :city
      f.input :country, :as => :select, :required => true, :include_blank => false, :collection => Carmen::Country.all.sort, :input_html => {
      :onchange => "getStates($(this).find(':selected').val(),false,'event');"}
      if f.object.new_record?
          f.input :state, as: :select, :required => true, :include_blank => "Please select a state",:input_html => {:style=>"margin:6px 0 3px;"}
      else
          f.input :edit_state, as: :string, :input_html => {:value => f.object.try(:state).try(:upcase) }
          f.input :state, as: :select, :required => true, :include_blank => "Please select a state", :input_html => {:style=>"margin:6px 0 3px;"}
      end
			# f.input :country, as: :country, :include_blank => 'Please choose an event location...'
			f.input :image, as: :file, :input_html => {:onchange => "validate_file_field($(this).attr('id'))"}, :label => "Event Image", :hint => f.template.image_tag(f.try(:object).try(:image_file_name).present? ? "#{AppConfig.s3_url}/attachments/#{f.object.try(:id)}/small/"+f.try(:object).try(:image_file_name) : "#{AppConfig.image_url}" + "/attachments/small/missing.png")
			f.input :_destroy, as: :boolean, :label => "Do you want to remove image?"
			f.input :paticipate_url, :label => "Participate url"
      f.input :non_profit, :label => "Non Profit (in $)"

      # Newly added fields
      f.input :nonprofit_cause
      f.input :no_of_categories, :required => true
      f.input :no_of_judges, :required => true
      f.input :no_of_expected_competitors, :required => true
      f.input :user_id, :as => :select, :collection => User.all(:order => 'last_name asc').map {|x| ["#{x.last_name}, #{x.first_name}",x.id]}, :include_blank => "Please Select Requester Name", :label => "Event Requester Name"
      f.input :requester_mobile, :required => true, :label => "Event Requester Mobile"
      # Newly added fields ends here
      

      # f.inputs for: [:event_categories, f.object.event_categories || EventCategory.build]  do |event|
			f.has_many :event_categories  do |event|
    		event.input :main_category_id, :as => :select, :include_blank => 'Please select a main category', :collection => MainCategory.all(:order => 'name asc'), :input_html => {
        :onchange => "populate_category_by_main_category($(this), $(this).val());" }
        event.input :category_id, :as => :select, :required => true, :collection => Category.all(:order => 'name asc'), :label => "Category", :include_blank => "Select Category"
    	end
    	# f.has_many :event_clubs  do |event|
    	# 	event.input :club_id, :as => :select, :collection => Club.all(:order => 'name asc'), :label => "Add Clubs", :include_blank => "Select Club"
    	# end
			f.has_many :attachments do |event|
    		event.input :attachment, as: :file, :input_html => {:onchange => "validate_file_field($(this).attr('id'))"}, :hint => event.template.image_tag(event.try(:object).try(:attachment_file_name).present? ? "#{AppConfig.s3_url}/attachments/#{event.object.try(:id)}/small/"+event.try(:object).try(:attachment_file_name) : "#{AppConfig.image_url}" + "/attachments/small/missing.png"), :label => "Add Event Photos"
    		event.input :_destroy, as: :boolean, :label => "Do you want to remove attachment?"
      end            
            
      f.inputs "Pull checked-in competitors from other events", :class => "inputs customStyle", :for => [:event, f.object]  do |event_obj|
        events = event_obj.object.new_record? ? Event.order("name asc") : Event.where("id not in (?)",event_obj.object.id).order("name asc")
        event_obj.input :event_id, :as => :select, :collection => events, :label => "Event", :include_blank => "Select an event to pull competitors", :input_html => {:onchange=>"pull_event_registrants($(this).find(':selected').val(),'#{event_obj.object.try(:id)}');"}
        if event_obj.try(:object).new_record? || !event_obj.try(:object).try(:event_id).present?
          users = []
        elsif event_obj.try(:object).try(:event_id).present?
          event_cat_regs = event_obj.try(:object).try(:event_id).present? ? EventCategoryRegistration.where(:event_id => event_obj.try(:object).try(:event_id)).pluck(:user_id) : []
          participated_users = event_obj.try(:object).try(:id).present? ? EventCategoryRegistration.where(:event_id => event_obj.try(:object).try(:id)).pluck(:user_id) : [] # participated comps in a current event
          non_participated_users = event_cat_regs - participated_users # Non participated comps
          users = non_participated_users.present? ? User.where(:id=>non_participated_users).order('last_name asc, first_name asc') : []
          # event_cat_regs = event_obj.try(:object).try(:event_id).present? ? EventCategoryRegistration.where(:event_id => event_obj.try(:object).try(:event_id)).pluck(:id) : []
          # participated_users = event_obj.try(:object).try(:id).present? ? EventCategoryRegistration.where(:event_id => event_obj.try(:object).try(:id)).pluck(:id) : [] # participated comps in a current event
          # non_participated_users = event_cat_regs - participated_users # Non participated comps
          # users = non_participated_users.present? ? User.where(:id=>EventCategoryRegistration.where(:id=>non_participated_users).pluck(:user_id)).order('last_name asc, first_name asc') : []
        end
        event_obj.input :user_ids, as: :check_boxes, :multiple => true, :label => "Competitors", :collection => users.present? ? users : []
      end
  	end

      f.inputs "Event requester Details", :for => [:event_requester_shipping_detail, f.object.event_requester_shipping_detail || EventRequesterShippingDetail.new] do |event_requester_shipping_detail|
        event_requester_shipping_detail.input :shipping_contact_name
        event_requester_shipping_detail.input :shipping_address_line_1
        event_requester_shipping_detail.input :shipping_address_line_2
        event_requester_shipping_detail.input :shipping_city
        # event_requester_shipping_detail.input :shipping_country
        # event_requester_shipping_detail.input :shipping_state

        event_requester_shipping_detail.input :shipping_country, :as => :select, :include_blank => false, :collection => Carmen::Country.all.sort, :input_html => {
        :onchange => "getStates($(this).find(':selected').val(),false,'shipping_event');"}
        if event_requester_shipping_detail.object.new_record?
            event_requester_shipping_detail.input :shipping_state, as: :select, :include_blank => "Please select a state",:input_html => {:style=>"margin:6px 0 3px;"}
        else
            event_requester_shipping_detail.input :edit_shipping_state, as: :string, :input_html => {:value => event_requester_shipping_detail.object.try(:shipping_state).try(:upcase) }
            event_requester_shipping_detail.input :shipping_state, as: :select, :include_blank => "Please select a state", :input_html => {:style=>"margin:6px 0 3px;"}
        end

        event_requester_shipping_detail.input :shipping_zip

      end      
    f.actions
	end

    controller do
        def update
            event = Event.find_by_id(params[:id].to_i)
            if params[:event][:_destroy].to_i == 1                
                event.update_attributes!(:image_file_name => "", :image_content_type => "", :image_file_size => "")
            end
            event.update_attributes(:user_ids=>[])
            if params[:event].present? && params[:event][:user_ids].present?
               # Event.create_event_registrations_from_other_event_on_update(event,params[:event][:user_ids])
               Event.create_event_registrations_from_other_event_on_update(event,params[:event][:event][:event_id],params[:event][:user_ids])
            end
          update!
        end

        def select_event_photo
            event = Event.find_by_id(params[:id])
            render :partial=> "select_event_photo", :layout => false, :locals => {:event => event}
        end

        def update_event_photo
            event = Event.find_by_id(params[:event][:id])
            event.attachments.create(:attachment=>params[:event][:image]) if params[:event].present? && params[:event][:image].present?
            redirect_to admin_event_path(:id => event.try(:id))
        end
    end
end
