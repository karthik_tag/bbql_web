ActiveAdmin.register MainCategory do
	menu :url => "#{AppConfig.url}/main_categories", :if => proc{ User.toggle_main_menu(current_admin_user) }
  config.sort_order = "name_asc"
  index do
		column "Main Category", :name
		column :description
		default_actions
    paginated_collection(collection, download_links: false, entry_name: "Main Categories")
	end

  csv force_quotes: true, col_sep: ',' do
    column("Main Category")  do |main_category|
      main_category.try(:name)
    end
    column :description
  end

	sidebar "Main Category Image", :only => :show do
    	attributes_table_for main_category do
    	  row(" ") do |sub_cat|
    	    ul :class=>"left-algin-fix" do          
    	      img = main_category.try(:attachment)
    	      unless img.try(:attachment_file_name).nil?
    	        image_tag ("#{AppConfig.s3_url}/attachments/#{img.id}/small/"+img.attachment_file_name)
    	      else
                "There is no main category image"
              end
    	    end
    	  end
    	end
  	end

    show do
      panel "Main Category Details" do         
        attributes_table_for main_category, :name, :description
      end
    end

	filter :name, :label => "Main Category"
	filter :description

	form do |f|
    	f.inputs "New Main Category" do
			f.input :name
			f.input :description
			f.inputs for: [:attachment, f.object.try(:attachment) || Attachment.new] do |main_category|
        		main_category.input :attachment, as: :file, :hint => main_category.template.image_tag(main_category.try(:object).try(:attachment_file_name).present? ? "#{AppConfig.s3_url}/attachments/#{main_category.object.try(:id)}/small/"+main_category.try(:object).try(:attachment_file_name) : "#{AppConfig.image_url}" + "/attachments/small/missing.png")
        		main_category.input :_destroy, as: :boolean, :label => "Do you want to remove attachment?"
			end
    	end    	  
    	f.actions
  	end
end
