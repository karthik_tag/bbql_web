ActiveAdmin.register EventFanVotingResult do
	menu :url => "#{AppConfig.url}/event_fan_voting_results"
	actions :all, except: [:new, :destroy, :edit]
	config.clear_sidebar_sections!
	
	index :title => proc {
			if params[:event_id].present?
				event = Event.find_by_id(params[:event_id])
				dots = event.try(:name).try(:size) >= 50 ? "..." : ""
				"Fan Voting Results >#{event.id}>#{event.try(:name).slice(0, 50)}#{dots}"
			else
				""
			end
		} do
	# paginated_collection(collection, download_links: false)
	end
	# filter :created_at
end
