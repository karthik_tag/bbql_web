ActiveAdmin.register EventCompetitor do
	actions :all, except: [:show, :new]
	scope :joined, :default => true do |event_competitor|
  		# event_competitor.includes [:event_category, :competitor]
      event_competitor.includes(:competitor=>:user, :event_category=>[:event,:category])
	end
	menu :parent => "events", :url => "#{AppConfig.url}/all_events?order=events.from_date_asc&route=evt_comp&event_type=all"
  config.sort_order = ""

  filter :competitor_filter, as: :string, :label => "Competitor (Nick Name)"
  filter :event_filter, as: :string, :label => "Event"
  filter :category_filter, as: :string, :label => "Category"
  filter :first_name_filter, as: :string, :label => "First Name"
  filter :last_name_filter, as: :string, :label => "Last Name"

  index :title => proc {
      dynamic_index_title(params)
    } do		
    column "First Name", :first_name, :sortable => 'users.first_name' do |event_competitor|
      event_competitor.try(:competitor).try(:user).try(:first_name).try(:capitalize)
    end
    column "Last Name", :last_name, :sortable => 'users.last_name' do |event_competitor|
      event_competitor.try(:competitor).try(:user).try(:last_name).try(:capitalize)
    end
    column "Competitor (Nick Name)", :event, :sortable => 'competitors.nick_name' do |event_competitor|
      event_competitor.competitor.try(:nick_name).try(:capitalize)
    end
		column :event, :sortable => 'events.name' do |event_competitor|
      event_competitor.try(:event_category).try(:event).try(:name)
    end
		column :category, :sortable => 'categories.name' do |event_competitor|
      event_competitor.try(:event_category).try(:category).try(:name)
    end
    column :points
    default_actions
    paginated_collection(collection, download_links: false, entry_name: "Event Competitors")
	end

  csv force_quotes: true, col_sep: ',' do
    column("Competitor (Nick Name)")  do |event_competitor|
      event_competitor.competitor.try(:nick_name).try(:capitalize)
    end
    column("First Name")  do |event_competitor|
      event_competitor.try(:competitor).try(:user).try(:first_name).try(:capitalize)
    end
    column("Last Name")  do |event_competitor|
      event_competitor.try(:competitor).try(:user).try(:last_name).try(:capitalize)
    end
    column("Event")  do |event_competitor|
      event_competitor.try(:event_category).try(:event).try(:name)
    end
    column("Category")  do |event_competitor|
      event_competitor.try(:event_category).try(:category).try(:name)
    end
    column :points
  end

  controller do |event_competitor|
    def index
      super do |format|
        per_page = (request.format == 'text/html') ? 30 : 10_000
        params[:page] = nil unless (request.format == 'text/html')
        @event_competitors = @event_competitors.includes(:event_category=>[:event,:category],:competitor => :user).joins(:event_category=>[:event,:category],:competitor => :user).where("events.name = ?",params[:q][:event_filter_equals]).order("users.first_name asc, users.last_name asc").page(params[:page]).per(per_page) #if params[:q] && params[:q][:event_filter_equals] && !params[:commit].present? && params[:order] == ""
        @event_competitors ||= end_of_association_chain.paginate if @event_competitors.present?
      end
    end

    def destroy
      event = EventCompetitor.find_by_id(params[:id]).try(:event_category).try(:event)
      super do |format|
        redirect_to admin_event_competitors_path("q[event_filter_equals]" => event.try(:name), :order =>"competitors.nick_name_asc") and return if resource.valid?
      end
    end

    def update
      event = EventCompetitor.find_by_id(params[:id]).try(:event_category).try(:event)
      super do |format|
        redirect_to admin_event_competitors_path("q[event_filter_equals]" => event.try(:name)) and return if resource.valid?
      end
    end

  end

  form do |f|
    f.inputs "Edit Event Competitor Details" do
      f.input :points
    end
    f.actions
  end
end
