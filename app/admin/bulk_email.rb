ActiveAdmin.register BulkEmail do
	menu :parent => "Administration", :url => "#{AppConfig.url}/bulk_emails", :if => proc{ User.toggle_main_menu(current_admin_user) }
	# config.clear_sidebar_sections!
	actions :all, except: [:edit]

	index do
		column ("Subject"), :title
		column ("Message"), :content
    	default_actions
      	paginated_collection(collection, download_links: false, entry_name: "Bulk Emails")
	end

	csv force_quotes: true, col_sep: ',' do
	    column :title
	    column :content
  	end

    show do
		panel "Bulk Email Details" do
        	attributes_table_for bulk_email, :title, :content
        	attributes_table_for bulk_email do
	          users_list = User.where(:id=>bulk_email.user_ids).map { |user| link_to(user.try(:first_name), admin_user_path(user))} #if category.category_questions.present?
	          row("Users List") {                    
	            if (users_list.present?)
	                safe_join users_list, '<br>'.html_safe
	            else
	              "None"
	            end
	          }
	        end
        end
    end

    filter :title, :label => "Subject"
    filter :content, :label => "Message"

	form do |f|
  		f.inputs "Bulk Email" do
			f.input :title, :label => "Subject"
			f.input :content, :label => "Message"
			f.input :select_users_list, :label=>"Select user type", :include_blank=>"Select a user type", :collection => [["All Users",1],["Non-Visited Users",2],["Custom",3]], :input_html => 
	        {
	          :onchange => "generate_users_list($(this).find(':selected').val());"
	        }
			users = User.order('last_name asc, first_name asc')
			f.input :user_ids, as: :check_boxes, :multiple => true, :label => "Users list", :collection => [] #users.present? ? users : []
		end
		f.actions
	end
end
