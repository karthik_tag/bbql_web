ActiveAdmin.register LoginImage do
    menu :parent => "Administration", :url => "#{AppConfig.url}/login_images", :if => proc{ User.toggle_main_menu(current_admin_user) }
    config.clear_sidebar_sections!
    config.sort_order = ""
	index do #:download_links => false
		column :first_name do |login_image|
      login_image.try(:first_name).try(:capitalize)
    end
    column :last_name do |login_image|
      login_image.try(:last_name).try(:capitalize)
    end
		column :nick_name		
		column "Charity", :charity do |login_image|    	  
			link_to(login_image.try(:charity).try(:name), admin_charity_path(login_image.try(:charity)))
		end
		column "Login Images" do |login_image|
			image_tag "#{AppConfig.s3_url}/attachments/#{login_image.try(:id)}/original/"+ (login_image.try(:login_image_file_name) || ""), class: 'my_image_size', :style=>"max-height:230px;"
		end
    	default_actions
      paginated_collection(collection, download_links: false, entry_name: "Login Images")
	end

  csv force_quotes: true, col_sep: ',' do
    column :first_name do |login_image|
      login_image.try(:first_name).try(:capitalize)
    end
    column :last_name do |login_image|
      login_image.try(:last_name).try(:capitalize)
    end
    column :nick_name
    column("Charity")  do |login_image|
      login_image.try(:charity).try(:name)
    end
  end

	show do
      panel "Login Image" do
      	attributes_table_for login_image, :first_name, :last_name, :nick_name
      	attributes_table_for login_image do
        	row("Charity") { link_to(login_image.try(:charity).try(:name), admin_charity_path(login_image.try(:charity))) }
      	end
      end
    end

    sidebar "Login Image", :only => :show do
    	attributes_table_for login_image do
    	  row(" ") do |login_image|
    	    ul :class=>"left-algin-fix" do          
    	      unless login_image.try(:login_image_file_name).nil?
    	        image_tag ("#{AppConfig.s3_url}/attachments/#{login_image.id}/small/"+login_image.login_image_file_name)
              else
                "There is no Login image"
    	      end
    	    end         
    	  end
    	end
  	end

    users_common = User.where(:id=>Competitor.where("nick_name IS NOT NULL and nick_name != ''").pluck(:user_id))
    all_users_1 = users_common.where("first_name IS NOT NULL and first_name != '' and last_name != ''").order("last_name asc,first_name asc") #.order(:last_name)
    all_users_2 = users_common.where("first_name IS NOT NULL and first_name != '' and last_name = ''").order("last_name asc,first_name asc") #.order(:last_name)
    all_users = all_users_1 + all_users_2
    user_ids = all_users.map(&:id)
    first_names = all_users.map(&:first_name)
    last_names = all_users.map(&:last_name)
    full_array = []
    array = last_names.zip(first_names)
    
    array.each do |arr|
      if arr[0].present?
        full_array.push(arr.join(", "))
      else
        full_array.push(arr.join)
      end
    end
    user_arr = full_array.zip(user_ids)



	form multipart: true do |f|
    	f.inputs "Upload Image" do
    	  f.input :user_id, :as => :select, :collection => user_arr, :include_blank => "Select User", :label => "User (Last Name, First Name)", :input_html => {
            :onchange => "populate_user_details($(this), $(this).val());" 
          }, :required => true
		  f.input :charity_id, :as => :select, :include_blank => 'Please select a charity', as: :hidden
    	f.input :login_image, as: :file, :input_html => {:onchange => "validate_file_field($(this).attr('id'))"}, :hint => f.template.image_tag(f.try(:object).try(:login_image_file_name).present? ? "#{AppConfig.s3_url}/attachments/#{f.object.try(:id)}/small/"+f.try(:object).try(:login_image_file_name) : "#{AppConfig.image_url}" + "/attachments/small/missing.png")
      f.input :_destroy, as: :boolean, :label => "Do you want to remove image?", as: :hidden
      f.input :first_name, :input_html => {:readonly=>true}, as: :hidden
      f.input :last_name, :input_html => {:readonly=>true}, as: :hidden
      f.input :nick_name, :input_html => {:readonly=>true}, as: :hidden
    	end
    	f.actions
  end

  controller do
    def index
      super do |format|
        @login_images = LoginImage.order("first_name asc, last_name asc").page(params[:page]).per(30) if params[:order] == ""
        @login_images ||= end_of_association_chain.paginate if @login_images.present?
      end
    end
  end
end
