ActiveAdmin.register AppImage do
	menu :parent => "Administration", :url => "#{AppConfig.url}/app_images", :if => proc{ User.toggle_main_menu(current_admin_user) }
	config.clear_sidebar_sections!
	config.sort_order = "app_images.content_asc"

	index do
		column :content
		column "App Images" do |app_image|
			image_tag "#{AppConfig.s3_url}/attachments/#{app_image.try(:id)}/original/"+ (app_image.try(:app_image_file_name) || ""), class: 'my_image_size', :style=>"max-height:230px;"
		end
    	default_actions
      	paginated_collection(collection, download_links: false, entry_name: "App Images")
	end

	csv force_quotes: true, col_sep: ',' do
	    column :content
  	end

  	show do
		panel "App Image Details" do
        	attributes_table_for app_image, :content
        end
    end

    sidebar "App Image", :only => :show do
    	attributes_table_for app_image do
    	  row(" ") do |app_image|
    	    ul :class=>"left-algin-fix" do          
    	      unless app_image.try(:app_image_file_name).nil?
    	        image_tag ("#{AppConfig.s3_url}/attachments/#{app_image.id}/small/"+app_image.app_image_file_name)
              else
                "There is no App image"
    	      end
    	    end         
    	  end
    	end
  	end

  	form multipart: true do |f|
		f.inputs "Upload Image" do			
			f.input :content
			f.input :app_image, as: :file, :input_html => {:onchange => "validate_file_field($(this).attr('id'))"}, :hint => f.template.image_tag(f.try(:object).try(:app_image_file_name).present? ? "#{AppConfig.s3_url}/attachments/#{f.object.try(:id)}/small/"+f.try(:object).try(:app_image_file_name) : "#{AppConfig.image_url}" + "/attachments/small/missing.png")
			f.input :_destroy, as: :boolean, :label => "Do you want to remove image?", as: :hidden			
		end
		f.actions
  end
end
