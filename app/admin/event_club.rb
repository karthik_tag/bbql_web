ActiveAdmin.register EventClub do
  menu false
	scope :joined, :default => true do |event_club|
  		event_club.includes [:club, :event]
	end
	menu :parent => "events", :url => "#{AppConfig.url}/all_events?order=events.from_date_asc&route=evt_clb&event_type=all"
  config.sort_order = "events.name_asc"
  config.clear_sidebar_sections!

	index :title => proc {
      dynamic_index_title(params)
    } do
		column "Event", :event_id, :sortable => 'events.name' do |event_club|
      		event_club.event.try(:name)
    	end
		column "Club", :club_id, :sortable => 'clubs.name' do |event_club|
      		event_club.club.try(:name)
    	end
		default_actions
    paginated_collection(collection, download_links: false, entry_name: "Event Club")
	end

  csv force_quotes: true, col_sep: ',' do
    column("Event")  do |event_club|
      event_club.event.try(:name)
    end
    column("Club")  do |event_club|
      event_club.club.try(:name)
    end
  end

  filter :event_id, as: :Select, collection: ActiveRecord::Base.connection.table_exists?('events') ? Event.all(:order => 'name asc') : []
  filter :club_id, as: :Select, collection: ActiveRecord::Base.connection.table_exists?('clubs') ? Club.all(:order => 'name asc') : []

	show do
  	panel "Event Clubs Details" do
    attributes_table_for event_club do
      row("event") { link_to(event_club.try(:event).try(:name), admin_event_path(event_club.try(:event))) }
      row("club") { link_to(event_club.try(:club).try(:name), admin_club_path(event_club.try(:club))) }
    end
  	end
	end

  controller do |event_club|
    def destroy
      event_id = EventClub.find_by_id(params[:id]).try(:event).try(:id)
      super do |format|
        redirect_to admin_event_clubs_path("q[event_id_eq]" => event_id, :order =>"clubs.name_asc") and return if resource.valid?
      end
    end

    def select_event_clubs
      render :partial=> "select_event_clubs"
    end

    def update_event_clubs
      EventClub.create(:club_id=>params[:add_event_clubs_dropdown],:event_id=>params[:event_id])
      redirect_to admin_event_path(params[:event_id])
    end

  end

	form do |f|
  	f.inputs "Event Club Details" do
  	  f.input :event_id, :as => :select, :collection => Event.all(:order => 'name asc'), :include_blank => "Select Event"
  	  f.input :club_id, :as => :select, :collection => Club.all(:order => 'name asc'), :include_blank => "Select Club"
  	end       
  	f.actions
	end
end