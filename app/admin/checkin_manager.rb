# ActiveAdmin.register CheckinManager do
#   menu :parent => "Users", :url => "#{AppConfig.url}/checkin_managers"
#   config.sort_order = "email_asc"
#   index do
#     column :email
#     column :current_sign_in_at
#     column :last_sign_in_at
#     column :sign_in_count
#     default_actions
#   end

#   csv force_quotes: true, col_sep: ',' do
#     column :email
#     column :current_sign_in_at
#     column :last_sign_in_at
#     column :sign_in_count
#   end

#   show do
#     panel "Admin User Details" do
#       attributes_table_for checkin_manager, :email, :current_sign_in_at, :last_sign_in_at, :current_sign_in_ip, :last_sign_in_ip, :sign_in_count
#     end
#   end  

#   filter :email

#   form do |f|
#     f.inputs "Admin Details" do
#       f.input :email
#       f.input :password
#       f.input :password_confirmation
#     end
#     f.actions
#   end

#   controller do
#     def update
#       if params[:checkin_manager][:password].blank? && params[:checkin_manager][:password_confirmation].blank?
#         params[:checkin_manager].delete("password")
#         params[:checkin_manager].delete("password_confirmation")
#       end
#       super
#     end    
#   end
# end
