ActiveAdmin.register JudgeVote do
  menu :url => "#{AppConfig.url}/judge_votes"
	actions :all, except: [:show, :new, :edit, :destroy]
	config.clear_sidebar_sections!

	index do
   		column "Competitor", :event_competitor_id, :sortable => false do |judge_vote|
   			competitor =  judge_vote.try(:event_competitor).try(:competitor).try(:nick_name).try(:capitalize)
   			competitor.present? ? competitor.titleize : "None"
   		end
   		column "Category", :event_category_id, :sortable => false do |judge_vote|
   			judge_vote.try(:event_category).try(:category).try(:name)
   		end
   		column "Judging Criteria", :category_question_id, :sortable => false do |judge_vote|
   			judge_vote.try(:category_question).try(:question)
   		end
   		# column :rating, :sortable => false, :class=> "avg_rating"
      column "Rating", :rating, :sortable => false do |judge_vote|
        if params.has_key?(:user_id)
          judge_vote.try(:rating)
        elsif judge_vote.try(:event_category).try(:event).try(:is_olympic_rules)
          "<span class='olympic_rules_avg'>#{judge_vote.try(:rating)}</span>".html_safe
        else
          judge_vote.try(:rating)
        end
      end
   		column "Judge", :user_id, :sortable => false do |judge_vote|
        judge = judge_vote.try(:user)
   			judge.present? ? "#{judge.try(:last_name).try(:capitalize)}, #{judge.try(:first_name).try(:capitalize)}" : "None"
   		end
      paginated_collection(collection, download_links: false, entry_name: "Judge Vote")
	end

	controller do
    	def index
        if params.has_key?(:user_id)
          @judge_votes = JudgeVote.where( :event_category_id => params[:event_category_id], :user_id => params[:user_id]).page(params[:page]).per(50)
        else
    		  @judge_votes = JudgeVote.where( :event_competitor_id => params[:format]).page(params[:page]).per(50)
        end
    	end
  	end
end
