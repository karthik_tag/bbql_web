ActiveAdmin.register EventMc do
  scope :joined, :default => true do |event_mc|
  	event_mc.includes [:event, :user]
	end

  menu :parent => "events", :url => "#{AppConfig.url}/all_events?order=events.from_date_asc&route=evt_mc&event_type=all"
  config.sort_order = "events.name_asc"
  config.clear_sidebar_sections!

  action_item :only => [:index,:show] do
    config.clear_action_items!
    link_to "New Event MC" , new_admin_event_mc_path("q[event_id_eq]"=>params[:q] && params[:q][:event_id_eq])
  end

	index :title => proc {
      dynamic_index_title(params)
    } do
	column "Event", :event_id, :sortable => 'events.name' do |event_mc|
      event_mc.event.try(:name)
    end
    column "MC", :user_id, :sortable => 'users.first_name' do |event_mc|
     	"#{event_mc.user.try(:first_name).try(:capitalize)} #{event_mc.user.try(:last_name).try(:capitalize)}"
    end

    column :actions do |object|
      raw( %(#{link_to "View", [:admin, object]} 
        #{(link_to "Edit", [:edit, :admin, object]) if !object.user_id.present? }
        #{link_to "Delete", [:admin, object], method: :delete, :confirm => 'Are you sure you want to delete this?' }) )
    end
    paginated_collection(collection, download_links: false, entry_name: "Event MC")
	end

  csv force_quotes: true, col_sep: ',' do
    column("Event")  do |event_mc|
      event_mc.event.try(:name)
    end
    column("MC")  do |event_mc|
      event_mc.user.try(:first_name).try(:capitalize)
    end    
  end

  filter :event_id, as: :Select, collection: ActiveRecord::Base.connection.table_exists?('events') ? Event.all(:order => 'name asc') : []

	show do
    panel "Event MC Details" do
      attributes_table_for event_mc do
        row("event") { link_to(event_mc.try(:event).try(:name), admin_event_path(event_mc.try(:event))) }
        row("MC") { event_mc.try(:user).present? ? link_to(event_mc.try(:user).try(:email), admin_user_path(event_mc.try(:user))) : 'None' }
      end
    end
  end

  form do |f|
  	f.inputs "Event MC Details" do      
      if f.object.new_record?
        f.input :event_id, :as => :select, :required => true, :collection => Event.find_all_by_id(params[:q] && params[:q][:event_id_eq]) + Event.where("id not in (#{params[:q] && params[:q][:event_id_eq]})").order("name asc"), :include_blank => false, :input_html => {
        :onchange => "populate_user_not_participated_in_this_event($(this), $(this).val(),'event_mc');" }
      else
        f.input :event_id, :as => :select, :required => true, :collection => Event.all(:order => 'name asc'), :include_blank => false, :input_html => {
        :onchange => "populate_user_not_participated_in_this_event($(this), $(this).val(),'event_mc');" }
      end
      # judge_code_ids = EventJudgeCode.pluck(:judge_code_id)
      # judge_code_ids = judge_code_ids.empty? ? [0] : judge_code_ids
      # selected_judge_code = JudgeCode.where(:id => f.object.judge_code_id, :status => false)
  	  # f.input :judge_code_id, :as => :select, :collection => selected_judge_code + JudgeCode.where("id not in (?) and status = ?", judge_code_ids,false), :include_blank => "Select Judge Code"
      f.input :user_id, :as => :select, :collection => User.all(:order => 'first_name asc'), :include_blank => "Please Select MC", :label=> "MC"
      # f.inputs for: [:judge_code, f.object.judge_code || JudgeCode.new], :class => "judge_code_customClass" do |judge_code|
      #   judge_code.input :status, :input_html => { :type => :hidden }
      #   judge_code.input :name, :label=> "Judge Code"
      #   judge_code.input :generate_judge_code, :label=> "Click here to generate a judge code", :input_html => 
      #   {
      #     :onclick => "generate_judge_code();"
      #   }
      # end
  	end
  	f.actions
  end

  controller do |event_mc|
    def destroy
      event_mc = EventMc.find_by_id(params[:id])
      event_id = event_mc.try(:event).try(:id)
      super do |format|
        redirect_to admin_event_mcs_path("q[event_id_eq]" => event_id, :order =>"users.first_name_asc") and return if resource.valid?
      end
    end
  end
end
