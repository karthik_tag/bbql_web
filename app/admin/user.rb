require 'carmen'
include Carmen
ActiveAdmin.register User do

  # To resolve the duplicate badge id generation on event registration.
  before_save do |user|
    categories,event_number,no_event = populate_badge_id_for_an_event(params[:user][:competitor_attributes][:event_id])
    user.competitor.update_attributes(:number=>event_number+1)
  end

  menu :parent => "Users", :url => "#{AppConfig.url}/users", :if => proc{ User.toggle_main_menu(current_admin_user) }
  scope :joined, :default => true do |user|
      user.includes [:user_type]
  end
  config.sort_order = ""
  index do
    column "Member Id", :card_id
    column :first_name, :sortable => 'users.first_name' do |user|
      user.try(:first_name).try(:capitalize)
    end
    column :last_name, :sortable => 'users.last_name' do |user|
      user.try(:last_name).try(:capitalize)
    end
    column :email
    column "User Type", :user_type_id, :sortable => 'user_types.name' do |user|
      user.user_type.try(:name) == "Visitor" ? "Fan" : user.user_type.try(:name)
    end
    column :country

    column :actions do |object|
      raw( %(#{link_to "View", [:admin, object]} 
        #{(link_to "Edit", [:edit, :admin, object]) }      
        #{link_to "Delete", admin_user_path("id"=>object.id,"q"=>params[:q] ), :method=>:delete, :confirm=>"Are you sure you want to delete this user?"}
      ))
    end
    # {(link_to "Delete", [:admin, object], :method=>:delete, :confirm=>"Are you sure?") }
    # default_actions

    paginated_collection(collection, download_links: false, entry_name: "Users")
  end

  csv force_quotes: true, col_sep: ',' do
    column("Member Id") do |user|
      user.try(:card_id)
    end    
    column :first_name
    column :last_name
    column :email
    column("User Type")  do |user|
      user.user_type.try(:name) == "Visitor" ? "Fan" : user.user_type.try(:name)
    end
    column :country
  end

  sidebar "User Image", :only => :show do
    attributes_table_for user do
      row(" ") do |usr|
        ul :class=>"left-algin-fix" do
          img = user.attachment
          unless img.try(:attachment_file_name).nil?
            image_tag ("#{AppConfig.s3_url}/attachments/#{img.id}/small/"+img.attachment_file_name)
          else
            "There is no user image"
          end
        end         
      end
    end
  end

  sidebar "More User Images", :only => :show do
    attributes_table_for user do
      # if user.user_type.name == "Competitor"
        row(" ") do |usr|
          ul :class=>"left-algin-fix" do
            imgs = user.competitor.attachments if user.competitor.present?
            if imgs.present?
              imgs.each do |img|
                li do
                  unless img.try(:attachment_file_name).nil?
                    image_tag ("#{AppConfig.s3_url}/attachments/#{img.id}/small/"+img.attachment_file_name)
                  end
                end
              end
            else
              "There are no user photos"
            end
          end         
        end
      # end
    end
  end

  sidebar :filters, only: :show do
    render 'admin/users/sidebar_filter'
  end

  show do
    panel "User Details" do
      attributes_table_for user do
        row("Member Id") { user.card_id }
        row("First Name") { user.first_name.try(:capitalize) }
        row("Last Name") { user.last_name.try(:capitalize) }
      end
      attributes_table_for user, :email, :country, :zip_code, :status
      attributes_table_for user do
        row("User type") { user.user_type.try(:name) == "Visitor" ? "Fan" : user.user_type.try(:name) }
        if user.user_type.try(:name) == "Competitor"
          attributes_table_for user.try(:competitor) do
            row("Nick Name") { user.try(:competitor).try(:nick_name).try(:capitalize) }
            row :city #, user.try(:competitor).try(:city)
            row :state #, user.try(:competitor).try(:state)
            row("Category") {user.try(:competitor).try(:category).present? ? link_to(user.try(:competitor).try(:category).try(:name),admin_category_path(user.try(:competitor).try(:category))) : "None"}
            row("Charity") {user.try(:competitor).try(:charity).present? ? link_to(user.try(:competitor).try(:charity).try(:name),admin_charity_path(user.try(:competitor).try(:charity))) : "None"}
            # row("Club") {user.try(:competitor).try(:club).present? ? link_to(user.try(:competitor).try(:club).try(:name),admin_club_path(user.try(:competitor).try(:club))) : "None"}
            
            @evnt_catg_regs = EventCategoryRegistration.where("user_id = ? and event_id IS NOT NULL", user.try(:id))
          end if user.try(:competitor).present?
        end
      end      
    end
    
    @evnt_catg_regs.each do |evnt_catg_reg|
      event = evnt_catg_reg.try(:event)
      category = evnt_catg_reg.try(:category)
      panel "#{event.try(:name)}" do
        attributes_table_for user do
          row("Event to compete in") {
            event.present? ? link_to(event.try(:name), admin_event_path(event)) : ""
          }
          row("Category to compete in") {
            category.present? ? link_to(category.try(:name), admin_category_path(category)) : "Not registered to a category yet"
          }
          row("Badge Id") {
            evnt_catg_reg.try(:number)
          }          
          unless evnt_catg_reg.event_photo.present?
            row("Check-in") {link_to 'Choose Photo', '#', :class => "upload_photo fu-button", :onclick => "take_photo($(this),#{evnt_catg_reg.id},'take_photo');"}
          else
            row("Checked-in") {
              # link_to image_tag("#{AppConfig.image_url}/assets/attachments/#{evnt_catg_reg.id}/small/"+evnt_catg_reg.event_photo_file_name), admin_event_category_registration_path(evnt_catg_reg) # Need to change this url into s3_url once the amazon branch is merged
              link_to image_tag("#{AppConfig.s3_url}/attachments/#{evnt_catg_reg.id}/small/"+evnt_catg_reg.event_photo_file_name), admin_event_category_registration_path(evnt_catg_reg)
            }
          end
        end
      end
    end if @evnt_catg_regs.present?
  end

  filter :card_id, :label => "Member Id"
  filter :first_name
  filter :last_name
  filter :email
  filter :country, as: :select, collection: proc { User.pluck(:country).uniq.compact.sort - [""]}
  # user_types_arr = ActiveRecord::Base.connection.table_exists?('user_types') ? UserType.all(:order => 'name asc') : []
  # user_types_arr = [["Competitor", 2],["Fan",3],["Judge",1]]
  user_types_arr = [["Competitor", 2],["Fan",3]]
  filter :user_type_id, as: :Select, collection: user_types_arr

  form do |f|
    f.inputs "User Details" do
      f.input :card_id, :label => "Member Id"
      f.input :first_name
      f.input :last_name
      f.input :email
      f.input :country, :as => :select, :required => true, :include_blank => false, :collection => Carmen::Country.all.sort, :input_html => {
        :onchange => "getStates($(this).find(':selected').val(),false,'user');" }      
      f.input :state, :as => "hidden"
      f.input :zip_code
      f.inputs for: [:attachment, f.object.attachment || Attachment.new] do |user|
            user.input :attachment, as: :file, :input_html => {:onchange => "validate_file_field($(this).attr('id'))"}, :hint => user.template.image_tag(user.try(:object).try(:attachment_file_name).present? ? "#{AppConfig.s3_url}/attachments/#{user.object.try(:id)}/small/"+user.try(:object).try(:attachment_file_name) : "#{AppConfig.image_url}" + "/attachments/medium/missing.png")
            user.input :_destroy, as: :boolean, :label => "Do you want to remove attachment?"
      end
      # f.input :legal_copy
      # user_types_arr = [["Competitor", 2],["Fan",3],["Judge",1]]
      user_types_arr = [["Competitor", 2],["Fan",3]]
      f.input :user_type_id, :as => :select, :include_blank => 'Please select role', :collection => user_types_arr, :input_html => {
        :onchange => "toggle_show_judge_code_dropdown($(this).find(':selected').text());"
      }
      
      if f.object.id.present?
        f.input :judge_code, :value => JudgeCode.find_by_id(f.object.judge_code_id).try(:name), :input_html => { :disabled => true}
      else
      f.input :judge_code_id, :as => :select, :include_blank => 'Please select a judge code', :collection => JudgeCode.where("id in (select judge_code_id from event_judge_codes where user_id is null)")
      end

      f.inputs "Competitor", :for => [:competitor, f.object.competitor || Competitor.new] do |competitor|
        competitor.input :main_category_id, :as => :select, :include_blank => 'Please select a main category', :collection => MainCategory.all(:order => 'name asc'), :input_html => {
        :style=>"margin:6px 0 3px;", :onchange => "populate_category_by_main_category($(this), $(this).val());" }
        competitor.input :category_id, :as => :select, :required => true, :include_blank => 'Please select a category', :input_html => {:style=>"margin:6px 0 3px;"}, :collection => Category.all(:order => 'name asc')
        competitor.input :nick_name, :input_html => {:style=>"margin:6px 0 3px;"}
        competitor.input :city, :input_html => {:style=>"margin:6px 0 3px;"}        
        if competitor.object.new_record?
          competitor.input :state, as: :select, :include_blank => "Please select a state",:input_html => {:style=>"margin:6px 0 3px;"}
        else
          competitor.input :edit_state, as: :string, :input_html => {:value => competitor.object.try(:state).try(:upcase) }
          competitor.input :state, as: :select, :include_blank => "Please select a state", :input_html => {:style=>"margin:6px 0 3px;"}
        end
        competitor.input :charity_id, :as => :select, :required => true, :include_blank => 'Please select a charity', :input_html => {:style=>"margin:6px 0 3px;"}, :collection => Charity.all(:order => 'name asc')
        # competitor.input :club_id, :as => :select, :required => true, :include_blank => 'Please select a club', :input_html => {:style=>"margin:6px 0 3px;"}, :collection => Club.all(:order => 'name asc')
        competitor.has_many :attachments do |compt|
          compt.input :attachment, as: :file, :input_html => {:onchange => "validate_file_field($(this).attr('id'))"}, :hint => compt.template.image_tag(compt.try(:object).try(:attachment_file_name).present? ? "#{AppConfig.s3_url}/attachments/#{compt.object.try(:id)}/small/"+compt.try(:object).try(:attachment_file_name) : "#{AppConfig.image_url}" + "/attachments/small/missing.png"), :label => "Add Competitor Photos"
          compt.input :_destroy, as: :boolean, :label => "Do you want to remove attachment?"
        end        
      end
      f.input :edit_password, :label=> "Add/Edit password", :input_html => 
      {
        :onclick => "edit_password();"
      }
      f.input :password
      f.input :password_confirmation
      # if true
        f.inputs "Event Registration", :class => "inputs customStyle", :for => [:competitor, f.object.competitor || Competitor.new] do |competitor|
          unless f.object.new_record?
            judge_of_events = f.object.event_judge_codes.pluck(:event_id)
            mc_of_events = f.object.event_mcs.pluck(:event_id)
            jud_and_mcs_of_events = judge_of_events + mc_of_events
          end
          if jud_and_mcs_of_events.blank? || f.object.new_record?
            event_collection = Event.where("status = ? or status = ?", "Upcoming", "OnGoing").order("name asc")
          else
            event_collection = Event.where("(status = ? or status = ?) and id not in (?)", "Upcoming", "OnGoing",jud_and_mcs_of_events).order("name asc")
          end
          competitor.input :event_id, :as => :select, :include_blank => 'Please select an event', :input_html => {:style=>"margin:6px 0 3px;", :onchange => "populate_event_categories($(this), $(this).val());"}, :collection => event_collection
          competitor.input :compete_category_id , :as => :select, :include_blank => 'Please select a category', :input_html => {:style=>"margin:6px 0 3px;"}, :collection => Category.all(:order => 'name asc'), :label => "Select a Category to Compete In"
          competitor.input :edited_by
          # competitor.input :number, :label=> "Badge ID", :input_html => {:style=>"margin:6px 0 3px;", :onkeyup => "validate_number_field($('#user_competitor_attributes_number').val(),$('#user_competitor_attributes_event_id').find(':selected').val(),$('#user_competitor_attributes_compete_category_id').find(':selected').val())"}
          competitor.input :number, :label=> "Badge ID", :input_html => {:style=>"margin:6px 0 3px;", :readonly=>true}
          
        end
      # end
    end
    f.actions
  end


  controller do
    def index
      super do |format|
        per_page = (request.format == 'text/html') ? 30 : 10_000
        params[:page] = nil unless (request.format == 'text/html')
        @users = @users.order("first_name asc, last_name asc").page(params[:page]).per(per_page) #if !params[:commit].present? && params[:order] == ""
        @users ||= end_of_association_chain.paginate if @users.present?
      end
    end

    def create
      merge_step_value
      # create!
      begin
        super do |format|
          Competitor.where(:user_id=>nil).delete_all
          unless @user.new_record?
            Thread.new{UserMailer.create_user(@user,@user.try(:password)).deliver}
            redirect_to admin_user_path(:id => @user.try(:id)) and return if resource.valid?
          end
        end
      rescue
        redirect_to new_admin_user_path
      end

    end

    def update
      merge_step_value
      # evnt_cat_reg = EventCategoryRegistration.where(:user_id =>params[:id],:event_id=>params[:user][:competitor_attributes][:event_id]) if params[:user][:competitor_attributes][:event_id].present?
      # if evnt_cat_reg.present?
      #   EventCompetitor.where(:event_category_registration_id => evnt_cat_reg.pluck(:id)).delete_all
      #   evnt_cat_reg.delete_all   #To delete the existing record in event category registration, when user changes his profile with event change.
      # end

      ## EventCategoryRegistration.where(:user_id =>params[:id]).delete_all if params[:user][:competitor_attributes][:event_id].present? #unhide the above and hide this if one user can participate into multiple events

      if params[:user][:password].blank? && params[:user][:password_confirmation].blank?
        params[:user].delete("password")
        params[:user].delete("password_confirmation")
      end
      super
      # update!
    end

    def destroy
      user = User.find_by_id(params[:id])
      if user.try(:judge_code_id).present?
        judge_code = JudgeCode.find_by_id(user.judge_code_id)
        judge_code.update_attributes(:status => false) if judge_code.present?
      end
      super do |format|
        redirect_to admin_users_path(params) and return
      end
    end

    def merge_step_value
      user_type = UserType.find_by_id(params[:user][:user_type_id])
      if user_type.try(:name) == "Judge"
        judge_code = JudgeCode.find_by_id(params[:user][:judge_code_id])
        judge_code.update_attributes(:status => 1) if judge_code.present?
        params[:user][:competitor_attributes].merge!({step:"1"})
      elsif user_type.try(:name) == "Competitor"
        params[:user][:competitor_attributes].merge!({step:"2"})          
      end
    end
  end


end
