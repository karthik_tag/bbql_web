ActiveAdmin.register AdminPushNotification, :as => "Push Notification"  do
	menu :parent => "Administration", :url => "#{AppConfig.url}/push_notifications", :if => proc{ User.toggle_main_menu(current_admin_user) }
	actions :all, except: [:edit]
	config.sort_order = "admin_push_notifications.created_at_desc"

	index do
		column :content
		column :status, :sortable => 'admin_push_notifications.status' do |push_notification|    		
    		status_tag (push_notification.try(:status) ? "Published" : "Not Published"), (push_notification.try(:status) ? :okk : :error)
    	end
    	column :created_at, :sortable => 'admin_push_notifications.created_at' do |push_notification|    		
    		push_notification.try(:created_at).strftime('%A, %B %d, %Y')
    	end
    	default_actions
      	paginated_collection(collection, download_links: false, entry_name: "Push Notification")
	end

	csv force_quotes: true, col_sep: ',' do
        column :content        
        column("status")  do |push_notification|
          push_notification.try(:status) ? "Published" : "Not Published"
        end
        column("created_at")  do |push_notification|
          push_notification.try(:created_at).strftime('%A, %B %d, %Y')
        end
    end

    show do
		panel "Push Notification Details" do
        	attributes_table_for push_notification do
                row :content
                row("Status") {
                    push_notification.try(:status) ? "Published" : "Not Published"
                }
                row("created_at") {
                    push_notification.try(:created_at).strftime('%A, %B %d, %Y')
                }
            end
        end
    end

    filter :content
	filter :status, as: :Select, collection: [['Published', true],['Not Published', false]]

	controller do
        def create
        	super do |format|
        		# Android Push Notification to be sent first . Before IOS notifications. because IOS creates table records.
				title = "Facial Hair League"
				message = @push_notification.try(:content)
		    	users_ids = UserRegId.where(:status=>true).pluck(:user_id).uniq
		    	# Android push notification code
				android_push_notification(title,message,users_ids,"")

				# IOS push notification code
				ios_push_notification(message,users_ids,"")

  				redirect_to admin_push_notifications_path and return if resource.valid?
  			end
        end
    end

end
