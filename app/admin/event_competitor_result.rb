require 'gcm'
ActiveAdmin.register EventCompetitorResult do
	menu :url => "#{AppConfig.url}/event_competitor_results"
  actions :all, except: [:show, :destroy]
	# scope :joined, :default => true do |results|
 #  		results.includes [:event_competitor]
	# end
	config.clear_sidebar_sections!

	collection_action :publish, :method => :put do

    event_category = EventCategory.find params[:id]
    # event_category.update_attributes(:is_published => true)
    # # redirect_to admin_event_competitor_results_path(params) #old redirection
    # # Redirect to the next un-published category of this event to publish with view results calculations
    # cat_event = event_category.event

    # # Find the 1st place winner
    # top_winner = EventCompetitorResult.where(:event_category_id => event_category.try(:id), :avg_visitor_results => 0).order(:position).first.try(:event_competitor)
    # top_winner_competitor = top_winner.try(:competitor)
    # top_winner_user = top_winner_competitor.try(:user)

    # ##Push Notification
    # # competitors = Competitor.where(:id=>EventCompetitor.where(:event_category_id=>event_category.try(:id)).pluck(:competitor_id)).pluck(:user_id)
    # # judges = EventJudgeCode.where(:event_id=>cat_event.try(:id)).pluck(:user_id)
    # # fans = VisitorVote.where(:event_category_id=>event_category.try(:id)).pluck(:user_id)
    # # all_users = competitors + judges + fans

    # visitors_event_competitor_results = event_category.event_competitor_results.where(:avg_judge_results=>0)
    # unless visitors_event_competitor_results.present?
    #   EventCategory.close_fan_voting(event_category)
    # end

    cat_event = EventCompetitorResult.close_event_category(event_category)

    redir_evt_cat = cat_event.event_categories.joins(:judge_votes).where("is_published IS NULL").uniq.first
    redir_judge_comp_results = redir_evt_cat.try(:event_competitor_results).where(:avg_visitor_results => 0) if redir_evt_cat.present?
    if (redir_evt_cat.present? && redir_judge_comp_results.present? && !redir_evt_cat.is_published && (cat_event.status == "OnGoing" || cat_event.status == "Completed"))
      # redirect_to admin_event_competitor_results_path(:id=> redir_evt_cat.id)
      redirect_to admin_event_competitor_results_path(:id=> event_category.try(:id), :next_category_id=>redir_evt_cat.try(:id))
    else
      redirect_to admin_event_categories_path("q[event_id_eq]"=>cat_event.try(:id))
    end
    # redirection method Ends here
	end

	action_item :only => :index do
    unless params[:best_in_show].present?
    	event_category = EventCategory.find params[:id]    
      if (event_category.is_published)
      	status_tag ("Category Closed"), ((event_category.is_published) ? :ok : :error)
      else
      	## Add - a condition if there is no results, shouldnt publish
      	link_to 'Close Category', publish_admin_event_competitor_results_path(params[:id], params), method: :put if event_category.event_competitor_results.present?
      end
    end
  end
  action_item :only => :index do
    unless params[:best_in_show].present?
      event_category = EventCategory.find params[:id]
      link_to 'Add Missed Out Result', new_admin_event_competitor_result_path(params)#, method: :put if event_category.event_competitor_results.present?    
    end
  end

  action_item :only => :index do
    unless params[:best_in_show].present?
      event_category = EventCategory.find params[:id]
      if event_category.is_published
        if params[:next_category_id].present?
          next_event_category = EventCategory.find params[:next_category_id]
        else      
          next_event_category = event_category.try(:event).try(:event_categories).joins(:judge_votes).where("is_published IS NULL").uniq.first
        end
        link_to 'Next Category', admin_event_competitor_results_path(:id=> next_event_category.id) if next_event_category.present?
      end
    end
  end

	index :title => proc {
      unless params[:best_in_show].present?
        EventCategory.find(params[:id]).try(:category).try(:name).try(:capitalize)
      else
        "#{Event.find_by_id(params[:q][:event_id_eq]).try(:name).try(:titleize)} > Best in Show List"
      end
      } do
		section :style => 'float:left;margin-right:20px;' do
  	  div do
    	  render "filter_judge_results"
  	  end
		end

		section :style => 'float:left;margin-right:20px;' do
		  div do
		    render "filter_visitor_results"
		  end
		end

    column "Rank", :position, :sortable => false do |result|
      # result.position
      unless params[:best_in_show].present?
        classN = EventCompetitorResult.dynamic_class_colorcode(result.position)
        best_in_place result, :position, :type => :input, :path => [:admin, result], :class=> classN
      else
        result.position
      end
    end

    column "First Name", :first_name, :sortable => false do |result|
      user = result.try(:event_competitor).try(:competitor).try(:user).try(:first_name).try(:capitalize)
    end

    column "Last Name", :last_name, :sortable => false do |result|
      user = result.try(:event_competitor).try(:competitor).try(:user).try(:last_name).try(:capitalize)
    end

    column "Badge Id", :badge_id, :sortable => false do |result|
      evt_comp = result.try(:event_competitor)
      comp = evt_comp.try(:competitor)
      user = comp.try(:user)
      evt_cat = result.try(:event_category)
      evt_cat_reg = EventCategoryRegistration.where(:user_id=>user.try(:id), :event_id =>evt_cat.event_id) #, :category_id => evt_cat.category_id # Hided this, because category was not showing since it is edited to someother category
      evt_cat_reg.first.try(:number)
    end

	  column "Nick Name", :event_competitor_id, :sortable => false do |result|
      competitor = result.try(:event_competitor).try(:competitor)
      competitor = competitor.try(:nick_name).present? ? competitor.try(:nick_name).try(:capitalize) : competitor.try(:user).try(:first_name).try(:capitalize)      
    end
	  
    column "Avg Fan Results", :avg_visitor_results, :sortable => false do |result|      
      link_to number_with_precision(result.try(:avg_visitor_results), :precision => 3), admin_visitor_votes_path(result.try(:event_competitor))
    end if params[:q].present? && params[:q][:event_category_id_eq] == "visitor_results"
    column :avg_judge_results, :sortable => false do |result|
      avg_judge_results_with_precision = number_with_precision(result.try(:avg_judge_results), :precision => 3)
      # if false
        # best_in_place result, :avg_judge_results, :type => :input, :path => [:admin, result], :value =>avg_judge_results_with_precision, :display_with => lambda{ |v| avg_judge_results_with_precision }
      # else
      link_to avg_judge_results_with_precision, admin_judge_votes_path(result.try(:event_competitor))
      # end
    end if (params[:q].present? && params[:q][:event_category_id_eq] == "judge_results" || params[:q].present? && params[:q][:event_id_eq].present? || !params[:q].present?) 
	  # column :position, :sortable => false do |result|
    #  	status_tag (result.position.present? ? "Rank #{result.position}" : "No Rank"), (((1..3).include?(result.position)) ? :ok : :error)
    # end
    column "Category", :category_name, :sortable => false do |result|
      result.try(:event_category).try(:category).try(:name)
    end if params[:best_in_show].present?
    
    default_actions if (params[:q].present? && params[:q][:event_category_id_eq] == "judge_results" || params[:q].present? && params[:q][:event_id_eq].present? || !params[:q].present?) && EventCategory.find_by_id(params[:id]).try(:is_published)
    paginated_collection(collection, download_links: false, entry_name: "Event Competitor Results")
  end

  form do |f|
    f.inputs "Add Missed out result" do
      if f.object.new_record?
        existing_results = EventCompetitorResult.where("event_category_id=? and avg_judge_results!=0",params[:id]).pluck(:event_competitor_id)
        pending_event_category_competitors = EventCompetitor.where("event_category_id=? and id not in (?)",params[:id],existing_results).includes(:competitor).sort{|a,b| a.try(:event_category_registration).try(:number) <=> b.try(:event_category_registration).try(:number)}.collect{|obj| ["#{obj.try(:event_category_registration).try(:number)} - #{obj.try(:competitor).try(:user).try(:last_name).try(:capitalize)}, #{obj.try(:competitor).try(:user).try(:first_name).try(:capitalize)}",obj.id]} #.select("event_competitors.competitor_id AS id,event_competitors.competitors.nick_name AS name")
        f.input :event_competitor_id,  as: :select, :required => true, :include_blank => 'Please select a Competitor', :collection =>  pending_event_category_competitors
        f.input :event_category_id,  as: :hidden,  :input_html => {:value=>params[:id]}
        f.input :avg_judge_results, :input_html => 
        {
          :onblur => "generate_competitor_position($(this).val(),#{params[:id]});"
        }
        f.input :position,  :input_html => {:readonly=>true}
        f.input :avg_visitor_results, as: :hidden, :input_html => {:value=>0}
      else
        user = f.object.try(:event_competitor).try(:competitor).try(:user)
        f.input :user_name, :input_html => {:value=>"#{user.try(:first_name).try(:capitalize)} #{user.try(:last_name).try(:capitalize)}", :disabled =>true}
        category_array = f.object.try(:event_category).try(:event).try(:event_categories).where(:is_published=>true).select([:id, :category_id]).map { |e| [ e.try(:category).try(:name), e.try(:id)] } 
        f.input :event_category_id, as: :select, :include_blank=>false, :collection => category_array
      end
    end
    f.actions
  end

	controller do
    def index
      if params[:best_in_show].present?
        @event_competitor_results = EventCompetitorResult.where(:id => params[:best_in_show_val_ids]).includes(:event_category=>[:category], :event_competitor=>[:competitor,:event_category_registration]).order("avg_judge_results desc").page(params[:page]).per(50)
      else
        if params[:q].present? && params[:q][:event_category_id_eq] == "visitor_results"
          @event_competitor_results = EventCompetitorResult.where(:event_category_id => params[:id], :avg_judge_results => 0).page(params[:page]).per(50).order("position asc")
        else
          @event_competitor_results = EventCompetitorResult.where(:event_category_id => params[:id], :avg_visitor_results => 0).page(params[:page]).per(50).order("position asc")
        end
      end
          
    end

    def create
      super do |format|
        if params[:event_competitor_result].present? && params[:event_competitor_result][:event_category_id].present?
          redirect_to admin_event_competitor_results_path(:id => params[:event_competitor_result][:event_category_id], "q[event_category_id_eq]"=>"judge_results") and return if resource.valid?
        end
      end
    end

    def update
      result = EventCompetitorResult.find_by_id(params[:id])
      if params[:event_competitor_result].present? && params[:event_competitor_result][:event_category_id].present?
        result.update_attributes(:position=>0, :avg_judge_results=>0.001, :avg_visitor_results=>0, :event_category_id=>params[:event_competitor_result][:event_category_id])
        super do |format|
          redirect_to admin_event_competitor_results_path(:id => result.try(:event_category_id), "q[event_category_id_eq]"=>"judge_results") and return if resource.valid?
        end
      elsif params[:event_competitor_result].present? && params[:event_competitor_result][:position].present?
        result.update_attributes(:position=>params[:event_competitor_result][:position])
        super
      elsif params[:event_competitor_result].present? && params[:event_competitor_result][:avg_judge_results].present?
        result.update_attributes(:avg_judge_results=>params[:event_competitor_result][:avg_judge_results])
        super
      end      
    end
  end
end