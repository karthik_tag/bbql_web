ActiveAdmin.register EventCategoryRegistration do

    csv force_quotes: true, col_sep: ',' do
        column("Badge ID")  do |event_category_registration|
          event_category_registration.try(:number)
        end

        column :last_name do |event_category_registration|
          event_category_registration.user.try(:last_name).try(:capitalize)
        end

        column :first_name do |event_category_registration|
          event_category_registration.user.try(:first_name).try(:capitalize)
        end

        column :category do |event_category_registration|
          event_category_registration.category.try(:name)
        end

        column :is_checked_in do |event_category_registration|
          event_category_registration.try(:is_checked_in) ? "Checked-in" : "Not Checked-in"
        end
    end

    breadcrumb do
        [
          link_to('Admin', admin_root_path),
          link_to('Event Registrations', admin_event_registrations_path)
        ]
    end

	menu :url => "#{AppConfig.url}/event_category_registrations"
	actions :all, except: [:new]
	scope :joined, :default => true do |event_category_registration|
  		event_category_registration.includes [:event_registration,:event,:category,:user, :competitor]
	end

	action_item do
		link_to "Register For This Event", admin_users_path(:event_id=>params[:q].present? ? params[:q][:event_id_eq] : nil)
 	end

 	action_item do
    	link_to "Create New BBQL Member", new_admin_user_path(:event_id=>params[:q].present? ? params[:q][:event_id_eq] : nil)
 	end

	index :title => proc {
			dynamic_index_title(params)
		} do

        section :style => 'float:left;margin-right:20px;' do
          div do
              render "checked_in_competitors"
          end
        end

        section :style => 'float:left;margin-right:20px;' do
          div do
            render "not_checked_in_competitors"
          end
        end

        column "Badge ID", :number, :sortable => 'event_category_registrations.number' do |event_category_registration|
            best_in_place event_category_registration, :number, :type => :input, :path => update_admin_event_category_registrations_path(:id=>event_category_registration.id,:event_id=>event_category_registration.try(:event_id)) #, :path => [:admin, event_category_registration]
        end

    	# column :registrant_name, :sortable => 'users.last_name' do |event_category_registration|    	  
    	#   "#{event_category_registration.user.try(:last_name).capitalize}, #{event_category_registration.user.try(:first_name).capitalize}"
    	# end

        column :last_name, :sortable => 'users.last_name' do |event_category_registration|
          event_category_registration.user.try(:last_name).try(:capitalize)
        end

        column :first_name, :sortable => 'users.first_name' do |event_category_registration|
          event_category_registration.user.try(:first_name).try(:capitalize)
        end

    	column :category, :sortable => 'categories.name' do |event_category_registration|
    	  event_category_registration.category.try(:name)
    	end


    	actions defaults: false do |event_category_registration|
            unless event_category_registration.event_photo.present?
            	link_to 'Choose Photo', '#', :class => "upload_photo fu-button", :onclick => "take_photo($(this),#{event_category_registration.id},'take_photo');"
            else
                # text_node "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp".html_safe                
                # "Checked-in"
                unless event_category_registration.try(:event_photo_file_name).nil?
                    image_url = "#{AppConfig.s3_url}/attachments/#{event_category_registration.id}/small/"+event_category_registration.event_photo_file_name
                end
                link_to (image_tag (image_url), :size => "75x75"), '#', :class => "upload_photo", :onclick => "take_photo($(this),#{event_category_registration.id},'image_pop');"
            end
        end
        default_actions
        paginated_collection(collection, download_links: false, entry_name: "Event Category Registrations")

     #    column :actions do |object|
     #  		# raw( %(#{link_to "View", [:admin, object]} 
     #    	#{(link_to "Edit", [:edit, :admin, object.user]) }) )
     #        raw( %(#{link_to "View", [:admin, object]} 
     #        ) )
    	# end
	end    

	show do
    	panel "Event Registration" do
    	  attributes_table_for event_category_registration do
    	    row("Event") { link_to(event_category_registration.event.try(:name),admin_event_path(event_category_registration.try(:event))) }
    	    row("Badge ID") { event_category_registration.try(:number) }
    	    row("Registrant Name") { link_to("#{event_category_registration.user.try(:last_name).try(:capitalize)}, #{event_category_registration.user.try(:first_name).try(:capitalize)}",admin_user_path(event_category_registration.try(:user))) }
    	    row("Category") { link_to(event_category_registration.try(:category).try(:name),admin_category_path(event_category_registration.try(:category))) } if event_category_registration.try(:category).present?
    	  end      
    	end
  	end

  	sidebar "Registrant Image", :only => :show do
    	attributes_table_for event_category_registration do
    	  row(" ") do |evnt_cat_reg|
    	    ul :class=>"left-algin-fix" do          
    	      unless event_category_registration.try(:event_photo_file_name).nil?
    	        image_tag ("#{AppConfig.s3_url}/attachments/#{event_category_registration.id}/small/"+event_category_registration.event_photo_file_name)
              else
                "There is no Registrant image"
    	      end
    	    end         
    	  end
    	end
  	end
	
    filter :first_name_filter, as: :string, :label => "First Name"
    filter :last_name_filter, as: :string, :label => "Last Name"
    filter :number, :label => "Badge Id"
    filter :category, as: :select, :collection => Category.order("name asc")
    filter :email_filter, as: :string, :label => "Email" # Check this model for its method definition
    filter :event

    form do |f|
        f.inputs "Event Category Registration Details" do
            # f.input :event, :as => :select, :required => true, :include_blank => 'Please select an event', :input_html => {:onchange => "populate_event_categories($(this), $(this).val());", :readonly=>true}, :collection => Event.where("status = ? or status = ?", "Upcoming", "OnGoing").order("name asc")
            f.input :event, :as => :select, :required => true, :include_blank => false, :input_html => {:readonly=>true}, :collection => Event.where(:id=>f.object.event.id)
            f.input :category, :as => :select, :required => true, :include_blank => 'Please select an event'
            f.input :old_number, :label => "Old Badge Id", as: :hidden, :input_html => {:value=>f.object.number}
            f.input :number, :label => "Badge Id"#, :input_html => {:readonly=>true}
            f.input :user_id, :label => "User Id", as: :hidden, :input_html => {:value=>f.object.user_id}
            f.input :event_photo, as: :file, :input_html => {:onchange => "validate_file_field($(this).attr('id'))"}, :hint => f.template.image_tag(f.try(:object).try(:event_photo_file_name).present? ? "#{AppConfig.s3_url}/attachments/#{f.object.try(:id)}/small/"+f.try(:object).try(:event_photo_file_name) : "#{AppConfig.image_url}" + "/attachments/small/missing.png")
            f.input :_destroy, as: :boolean, :label => "Do you want to remove image?"            
        end
        f.actions
    end

    controller do |event_category_registration|
        # def index
        #     super do |format|
        #         @event_category_registrations = EventCategoryRegistration.includes(:event_registration,:event,:category,:user, :competitor).joins(:event_registration,:event,:category,:user, :competitor).where("event_category_registrations.event_id =? && event_category_registrations.is_checked_in =?",params[:q][:event_id_eq],params[:q][:is_checked_in_eq]).order("users.last_name asc, users.first_name asc").page(params[:page]) if params[:q].present?
        #         @event_category_registrations ||= end_of_association_chain.paginate if @event_category_registrations.present?
        #     end
        # end

        def take_photo
    		evnt_cat_reg = EventCategoryRegistration.find_by_id(params[:id])
    		user_name = "#{evnt_cat_reg.user.try(:last_name).try(:capitalize)}, #{evnt_cat_reg.user.try(:first_name).try(:capitalize)}"
    		render :partial=> "take_photo", :layout => false, :locals => {:evnt_cat_reg => evnt_cat_reg, :user_name => user_name}
    	end

        def image_pop
            evnt_cat_reg = EventCategoryRegistration.find_by_id(params[:id])
            user_name = "#{evnt_cat_reg.user.try(:last_name).try(:capitalize)}, #{evnt_cat_reg.user.try(:first_name).try(:capitalize)}"
            unless evnt_cat_reg.try(:event_photo_file_name).nil?
                image_url = "#{AppConfig.s3_url}/attachments/#{evnt_cat_reg.id}/medium/"+evnt_cat_reg.event_photo_file_name
            end
            render :partial=> "image_pop", :layout => false, :locals => {:evnt_cat_reg => evnt_cat_reg, :user_name => user_name, :image_url=>image_url}
        end

    	def update_photo
    		evnt_cat_reg = EventCategoryRegistration.find_by_id(params[:event_category_registration][:id])
    		evnt_cat_reg.update_attributes(:event_photo=>params[:event_category_registration][:event_photo], :is_checked_in=>true) if params[:event_category_registration].present? && params[:event_category_registration][:event_photo].present?
            val = EventCategoryRegistration.find_checkedin_val_or_not(evnt_cat_reg.event)
            redirect_to admin_event_category_registrations_path("q[event_id_eq]" => evnt_cat_reg.event.try(:id), "q[is_checked_in_eq]" =>val, :order =>"users.last_name_asc")
    	end

        def update
            evnt_cat_reg = EventCategoryRegistration.find_by_id(params[:id])
            unless params[:event_id].present?
                if params[:event_category_registration].present?                    
                    evnt_cat_reg.update_attributes(:event_photo=>params[:event_category_registration][:event_photo], :is_checked_in=>true) if params[:event_category_registration][:event_photo].present?
                    evt_cat = EventCategory.find_by_event_id_and_category_id(params[:event_category_registration][:event_id],params[:event_category_registration][:category_id])
                    evnt_cat_reg.try(:event_competitor).update_attributes(:event_category_id =>evt_cat.try(:id)) if evnt_cat_reg.try(:event_competitor).present?
                    if params[:event_category_registration][:_destroy].to_i == 1
                        evnt_cat_reg.update_attributes(:event_photo=> nil,:is_checked_in=>false)
                    end
                end
                super
            else
                respond_to do |format|
                    number_param = params[:event_category_registration][:number].to_i
                    find_evnt_cat_reg = EventCategoryRegistration.where(:event_id=>params[:event_id],:number=>number_param)
                    if !find_evnt_cat_reg.present? && (number_param != "" && number_param != "-" && number_param > 0)
                      evnt_cat_reg.update_attributes(params[:event_category_registration])
                      evnt_cat_reg.try(:competitor).update_attributes(:number=>number_param)
                      format.html { redirect_to(evnt_cat_reg, :notice => 'User was successfully updated.') }
                      format.json { respond_with_bip(evnt_cat_reg) }
                    else
                      format.html { render :action => "edit"}
                      if (number_param == "" || number_param == "-" || number_param <= 0)
                        format.json { render :json => {:status => "empty_number", :value => evnt_cat_reg.try(:number) } }
                      else
                        format.json { render :json => {:status => false, :value => evnt_cat_reg.try(:number) } }
                      end
                    end
                end
            end
        end

        def destroy
            evnt_cat_reg = EventCategoryRegistration.find_by_id(params[:id])
            event = evnt_cat_reg.event
            evnt_cat_reg.destroy
            val = EventCategoryRegistration.find_checkedin_val_or_not(event)            
            redirect_to admin_event_category_registrations_path("q[event_id_eq]" => event.try(:id), "q[is_checked_in_eq]" =>val, :order =>"users.last_name_asc")
        end
	end

end
