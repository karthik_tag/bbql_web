ActiveAdmin.register EventBanner do
	menu :parent => "Administration", :url => "#{AppConfig.url}/event_banners", :if => proc{ User.toggle_main_menu(current_admin_user) }
	config.clear_sidebar_sections!
	config.sort_order = "event_banners.content_asc"

	index do
		column :event
		column :content
		column "Event Banners" do |event_banner|
			image_tag "#{AppConfig.s3_url}/attachments/#{event_banner.try(:id)}/original/"+ (event_banner.try(:event_banner_file_name) || ""), class: 'my_image_size', :style=>"max-height:230px;"
		end
    	default_actions
      	paginated_collection(collection, download_links: false, entry_name: "Event Banners")
	end

	csv force_quotes: true, col_sep: ',' do
	    column :content
  	end

  	show do
		panel "Event Banner Details" do
        	attributes_table_for event_banner, :content, :event
        end
    end

    sidebar "Event Banner", :only => :show do
    	attributes_table_for event_banner do
    	  row(" ") do |event_banner|
    	    ul :class=>"left-algin-fix" do          
    	      unless event_banner.try(:event_banner_file_name).nil?
    	        image_tag ("#{AppConfig.s3_url}/attachments/#{event_banner.id}/small/"+event_banner.event_banner_file_name)
              else
                "There is no Event Banner"
    	      end
    	    end         
    	  end
    	end
  	end

  	form multipart: true do |f|
		f.inputs "Upload a Banner" do			
			f.input :event_id, :as => :select, :include_blank => 'Please select an event', :collection => Event.all(:order => 'name asc')
			f.input :event_banner, as: :file, :input_html => {:onchange => "validate_file_field($(this).attr('id'))"}, :hint => f.template.image_tag(f.try(:object).try(:event_banner_file_name).present? ? "#{AppConfig.s3_url}/attachments/#{f.object.try(:id)}/small/"+f.try(:object).try(:event_banner_file_name) : "#{AppConfig.image_url}" + "/attachments/small/missing.png")
			f.input :_destroy, as: :boolean, :label => "Do you want to remove the banner?", as: :hidden			
			f.input :content			
		end
		f.actions
  end
end
