ActiveAdmin.register Club do
    menu false
    menu :parent => "Administration", :url => "#{AppConfig.url}/clubs", :if => proc{ User.toggle_main_menu(current_admin_user) }
    config.sort_order = "name_asc"
	index do
		column :name
    column :president
    column :email
		column :location
    column :state
		default_actions
    paginated_collection(collection, download_links: false, entry_name: "Clubs")
	end
    
  csv force_quotes: true, col_sep: ',' do
    column :name
    column :president
    column :email
    column :location
    column :state
  end

	sidebar "Club Image", :only => :show do
    	attributes_table_for club do
    	  row(" ") do
    	    ul :class=>"left-algin-fix" do
    	      img = club.attachment
    	      unless img.try(:attachment_file_name).nil?
    	        image_tag ("#{AppConfig.s3_url}/attachments/#{img.id}/small/"+img.attachment_file_name)
    	      else
                "There is no club image"
              end
    	    end
    	  end
    	end
  	end

    sidebar "Group Photo", :only => :show do
        attributes_table_for club do
          row(" ") do |clb|
            ul :class=>"left-algin-fix" do          
              unless club.try(:group_image_file_name).nil?
                image_tag ("#{AppConfig.s3_url}/attachments/#{club.id}/small/"+club.group_image_file_name)
              else
                "There is no group photo"
              end
            end         
          end
        end
    end

    show do
      panel "Club Details" do         
        attributes_table_for club, :name, :president, :email, :location, :state
        attributes_table_for club do
          row("club Url") {
            link_to(club.club_url, "#{club.club_url}", target: "_blank")
          }
        end
      end
    end

	filter :name
	filter :location
    filter :president
    filter :email

	form do |f|
    	f.inputs "Club Details", :style=>"margin-bottom:0px;" do
        f.input :name
        f.input :club_url
        f.input :president
        f.input :email
        f.input :location
        f.input :state
        f.input :group_image, as: :file, :input_html => {:onchange => "validate_file_field($(this).attr('id'))"}, :label => "Group Photo", :hint => f.template.image_tag(f.try(:object).try(:group_image_file_name).present? ? "#{AppConfig.s3_url}/attachments/#{f.object.try(:id)}/small/"+f.try(:object).try(:group_image_file_name) : "#{AppConfig.image_url}" + "/attachments/small/missing.png")
        f.input :_destroy, as: :boolean, :label => "Do you want to remove group photo?"			
    	end
      f.inputs for: [:attachment, f.object.attachment || Attachment.new] do |club|
        club.input :attachment, as: :file, :input_html => {:onchange => "validate_file_field($(this).attr('id'))"}, :hint => club.template.image_tag(club.try(:object).try(:attachment_file_name).present? ? "#{AppConfig.s3_url}/attachments/#{club.object.try(:id)}/small/"+club.try(:object).try(:attachment_file_name) : "#{AppConfig.image_url}" + "/attachments/small/missing.png")
        club.input :_destroy, as: :boolean, :label => "Do you want to remove attachment?"
      end
    	f.actions
  	end

    controller do
        def update
            if params[:club][:_destroy].to_i == 1
                club = Club.find_by_id(params[:id].to_i)
                club.update_attributes!(:group_image_file_name => "", :group_image_content_type => "", :group_image_file_size => "")
            end
          update!
        end
    end
end
