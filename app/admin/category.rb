ActiveAdmin.register Category do
	scope :joined, :default => true do |category|
  		category.includes [:main_category, :category_questions]
	end
	menu :parent => "main_categories", :url => "#{AppConfig.url}/categories"
	config.sort_order = "main_categories.name_asc"
	index do
		column "Main Category", :main_category_id, :sortable => 'main_categories.name' do |category|
      category.main_category.try(:name)
    end
		column "Category", :name
		column :description
    column "Judging Criterium", :sortable => false do |category|
        category_questions_list = category.category_questions.map { |category_question| link_to(category_question.try(:question), admin_judging_criterium_path(category_question))} if category.category_questions.present?
      if (category_questions_list.present?)
          safe_join category_questions_list, '<br>'.html_safe
      else
        "None"
      end
    end

		default_actions
    paginated_collection(collection, download_links: false, entry_name: "Categories")
	end

  csv force_quotes: true, col_sep: ',' do
    column("Main Category")  do |category|
      category.main_category.try(:name)
    end
    column("Category")  do |category|
      category.try(:name)
    end
    column :description

    column("Judging Criterium")  do |category|
        category_questions_list = category.category_questions.map { |category_question| category_question.try(:question)} if category.category_questions.present?
      if (category_questions_list.present?)
          safe_join category_questions_list, '<br>'.html_safe
      else
        "None"
      end
    end    
  end

	sidebar "Category Image", :only => :show do
    	attributes_table_for category do
    	  row(" ") do |cat|
    	    ul :class=>"left-algin-fix" do          
    	      img = category.attachment
    	      unless img.try(:attachment_file_name).nil?
    	        image_tag ("#{AppConfig.s3_url}/attachments/#{img.id}/small/"+img.attachment_file_name)
    	      else
                "There is no category image"
    	      end
    	    end         
    	  end
    	end
  	end

  	show do
      panel "Category Details" do
      	attributes_table_for category do
        	row("Main Category") {link_to(category.main_category.name,admin_main_category_path(category.main_category))}
        end
        attributes_table_for category, :name, :description        
        attributes_table_for category do
          category_questions_list = category.category_questions.map { |category_question| link_to(category_question.try(:question), admin_judging_criterium_path(category_question))} if category.category_questions.present?
          row("Judging Criterium") {                    
            if (category_questions_list.present?)
                safe_join category_questions_list, '<br>'.html_safe
            else
              "None"
            end
          }
        end
      end
    end

	filter :main_category, as: :Select, collection: ActiveRecord::Base.connection.table_exists?('main_categories') ? MainCategory.all(:order => 'name asc') : []
	filter :name, :label => "Category"
	filter :description

	form do |f|
		f.inputs "Category Details" do
			f.input :main_category_id, :as => :select, :required => true, :include_blank => 'Please select a main category', :collection => MainCategory.all(:order => 'name asc')
			f.input :name
			f.input :description
      f.has_many :category_questions  do |category|
        category.input :question, :label => "Judging Criteria" #, :required => true, :collection => Category.all(:order => 'name asc'), :label => "Category", :include_blank => "Select Category"
      end
			f.inputs for: [:attachment, f.object.attachment || Attachment.new] do |category|
    		category.input :attachment, as: :file, :input_html => {:onchange => "validate_file_field($(this).attr('id'))"}, :hint => category.template.image_tag(category.try(:object).try(:attachment_file_name).present? ? "#{AppConfig.s3_url}/attachments/#{category.object.try(:id)}/small/"+category.try(:object).try(:attachment_file_name) : "#{AppConfig.image_url}" + "/attachments/small/missing.png")
    		category.input :_destroy, as: :boolean, :label => "Do you want to remove attachment?"
			end
		end
		f.actions
	end

end
