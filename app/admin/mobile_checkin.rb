ActiveAdmin.register MobileCheckin do
	# menu :url => proc{ current_admin_user.try(:event_id).present? ? "#{AppConfig.ajax_url}/mobile_checkins/#{current_admin_user.try(:event_id)}/event_checkin" : "#{AppConfig.url}/mobile_checkins"}, :if => proc{ current_admin_user.try(:is_checkin_manager) || ["admin@fhl.com","brett@facialhairleague.com","brett@fhl.com"].include?(current_admin_user.try(:email)) }
	menu :url => proc{ current_admin_user.try(:event_ids).reject(&:empty?).count.eql?(1) ? "#{AppConfig.ajax_url}/mobile_checkins/#{current_admin_user.try(:event_ids).try(:last)}/event_checkin" : "#{AppConfig.url}/mobile_checkins"}, :if => proc{ current_admin_user.try(:is_checkin_manager) || ["admin@bbql.com","brett@facialhairleague.com","brett@fhl.com"].include?(current_admin_user.try(:email)) }
	controller do
        def index
        	redirect_to mobile_checkins_path
        end
    end
end
