ActiveAdmin.register VisitorVote do
  menu :url => "#{AppConfig.url}/visitor_votes"
	actions :all, except: [:show, :new, :edit, :destroy]
	config.clear_sidebar_sections!

	index do	 
   		# column :position1, :sortable => false do |visitor_vote|
   		# 	competitor =  EventCompetitor.find_by_id(visitor_vote.try(:position1)).try(:competitor).try(:nick_name)
   		# 	status_tag (competitor.present? ? competitor : "None"), ((visitor_vote.position1 == params[:format].to_i) ? :ok : :error)
   		# end
   		# column :position2, :sortable => false do |visitor_vote|
   		# 	competitor = EventCompetitor.find_by_id(visitor_vote.try(:position2)).try(:competitor).try(:nick_name)
   		# 	status_tag (competitor.present? ? competitor : "None"), ((visitor_vote.position2 == params[:format].to_i) ? :ok : :error)
   		# end
   		# column :position3, :sortable => false do |visitor_vote|
   		# 	competitor = EventCompetitor.find_by_id(visitor_vote.try(:position3)).try(:competitor).try(:nick_name)
   		# 	status_tag (competitor.present? ? competitor : "None"), ((visitor_vote.position3 == params[:format].to_i) ? :ok : :error)
   		# end
   		# column :event_category_id, :sortable => false do |visitor_vote|
   		# 	visitor_vote.try(:event_category).try(:category).try(:name)
   		# end
   		# column "Judge", :user_id, :sortable => false do |visitor_vote|
   		# 	visitor_vote.try(:user).present? ? visitor_vote.try(:user).try(:first_name) : "None"
   		# end
	end

	controller do
    	def index
    		@visitor_votes = VisitorVote.where("position1 = ? or position2 = ? or position3 = ?", params[:format],params[:format],params[:format]).page(params[:page]).per(50)
    	end
  end
end