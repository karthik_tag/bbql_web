# ActiveAdmin.register JudgeCode do
#     actions :all, except: [:edit]
#     menu :parent => "Administration", :url => "#{AppConfig.url}/judge_codes"
#     config.sort_order = "status_asc"
# 	index do
# 		column :name
# 		column :status, :sortable => 'judge_codes.status' do |judge_code|      		
#      		status_tag (judge_code.status ? "Used" : "Active"), (judge_code.status ? :error : :ok)
#     	end
#     	default_actions
# 	end
    
#     csv force_quotes: true, col_sep: ',' do
#         column :name
#         column("Status")  do |judge_code|
#           judge_code.status ? "Used" : "Active"
#         end
#     end

#     show do
#       panel "Judge Code Details" do         
#         attributes_table_for judge_code do
#             row :name
#             row("Status") { judge_code.status ? "Used" : "Active" }
#         end
#       end
#     end

#     filter :name
#     filter :status, as: :Select, collection: [['Active', 0],['Used', 1]]

# 	form do |f|
#     	f.inputs "Generate Judge Code" do
#     	  f.input :name
#     	  f.input :status, :input_html => { :type => :hidden, :value => 0 }
#     	  f.input :generate_judge_code, :label=> "Click here to generate a judge code", :input_html => 
#     	  {
#     	    :onclick => "generate_judge_code();"
#     	  }
#     	end    	  
#     	f.actions
#   	end
# end
