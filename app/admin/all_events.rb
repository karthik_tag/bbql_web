ActiveAdmin.register_page "All Events" do
  content do
  	if params[:event_type].present?
  		event_status = (params[:event_type] == "all") ? ['Upcoming','OnGoing','Completed'] : params[:event_type]
  		total_events_sorted = Event.ordered_events(event_status)
  	end
    render partial: 'all_events', :locals => {:total_events_sorted => total_events_sorted}
  end
end
