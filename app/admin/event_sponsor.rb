ActiveAdmin.register EventSponsor do
	scope :joined, :default => true do |event_sponsor|
  		event_sponsor.includes [:event]
	end
	menu :parent => "events", :url => "#{AppConfig.url}/all_events?order=events.from_date_asc&route=evt_spn&event_type=all"
  config.clear_sidebar_sections!

  action_item :only => [:index, :show] do
    # config.clear_action_items!
    link_to "New Event Sponsor" , new_admin_event_sponsor_path("q[event_id_eq]"=>params[:q] && params[:q][:event_id_eq])
  end
  
  filter :event_id, as: :Select, collection: ActiveRecord::Base.connection.table_exists?('events') ? Event.all(:order => 'name asc') : []
  filter :sponsor_name

  index :title => proc {
      dynamic_index_title(params)
    } do
		column "Event", :event_id, :sortable => 'events.name' do |event_sponsor|
      		event_sponsor.event.try(:name)
    end
		column :sponsor_name
		column "Description", :sponsor_description
		# default_actions
    column :actions do |object|
      raw( %(#{link_to "View", admin_event_sponsor_path("id"=>object.id, "q[event_id_eq]"=>params[:q] && params[:q][:event_id_eq])} 
      #{(link_to "Edit", [:edit, :admin, object]) }
      #{(link_to "Delete", [:admin, object], :method=>:delete) }
      ))
    end
    paginated_collection(collection, download_links: false, entry_name: "Event Sponsors")
	end

  csv force_quotes: true, col_sep: ',' do
    column("Event")  do |event_sponsor|
      event_sponsor.event.try(:name)
    end
    column :sponsor_name
    column("Description") do |event_sponsor|
      event_sponsor.try(:sponsor_description)
    end    
  end

  sidebar "Event Sponsor Image", :only => :show do
      attributes_table_for event_sponsor do
        row(" ") do |cat|
          ul :class=>"left-algin-fix" do          
            img = event_sponsor.attachment
            unless img.try(:attachment_file_name).nil?
              image_tag ("#{AppConfig.s3_url}/attachments/#{img.id}/small/"+img.attachment_file_name)
            else
                "There is no event sponsor image"
            end
          end         
        end
      end
    end

	show do
    panel "Event Sponsor Details" do
    	attributes_table_for event_sponsor, :sponsor_name, :sponsor_description
      attributes_table_for event_sponsor do
        row("event") { event_sponsor.try(:event).present? ? link_to(event_sponsor.try(:event).try(:name), admin_event_path(event_sponsor.try(:event))) : 'None' }
        row("Sponsor Url") {
          link_to(event_sponsor.sponsor_url, "#{event_sponsor.sponsor_url}", target: "_blank")
        }
      end
    end
  end

  controller do |event_sponsor|
    def destroy
      event_id = EventSponsor.find_by_id(params[:id]).try(:event).try(:id)
      super do |format|
        redirect_to admin_event_sponsors_path("q[event_id_eq]" => event_id, :order =>"sponsor_name_asc") and return if resource.valid?
      end
    end

    def create
      event_id = params[:event_sponsor] && params[:event_sponsor][:event_id]
      super do |format|
        redirect_to admin_event_sponsors_path("q[event_id_eq]" => event_id, :order =>"sponsor_name_asc") and return if resource.valid?
      end
    end

    def update
      event_id = EventSponsor.find_by_id(params[:id]).try(:event).try(:id)
      super do |format|
        redirect_to admin_event_sponsor_path("id"=>params[:id], "q[event_id_eq]" => event_id) and return if resource.valid?
      end
    end

  end

  form do |f|
    f.inputs "Event Sponsor Details" do      
      # f.input :event_id, :as => :select, :required => true, :include_blank => 'Please select an Event', :collection => Event.all(:order => 'name asc')
      if f.object.new_record?
        event_collection = Event.find_all_by_id(params[:q] && params[:q][:event_id_eq]) + Event.where("id not in (#{params[:q] && params[:q][:event_id_eq]}) and status in (?)",["Upcoming","OnGoing"]).order("name asc")
        f.input :event_id, :as => :select, :required => true, :collection => event_collection, :include_blank => false
      else
        f.input :event_id, :as => :select, :required => true, :collection => Event.find_all_by_id(f.object.event_id), :include_blank => false,:input_html => {:disabled=>true}
      end

      f.input :sponsor_name
      f.input :sponsor_description, :label => "Description"
      f.input :sponsor_url
      f.inputs for: [:attachment, f.object.attachment || Attachment.new] do |event_sponsor|
            event_sponsor.input :attachment, as: :file, :input_html => {:onchange => "validate_file_field($(this).attr('id'))"}, :hint => event_sponsor.template.image_tag(event_sponsor.try(:object).try(:attachment_file_name).present? ? "#{AppConfig.s3_url}/attachments/#{event_sponsor.object.try(:id)}/small/"+event_sponsor.try(:object).try(:attachment_file_name) : "#{AppConfig.image_url}" + "/attachments/small/missing.png")
            event_sponsor.input :_destroy, as: :boolean, :label => "Do you want to remove attachment?"
      end
    end       
    f.actions
  end
end
