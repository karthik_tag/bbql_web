ActiveAdmin.register EventCategory do
	scope :joined, :default => true do |event_category|
    event_category.includes(:category, :judge_votes, :event_competitor_results, :event=>[:event_judge_codes])
	end

  menu :parent => "events", :url => "#{AppConfig.url}/all_events?order=events.from_date_asc&route=evt_cat&event_type=all"
  config.sort_order = "sort_order_asc"
  config.paginate   = false
  # sortable
  config.clear_sidebar_sections!

  action_item :only => :index do
    event = Event.find_by_id(params[:q][:event_id_eq])
    link_to 'Best In Show', best_in_show_admin_event_categories_path(params), method: :get if (event.try(:status) == "Completed" || event.try(:status) == "OnGoing")
  end

  action_item :only => [:index] do
    # config.clear_action_items!
    link_to "New Event Category" , new_admin_event_category_path("q[event_id_eq]"=>params[:q] && params[:q][:event_id_eq])
  end

  collection_action :close_event, :method => :get do
    redirect_to wind_up_admin_event_path(params[:event_id])
  end

  collection_action :best_in_show, :method => :get do
    event = Event.find_by_id(params[:q][:event_id_eq])
    best_in_show = Event.best_in_show(event)
    redirect_to admin_event_competitor_results_path("q[event_id_eq]"=> event.try(:id), "best_in_show"=>true, :best_in_show_val_ids=>best_in_show)
  end

	  # actions :all, except: [:new, :edit, :destroy]
	  member_action :view_results, :method => :get do
      event_category = EventCategory.find params[:id]
      # EventCompetitorResult.where(:event_category_id=>params[:id]).delete_all
      # event_category.update_attributes(:is_published => true)
      # unless EventCompetitorResult.where(:event_category_id=>event_category.try(:id)).present?
        ### Judge Votes Calculation goes here ====> starts#
        unless event_category.event_competitor_results.where(:avg_visitor_results=>0).present?
          EventCategory.close_judge_voting(event_category)
        end
        ### Judge Votes Calculation goes here ====> ends#
        
      # end
      redirect_to admin_event_competitor_results_path(params)
    end

    filter :event_id, as: :Select, collection: ActiveRecord::Base.connection.table_exists?('events') ? Event.where("id > ?",0).order('name asc') : []
    filter :category_id, as: :Select, collection: ActiveRecord::Base.connection.table_exists?('categories') ? Category.where("id > ?",0).order('name asc') : []
    filter :is_published, as: :Select, collection: [['Judging Closed', 1],['Not Closed', ""]], :label => "Status"

    index :title => proc {
      dynamic_index_title(params)
      } do
      event = Event.find_by_id(params[:q][:event_id_eq])
      # sortable_handle_column if !(["OnGoing","Completed"].include?(event.status))
  		column "Sort Order", :sort_order, :sortable => 'categories.sort_order' do |event_category|
        if !(["OnGoing","Completed"].include?(event.status))
          best_in_place event_category, :sort_order, :type => :input, :path => update_admin_event_categories_path(:id=>event_category.id,:event_id=>event_category.try(:event_id))
        else
          event_category.try(:sort_order)
        end        
      end
      column "Category", :category_id, :sortable => 'categories.name' do |event_category|
        event_category.category.try(:name)
      end
      # column "Judging Criteria", :category_question_id, :sortable => 'category_questions.question' do |event_category|
      #   event_category.try(:category).try(:category_questions).try(:first).try(:question)
      # end
  		column "Status", :is_published, :sortable => 'event_categories.is_published' do |event_category|
       	status_tag (event_category.is_published ? "Category Closed" : (!event_category.is_published && event_category.event_competitor_results.present?) ? "Judging Closed" : "Judging Open"), ((!event_category.event_competitor_results.present?) ? :ok : (!event_category.is_published && event_category.event_competitor_results.present?) ? :error : :status)
      end
      column "Judges Voted", :is_published do |event_category|
        total_judges = event_category.try(:event).try(:event_judge_codes)
        judges_voted = event_category.try(:judge_votes).where("user_id IS NOT NULL").pluck(:user_id).uniq
        # judges = User.includes(:attachment).where(:id=>judges_voted)
        non_voted_judge_ids = total_judges.pluck(:user_id).uniq - judges_voted
        # non_voted_judges = User.includes(:attachment).where(:id=>non_voted_judge_ids)
        all_judges = User.includes(:attachment).where(:id=>(judges_voted + non_voted_judge_ids).compact.sort)
        vote_status = ""
        # judges.includes(:attachment).each do |judge_user|
        #   title = "#{judge_user.try(:last_name)}, #{judge_user.try(:first_name)}"
        #   vote_status += "<a style='text-decoration:none;' href='#{AppConfig.url}/judge_votes?user_id=#{judge_user.try(:id)}&event_category_id=#{event_category.try(:id)}'> <img src='#{judge_user.try(:attachment).try(:attachment).url(:small)}' class='voted_judge_circle_img' title='#{title}'></img></a>"
        # end
        # non_voted_judges.includes(:attachment).each do |non_voted_judge_user|
        #   title = "#{non_voted_judge_user.try(:last_name)}, #{non_voted_judge_user.try(:first_name)}"
        #   vote_status += "<img src='#{non_voted_judge_user.try(:attachment).try(:attachment).url(:small)}' class='awaiting_judge_votes_img' title='#{title}'></img>"
        # end
        all_judges.includes(:attachment).each do |judge_user|
          title = "#{judge_user.try(:last_name)}, #{judge_user.try(:first_name)}"
          if judges_voted.include?(judge_user.try(:id))
            vote_status += "<a style='text-decoration:none;' href='#{AppConfig.url}/judge_votes?user_id=#{judge_user.try(:id)}&event_category_id=#{event_category.try(:id)}'> <img src='#{judge_user.try(:attachment).try(:attachment).url(:small)}' class='voted_judge_circle_img' title='#{title}'></img></a>"
          else
            vote_status += "<img src='#{judge_user.try(:attachment).try(:attachment).url(:small)}' class='awaiting_judge_votes_img' title='#{title}'></img>"
          end
        end

        vote_status.present? ? vote_status.html_safe : "There are no judges for this event"        
      end      
      column "Regd.<br>Comps".html_safe, :regd_comps, :class=>"evnt-cat-counts", :sortable => 'event_categories.regd_comps' do |event_category|
        registered_comps = EventCategoryRegistration.where(:event_id=>event_category.try(:event_id),:category_id=>event_category.try(:category_id),:is_checked_in=>false)
        registered_comps.length
      end
      column "Chkd-in.<br>Comps".html_safe, :chckd_in_comps, :class=>"evnt-cat-counts", :sortable => 'event_categories.chckd_in_comps' do |event_category|
        checked_in_comps = EventCategoryRegistration.where(:event_id=>event_category.try(:event_id),:category_id=>event_category.try(:category_id),:is_checked_in=>true)
        checked_in_comps.length
      end
      column "Tot.<br>Comps".html_safe, :total_comps, :class=>"evnt-cat-counts", :sortable => 'event_categories.total_comps' do |event_category|
        total_comps = EventCategoryRegistration.where(:event_id=>event_category.try(:event_id), :category_id=>event_category.try(:category_id))
        total_comps.present? ? "#{total_comps.length}" : "0"
      end
      # events_category_publish_status = EventCategory.pluck(:is_published)    
      # unless (events_category_publish_status.include?(true))
        actions defaults: false do |event_category|
          cat_event = event_category.event
          judges_event_competitor_results = event_category.event_competitor_results.where(:avg_visitor_results=>0)
          if !event_category.judge_votes.present?
            "There are no judges votes yet"          
          elsif (!judges_event_competitor_results.present? && (cat_event.status == "OnGoing" || cat_event.status == "Completed"))
            total_judges,voted_judges = EventCategory.total_judges_voted(event_category)
            link_to 'Close Judging', view_results_admin_event_category_path(event_category.id,params) , method: :get, :confirm => "#{voted_judges} out of #{total_judges} judges have voted for this category. Please make sure the voting has been completed. No more votings will be considered if you click OK."
          else
            link_to 'View Judge Results', admin_event_competitor_results_path(:id=> event_category.id)
          end
        end
      # end

  		default_actions
      paginated_collection(collection, download_links: false, entry_name: "Event Categories")
    end

    action_item do |event_category|
      event = Event.find_by_id(params[:q][:event_id_eq]) if params[:q].present? && params[:q][:event_id_eq].present?
      link_to 'Close Event', close_event_admin_event_categories_path(:event_id=>params[:q][:event_id_eq]) if (event.try(:status) == "OnGoing" && event.try(:event_categories).present? && !event.try(:event_categories).pluck(:is_published).include?(nil))
    end

    csv force_quotes: true, col_sep: ',' do
      column("Category")  do |event_category|
        event_category.category.try(:name)
      end
      # column("Judging Criteria")  do |event_category|
      #   event_category.try(:category).try(:category_questions).try(:first).try(:question)
      # end      
      column("Status")  do |event_category|
        event_category.is_published ? "Category Closed" : (!event_category.is_published? && event_category.event_competitor_results.present?) ? "Judging Closed" : "Judging Open"
      end
    end

	show do
    panel "Event Categories Details" do
    	attributes_table_for event_category do
        row("event") { event_category.try(:event).present? ? link_to(event_category.try(:event).try(:name), admin_event_path(event_category.try(:event))) : ""}
        row("category") { link_to(event_category.try(:category).try(:name), admin_category_path(event_category.try(:category))) }
        row("Status") { event_category.is_published ? "Category Closed" : (!event_category.is_published? && event_category.event_competitor_results.present?) ? "Judging Closed" : "Judging Open" }
      end     
    end
  end

  controller do |event_category|
    def index
      super do |format|
        per_page = (request.format == 'text/html') ? 50 : 10_000
        params[:page] = nil unless (request.format == 'text/html')
        @event_categories = EventCategory.where(:event_id=>params[:q][:event_id_eq]).order('sort_order IS NULL, sort_order ASC').page(params[:page]).per(per_page)
        @event_categories ||= end_of_association_chain if @event_categories.present?
      end
    end

    def create
      event_id = params[:event_category] && params[:event_category][:event_id]
      super do |format|
        redirect_to admin_event_categories_path("q[event_id_eq]" => event_id, :order =>"name_asc") and return if resource.valid?
      end
    end

    def update
      event_category = EventCategory.find_by_id(params[:id])
      
      unless params[:event_id].present?
        event = event_category.try(:event)
        super do |format|
          EventCategoryRegistration.where(:event_id=>event.try(:id),:category_id=>event_category.try(:category_id)).update_all(:category_id=>params[:event_category][:category_id])
          redirect_to admin_event_category_path(params[:id]) and return if resource.valid?
        end
      else
        event_categories = EventCategory.where(:event_id=>params[:event_id])
        respond_to do |format|
          sort_order_param = params[:event_category][:sort_order].to_i
          if event_category.present? && (sort_order_param != "" && sort_order_param != "-" && sort_order_param > 0 && sort_order_param <= event_categories.try(:count))
            old_order = event_category.try(:sort_order).to_i
            adjust_all_sort_orders(params[:event_id],old_order,sort_order_param)
            event_category.update_attributes(params[:event_category])

            format.html { redirect_to(event_category, :notice => 'Event Category was successfully updated.') }
            format.json { respond_with_bip(event_category) }
          else
            format.html { render :action => "edit"}
            if (sort_order_param == "" || sort_order_param == "-" || sort_order_param <= 0)
              format.json { render :json => {:code=> 200, :status => "empty_sort_order", :value => event_category.try(:sort_order) } }
            elsif (sort_order_param > event_categories.try(:count))
              format.json { render :json => {:code=>event_categories.try(:count),:status => "higher_sort_order", :value => event_category.try(:sort_order) } }              
            else
              format.json { render :json => {:status => "empty_sort_order", :value => event_category.try(:sort_order) } }
            end
          end
        end
      end
    end

    def adjust_all_sort_orders(event_id,old_order,new_order)
      event = Event.find_by_id(event_id)
      if old_order < new_order
        event_categories = event.try(:event_categories).where("sort_order > ? and sort_order <= ?",old_order,new_order).order("sort_order asc")
        event_categories.each do |event_category|
          event_category.update_attributes(:sort_order=>event_category.sort_order - 1)
        end
      else
        event_categories = event.try(:event_categories).where("sort_order < ? and sort_order >= ?",old_order,new_order).order("sort_order desc")
        event_categories.each do |event_category|
          event_category.update_attributes(:sort_order=>event_category.sort_order + 1)
        end
      end
    end

    def destroy
      event = EventCategory.find_by_id(params[:id]).try(:event)
      event_id = event.try(:id)
      event_category = EventCategory.find_by_id(params[:id])
      event_categories = event.try(:event_categories).where("sort_order > ?",event_category.try(:sort_order)).order("sort_order asc")
      event_categories.each do |event_category|
          event_category.update_attributes(:sort_order=>event_category.sort_order - 1)
      end
      super do |format|
        redirect_to admin_event_categories_path("q[event_id_eq]" => event_id, :order =>"categories.name_asc") and return if resource.valid?
      end
    end
  end

  form do |f|
    f.inputs "Event Category Details" do
      f.input :main_category_id, :as => :select,  :include_blank => 'Please select a main category', :collection => MainCategory.all(:order => 'name asc'), :input_html => {
      :onchange => "populate_category_by_main_category($(this), $(this).val());" }
      f.input :category_id, :as => :select, :required => true, :collection => Category.all(:order => 'name asc'), :include_blank => "Select Category"
      if f.object.new_record?
        event_collection = Event.find_all_by_id(params[:q] && params[:q][:event_id_eq]) + Event.where("id not in (#{params[:q] && params[:q][:event_id_eq]}) and status in (?)",["Upcoming","OnGoing"]).order("name asc")
        f.input :event_id, :as => :select, :required => true, :collection => event_collection, :include_blank => false
      else
        f.input :event_id, :as => :select, :required => true, :collection => Event.find_all_by_id(f.object.event_id), :include_blank => false,:input_html => {:disabled=>true}
      end
    end       
    f.actions
  end
end