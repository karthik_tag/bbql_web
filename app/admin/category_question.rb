ActiveAdmin.register CategoryQuestion, :as => "Judging Criteria" do	
	scope :joined, :default => true do |question|
  		question.includes [:category]
	end
	menu :parent => "Administration", :url => "#{AppConfig.url}/judging_criteria", :if => proc{ User.toggle_main_menu(current_admin_user) }
	config.sort_order = "categories.name_asc"

	action_item :only => :show do
	    link_to 'Add New Judging Criteria', new_admin_judging_criterium_path
  	end
	index do
		column "Category", :category_id, :sortable => 'categories.name' do |question|
      		question.category.try(:name)
    	end
		column "Judging Criteria", :question
		default_actions
		paginated_collection(collection, download_links: false, entry_name: "Judging Criteria")
	end

	csv force_quotes: true, col_sep: ',' do
        column("Category")  do |judging_criteria|
          judging_criteria.category.try(:name)
        end
        column("Judging Criteria")  do |judging_criteria|
          judging_criteria.try(:question)
        end
    end

	show do
      panel "Judging Criteria Details" do         
        attributes_table_for judging_criteria do
        	row("Category") {judging_criteria.category.present? ? link_to(judging_criteria.category.name,admin_category_path(judging_criteria.category)) : "None"}
        	row("Judging Criteria") { judging_criteria.question }
        end
      end
    end

	filter :category, as: :Select, collection: ActiveRecord::Base.connection.table_exists?('categories') ? Category.all(:order => 'name asc') : []
	filter :question, :label => "Judging Criteria"

	form do |f|
		f.inputs "Add/Edit Judging Criteria" do
			f.input :main_category_id, :as => :select, :include_blank => 'Please select a main category', :collection => MainCategory.all(:order => 'name asc'), :input_html => {
        	:onchange => "populate_category_by_main_category($(this), $(this).val());" }
			f.input :category_id, :as => :select, :required => true, :include_blank => 'Please select a category', :collection => Category.all(:order => 'name asc')
			f.input :question, :label => "Judging Criteria"
		end
		f.actions
	end
end
