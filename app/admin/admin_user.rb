ActiveAdmin.register AdminUser do
  menu :parent => "Users", :url => "#{AppConfig.url}/admin_users", :if => proc{ User.toggle_main_menu(current_admin_user) }
  config.sort_order = "email_asc"
  
  action_item :only => :index do
    link_to 'Create Bulk checkin managers', '#', :class => "upload_photo", :onclick => "take_photo($(this),'null_id','new_bulk_ck_mnrs');"
  end

  action_item :only => [:show] do
    link_to "New Admin User" , new_admin_admin_user_path
  end

  index do
    section :style => 'float:left;margin-right:20px;' do
      div do
          render "admin_users"
      end
    end
    section :style => 'float:left;margin-right:20px;' do
      div do
        render "checkin_managers"
      end
    end
    column :email
    column :current_sign_in_at
    column :last_sign_in_at
    column :sign_in_count
    column :is_checkin_manager, :sortable => 'admin_users.is_checkin_manager' do |admin_user|
        status_tag (admin_user.is_checkin_manager ? "YES" : "NO"), ((admin_user.is_checkin_manager) ? :ok : :error)
    end
    default_actions
    paginated_collection(collection, download_links: false, entry_name: "Admin Users")
  end

  csv force_quotes: true, col_sep: ',' do
    column :email
    column :current_sign_in_at
    column :last_sign_in_at
    column :sign_in_count
    column :is_checkin_manager do |admin_user|
        admin_user.is_checkin_manager ? "YES" : "NO"
    end
  end

  show do
    panel "Admin User Details" do
      attributes_table_for admin_user, :email, :current_sign_in_at, :last_sign_in_at, :current_sign_in_ip, :last_sign_in_ip, :sign_in_count
      
      attributes_table_for admin_user do
        row("Is Checkin Manager") {
          admin_user.is_checkin_manager ? "Yes" : "No"
        }
        row("Events") {
          Event.where(:id=>admin_user.try(:event_ids)).pluck(:name).sort.join(", <br>").html_safe          
        }
      end
    end
  end

  sidebar "Donation URL", :only => :index do    
    div :class=>"form_class" do
      render "create_non_profit_url"
    end
  end

  # sidebar "Push Notification", :only => :index do    
  #   div :class=>"form_class_push_notif" do
  #     render "create_push_notification"
  #   end
  # end

  filter :email
  # filter :event_ids, as: :select, multiple: true, :collection => Event.where(:status=>["Upcoming","OnGoing"]).order('name asc')

  form do |f|
    f.inputs "Admin Details" do
      f.input :email
      f.input :password
      f.input :password_confirmation
      unless ( current_admin_user.try(:is_checkin_manager) && !["admin@bbql.com","brett@facialhairleague.com","brett@fhl.com"].include?(current_admin_user.try(:email)) )
        f.input :is_checkin_manager
        f.input :event_ids, as: :select, :multiple   => true, :collection => Event.where(:status=>["Upcoming","OnGoing"]).order('name asc'), :include_blank => 'Please select an event'
      end
    end
    f.actions
  end

  controller do

    def index
      dashboard_boolean = (current_admin_user.try(:is_checkin_manager) && !["admin@bbql.com","brett@facialhairleague.com","brett@fhl.com"].include?(current_admin_user.try(:email)))
      if dashboard_boolean
        super do |format|
          params[:id] = current_admin_user.try(:id)
          redirect_to admin_admin_user_path(current_admin_user.try(:id)) and return if resource.valid?
        end
      end
      super
    end

    def update
      if params[:admin_user][:password].blank? && params[:admin_user][:password_confirmation].blank?
        params[:admin_user].delete("password")
        params[:admin_user].delete("password_confirmation")
      end
      super
    end

    def new_bulk_ck_mnrs
      admin_user = AdminUser.new
      render :partial=> "new_bulk_ck_mnrs", :layout => false, :locals => {:admin_user => admin_user}
    end

    def save_bulk_ck_mnrs
      if params[:admin_user].present?
        admin_params = params[:admin_user]
        admin_params[:quantity].to_i.times do |index|
          begin
            AdminUser.create!(:email=>"#{admin_params[:email_text]}#{index+1}@bbql.com",:password=>admin_params[:password],:password_confirmation=>admin_params[:password_confirmation],:is_checkin_manager=>true,:event_ids=>admin_params[:event_ids])          end
          end
      end
      redirect_to admin_admin_users_path
    end

    def bulk_checkin_managers_onsubmit_validation
      arr = []
      params[:quantity].to_i.times do |index|
        arr << "#{params[:email_text]}#{index+1}@bbql.com"
      end if params[:quantity].present?
      existed_admin_users = AdminUser.where(:email=>arr)
      is_present = (existed_admin_users.count > 0 ) ? true : false
      render :json => {:status=>is_present, :message=>"Sucess", :code=>200}
    end

  end

end
