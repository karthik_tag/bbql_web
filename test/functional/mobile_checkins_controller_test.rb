require 'test_helper'

class MobileCheckinsControllerTest < ActionController::TestCase
  test "should get index" do
    get :index
    assert_response :success
  end

  test "should get event_checkin" do
    get :event_checkin
    assert_response :success
  end

end
